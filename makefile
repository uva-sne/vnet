.PHONY: all

CONFIG = data
STATIC = data/htdocs/static
UI = data/htdocs/ui

NMBIN = node_modules/.bin
HANDLEBARS_RUNTIME := node_modules/handlebars/dist/handlebars.runtime.min.js
HANDLEBARS := node_modules/.bin/handlebars

define js_compressor_
	cat $2 > $1
endef
define js_compressor
	${NMBIN}/uglifyjs \
		--comments '/[lL]icense|[cC]opyright/' \
		--screw-ie8 \
		--output $1 \
		--source-map-root /demo/static/ \
		--source-map-url /demo/static/vnet.js.map \
		--source-map $1.map \
		$2
endef


all: node_modules $(STATIC)/vnet.js $(STATIC)/grid.png

clean:
	rm -rf node_modules \
		"$(STATIC)/templates.js" \
		"$(STATIC)/vnet.js"

node_modules:
	npm install handlebars uglify-js


SOURCES := \
	$(UI)/license.js \
	$(UI)/config.js \
	$(UI)/ws.js \
	$(UI)/util.js \
	$(UI)/tuio.js \
	$(UI)/data.js \
	$(UI)/view.js \
	$(UI)/chart.js \
	$(UI)/ui.js \
	$(UI)/main.js


$(STATIC)/templates.js: $(wildcard $(UI)/tpl/*) | $(HANDLEBARS_RUNTIME)
	cat $(HANDLEBARS_RUNTIME) > $@
	$(HANDLEBARS) -m $^ >> $@

$(STATIC)/vnet.js: $(SOURCES) | $(STATIC)/templates.js
	$(call js_compressor,$@,$(SOURCES))

# ----

.PHONY: build-container
.PHONY: container
build-container:
	./bin/build-demo
	(cd container/ && make)
container: build-container
	docker stop vnet || true
	docker rm vnet || true
	docker create -p 8500-8501:8500-8501 -p 8503:8503/udp \
		--log-driver=journald \
		--read-only \
		--volume ${PWD}/${CONFIG}:/vnet:ro \
		--name vnet skynet.lab.uvalight.net:5000/vnet
	docker start vnet

container-push:
	docker tag skynet.lab.uvalight.net:5000/vnet:latest skynet.lab.uvalight.net:5000/vnet:sc16
	docker push skynet.lab.uvalight.net:5000/vnet:sc16

# ----

include docs/include.mk

$(STATIC)/grid.png: bin/mkgrid
	bin/mkgrid
	optipng $@ || true


.PHONY: service-sim
service-sim:
	bin/build-demo-services

.PHONY: vnetmon
vnetmon:
	./bin/build-vnetmon
