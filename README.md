# vnet

A tool to visualize and manipulate network topologies.

This repository contains:

- VNET UI
- VNET container build scripts
- SC17 VNET controller

The SC15/SC16 VNET controller can be found in the vnet-controller repository.


## Dependencies

Use the "installer": https://skynet.lab.uvalight.net/ben/bootstrap.sh

Frontend:

* A modern browser (Firefox, Chrome)
* Optional: uglifyjs for JS compression
* Included: D3 and Handlebars


## Bugs

Known bug: metrics page does not update the MAC address of the NFV host if it
remains running between different slice instances. TODO.

Known bug: remove broken clients from broadcast list right away


## Dependencies

* xo-tools
