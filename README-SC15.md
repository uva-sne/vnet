# Ciena SC15 demo

The slice name for the Ciena version of the demo is `ciena-sc15`.
The slice name for the UvA version of the demo is `ralph-router` running
on the UvA controller.

NB: It is highly recommended that you start tmux or screen and run the commands
in there.


## Preparation

Make sure the following software is installed:

* Python 3
* Go 1.5+ (older version may work, YMMV)
* Docker


## TODO/Automation

* Full slice orchestration

    * Build request XML (make sure to select proper controller and such;
      based on vnet-config.ini automatically?)
    * Bootstrap script with VM config
    * Await
    * Do whatever prepare-slice does (Ansible inv, SSH hosts)



## Setting up the demo

See `docs/container.md` for the server configuration.

Commands to execute to boot the slice:

    # -!- MUST BE IN THIS DIRECTORY WHEN RUNNING ANY OF THE TOOLS
    cd ~/vnet

    # Orchestrate slice
    bin/create-slice ciena-sc15 ciena

    # Run additional slice tasks
    #
    # WARNING: if you run this right after await finishes, Neuca will not have
    # configured the network interfaces yet.  This will cause the network to
    # simply not work.
    #
    # Workaround: wait a couple of minutes before preparing the slice!
    #
    # Fix: Start neuca on all VMs, run vnet-routing on all VMs, run
    #  prepare-slice again
    ./bin/prepare-slice.sh ciena-sc15

    # Update vnetmon (TODO: update the VM images!!)
    #
    # Wait a bit until after the slice has started!
    ashell all 'vnet-vm-install; service vnetmon restart'

    # (Re)start the demo container


In order to monitor the demo:

    sudo journalctl -f CONTAINER_NAME=vnet


You can restart the demo at any time if you need to reset it.


## General notes

To run commands on all nodes:

    ansible -i inv all -m shell -a 'echo Hello'

You can replace `all` with e.g. `'client*;server*'` to target only the
