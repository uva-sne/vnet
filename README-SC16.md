# SC16 Demo

## Before you start

Mandatory files:

* data/vnet.pem             -- X.509 certificate for webservices
* data/vnet-key.pem         -- Private key for certificate
* data/exogeni/geni.pem     -- X.509 certificate for ExoGENI controller
* data/exogeni/geni-key.pem -- Private key for certificate


Install:

* python3
* docker-engine (not docker.io; https://docs.docker.com/engine/installation/linux/debian/)
* openssl
* xo-tools (git@bitbucket.org:uva-sne/xo-tools.git; tag sc16)


## Control

Start & configure:

./sc16/bin/start


Stop:

./sc16/bin/stop
