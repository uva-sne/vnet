#!/bin/sh

id=$(cloudmonkey -d json list templates templatefilter=self | jq -r '.template[0].id')
if [ "$id" != "" ]; then
    cloudmonkey delete template "id=$id"
fi

exit 0

# ?command=registerTemplate&response=json&
# name=SARNET+Alpine
# &displayText=SARNET+Alpine
# &url=http%3A%2F%2Fskynet.lab.uvalight.net%2Fben%2Fhost.vhd
# &zoneid=eba963d4-652f-466c-b772-31cbe9dcb937
# &format=VHD
# &isextractable=true
# &passwordEnabled=false&isdynamicallyscalable=false&osTypeId=aa07ee1a-dd36-11e5-b348-0cc47a3a1bec&hypervisor=XenServer&ispublic=true&requireshvm=false&_=1516107916912
set -x
cloudmonkey register template \
    "name=SARNET Alpine" \
    "displaytext=SARNET Alpine" \
    zoneid=eba963d4-652f-466c-b772-31cbe9dcb937 \
    hypervisor=XenServer \
    format=VHD \
    ostypeid=aa07ee1a-dd36-11e5-b348-0cc47a3a1bec \
    ispublic=true \
    passwordenabled=false \
    requireshvm=false \
    url=http://skynet.lab.uvalight.net/ben/host.vhd
