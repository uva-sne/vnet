# SC17

## Development

Install the following packages with pip3:

    pip3 install --user websockets pyopenssl

Manually start controllers:

    (cd config && PYTHONPATH=$PWD/.. ../bin/vnet-infra)
    (cd config && PYTHONPATH=$PWD/.. ../bin/vnet-ui)


## Addressing scheme

IPv4:

* 172.20.x.y -- link local, where x is the link number and y the AS (CHANGEME)
* 172.30.x.y -- containers, BGP advertised /24, x is AS and y container


## TODO

IP tunneling support:

    ip tunnel add tunAS mode ipip local $our_ip remote $as_ip dev eth0
    ip addr add tunAS 172.20.x.y/24
    # OR: ip addr add tunAS 172.20.a.b peer 172.20.c.d/32
    ip link set tunAS up

Also TODO: special IP tunnel for SARNET traffic


TODO: flow-reporting should support IPIP tunneling
TODO: vnet-ui memleak?

* vnetmon online/offline tracking bugged ~ vnetmon timeout issues

* limitation: running demo won't update :latest container images


## Node metadata

Things that set metadata:

* vnetmon
    - ifaces
    - netstats

* sarnet-agent
    - observables
    - [coms // log stream]

* host-netctl and similar tools [port to Go]
    - ?run flow-aggr?
        - vnetmon could (with toggle) compress detailed stream into UI-stream:
            UI={rx,tx}-domain-size


Metadata keys:

    vm/if/eth1
    vm/ns/eth1
    vm/containers
    vm/flow
    c/service64/service-sales
    c/sarnet254/log
