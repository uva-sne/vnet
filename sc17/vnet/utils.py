import logging
from asyncio import sleep, wait_for, CancelledError, TimeoutError
from websockets.exceptions import ConnectionClosed
from socket import IPPROTO_TCP
from json import loads
from json import dumps as _dumps
from hashlib import sha256
from re import sub
from base64 import b64decode

domain_kinds = {'transit', 'client', 'service', 'nfv'}

TCP_USER_TIMEOUT = 18


def set_timeout(ws, sec):
    socket = ws.writer.get_extra_info('socket')
    socket.setsockopt(IPPROTO_TCP, TCP_USER_TIMEOUT, sec * 1000)


def kind_from_name(name):
    """ $kind.as$number.$slice or $name.$kind.as$number.$slice """
    for token in name.split('.'):
        token = sub(r'[0-9]+$', '', token)
        if token in domain_kinds:
            return token
    raise ValueError(name)


def domain_from_name(name):
    for p in name.split('.'):
        if not p.startswith('as'):
            continue
        asn = p[2:]
        if asn.isdigit():
            return asn
    raise ValueError(name)


def dumps(v):
    return _dumps(v, separators=(',', ':'))


async def monitor_json(fn, announce):
    # TODO: inotify
    before = None
    while True:
        await sleep(5)
        try:
            data = open(fn, 'rb').read()
        except:
            logging.exception('Failed to read %s', fn)
            continue

        hash = sha256(data).hexdigest()
        if hash != before:
            logging.info('monitor_json: %r was updated', fn)
            before = hash
            try:
                await announce(loads(data.decode('utf8')))
            except:
                logging.exception('Barf')


def url_to_ident(url, prefix='http://geni-orca.renci.org/owl/'):
    if not url or not url.startswith(prefix):
        return
    return url[len(prefix):]


def pubkey_fprint(pubkey):
    return sha256(b64decode(pubkey)).hexdigest()


async def send_frames(ws, w):
    while True:
        ping = False
        try:
            frame = await wait_for(w.get(), 5)
        except TimeoutError:
            ping = True
        except (RuntimeError, CancelledError):
            break
        # TODO: ping KA

        try:
            if ping:
                await wait_for(ws.ping(), 5)
            else:
                await wait_for(ws.send(frame), 5)
        except (RuntimeError, CancelledError, ConnectionClosed):
            break
        except TimeoutError:
            logging.info('Send timeout: %s', id(ws))
            ws.close()
            break
