# SC17

## Topology

The topology controller syncs the (user) desired topology with the
orchestration platform.

Proposed modules:

    infra
        exogeni
        exogeni-bm
        openstack
        blueplanet



Topology phases:

- Teardown: remove undesired elements
- Bootup: add desired elements
- Config: (re)configure elements


## Topology patterns

- 3 hosts, router, (virtual) switch
    - NFV? SDN?
    - sFlow/IPFIX?
    - Domain agent?

- Random selection of domain types
- Random interconnection



Domain properties:
- AS number (from which we also derive IP addresses)
- Type
    - Transit: multiple routers
    - Service: router, one or two servers
    - Client: router, two or more clients


Connecting clients to servers:

- Random 1:1, 1:N?
- Path bandwidth concerns?
    - Optimal/default path should not be congested by non-attack traffic

API:

    propose_topology(n_domains, n_clients, n_servers,
                     seed=None) -> topology

    submit_topology(topology)


## Controller placement

- vnetmon/host-sflowd?
- SARNET agent?


## N2S

- Management networks
    - Infrastructure
    - Agent to agent
- Topology/VM MAC catalog


# Host post-configuration?

Would be easiest if software stack runs as container and VM.

- exogeni: SSH to host
- exogeni-bm: filesystem


What to configure:
- Non-management links
- BGP
- Client config
- Service config


# Demo topology

SARNET agents connect to data collection (UI) controller, send all state.

On the UvA rack, sending directly from the nodes to Skynet/SARNET is fine;
using other racks may be an issue.

UI controller cares about:
- Network element metadata
- Traffic overlay
- Flow overlay (i.e. aggregated)
- SARNET agent state
