# Shut up!
import logging
from websockets import protocol
from websockets import server

protocol.logger.setLevel(logging.ERROR)
server.logger.setLevel(logging.ERROR)
