"""
Certificate management
"""

from os import remove, urandom
from os.path import join, exists
from base64 import b64encode, b64decode
from OpenSSL import crypto
from OpenSSL.crypto import X509, X509Extension


def load_pair(path, cn, prefix='ca', bits=2048):
    pkey_path = join(path, prefix + '-key.pem')
    cert_path = join(path, prefix + '.pem')

    if not exists(pkey_path):
        if exists(cert_path):
            remove(cert_path)
        k = crypto.PKey()
        k.generate_key(crypto.TYPE_RSA, bits)
        data = crypto.dump_privatekey(crypto.FILETYPE_PEM, k)
        with open(pkey_path, 'wb') as fp:
            fp.write(data)
    else:
        data = open(pkey_path, 'rb').read()
        k = crypto.load_privatekey(crypto.FILETYPE_PEM, data)

    if True or not exists(cert_path):
        cert = X509()
        c = cert.get_subject()
        c.CN = cn
        cert.set_version(2)
        cert.set_serial_number(1)
        cert.gmtime_adj_notBefore(0)
        cert.gmtime_adj_notAfter(60 * 60 * 24 * 365)
        cert.set_issuer(c)
        cert.set_pubkey(k)
        cert.add_extensions([
            X509Extension(b'basicConstraints', True, b'CA:TRUE, pathlen:0'),
            X509Extension(b'keyUsage', True, b'keyCertSign, cRLSign'),
            X509Extension(b'subjectKeyIdentifier', False, b'hash', subject=cert)
        ])
        cert.sign(k, 'sha256')
        data = crypto.dump_certificate(crypto.FILETYPE_PEM, cert)
        with open(cert_path, 'wb') as fp:
            fp.write(data)

    pk = crypto.dump_publickey(crypto.FILETYPE_ASN1, k)
    pk = b64encode(pk).decode('utf8')

    return k, pk


def sign_cert(path, cn, k, prefix='ca'):
    pkey_path = join(path, prefix + '-key.pem')
    data = open(pkey_path, 'rb').read()
    ca_key = crypto.load_privatekey(crypto.FILETYPE_PEM, data)

    cert_path = join(path, prefix + '.pem')
    data = open(cert_path, 'rb').read()
    ca = crypto.load_certificate(crypto.FILETYPE_PEM, data)

    k = crypto.load_publickey(crypto.FILETYPE_ASN1, b64decode(k))

    cert = X509()
    c = cert.get_subject()
    c.CN = cn
    cert.set_version(2)
    serial = int.from_bytes(urandom(16), 'big', signed=False) + 2
    cert.set_serial_number(serial)
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(60 * 60 * 24 * 365)
    cert.set_issuer(ca.get_subject())
    cert.set_pubkey(k)
    cert.add_extensions([
        X509Extension(b'basicConstraints', True, b'CA:FALSE'),
        X509Extension(b'keyUsage', True, b'digitalSignature'),
        X509Extension(b'extendedKeyUsage', False, b'serverAuth, clientAuth'),
        X509Extension(b'subjectKeyIdentifier', False, b'hash', subject=cert)
    ])
    cert.sign(ca_key, 'sha256')

    return b64enc(ca), b64enc(cert)


def b64enc(crt):
    data = crypto.dump_certificate(crypto.FILETYPE_ASN1, crt)
    return b64encode(data).decode('utf8')


if __name__ == '__main__':
    p = '/home/ben/vnet/sc17/config'
    load_pair(p, 'SARNET CA')
    _, a = sign_cert(p, 'skynet.lab.uvalight.net',
                     'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvtIQKe/D+ps5'
                     'G5X/UqTmCR3AjH7h54uWf9cCrH+ttzx27zSAVBUapoqIcAK4stSieR4N'
                     'P/t+5iOo57CeheM1oCreEdb3Be6oavC6UqZyGhgYQMDJ9XAim9h7v+xp'
                     'i9rmOmuhuNFvDouzAZhmorf+va/YqIowU6UtYivxpPtNkBaM4Pnle70F'
                     'UsCu+Z6zFdA/wMq7qdhfp0tF45CSnDG79L0diSrslEsQz4Faxh83WxNY'
                     'LyWW85O5dArDvDDAYCXoUHEhKO+zvcCODVNXNtutFAvbf9TTCA+kttwo'
                     '6TdkfGLnyZ7Lcq181v4kcyBr2k5lpl8sYI12v6bRO/DlDskLWwIDAQAB')
    print(a)
