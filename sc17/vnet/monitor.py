"""
Handle vnetmon connections & RPC

- Order of sending messages is significant

Monitor:
- List of clients by ID
- No real broadcasting, sending is by ID

UI:
- Client identity not important
- True broadcasting
"""
import logging
from asyncio import ensure_future, Lock, Queue, wait_for, CancelledError
from websockets import serve
from websockets.exceptions import ConnectionClosed
from json import loads, dumps

from . import silence_loggers  # NOQA
from .utils import url_to_ident, send_frames, set_timeout


def dsum(d):
    if d is None or not hasattr(d, 'items'):
        return str(d)
    return ' '.join('%s=%s' % kv for kv in sorted(d.items()))


class Monitor:
    """Monitor vnetmon clients"""
    def __init__(self, ctrl):
        self.ctrl = ctrl
        self.clients_connected = {}
        self.serialize = Lock()

    async def client_add(self, ident, ws):
        with (await self.serialize):
            # *Must* prevent double connects; these can happen under racey
            # conditions when a client reconnects
            if ident in self.clients_connected:
                raise ValueError('Double connect from %s', ident)
            self.clients_connected[ident] = ws
        with (await self.ctrl.serialize):
            await self.ctrl.client_add(ident)

    async def client_remove(self, ident):
        with (await self.serialize):
            del self.clients_connected[ident]
        with (await self.ctrl.serialize):
            await self.ctrl.client_remove(ident)

    async def send_serialized(self, ident, data):
        with (await self.serialize):
            ws = self.clients_connected.get(ident)
            if ws is None:
                logging.warning('Client not active: %s', ident)
                return
            # TODO: limit write size, kick out slow clients
            await ws.put(dumps(data))

    async def relay_task(self, ident, command, args):
        await self.send_serialized(ident, {'kind': command, 'data': args})

    async def handle_client(self, ws, _):
        ident = None
        identified = False
        set_timeout(ws, 5)
        w = Queue()
        writer = ensure_future(send_frames(ws, w))
        try:
            while True:
                # recv may block forever
                try:
                    msg = loads(await ws.recv())
                except (TimeoutError, ConnectionClosed):
                    break
                kind = msg.get('@') or msg.get('kind')
                data = msg.get(kind) or msg.get('data')
                if kind in ('i', 'identify'):
                    ident = url_to_ident(data.get('node') or data.get('fqdn'))
                    logging.debug('monitor[%s] %s', id(ws), dsum(data))
                    if not identified and ident:
                        identified = True
                        await self.client_add(ident, w)
                # else:
                #     logging.debug('monitor %s: %s %s', ident, kind,
                #                   dsum(data))
                if not ident:
                    logging.warn('Client did not identify properly')
                    break
                if kind == 'm':
                    with (await self.ctrl.serialize):
                        await self.ctrl.client_metadata(ident, data)
        #except Exception as e:
        #    logging.warn('Monitor exception: %s: %s', type(e), e)
        finally:
            if ident:
                await self.client_remove(ident)
            ws.close()
            logging.debug('Task ended: %s', ident)
            writer.cancel()


async def monitor(config, ctrl):
    ctrl.rpc = rpc = Monitor(ctrl)
    await serve(rpc.handle_client, '0.0.0.0', 8100)
