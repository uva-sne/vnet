import logging
from asyncio import ensure_future, Lock
from json import loads
from copy import deepcopy

from vnet.bi import bi
from .utils import domain_from_name, url_to_ident, dumps, pubkey_fprint
from vnet.cert import sign_cert


def _to_name(url):
    return url.split('#', 1)[-1]


class Controller:
    """
    The VNET controller glues together the other components.

    Metadata quirks:
    - Nodes that are not defined in the topology can still connect and send
      metadata, but may get flushed during metadata updates
    """
    def __init__(self):
        # Serialize topology/metadata processing
        self.serialize = Lock()

        self.ui = None
        self.rpc = None

        self.clients_connected = set()
        self.clients_expected = set()
        self.active_client_map = set()
        self.had_all_connected = False
        self.owner_macs = set()

        # Metadata cache (TODO: move to UI)
        self.cache = {}  # Active only
        self.cache_all = {}  # All

        self.snapshot = {}
        self.snapshot_counter = 0
        self.containers = {}
        self.container_counter = 0

    # Topology (container hosts)
    # --------------------------
    #
    # There are two notions of topology:
    # - The desired virtual topology
    # - The current IaaS platform layout, details
    #
    # IaaS details are arguably only relevant insofar as they add additional
    # metadata to the virtual topology:
    # - Deployed site
    # - Management IP
    # - Startup status
    # - Provisioned link bandwidth
    # - VLAN numbers
    #
    # The virtual topology can comprise of multiple instances of physical
    # topology "chunks", provided by multiple instances. It will be a priori
    # clear from the virtual topology how the physical aspect is divided.

    async def update_topology(self, topology):
        """Called when topology updates become available"""
        logging.info('Topology update received')

        nodes = topology['nodes']
        expected = set()
        owner_macs = set()
        for node in nodes.values():
            url = url_to_ident(node.get('unit_url'))
            if url:
                expected.add(url)
        for l in topology['links'].values():
            macs = []
            for k in ('source', 'target'):
                name = l[k]
                n = nodes.get(name, {})
                for iface in n.get('ifaces', []):
                    if l['name'] in iface.get('links', []):
                        mac = iface.get('mac')
                        if mac:
                            macs.append(mac)
            if not macs:
                logging.info('No owner for %s', l['name'])
            else:
                macs.sort()
                mac = macs[0]
                logging.info('Owner for %s: %s', l['name'], mac)
                owner_macs.add(mac.replace(':', ''))
        self.owner_macs = owner_macs

        self.snapshot = topology
        if self.clients_expected != expected:
            logging.info('Topology has changed')
            self.containers = containers_from_topology(topology)
            self.host_config = host_config_from_topology(topology)
            self.clients_expected = expected
            await self.clients_changed()
            await self.ui.set_expected(expected)

        #await self.sync_config_sync(ident)

        # Enqueue a topology update
        await self.ui.set_latest_topology(topology)

    # Clients
    # -------
    #
    # Clients are vnetmon agents. Old clients may linger a bit, and new clients
    # may join before we received a topology update.

    async def clients_changed(self):
        """Called when clients expected/connected changes"""
        missing = self.clients_expected - self.clients_connected
        self.had_all_connected = not missing

        if missing:
            logging.info('Still missing %s client(s)', len(missing))
            return

        logging.info('All nodes are connected')
        await self.sync_containers()

    async def client_add(self, ident):
        """Add a particular vnetmon client; send container config if
        appropriate"""
        if not ident:
            raise ValueError(ident)
        if ident in self.clients_connected:
            raise ValueError('Invalid add of %s' % (ident,))
        self.clients_connected.add(ident)
        if ident not in self.clients_expected:
            # Maybe a topology update will follow
            logging.info('Node %s not expected (yet)', ident)
            return

        await self.sync_config_sync(ident)

        #await self.ui.set_metadata(ident, {online: True})
        if self.had_all_connected:
            # Can resend only to host
            await self.sync_container_client(ident)
        else:
            await self.clients_changed()

    async def client_remove(self, ident):
        """Remove a particular vnetmon client"""
        if not ident:
            raise ValueError(ident)
        if ident not in self.clients_connected:
            raise ValueError('Invalid remove of %s' % (ident,))
        self.clients_connected.remove(ident)
        await self.ui.remove_ident(ident)

    async def client_metadata(self, ident, data):
        # HERE: hook into metadata received from vnetmon
        if 'sarnet/pubkey' in data:
            await self.process_sarnet_pubkey(ident, data['sarnet/pubkey'])
        await self.ui.set_metadata(ident, data)

    # Containers
    # ----------
    #
    # Change tracking follows the topology counter to ensure multiple
    # asynchronous requests don't end up rolling back a configuration change.

    async def set_containers(self, cs):
        """Set latest container configuration"""
        self.containers = cs
        c = self.container_counter + 1
        self.container_counter = c
        return c

    async def publish(self, ident, k, v, retain=False, encode_str=False):
        if encode_str or not isinstance(v, str):
            v = dumps(v)
        await self.rpc.relay_task(ident, 'publish',
                                  {'k': k, 'v': v, 'r': retain})

    async def publish_all(self, key, value, retain=False, encode_str=False):
        s = self.snapshot
        for n in s.get('nodes', {}).values():
            if 'unit_url' in n:
                await self.publish(url_to_ident(n['unit_url']),
                                   key, value, retain, encode_str)

    async def send_task(self, ident, task, args):
        await self.rpc.relay_task(ident, 'task-request',
                                  {'task': task, 'args': args})

    async def sync_containers(self):
        """Send out an update to all online container hosts"""
        for ident in self.clients_expected:
            await self.sync_container_client(ident)

    async def sync_container_client(self, ident):
        containers = self.containers.get(ident)
        logging.info('Update containers for %s: %s', ident, containers)
        await self.publish(ident, 'cmd/host-netctl/containers/vc',
                           {'pfx': 'vc', 'cs': containers}, True)

    async def sync_configs(self, ident):
        for ident in self.clients_expected:
            await self.sync_config_sync(ident)

    async def sync_config_sync(self, ident):
        config = self.config.get(ident)
        logging.info('Update config for %s: %s', ident, config)
        config = config or ''
        #await self.rpc.relay_task(ident, 'host-config', {'config': config})
        # Runs vni-host-config
        await self.rpc.relay_task(ident, 'itask-request', {'task': 'host-config',
                                                           'exclusive': True,
                                                           'input': config})

    # Misc
    # ----
    #
    # TODO: while state manipulation benefits from serialization, sending
    # things over the network while holding the serialization lock will slow
    # things down. These things should be decoupled for better performance.

    async def process_sarnet_pubkey(self, ident, pubkey):
        # Depending on the mode, either produce a cert or notify other agents
        # of the key <=> identity mapping
        ca, cert = sign_cert('', _to_name(ident),
                             pubkey)
        logging.info('Sending certificates to %s', ident)
        await self.publish(ident, 'cmd/sarnet/auth-cert', '%s %s' % (ca, cert),
                           True)
        # if ident not in self.clients_expected:
        #     return
        # await self.sarnet_sync_pubkeys()

    async def user_command(self, kind, data):
        with (await self.serialize):
            if kind == 'pub':
                k = data.get('k')
                v = data.get('v')
                r = data.get('r', False)
                if k and v:
                    await self.publish_all(k, v, r)
            elif kind == 'learn':
                await self.publish_all('cmd/sarnet/learn', 'true')
            elif kind == 'attack':
                attack = data.get('attack')
                if attack == 'cs':
                    await self.set_va_containers(data.get('cs'))
                elif attack == 'stop':
                    await self.stop_attack()
            else:
                logging.warn('Unknown command: %r', kind)

    # Attack helpers
    # --------------

    # TODO: change attack container command

    async def stop_attack(self):
        for n in self.clients_connected:
            await self.publish(n, 'cmd/host-netctl/containers/va',
                               {'pfx': 'va', 'cs': []})

    async def set_va_containers(self, cs):
        if not cs:
            return
        for name, clist in cs.items():
            await self.publish(name, 'cmd/host-netctl/containers/va',
                               {'pfx': 'va', 'cs': clist})


def host_config_from_topology(g):
    cfg = {}
    for name, attr in g['nodes'].items():
        c = attr.get('config')
        cfg[name] = c
    return cfg


def containers_from_topology(g):
    def build_sarnet_agent(name):
        links = []
        neighbors = [v for v in alliance if v != name]
        for link in g['links'].values():
            friend = None
            if link['source'] == name:
                friend = link['target']
            elif link['target'] == name:
                friend = link['source']
            else:
                continue
            links.append(friend)
            #if friend in alliance:
            #    neighbors.append(friend)

        links.sort()
        neighbors.sort()
        env = {'SN_NAME': name,
               'SN_DOMAIN': domain_from_name(name),
               'SN_LINKS': ','.join(links),
               'SN_ALLIANCE': ','.join(neighbors)}
        return {'name': 'vc-sarnet',
                'img': 'sarnet-agent',
                'pull': True,
                'net': 'host',
                'env': env}

    cs = {}
    targets = []
    alliance = []

    # Collect services & alliance members
    for name, attr in g['nodes'].items():
        c = attr.get('containers', {})
        kind = c.get('kind')
        if kind == 'service':
            for i in range(c.get('nodes', 1)):
                targets.append('172.30.%s.%s' % (domain_from_name(name),
                                                 64 + i))
        if kind != 'client' and c: # and 'surf' not in name:
            alliance.append(name)

    logging.info('Alliance: %r', alliance)

    # Connect stuff
    j = 0
    for name, attr in g['nodes'].items():
        c = attr.get('containers', {})
        kind = c.get('kind')
        n = c.get('nodes', 1)
        logging.info('%s (%s) => %s', name, kind, n)
        ctr = []

        if 'unit_url' in attr and 'surf' not in name:
            ctr.append({'name': 'vc-reflect',
                        'img': 'vnet-ddos-reflect',
                        'ip': '63'})

        if name in alliance:
            ctr.append(build_sarnet_agent(name))

        if kind == 'service':
            ctr.extend({'name': 'vc-%s' % (64 + i,),
                        'img': 'vnet-service',
                        'ip': str(64 + i),
                        'env': {'SERVICE_ID': str(64 + i)}}
                       for i in range(n))

        elif kind == 'client' and targets:
            # TODO: better selection
            nodes = []
            for i in range(n):
                args = '-workers,10,' + targets[j % len(targets)]
                j += 1
                nodes.append({'name': 'vc-%s' % (64 + i,),
                              'img': 'vnet-client',
                              'ip': str(64 + i),
                              'env': {'CONTAINER_ARGS': args}})
            ctr.extend(nodes)
        if ctr:
            cs[url_to_ident(attr['unit_url'])] = ctr

    return cs
