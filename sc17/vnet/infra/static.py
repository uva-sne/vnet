import logging


class Static:
    def __init__(self, slice_name, report_topo):
        self.slice_name = slice_name
        self.report_topo = report_topo
        self.topology = None

    async def set_topology(self, topology):
        # TODO: needs to be async safe
        self.topology = topology
        await self.report_topo.put((self.slice_name, topology))


async def initialize(slice_name, report_topo, config, sec):
    s = Static(slice_name, report_topo)
    # <Launch tasks>
    return s
