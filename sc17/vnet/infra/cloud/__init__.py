"""
Cloud infrastructure manager
----------------------------

FIXME: this implementation uses a lot of blocking calls.

Not all IaaS platforms support modifying instance attributes (e.g. OpenStack,
CloudStack), therefore base configuration (i.e. vnetmon controller address) is
set via the user-data attribute once. [???]

The controller is from then on responsible for propagating additional
configuration.

TODO:
- Deploy VMs in background; libcloud threading?
- Automatically detect image (ID) changes and reload nodes

TODO:
- Make "desired node config" key more complex than just name (e.g. instance
  type, image name, ...)
"""

from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

import logging
from asyncio import sleep, wait_for, Queue, TimeoutError, ensure_future
from base64 import b64encode
from json import dumps
from re import sub
from copy import deepcopy
from os import urandom

from ...utils import domain_from_name, kind_from_name

# states: pending, running, rebooting, pending


def gen_ppp(v):
    """Generate an IP used for point-to-point communication"""
    d = v & 0xff
    c = (v >> 8) & 0xff
    return '172.20.%s.%s/30' % (c, d)


def gen_mac():
    b = urandom(5)
    return '02:%02x:%02x:%02x::%02x:%02x:%02x' % (b[0], b[1], b[2], b[3], b[4])


def sdump(obj):
    return dumps(obj, sort_keys=True)


def by_name(lst, name):
    for i in lst:
        if i.name == name:
            return i


class InstanceTrack:
    def __init__(self, id, name=None):
        self.id = id
        self.name = name
        self.booting = True
        self.config = ''
        self.last_state = None
        self.terminated = False

    def __repr__(self):
        if self.name:
            return '%s[%s]' % (self.name, self.id)
        return self.id


def gen_config(name, props, interfaces):
    """ TODO: generate vnet config """
    buf = ''
    for k, v in props.items():
        buf += '%s=%s\n' % (k, v)
    for i in interfaces:
        buf += i + '\n'
    return buf


async def trigger_every(q, delay):
    while True:
        await sleep(delay)
        logging.info('sz %s', q.qsize())
        # Fill queue only if there's nothing else to do
        if not q.qsize():
            await q.put(None)


def gen_exo_config(x, node, base):
    """Minimal subset of ExoGENI-compatible config to get host running"""

    # TODO: loghost
    # TODO: make this independent of infra provider
    bs = '#!/bin/sh\ncat <<VNET_CONFIG > /etc/.vnet-config\n'
    for opt in base:
        bs += opt + '\n'
    bs += 'VNET_CONFIG\n'
    bs += 'mv /etc/.vnet-config /etc/vnet-config\n'
    #bs += 'vnet-setup-logging > /tmp/loglog 2>&1\n'
    #bs += 'vnet-map-ifaces >> /tmp/map-ifaces 2>&1\n'
    bs += 'iptables -t raw -A PREROUTING -j NOTRACK\n'  # XXX

    props = {'host_name': node['name'],
             'unit_url': node['unit_url'],
             'physical_host': x.cloud_name,
             'slice_name': x.slice_name}
    buf = '[global]\n'
    for kv in props.items():
        buf += '%s=%s\n' % kv
    buf += '[interfaces]\n[scripts]\nbootscript='
    for i, line in enumerate(bs.split('\n')):
        if i != 0:
            buf += '\t'
        buf += line + '\n'
    return buf


def gen_node_guid(slice_name, name):
    prefix = name.split('.', 1)[0]
    # TODO: increase size if you ever get collisions
    suffix = b64encode(urandom(6)).decode('utf8')
    return (slice_name + '-' +
            sub(r'[^0-9a-zA-Z\-]+', '-', prefix + '-' + suffix).rstrip('-'))


def prepare_cloud_topo(x, topo, names, _reuse_vms=[]):
    retain = {}
    for k, v in names.items():
        retain[v] = k
    cloud = x.cloud_name
    slice_name = x.slice_name
    # TODO
    graph = {'nodes': {}, 'links': {}}
    nodes = graph['nodes']
    domains = {}
    names = {}

    for n in topo.get('domains', []):
        if '.' in n:
            asn = domain_from_name(n)
            name = '%s.%s' % (n, slice_name)
        else:
            asn = sub(r'^[^0-9]+', '', n)
            name = '%s.as%s.%s' % (n, asn, slice_name,)
        assert asn.isdigit()
        guid = retain.get(name)
        if not guid:
            guid = _reuse_vm(slice_name, name,
                             _reuse_vms) or gen_node_guid(slice_name, name)
        kind = kind_from_name(n)
        domains[n] = {'name': name, 'as': asn, 'kind': kind}
        nodes[name] = {'name': name,
                       'kind': kind,
                       'ifaces': [],
                       'unit_url': 'cloud:%s#%s' % (cloud, guid),
                       'guid': guid,
                       'containers': {'kind': kind},
                       'config': None}
        names[guid] = name

    # TODO: this should be more consistent, i.e. addresses assigned to links
    # should not change even if the topology changes

    point_to_point = 0

    links = graph['links']
    for n in topo.get('links', []):
        sname = domains[n['source']]['name']
        tname = domains[n['target']]['name']
        name = 'link-%s-%s' % (domain_from_name(sname),
                               domain_from_name(tname))
        links[name] = {'name': name,
                       'source': sname,
                       'target': tname,
                       'sourcev4': gen_ppp(point_to_point + 1),
                       'targetv4': gen_ppp(point_to_point + 2),
                       # guid
                       'metadata': {}}
        point_to_point += 4

    return graph, names


def augment_node(graph, name, state):
    node = graph['nodes'].get(name)
    if node:
        node['cloud#state'] = state


def _reuse_vm(slice_name, name, have):
    x = slice_name + '-' + name.split('.', 1)[0] + '-'
    for ident in have:
        if ident.startswith(x):
            logging.debug('cloud: Reuse %s as %s', ident, name)
            return ident


# TODO: exception monitoring
# TODO: make sure slice status is visible in UI
async def slice_monitor(x):
    # Things we need:
    # - Addressing
    # - Node post-boot (re)config

    # Tag instances with a slice_name, then we can delete unexpected instances
    # with this tag (instead of all, or none)

    had_topology = False
    allow_inherit = True  # <MAKE OPTIONAL>

    desired_topo, names = prepare_cloud_topo(x, {}, {})
    had_all_instances = False
    tracking = {}  # Track instances
    instances = set()

    reported_topo = ''

    while True:
        try:
            m = await wait_for(x.queue.get(), 10)
        except TimeoutError:
            m = None
        if m is False:
            logging.info('cloud: Topology delete request')
            desired_topo, names = prepare_cloud_topo(x, {}, names)
            had_topology = True
        elif m is not None:
            logging.info('cloud: Topology changed')
            # Pre-run list_slice_nodes to fix "names"
            if allow_inherit:
                allow_inherit = False
                _reuse_vms = [x.node_name(node)
                              for node in x.list_slice_nodes(x.slice_name)]
            else:
                _reuse_vms = []
            desired_topo, names = prepare_cloud_topo(x, m, names,
                                                     _reuse_vms)
            had_all_instances = False
            had_topology = True

        # Check the status of the resources we have
        know_about = instances
        instances = set()
        term = []

        topo = deepcopy(desired_topo)
        desired_nodes = topo['nodes']

        for node in x.list_slice_nodes(x.slice_name):
            _name = x.node_name(node)
            name = names.get(_name) or _name
            ident = x.node_id(node)
            instances.add(ident)
            state = x.node_state(node)

            t = tracking.get(ident)
            if t is None:
                t = tracking[ident] = InstanceTrack(ident, name)
                t.mgmt_ip = node.public_ips[0]  # TODO
                logging.debug('cloud: Discovered %r (%s)', t, t.mgmt_ip)
                if name not in desired_nodes:
                    logging.debug('cloud: Unexpected or old instance %r', t)
                    term.append(node)

            if t.last_state != state:
                logging.debug('cloud: Instance %r now has state %r', t, state)
                t.last_state = state

                '''
                if code == STATE_SHUTOFF:
                    logging.debug('cloud: Instance %r needs termination', i)
                if code >= STATE_TERM:
                    # if state has become >= TERM, delete named instance
                    pass
                '''

            if t.terminated:
                continue
            elif name not in desired_nodes:
                logging.debug('cloud: Undesired node %r', t)
                term.append(node)
            else:
                # Links?
                # Any other metadata we should need? Like IP addressess..
                # TODO: prepare instrunctions
                x.sync_node_meta(topo, name, node)

            #if code > STATE_RUNNING:
            #    continue

            # Sync user_data if necessary
            # XXX: What about pending?
            #ud = desired_config.get(i.id, '')
            #if ud != t.config:
            #    logging.debug('ec2: Update instance %r config', i)
            #    i.modify_attribute(Attribute='userData', UserData=ud)
            #    t.config = ud

        # Check for resources that went away
        for id in know_about:
            if id not in instances:
                t = tracking.get(id)
                logging.debug('cloud: Lost instance: %r', t or id)
                if t and t.name:
                    del tracking[id]

        # Erase resources that are no longer relevant
        if had_topology:
            for n in term:
                i = x.node_id(n)
                t = tracking.get(i)
                if t and not t.terminated:
                    logging.debug('cloud: Terminate instance: %r', t)
                    # Note: terminated resources can [remain visible for a
                    # while
                    x.node_terminate(n)
                    tracking[i].terminated = True

        # Create missing resources
        missing = False
        for name, node in desired_nodes.items():
            # These SHOULD be reflected in the next iteration. If this ever
            # turns out to not be the case, this will create a bunch of
            # instances at once
            if any(t.name == name for t in tracking.values()):
                continue

            missing = True
            # TODO: this can be slow
            logging.info('cloud: Create instance for %s', name)
            try:
                x.create_instance(name, node)
            except Exception:
                logging.exception('cloud: Failed')

            # <!!>

        if not missing and not had_all_instances:
            had_all_instances = True  # (Prevent logspam)
            logging.info('cloud: Have all instances')

            # Create new desired_config
            ifaces = {}

            def add_tunnel_if(link, source, target, swap):
                # Assumptions: only one point-to-point link between neighbors
                if swap:
                    b, a = 'source', 'target'
                else:
                    a, b = 'source', 'target'
                origin, neigh = link[a], link[b]
                lip, rip = link[a + 'v4'], link[b + 'v4']
                if swap:
                    nip, ip = source.mgmt_ip, target.mgmt_ip
                else:
                    ip, nip = source.mgmt_ip, target.mgmt_ip
                do = int(domain_from_name(origin))
                dn = int(domain_from_name(neigh))
                mac = '02:96:00:%02x:00:%02x' % (do, dn)
                obj = ifaces[origin]
                obj.append('tun%s=%s %s local %s %s remote %s %s' % (dn,
                                                                     neigh,
                                                                     mac,
                                                                     ip,
                                                                     lip,
                                                                     nip,
                                                                     rip))

            for name in desired_topo['nodes'].keys():
                ifaces[name] = []
            for link in desired_topo['links'].values():
                source = by_name(tracking.values(), link['source'])
                target = by_name(tracking.values(), link['target'])
                add_tunnel_if(link, source, target, False)
                add_tunnel_if(link, source, target, True)
            for name, node in desired_topo['nodes'].items():
                ifs = ifaces[name]
                ifs.sort()
                props = {}
                cfg = gen_config(name, props, ifs)
                topo['nodes'][name]['config'] = node['config'] = cfg

            _topo = sdump(topo)
            if _topo != reported_topo:
                logging.debug('cloud: Reporting topology change')
                reported_topo = _topo
                await x.report_topo.put((x.slice_name, topo))


class CloudInfra:
    """Implements cloud interface for vnet-infra"""
    def __init__(self, cloud_name, cloud, slice_name, report_topo, config, sec):
        if sub(r'[^a-z0-9\-]', '', slice_name) != slice_name:
            raise ValueError('Slice name may only contain [a-z0-9\-]')
        self.cloud_name = cloud_name
        self.cloud = cloud
        self.slice_name = slice_name
        self.report_topo = report_topo
        self.config = config
        self.sec = sec
        self.queue = Queue()

        self._zone = sec['zone']
        self._keypair = sec['keypair']

    async def set_topology(self, topology):
        """
        Handle user request for topology change

        TODO: slice modification
        TODO: support partial progress indicator
        TODO: deal with "soft updates" that only change container parameters
        """
        await self.queue.put(topology)

    # Abstraction
    # -----------
    # TODO: here you can implement driver-specific things if you want

    def list_slice_nodes(self, slice_name):
        slice_prefix = slice_name + '-'
        nodes = []
        for node in self.cloud.list_nodes():
            if node.name.startswith(slice_prefix):
                nodes.append(node)
        return nodes

    def node_id(self, node):
        return node.id

    def node_name(self, node):
        return node.name

    def node_state(self, node):
        return str(node.state)

    def node_terminate(self, node):
        self.cloud.destroy_node(node, ex_expunge=True)

    def create_instance(self, name, node):
        # TODO: spawn this in a background task
        # TODO: this needs to be more robust
        self._network = by_name(self.cloud.ex_list_networks(),
                                self.sec['network'])
        self._image = by_name(self.cloud.list_images(), self.sec['image'])
        self._size = by_name(self.cloud.list_sizes(), self.sec['size'])

        userdata = gen_exo_config(self, node, self.config)
        self.cloud.create_node(name=node['guid'],
                               displayname=name,
                               size=self._size,
                               image=self._image,
                               zone=self._zone,
                               ex_keyname=self._keypair,
                               networks=[self._network],
                               ex_userdata=userdata)

    def sync_node_meta(self, topo, name, node):
        # node.extra['image_name']
        # node.extra['size_name']
        # 'nics:': [{'broadcasturi': 'vlan://868',
        #    'gateway': '146.50.38.1',
        #    'id': '0a755d7e-51eb-4fc6-8b93-3b5e1642a292',
        #    'ipaddress': '146.50.38.91',
        #    'isdefault': True,
        #    'isolationuri': 'vlan://868',
        #    'macaddress': '06:22:da:00:11:7f',
        #    'netmask': '255.255.255.0',
        #    'networkid': 'f6a2b797-ca53-4f2b-bc0f-20b6dba6f7a4',
        #    'networkname': 'Agile-External',
        #    'secondaryip': [],
        #    'traffictype': 'Guest',
        #    'type': 'Shared'}],
        pass


async def initialize(slice_name, report_topo, config, sec):
    # Necessary for monitoring configuration
    cloud_factory = get_driver(sec['driver'])
    # TODO:
    cloud = cloud_factory(key=sec['api-user'],
                          secret=sec['api-secret'],
                          host=sec['host'],
                          path=sec['path'],
                          secure=False)
    x = CloudInfra(sec['host'],
                   cloud, slice_name, report_topo,
                   config, sec)
    ensure_future(slice_monitor(x))
    return x
