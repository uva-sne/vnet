from libcloud.compute.types import Provider
from libcloud.compute.base import NodeSize, NodeImage
from libcloud.compute.providers import get_driver
from pprint import pprint
from time import asctime

ACCESS_ID = 'pBON8anAo1ZWZQSjxi6z35_OAsx2ymblWOZ4x210mTvm_OfDZ20mCQpsCG5cPbNatP3tfUl1FJMajQvgReGnEg'
SECRET_KEY = '_FgvKDGg0KKFgmvHCa5u8HzEPUmFG7Q5PRPltM_SUcnwC8lS78dV4aLE4vpd9DZ7d29Y0V5qLvgmPXxHizcQUg'


def _find(nm, lst):
    for i in lst:
        if i.name == nm:
            return i


CloudStack = get_driver(Provider.CLOUDSTACK)
driver = CloudStack(key=ACCESS_ID,
                    secret=SECRET_KEY,
                    secure=False,
                    host='agile-h0.science.uva.nl:8080',
                    #host='exp-exo-c1.lab.uvalight.net:8080',
                    path='/client/api')

desired_set = {'test.sarnet-cloud-dev'}
have_set = set()

print('Current nodes:')
nodes = driver.list_nodes()
for node in nodes:
    print('Node:', node)
    pprint(node.extra)
    have_set.add(node.name.replace('--', '.'))

build_set = desired_set - have_set
if build_set:
    use_zone = 'AgileZone0'
    use_network = _find('Agile-External', driver.ex_list_networks())
    use_size = _find('AgileSmall', driver.list_sizes())
    use_image = _find('SARNET Alpine X', driver.list_images())
    use_disk = 'AgileSomething'

    print('Using size:', use_size)
    print('Using image:', use_image)
    print('Using network:', use_network)

    exit()

    for d in build_set:
        print('create', d)
        print(asctime())
        r = driver.create_node(name=d.replace('.', '--'),
                               size=use_size,
                               image=use_image,
                               zone=use_zone,
                               networks=[use_network],
                               ex_userdata='test')
        print(asctime())
        print('r', r)
        pprint(r.extra)
