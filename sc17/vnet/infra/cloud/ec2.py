"""
EC2 infrastructure manager
--------------------------

"""

import logging
from asyncio import sleep, Queue, ensure_future
from base64 import b64encode
from boto3 import resource

from ...utils import kind_from_name

STATE_PENDING = 0
STATE_RUNNING = 16
STATE_SHUTOFF = 32
STATE_TERM = 48
STATE_STOPPING = 64
STATE_STOPPED = 80


class EC2Cloud:
    def __init__(self, conf):
        self.conf = conf
        res = resource('ec2',
                       region_name=conf.get('region', 'nova'),
                       endpoint_url=conf['controller'],
                       aws_access_key_id=conf['access_key_id'],
                       aws_secret_access_key=conf['secret_access_key'])
        self.api = res.resource('ec2')

    def list_instances(self, slice_name):
        f = [{'Name': 'vnet-slice',
              'Values': [slice_name]}]
        for i in self.api.instances.filter(Filters=f):
            yield i.id, i

    def instance_state(self, _, i):
        state = i.State['Name']
        if state in ('pending', 'running', 'shutting-down'):
            return state
        return 'stopped'


def augment_container_info(graph, topology):
    if not graph or not topology:
        return
    nodes = graph.get('nodes', {})
    for name, attr in nodes.items():
        kind = kind_from_name(name)
        attr['containers'] = {'kind': kind}


def gen_userdata(name, interfaces, bootscript):
    """ExoGENI-compatible userdata"""
    # TODO: unit_url
    # physical_host
    # slice_name
    # management_ip
    buf = '[global]\nhost_name=%s\n[interfaces]\n' % (name,)
    for i in interfaces:
        buf += '...\n'
    buf += '[scripts]\nbootscript='
    for i, line in enumerate(bootscript.split('\n')):
        if i != 0:
            buf += '\t'
        buf += line + '\n'
    buf += '\n'
    return b64encode(buf.encode('utf8')).decode('utf8')


async def trigger_every(q, delay):
    while True:
        await sleep(delay)
        # Fill queue only if there's nothing else to do
        if not q.qsize():
            await q.put(None)


# TODO: exception monitoring
# TODO: make sure slice status is visible in UI
async def slice_monitor(x):
    # Things we need:
    # - Addressing
    # - Node post-boot (re)config

    # Tag instances with a slice_name, then we can delete unexpected instances
    # with this tag (instead of all, or none)

    s = resource('ec2',
                 region_name=x.sec.get('region', 'nova'),
                 endpoint_url=x.sec['controller'],
                 aws_access_key_id=x.sec['access_key_id'],
                 aws_secret_access_key=x.sec['secret_access_key'])

    ec2 = s.resource('ec2')

    desired_state = {}
    desired_userdata = {}
    had_all_instances = False
    tracking = {}  # Track *running* instances
    name_to_instance = {}  # Track *pending* instances
    instances = set()

    image_id = x.sec['image_id']
    instance_type = x.sec.get('instance_type', 't2.micro')

    # import_key_pair(KeyName='...', PublicKeyMaterial=b'base64==')

    while True:
        m = await x.queue.get()
        if m is False:
            logging.info('ec2: Topology delete request')
        elif m is not None:
            logging.info('ec2: Topology changed')
            desired_state = m
            had_all_instances = False
            # HERE: delete old userdata

        # Check the status of the resources we have
        know_about = instances
        instances = set()

        # <filter by slice tag>
        for i in ec2.instances.all():
            instances.add(i.id)
            code = i.state['Code'] & 0xFF

            t = tracking.get(i.id)
            if t is None:
                logging.debug('ec2: Learned new instance: %r', i)
                name = ...
                t = tracking[i.id] = InstanceTrack(i.id, name)

            if t.last_state != code:
                logging.debug('ec2: Instance %r now has state %r', i, code)
                t.last_state = code

                if code == STATE_SHUTOFF:
                    logging.debug('ec2: Instance %r needs termination', i)

                if code >= STATE_TERM:
                    # if state has become >= TERM, delete named instance
                    pass

            if code > STATE_RUNNING:
                continue

            # Sync user_data if necessary
            # XXX: What about pending?
            ud = desired_userdata.get(i.id, '')
            if ud != t.userdata:
                logging.debug('ec2: Update instance %r userdata', i)
                i.modify_attribute(Attribute='userData', UserData=ud)
                t.userdata = ud

        for id in know_about:
            if id not in instances:
                logging.debug('ec2: Lost instance: %r', id)
                # - Remove the named instance, if it exists

        # Build topology dict here, and report_topo on change

        # Erase resources that are no longer relevant
        term = []
        for name, i in name_to_instance.items():
            if name not in desired_state['nodes']:
                # TODO: If it is not already stopping/stopped
                term.append((name, i))
            # TODO: .
        for name, i in term:
            logging.debug('ec2: Terminate instance: %r', i)
            # Note: terminated resources remain visible for a while
            i.terminate()  # <failure?>
            del name_to_instance[name]

        # Create missing resources
        missing = False
        for name, node in desired_state['nodes'].items():
            # These SHOULD be reflected in the next iteration. If this ever
            # turns out to not be the case, this will create a bunch of
            # instances at once
            if name in name_to_instance:
                continue

            missing = True

            # TODO: we *could* batch call this
            logging.info('ec2: Create instance for %s', name)

            # TODO: slice? uuid?
            tags = {'ResourceType': 'instance',
                    'Tags': [{'Key': 'vnet-slice',
                              'Value': x.slice_name},
                             {'Key': 'vnet-name',
                              'Value': name}]}
            # TODO: check if PrivateIpAddress works?
            i = ec2.create_instances(ImageId=image_id,
                                     InstanceType=instance_type,
                                     MinCount=1,
                                     MaxCount=1,
                                     TagSpecifications=[tags])
            # <key pair via KeyName>?
            # <tags?>
            logging.info('ec2: %s => %s', name, i.id)
            name_to_instance[name] = i.id

        if not missing:
            if not had_all_instances:
                had_all_instances = True  # (Prevent logspam)
                logging.info('ec2: Have all instances')
                # Create new desired_userdata


class InfraEC2:
    def __init__(self, slice_name, report_topo, sec):
        self.slice_name = slice_name
        self.report_topo = report_topo
        self.sec = sec
        self.queue = Queue()

    async def set_topology(self, topology):
        """
        Handle user request for topology change

        TODO: slice modification
        TODO: support partial progress indicator
        TODO: deal with "soft updates" that only change container parameters
        """
        await self.queue.put(topology)


async def initialize(slice_name, report_topo, config, sec):
    # Necessary for monitoring configuration
    x = InfraEC2(slice_name, sec['site'], report_topo, config['vnet'])
    ensure_future(trigger_every(x.queue, 10))
    ensure_future(slice_monitor(x))
    return x
