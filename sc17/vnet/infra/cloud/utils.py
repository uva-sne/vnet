import logging
from re import sub
from copy import deepcopy
from exogeni.config import config as xoconfig
from exogeni.build import (resolve_domains, graph_to_elements,
                           elements_to_rdf, resolve_images, Literal)

from ...utils import domain_from_name, kind_from_name

default_vm_size = 'large'
vm_sizes = {
    'transit': 'small',
    'client': 'small',
}

_netmasks = {
    '31': '255.255.255.254',
    '24': '255.255.255.0'
}


def _ip_split(cidr):
    ip, bits = cidr.split('/')
    mask = _netmasks.get(bits)
    if mask is None:
        # Tell me about the mask, does it hurt when I pull it off?
        # - It would be extremely painful
        # You're a big guy
        # - For you
        raise ValueError(cidr)
    return ip, '255.255.255.254'


def network_graph(slice_name, topology, site):
    ns = []
    ls = deepcopy(topology.get('links', []))
    ss = []
    mgmt_vlan = {'name': 'mgmt-vlan',
                 'netmask': '255.255.255.0',
                 'nodes': []}
    g = {'nodes': ns, 'links': ls, 'stitches': ss,
         'vlans': [mgmt_vlan]}
    routers = {}
    domains = {}
    for n in topology['domains']:
        if '.' in n:
            asn = domain_from_name(n)
        else:
            asn = sub(r'^[^0-9]+', '', n)
        assert asn.isdigit()
        kind = kind_from_name(n)
        domains[n] = {'as': asn, 'kind': kind}

    def dname(dn):
        d = domains[dn]
        if '.' in dn:
            return '%s.%s' % (dn, slice_name)
        return '%s.as%s.%s' % (dn, d['as'], slice_name)

    resources = 0
    for dn, d in domains.items():
        name = dname(dn)
        n = {'name': name,
             'kind': d['kind'],
             'group': d.get('site', site),
             'size': vm_sizes.get(d['kind'], default_vm_size)}
        if 'rack' in d:
            n['group'] = d['rack']
        if n['size'] == 'large':
            resources += 2
        else:
            resources += 1
        mgmt_vlan['nodes'].append({'name': name,
                                   'ip': '169.254.1.%s' % (d['as'],)})
        routers[dn] = name
        ns.append(n)
    logging.info('Slice %s will use %s CPU core(s)', slice_name, resources)

    for link in ls:
        link['source'] = dname(link['source'])
        link['target'] = dname(link['target'])

    for k, s in topology.get('stitches', {}).items():
        site, port = s['port'].split('#', 1)
        iface = 'http://geni-orca.renci.org/owl/%s#%s' % (site, port)
        ip, netmask = _ip_split(s['local_ip'])
        ss.append({
            'name': k,
            'iface': iface,
            'node': {'name': routers[s['attach_to']],
                     'ipv4': ip,
                     'netmask_v4': netmask},
            'vlan': s['vlan'],
            'bandwidth': '1000000000',
        })

    return g


def setup_bootscripts(elements, nodes, base):
    for v in nodes:
        e = elements['local:' + v['name']]
        # edge_ips=1
        # hsflowd=1
        # vnet_node_id=
        bs = '#!/bin/sh\ncat <<VNET_CONFIG > /etc/.vnet-config\n'
        for line in base:
            bs += line + '\n'
        for line in v.get('config', []):
            bs += line + '\n'
        bs += 'VNET_CONFIG\n'
        bs += 'mv /etc/.vnet-config /etc/vnet-config\n'
        bs += 'vnet-setup-logging > /tmp/loglog 2>&1\n'
        bs += 'vnet-map-ifaces >> /tmp/map-ifaces 2>&1\n'
        bs += 'iptables -t raw -A PREROUTING -j NOTRACK\n'  # XXX
        e['request:postBootScript'] = Literal(bs)


def request_from_topology(slice_name, site, topology, sec):
    g = network_graph(slice_name, topology, site)
    resolve_domains(g['nodes'], xoconfig, 'uva')

    # Assign IPs
    preprocess(g, topology)
    elements = graph_to_elements(g, slice_name)
    resolve_images(elements, xoconfig, {})

    ws = 'ws://' + sec['hostname'] + ':8100'
    dr = sec.get('docker_registry', 'vnet.uvalight.net')
    dcv = sec.get('docker_container_version', 'sc17')

    # Add generic config
    # TODO: lock to SC17 version
    base = ['vnetmon_server=' + ws,
            'docker_registry=' + dr,
            'docker_container_version=' + dcv]
    if 'loghost' in sec:
        base.append('loghost=' + sec['loghost'])
        if 'logtag' in sec:
            base.append('logtag=' + sec['logtag'])
    setup_bootscripts(elements, g['nodes'], base)
    return elements_to_rdf({}, elements)


def fix_link_names(graph):
    for v in graph.get('links', []):
        if not v.get('name'):
            names = [v['source'], v['target']]
            names.sort()
            v['name'] = 'link.%s' % ('.'.join(names),)


def preprocess(graph, graph_base):
    fix_link_names(graph)

    """
    * Set IPs on interfaces so we can identify them later on the VM by
      MAC address
    * Support preset IPs for nodes with one external link

    Assumptions:
    * Hosts are named "$name.as$n"
    * Hosts have only one interface

    Routers:
    - Assign domain /24
    - Assign /31 to router interfaces (which IP space?)

    Non-routers:
    - Domain router IP as gateway

    """
    def _get_as(name):
        for p in name.split('.'):
            if p.startswith('as') and p[2:].isdigit():
                return p[2:]
        raise ValueError(name)

    def set_config(obj, name, peer, our_ip, peer_ip):
        ident = our_ip.replace('.', '_').split('/', 1)[0]
        cfg = obj['config']
        if peer and peer_ip:
            if '/' not in peer_ip:
                peer_ip += '/24'
            cfg.append('iface_%s=%s,%s,%s' % (ident, name, peer_ip, peer))
        else:
            cfg.append('iface_%s=%s' % (ident, name))

    netmask = '255.255.255.0'
    nodes = {}
    node_short = {}

    for n in graph.get('nodes', []):
        nodes[n['name']] = n
        node_short[n['name'].split('.', 1)[0]] = n
        asn = _get_as(n['name'])
        if 'as' in n:
            assert asn is n['as']
        else:
            n['as'] = asn
        cfgs = n.setdefault('config', [])
        cfgs.extend(['vnet_routing_asn=%s' % (asn,),
                     'vnet_role=%s' % (n['kind'],)])
        if 'ipv4' in n:
            cfgs.append('vnet_router_id=%s' % (n['ipv4'],))

    ip_pools = {}

    def get_next_ips(source, target):
        # TODO: maximum of 254 links
        latest = ip_pools.get('', 0)
        assert latest < 255
        sas = int(source['as'])
        tas = int(target['as'])
        assert sas > 0 and sas < 255
        assert tas > 0 and tas < 255
        ip_pools[''] = latest + 1
        return ('172.20.%s.%s' % (latest, sas),
                '172.20.%s.%s' % (latest, tas))

    # Domain properties:
    # as_number=$n
    # as_network=172.16.$n.0/24

    def get_next_ip(n):
        latest = ip_pools.get(n['as'], 1)
        ip_pools[n['as']] = latest + 1
        return '172.30.%s.%s' % (n['as'], latest)

    for l in graph.get('links', []):
        source = nodes[l['source']]
        target = nodes[l['target']]
        #l['source_v4'] = get_next_ip(source)
        #l['target_v4'] = get_next_ip(target)
        l['source_v4'], l['target_v4'] = get_next_ips(source, target)
        l['netmask_v4'] = netmask
        set_config(source, l['name'], target['name'], l['source_v4'], l['target_v4'])
        set_config(target, l['name'], source['name'], l['target_v4'], l['source_v4'])

    for v in graph.get('vlans', []):
        for n in v['nodes']:
            node = nodes[n['name']]
            set_config(node, v['name'], None, n['ip'], None)

    # XXX, topology request will not contain peer IPs
    for k, s in graph_base.get('stitches', {}).items():
        source = node_short[s['attach_to']]
        set_config(source, k, s['remote_node'], s['local_ip'], s['remote_ip'])
