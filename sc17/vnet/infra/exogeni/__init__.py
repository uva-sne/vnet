"""
In:
- topology requests

Out:
- topology bootup monitoring
- active topology
    - additional topology metadata

TODO:
- Support importing existing slices?
"""

import logging
from os.path import join
from time import clock_gettime, CLOCK_MONOTONIC
from asyncio import sleep, Queue, ensure_future
from datetime import timedelta

from .utils import request_from_topology

from exogeni import rpc, default_users
from exogeni.rdf import deconstruct
from exogeni.ndl import parse_manifest
from exogeni.await import slice_status_summary

from ...utils import kind_from_name

# XXX
from exogeni.config import search_config, config as xo_config
xo_path = 'exogeni'
search_config(xo_path)

"""
Contains:
- Slice creation request
- Slice await
- Slice poller
"""


def now():
    return clock_gettime(CLOCK_MONOTONIC)


DEFAULT_BACKOFF = 120.0
SLICE_FAILURE = -2
SLICE_NOT_FOUND = -1
SLICE_BOOTING = 0
SLICE_RUNNING = 1

Topology = object()
Destroy = object()


def augment_container_info(graph, topology):
    if not graph or not topology:
        return
    nodes = graph.get('nodes', {})
    for name, attr in nodes.items():
        kind = kind_from_name(name)
        attr['containers'] = {'kind': kind}


async def _query_boot_status(ctx, name, messages):
    """ Check the bootup status of the given slice """
    rdfs, err = rpc.slice_status(ctx, name)
    if err:
        logging.error('Failed to get slice status: %s', err)
        # TODO: temporary errors! for now assume perm failure
        return None, SLICE_FAILURE

    slice_is_booting = False
    slice_failed = False
    res = slice_status_summary(rdfs)
    for k, v in sorted(res.items()):
        # Output bootup status events
        if messages.get(k) != v:
            n = k.split('#')[-1]
            m = v[1].split()
            if len(m) > 2 and m[0] == 'Reservation':
                m = m[2:]
            m = ' '.join(m)
            if v[0] not in ('nascent', 'ticketed', 'activeticketed'):
                logging.info('%s %s', n, m)
            messages[k] = v

        if v[0] in ('nascent', 'ticketed', 'activeticketed'):
            slice_is_booting = True
        elif v[0] in ('failed', 'closed'):
            slice_failed = True

    if slice_failed:
        # Cancel slice
        return None, SLICE_FAILURE

    #if not slice_is_booting:
    #else:
    #    graph = None
    subjects, subject_types = deconstruct(rdfs)
    graph = parse_manifest(subjects, subject_types)
    # TODO: test when it is safe to push topology updates

    return graph, SLICE_BOOTING if slice_is_booting else SLICE_RUNNING


async def _query_exists(ctx, name):
    """ Query if slice exists """
    slices, err = rpc.list_slices(ctx)
    if err:
        # XXX
        logging.exception('Failed to list slices: %s', err)
    elif name not in slices:
        logging.info('Slice %r does not exist: %s', name, slices)
        return SLICE_NOT_FOUND
    return SLICE_RUNNING


async def create_slice(ctx, name, site, topology, base):
    req = request_from_topology(name, site, topology, base)
    logging.info('exogeni: Requesting slice %s', name)
    with open('_request-%s.xml' % (name,), 'w') as fp:
        fp.write(req)

    # TODO: should be able to abort this (low priority)
    users = default_users(xo_path)
    ret, err = rpc.create_slice(ctx, name, req, users)
    if err:
        logging.warning('exogeni: err=%r', err)
    #if ret: logging.info('exogeni: ret=%r', ret)


async def trigger_every(q, delay):
    while True:
        await sleep(delay)
        # Fill queue only if there's nothing else to do
        if not q.qsize():
            await q.put(None)


# TODO: exception monitoring
# TODO: make sure slice status is visible in UI
async def slice_monitor(ctx, x):
    topology = None
    erase_slice = True
    have_slice = False
    booting = False
    backoff = 0
    slice_start = 0
    messages = {}
    submit_graph = True

    while True:
        m = await x.queue.get()
        if m is False:
            logging.info('exogeni: Topology delete request')
            topology = None
            erase_slice = True
        elif m is not None:  # and not have_slice:
            logging.info('exogeni: New topology')
            erase_slice = erase_slice or bool(topology)
            topology = m
            backoff = 30

        try:
            if erase_slice:
                await destroy_slice(ctx, x.slice_name)
                erase_slice = False
                have_slice = False
                submit_graph = True

            # TODO: we could slightly delay this with a sleep() future
            if not have_slice and topology:
                logging.info('Building topology (backoff=%s)', backoff)
                booting = True
                await sleep(backoff)
                slice_start = now()
                # TODO: failures!
                await create_slice(ctx, x.slice_name, x.site, topology,
                                   x.config)
                have_slice = True
                messages = {}
                continue

            # TODO: minimal interval
            if booting:
                graph, status = await _query_boot_status(ctx, x.slice_name,
                                                         messages)
                augment_container_info(graph, topology)

                if (graph and submit_graph) or status == SLICE_RUNNING:
                    await x.report_topo.put((x.slice_name, graph))
                    submit_graph = False

                if status == SLICE_RUNNING:
                    # Assume everything is under control
                    d = timedelta(seconds=now() - slice_start)
                    logging.info('Slice booted successfully (%s)', d)
                    backoff = 30
                    booting = False
            else:
                status = await _query_exists(ctx, x.slice_name)

            if status == SLICE_NOT_FOUND:
                have_slice = False
            elif status == SLICE_FAILURE:
                logging.warning('Cancelling slice due to failure')
                erase_slice = True
                if backoff < DEFAULT_BACKOFF:
                    backoff = DEFAULT_BACKOFF
                else:
                    backoff *= 2

        except:  # NOQA
            # E.g. connection refused
            logging.exception('XO')
            await sleep(30.0)


class Exogeni:
    def __init__(self, slice_name, site, report_topo, config):
        self.slice_name = slice_name
        self.site = site
        self.report_topo = report_topo
        self.config = config
        self.queue = Queue()

    async def set_topology(self, topology):
        """
        Handle user request for topology change

        TODO: slice modification
        TODO: support partial progress indicator
        TODO: deal with "soft updates" that only change container parameters
        """
        await self.queue.put(topology)


async def initialize(slice_name, report_topo, config, sec):
    # Necessary for monitoring configuration
    x = Exogeni(slice_name, sec['site'], report_topo, config)

    ctx = rpc.build_context(xo_config.get('controllers', sec['controller']),
                            join(xo_path, 'geni.pem'),
                            join(xo_path, 'geni-key.pem'))

    ensure_future(trigger_every(x.queue, 10))
    ensure_future(slice_monitor(ctx, x))
    return x


async def destroy_slice(ctx, slice_name):
    logging.info('exogeni: Deleting slice %s', slice_name)
    _, err = rpc.delete_slice(ctx, slice_name)
    if err and 'ERROR: unable to find' in err:
        logging.info('exogeni: Slice did not exist: %s', err)
    elif err is not None:
        logging.info('exogeni: Failed to delete slice: %s', err)
