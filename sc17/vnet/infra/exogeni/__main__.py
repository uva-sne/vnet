from json import load
from configparser import ConfigParser
from .utils import request_from_topology
config = ConfigParser()
config.read('config/vnet-config.ini')
t = load(open('config/newstyle_topology.json'))
print(request_from_topology('test-slice', config, t))
