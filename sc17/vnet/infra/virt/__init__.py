'''

- Transfer slice config to host
- Run sync tool


vh-slice start $slice
    Start slice

    Brings an already running slice into the requested configuration


vh-slice stop $slice
    Stop and remove slice elements


vh-slice cleanup
    Clean up all resources, including resources that may have been orphaned


TODO:
- Bootscripts and such

'''
