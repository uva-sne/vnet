"""
* Send snapshots on connect
* Maintain a pool of clients to broadcast metadata to
* Relay chat messages between clients


Ordering:

- Client connects
- Sends identify

- Send topology snapshot


"""
from ssl import SSLContext

from asyncio import ensure_future, Lock, Queue, CancelledError
import logging
from websockets import serve
from websockets.exceptions import ConnectionClosed
from json import loads, JSONDecodeError
from .utils import dumps, send_frames, set_timeout

# Shut up!
from websockets.protocol import logger
logger.setLevel(logging.ERROR)

# Do not cache these entries
volatile = {'vm/fs', 'vm/ns', 'sarnet/log'}


def _to_name(url):
    return url.split('#', 1)[-1]


def xdomain(prefix):
    if prefix.startswith('ac1e'):
        return int(prefix[4:6], 16)
    return 0


def dsum(d):
    if not isinstance(d, dict):
        return d
    return ' '.join('%s=%s' % kv for kv in sorted(d.items()))


def _compress_flows(owner_macs, flows):
    mac = None
    skip = False
    send = {}
    current = {}
    time = 1.0
    for line in flows:
        if not line:
            continue
        line = line.split(' ')
        if line[0] == 'm':
            mac = line[1]
            skip = mac not in owner_macs
            if not skip:
                current = {}
                send[mac] = current
        elif line[0] == 't':
            time = float(line[1])
        elif line[0] == 'f':
            continue
        elif not skip:
            # <sender> <receiver> <dir> <kind> <pps> <bps>
            sender = xdomain(line[0])
            if not sender:
                continue
            #target = xdomain(line[1])
            dir = line[2]
            traf = line[3]
            pps = int(float(line[4]) / time)
            bps = int(float(line[5]) / time)
            key = (sender, dir, traf)
            _b, _p = current.get(key, (0, 0))
            current[key] = (_b + bps, _p + pps)
    f = {}
    if send:
        for mac, v in send.items():
            rx = []
            tx = []
            for (s, d, t), (b, p) in v.items():
                if d == 'r':
                    rx.append([s, t, b, p])
                else:
                    tx.append([s, t, b, p])
            f[mac] = [rx, tx]
    return bool(send), f


async def next_msg(ws):
    try:
        msg = loads(await ws.recv())
    except JSONDecodeError:
        logging.warn('Client sends malformed JSON')
        return None
    except ConnectionClosed:
        return None
    if not isinstance(msg, dict):
        logging.warn('Client sends non-dict: %r', msg)
        return None
    # TODO: more validation
    kind = msg.get('@') or msg.get('kind')
    if '@' in msg:
        data = msg
    else:
        data = msg.get('data')
    return (kind, data)


class UI:
    def __init__(self, controller):
        self.controller = controller  # TODO: remove dependency on this?
        self.cache_all = {}
        self.cache = {}
        self.serialize = Lock()
        self.topology = {}
        self.clients = {}
        self.expected = set()

    # UI clients
    # ----------

    async def client_add(self, ws):
        with (await self.serialize):
            self.clients[id(ws)] = ws
            if self.topology:
                await ws.put(dumps({'@': 't',
                                    't': self.topology}))
                await ws.put(dumps({'@': 'M', 'M': self.cache}))

    async def client_remove(self, ws):
        with (await self.serialize):
            del self.clients[id(ws)]

    # Controller commands
    # -------------------

    async def set_latest_topology(self, topology):
        with (await self.serialize):
            self.topology = topology
            await self.broadcast({'@': 't', 't': topology})

    async def set_expected(self, expected):
        with (await self.serialize):
            cache = {}
            send = {}
            for c in expected:
                if c in self.cache_all:
                    logging.info('M: set %s', c)
                    n = _to_name(c)
                    send[n] = cache[n] = self.cache_all[c]
            for n in self.cache.keys():
                if n not in cache:
                    logging.info('M: unset %s', n)
                    send[n] = None
            self.expected = expected
            self.cache = cache
            await self.broadcast(send)

    async def set_metadata(self, ident, kvs):
        with (await self.serialize):
            c = self.cache_all.setdefault(ident, {})
            for k, v in kvs.items():
                if k not in volatile:
                    c[k] = v
            n = _to_name(ident)
            if ident not in self.expected:
                return

            mtype = 'm'
            data = kvs

            # XXX: this takes advantage of the fact that flows come in a single
            # message, no other metadata will be lost
            if 'vm/fs' in kvs:
                send, flows = _compress_flows(self.controller.owner_macs,
                                              loads(kvs['vm/fs']))
                if send:
                    mtype = 'f'
                    data = flows
                else:
                    mtype = data = None

            if n not in self.cache:
                logging.info('Send full metadata: %s', ident)
                self.cache[n] = c
                await self.broadcast({'@': 'M', 'M': {n: c}})
                if mtype == 'm':
                    return
            if mtype:
                await self.broadcast({'@': mtype,
                                      'i': n,
                                      mtype: data})

    async def remove_ident(self, ident):
        with (await self.serialize):
            if ident in self.cache_all:
                del self.cache_all[ident]
            if ident not in self.expected:
                return
            n = _to_name(ident)
            if n in self.cache:
                del self.cache[n]
                logging.info('Remove %s', ident)
                await self.broadcast({'@': 'M', 'M': {n: None}})

    '''
    async def meta_send_activated(self, ident, kvs):


        for k, v in kvs.items():
            if k in volatile:
                continue
            if v is None or v == '':
                if k in client:
                    del client[k]
            else:
                client[k] = v

        if broadcast:
            await self.broadcast_ui({'@': 'M', 'M': {name: deepcopy(client)}})
        elif send:
            await self.broadcast_ui({'@': 'm',
                                     'i': name,
                                     'm': kvs})

    '''

    # Connections
    # -----------

    async def handle(self, ws, _):
        addr = ws.remote_address
        logging.info('ui-client: %s connected', addr)
        set_timeout(ws, 30)
        w = Queue()
        writer = ensure_future(send_frames(ws, w))
        await self.client_add(w)
        try:
            await self.session(ws, w)
        finally:
            logging.info('ui-client: %s disconnected', addr)
            await self.client_remove(w)
            ws.close()
            writer.cancel()

    async def session(self, ws, w):
        msg = await next_msg(ws)
        if not msg or msg[0] != 'identify':
            return
        logging.info('ui-client: %s', dsum(msg[1]))
        while True:
            msg = await next_msg(ws)
            if not msg:
                break
            kind, data = msg
            if kind == 'broadcast':
                with (await self.serialize):
                    await self.broadcast({'@': 'B', 'B': data})
            elif kind != 'ping':
                logging.debug('user-command %s: %s', kind, dsum(data))
                await self.controller.user_command(kind, data)

    async def broadcast(self, msg):
        msg = dumps(msg)
        for w in self.clients.values():
            await w.put(msg)


async def ui(config, c):
    # TODO: config
    ctx = SSLContext()
    ctx.load_cert_chain('vnet.pem', 'vnet-key.pem')

    c.ui = ui = UI(c)
    await serve(ui.handle, '0.0.0.0', 8101, write_limit=1500 * 4, ssl=ctx)


async def send_messages(q, ws):
    # TODO: consider write timeout
    while True:
        try:
            msg = await q.get()
            await ws.send(msg)
        except (CancelledError, TimeoutError):
            break
