#!/bin/sh
set -x

ssh surfbox sudo systemctl stop host-netctl
ssh surfbox sudo systemctl stop vnetmon

set -e

go install -v vnet.uvalight.net/host-netctl vnet.uvalight.net/vnetmon

ssh-add $HOME/.ssh/id_rsa
scp $HOME/code/bin/host-netctl surfbox:
scp $HOME/code/bin/vnetmon surfbox:

ssh surfbox sudo systemctl start vnetmon
sleep 0.5s
ssh surfbox sudo systemctl start host-netctl
