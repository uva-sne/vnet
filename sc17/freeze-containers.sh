#!/bin/sh
freeze() {
    docker tag "$1:latest" "$1:sc17"
    docker push "$1:sc17"
}
freeze skynet.lab.uvalight.net/vnet-squid-captcha
freeze skynet.lab.uvalight.net/vnet-captcha
freeze skynet.lab.uvalight.net/vnet-honeypot
freeze skynet.lab.uvalight.net/vnet-client
freeze skynet.lab.uvalight.net/vnet-service
freeze skynet.lab.uvalight.net/vnet-ddos-udp
freeze skynet.lab.uvalight.net/vnet-ddos-reflect
