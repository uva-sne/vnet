/* jshint esnext: true */

var pt2s = (pt) => pt.x + ',' + pt.y;

var direction = function(a, b) {
    if (a.x === b.x) {
        if (a.y === b.y)
            return 0;
        return 1;
    } else if (a.y === b.y)
        return 2;
    var sx = Math.sign(b.x - a.x);
    var sy = Math.sign(b.y - a.y);
    if (sx === sy)
        return 4;
    return 8;
};

// TODO: all this idx business kills performance
var astar = function(grid, start, end, prev) {
    var heuristic = function(start, end) {
        /* y = mx + c
         * find m=1 c=?
         */
        var a = Math.pow(start.x - end.x, 2);
        var b = Math.pow(start.y - end.y, 2);
        return Math.sqrt(a + b);
    };

    var cost = function(a, b) {
        var i = pt2s(b);
        var j = pt2s(a);
        var x = cameFrom[j];
        var cost = 0;

        var d = direction(a, b);

        // Direction switching cost
        if (x !== undefined) {
            if ((a.x === x.x && b.x === a.x) || (a.y === x.y && b.y === a.y)) {
                // No switching cost
            } else if (a.x !== b.x && a.y !== b.y) {
                cost += 0.1; // Cross
            } else {
                cost += 0.9; // 90 degree
            }

            //if ((a.x === x.x && b.x !== a.x) || (a.y === x.y && b.y !== a.y)) {
            //    cost += 0.7;
            //}
        } else {
            // Test if ideal starting position
        }

        cost += d <= 2 ? 1 : 2.76;
        //console.log(JSON.stringify(a), JSON.stringify(b), d, cost);

        // Avoid contented paths, but creates sub-optimal paths
        // No extra cost if there was no overlap
        // No overlap penalty for final hop
        // TODO: the type of overlap matters:
        // - Nodes should never be overlapped (TODO: remove from neigh candidate)
        // - Non-parallel overlap has only small penalty
        if (grid[j]) {
            cost += grid[j].c;
            if (grid[j].f & d) {
                cost += 20;
            } else {
                cost += 1;
            }
        }
        return cost;
    };

    var neighbors = [
        [1, 0], [-1, 0], [0,  1], [0,  -1],
        [1, -1], [1, 1], [-1, 1], [-1, -1]
    ];

    var visited = {};
    var cameFrom = {};
    var open = {};
    var gScore = {};

    open[start] = true;
    gScore[start] = 0;

    var findex = [start];
    var fscore = [heuristic(start, end)];

    var ii = 0;

    while (findex.length > 0) {
        ii += 1;
        var current = findex.shift();
        fscore.shift();
        var currenti = pt2s(current);

        //if (cameFrom[currenti] !== undefined && cameFrom[currenti].x == end.x && cameFrom[currenti].y == end.y) {
        // This does not select the optimal neighbor for the last hop?
        if (current.x == end.x && current.y == end.y) {
            var path = [current];
            for (;;) {
                var j = pt2s(current);
                if (cameFrom[j] === undefined)
                    break;
                var next = cameFrom[j];
                path[path.length - 1].d = direction(current, next);
                path.push(next); //{x: next.x, y: next.y, d: direction(current, next)});
                current = next;
            }
            if (path.length >= 2) {
                path[path.length - 1].d = direction(path[path.length - 2], path[path.length - 1]);
            } else {
                path[path.length - 1].d = 0;
            }
            return path;
        }

        delete open[currenti];
        visited[currenti] = true;

        if (Math.abs(current.y) > 100 || Math.abs(current.x) > 100)
            continue;

        neighbors.forEach((offz) => {
            var nb = {x: current.x + offz[0], y: current.y + offz[1]};
            var nbi = pt2s(nb);
            if (visited[nbi] === true)
                return;

            var gs = gScore[currenti] | 0;

            // Skip cost to final hop
            //if (nb.x !== end.x || nb.y !== end.y)
            gs += cost(current, nb);

            if (!open[nbi]) {
                // Wasn't a candidate before
            } else if (gs >= gScore[nbi]) {
                // Was a candidate before, and has better score
                return;
            }

            var fs = gs + heuristic(nb, end);
            cameFrom[nbi] = current;
            gScore[nbi] = gs;
            open[nbi] = true;

            var ins = false;
            for (var k = 0; k < fscore.length; k++) {
                if (fs < fscore[k]) {
                    ins = true;
                    fscore.splice(k, 0, fs);
                    findex.splice(k, 0, nb);
                    break;
                }
            }
            if (!ins) {
                fscore.push(fs);
                findex.push(nb);
            }
        });
    }

    console.log('no path', start, end);
    return [];
};
