var max_bandwidth = 12500000;
var CELL = 100;
var UPSCALE = 16;
if (window.innerWidth > 2000) {
    UPSCALE = 25;
}
var TILE_WIDTH = 4;
var TILE_HEIGHT = 2;
var TILE_HALF_WIDTH = TILE_WIDTH/2;
var TILE_HALF_HEIGHT = TILE_HEIGHT/2;

var traffic_types = {'01': 'ICMP', '06': 'TCP', '11': 'UDP'};

var domainFromName = function(name) {
    var parts = name.split('.');
    for (var i = 1; i < parts.length; i++) {
        if (parts[i].startsWith('as'))
            return parts[i].substr(2);
    }
};

var domainHues = {
    /* client */
    11: 300,
    12: 310,
    13: 320,
    14: 330,
    15: 340,

    /* service */
    101: 230,
    102: 200,
    107: 215,

    51: 120,
    52: 130,
    53: 140,
    54: 150,
    55: 160,
    56: 180,
    57: 135,

    42: 42,
    61: 50,
    62: 60,
    66: 70,
};

var domainHue = function(node) {
    var m = node.name.match(/as(\d+)/);
    var h;
    if (m && m[1]) {
        h = domainHues[m[1]] || 300;
    } else {
        h = 200;
    }
    return h;
};

var domainColor = function(node) {
    var h = domainHue(node);
    return 'hsl(' + h + ', 60%, 90%)';
};

var flowColors = {};
(function() {
    flowColors[0] = 'black';
    for (var k in domainHues) {
        flowColors[k] = 'hsl(' + domainHues[k] + ', 70%, 80%)';
    }
})();
var flowColor = function(s) {
    return flowColors[s] || 'silver';
};

var trafficolor = function(v, max) {
    if (!v) {
        return '';
    }
    var pct = clamp(v, 0, max) / max;
    pct = 1 - Math.pow(1 - pct, 2);
    // 0 = red
    // 200 = blue
    var z = 200;
    var h = pct * z;
    //var l = 80 + 10*pct;
    var l = 40 + 10*pct;
    return 'hsl(' + (z - h | 0) + ',80%,' + (l | 0) + '%)';
};
