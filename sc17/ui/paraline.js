/* jshint esnext: true */
// https://stackoverflow.com/questions/3824618/draw-parallel-lines-along-a-path-php-gd
var isPointToTheLeftOfLine = function(start, end, test) {
    return ((end.x - start.x) * (test.y - start.y) -
            (end.y - start.y) * (test.x - start.x)) > 0;
};

var psub = function(a, b) { return {x: a.x - b.x, y: a.y - b.y}; };
var padd = function(a, b) { return {x: a.x + b.x, y: a.y + b.y}; };
var pmul = function(a, s) { return {x: a.x * s, y: a.y * s}; };

var extendPoint = function(a, b) {
    var dx = b.x - a.x;
    var dy = b.y - a.y;
    return {x: a.x + dx, y: b.y + dy};
};

var distance = function(a, b) {
    return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
};

var offsetPoint = function(path, c, i, d) {
    // (-dy, dx)
    var nx = path[i].x - path[i - 1].x;
    var ny = -(path[i].y - path[i - 1].y);
    var mag = Math.sqrt(nx * nx + ny * ny);
    nx /= mag;
    ny /= mag;

        /*
    var dx = Math.pow(path[0].x - path[1].x, 2);
    var dy = Math.pow(path[0].y - path[1].y, 2);
    var snMag = Math.sqrt(dy * dy + dx * dx);
    var nx = c * (-dy / snMag) + c * (dx / snMag);
    var ny = c * (dx / snMag) + c * (dy / snMag);
    */
    return {x: path[d ? 0 : i].x + c * ny,
            y: path[d ? 0 : i].y + c * nx};
};

var paraline = function(path, co) {
    let lpoint = offsetPoint(path, -0.09, 1, true);
    let rpoint = offsetPoint(path, 0.09, 1, true);
    let left = [lpoint];
    let right = [rpoint];
    for (var i = 1; i < path.length; i++) {
        let lpoint = offsetPoint(path, -0.09, i);
        let rpoint = offsetPoint(path, 0.09, i);
        left.push(lpoint);
        right.push(rpoint);
        continue;
        //let start = path[i - 1];
        //let end = path[i];
        //let length = distance(start, end);
        //let az = Math.atan2(end.x - start.x, end.y - start.y);
        //let cosa = length * Math.sin(az);
        //let cosb = length * Math.cos(az);
        //lpoint = {x: lpoint.x + cosa, y: lpoint.y + cosb};
        //rpoint = {x: rpoint.x + cosa, y: rpoint.y + cosb};
        //left.push(lpoint);
        //right.push(rpoint);
    }
    return {left: left, right: right};
};

/*
https://softwareengineering.stackexchange.com/questions/280694/algorithm-to-generate-parallel-path-inside-polygon
 */

var _paraline = function(path, co) {
    var centerOffset = co || 0.05;
    var left = [], right = [];
    let i = 0;
    for (let point of path) {
        var prevPoint = (i === 0) ? point : path[i - 1];
        var nextPoint = (i === (path.length - 1)) ? point : path[i + 1];

        var toPrevPoint = psub(point, prevPoint);
        var toNextPoint = psub(point, nextPoint);
        var angleToPrevPoint = Math.atan2(toPrevPoint.y, toPrevPoint.x);
        var angleToNextPoint = Math.atan2(toNextPoint.y, toNextPoint.x);
        var offsetAngle = angleToNextPoint + ((angleToPrevPoint - angleToNextPoint) / 2);

        if (i > 0 && i < (path.length - 1)) {
        //    offsetAngle = angleToNextPoint + ((angleToPrevPoint - angleToNextPoint) / 2);
        } else if (i === 0) {
            offsetAngle = angleToNextPoint + Math.PI * 2;
        } else {
            offsetAngle = angleToPrevPoint + Math.PI * 2;
            offsetAngle = Math.PI/2;//angleToPrevPoint + Math.PI * 2;
        }

        var offset = {x: Math.cos(offsetAngle) * centerOffset,
                          y: Math.sin(offsetAngle) * centerOffset};
        var offsetLeft = padd(point, offset);
        var offsetRight = padd(point, pmul(offset, -1.0));

        if (isPointToTheLeftOfLine(prevPoint, point, offsetLeft)) {
            left.push(offsetLeft);
            right.push(offsetRight);
        } else {
            left.push(offsetRight);
            right.push(offsetLeft);
        }

        i++;
    }
    return {left: left, right: right};
};
