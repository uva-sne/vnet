/**
 * Reconnecting WebSocket client for JSON streams
 */
window.vnet ? true : window.vnet = {};
vnet.JSONStream = function(url, callbacks, interval, smart_backoff) {
    var had_initial_connection = false;

    var call = function(k, a, msg) {
        if (callbacks[k]) {
            var args = msg !== undefined ? [msg, callbacks] : [callbacks];
            args.push.apply(args, a);
            return callbacks[k].apply(callbacks, args);
        }
    };

    var onopen = function() {
        console.log('ws: Established:', url);
        had_initial_connection = true;
        call('onopen', arguments);
    };

    var onclose = function() {
        console.warn('ws: Disconnected:', url);
        if (!had_initial_connection && smart_backoff) {
            setTimeout(connect, 30000);
        } else {
            setTimeout(connect, interval);
        }
        try {
            call('onclose', arguments);
        } finally {
            callbacks.ws = null;
        }
    };

    var onerror = function() {
        console.warn('ws: Error:', url, arguments);
        call('onerror', arguments);
    };

    var onmessage = function(e) {
        var msg = JSON.parse(e.data);
        call('onmessage', arguments, msg);
    };

    var connect = function() {
        var ws;
        try {
            ws = callbacks.ws = new WebSocket(url);
        } catch (e) {
            console.log('WebSocket error: ' + e);
            return;
        }

        ws.onopen = onopen;
        ws.onclose = onclose;
        ws.onerror = onerror;
        ws.onmessage = onmessage;
    };

    callbacks.send = function(msg) {
        try {
            if (callbacks.ws) {
                msg = JSON.stringify(msg);
                console.log('ws: Send:', msg);
                callbacks.ws.send(msg);
            }
        } catch (e) {
            console.error('Failed to send:', e);
        }
    };

    callbacks.close = function() {
        try {
            if (callbacks.ws) {
                console.log('ws: Close');
                callbacks.ws.close();
            }
        } catch (e) {
            console.error('Close error: ' + e);
        }
    };

    connect();
    return callbacks;
};

/* Backend communication
 *
 * TODO: implement an action confirmation/progress protocol
 */
vnet.WSUI = function(server, onmsg) {
    var send = function(kind, data) {
        if (typeof kind !== 'string' && data === undefined) {
            con.send(kind);
        } else {
            con.send({kind: kind, data: data});
        }
    };
    var close = function() {
        con.close();
    };

    var ctx = {
        connected: false,
        layer: undefined,
        send: send,
        close: close,
        onmessage: null,

        // XXX
        selectLayer: function(name) {
            ctx.layer = name;
            send('layer:select', {layer: name});
        }
    };

    var recon = false;
    var con = vnet.JSONStream(server, {
        connected: false,
        close: close,
        onopen: function() {
            ctx.connected = true;
            var data = {version: 'sc17/0.1',
                        agent: navigator.userAgent,
                        screen: String(window.screen.availWidth) + 'x' + String(window.screen.availHeight) + '@' + String(window.devicePixelRatio),
                        url: location.href};
            if (recon) data.reconnect = recon;
            send('identify', data);

            // TODO: event based system so this isn't necessary here
            if (ctx.layer) {
                send('layer:select', {layer: ctx.layer});
            }
            recon = true;
        },
        onmessage: function(msg) {
            //console.log('ws: <<', msg.kind);
            //onmsg(msg.kind, msg.data, msg.ident);
            onmsg(msg);
        },
        onclose: function() {
            console.log('ws: Disconnected');
            ctx.connected = false;
        }
    }, 2000);

    setInterval(function() {
        send('ping');
    }, 30000);

    return ctx;
};
