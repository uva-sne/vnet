(function() {
    var pfx = 'img/logo/';
    var pix = ['uva.jpg', 'ciena.png', 'klm.png', 'tno.png', 'uva.jpg'];
    pix = pix.map(function(v) {
        var img = document.createElement('img');
        img.src = pfx + v;
        img.height = 32;
        return img;
    });
    //touch(document.querySelector('#sarnet-bar .left'), function(e) {
    //    e.stopPropagation();
    //    document.querySelector('html').classList.toggle('white');
    //});
    var bar = document.querySelector('#sarnet-bar .right');
    touch(bar, function(e) {
        e.stopPropagation();
        coms.send({'@': 'pub', 'k': 'cmd/sarnet/learn', 'v': 'true'});
    });
    var i = 0;
    runInterval(function() {
        // replaceChild?
        bar.innerHTML = '';
        bar.appendChild(pix[i]);
        i = (i + 1) % pix.length;
    }, 3000);
})();
