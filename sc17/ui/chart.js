/* jshint esnext: true */
/**
 */
ChartLines = function(elem, config, lines, N, w, h) {
    elem.setAttribute('class', 'chart chart-lines');

    (function() {
        if (document.getElementById('glow'))
            return;
        let filter = document.createElementNS(SVG, 'filter');
        let gb = document.createElementNS(SVG, 'feGaussianBlur');
        let m0 = document.createElementNS(SVG, 'feMerge');
        let m1 = document.createElementNS(SVG, 'feMergeNode');
        let m2 = document.createElementNS(SVG, 'feMergeNode');
        filter.setAttribute('id', 'glow');
        gb.setAttribute('stdDeviation', '3.5');
        gb.setAttribute('result', 'coloredBlur');
        m1.setAttribute('in', 'coloredBlur');
        m2.setAttribute('in', 'SourceGraphic');
        m0.appendChild(m1);
        m0.appendChild(m2);
        filter.appendChild(gb);
        filter.appendChild(m0);
        elem.appendChild(filter);
    })();

    var tline = null;
    if (config.line !== undefined) {
        tline = document.createElementNS(SVG, 'line');
        tline.style.stroke = '#aaa';
        elem.appendChild(tline);
    }

    lines.forEach(function(line, i) {
        line.elem = document.createElementNS(SVG, 'path');
       // line.elem.setAttribute('filter', 'url(#glow)');
        line.elem.setAttribute('stroke', line.color);
        line.elem.setAttribute('stroke-width', '3');
        line.elem.setAttribute('fill', 'transparent');
        line.elemLabel = document.createElementNS(SVG, 'text');
        line.elemLabel.setAttribute('font-size', '11px');
        line.elemLabel.setAttribute('stroke-width', '0.2');
        //line.elemLabel.setAttribute('fill', line.color);
        //line.elemLabel.setAttribute('stroke', line.color);
        line.elemLabel.setAttribute('x', '0');
        line.elemLabel.setAttribute('y', String(i * 20 + 20));
        elem.appendChild(line.elem);
        elem.appendChild(line.elemLabel);
    });

    if (config.vmin === undefined) config.vmin = 0;
    if (config.vmax === undefined) config.vmax = 100;
    var yfunc = config.y ? config.y : d3.scaleLinear;
    var y = yfunc().range([60, h - 5]).domain([config.vmax, config.vmin]);
    var x = d3.scaleLinear().domain([0, N - 1]).range([-1, w + 0.75]);

    var lgen = d3.line()
        .curve(d3.curveMonotoneX)
        .x(function(_, i) { return x(i); })
        .y(function(v) { return y(v); });

    config.draw = function() {
        var vmax;
        lines.forEach(function(line) {
            var m = Math.max.apply(null, line.values);
            vmax = Math.max(m, vmax === undefined ? m : vmax);
        });
        config.vmax = Math.max(vmax, config.vmax * 0.5);
        y.domain([config.vmax, config.vmin]);
        if (tline) {
            tline.setAttribute('x1', x(0));
            tline.setAttribute('x2', x(w));
            tline.setAttribute('y1', y(config.line));
            tline.setAttribute('y2', y(config.line));
        }
        lines.forEach(function(line) {
            var func = line.vshow ? line.vshow : function(v) {
                return String((v + 0.5) | 0);
            };
            line.elem.setAttribute('d', lgen(line.values));
            if (line.label) {
                var text = line.label + ': ' + func(line.values[line.values.length - 1]);
                line.elemLabel.textContent = text;
            }
        });
    };
    return config;
};
