var url_to_ident = function(ident) {
    if (ident.startsWith('http://geni-orca.renci.org/owl/')) {
        return ident.substr(31);
    }
    return ident;
};

var ident_to_name = function(ident) {
    var parts = ident.split('#');
    return parts[parts.length - 1];
};

/* Store metadata per node
 */
var $aux = {
    del: function(name) {
        delete this[name];
    },
    get: function(name, reset) {
        var n = this[name];
        if (n === undefined) {
            n = this[name] = {};
            reset = true;
        }
        if (reset) {
            n.iflink = {};
            n.ifstat = {};
            n.ifmac = {};
            n.metadata = {};
            n.observables = {};
        }
        return n;
    }
};
