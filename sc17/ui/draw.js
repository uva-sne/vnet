/* jshint esnext: true */

var _maxbw = 10000000;

/*
 */
var computeInitialPlacementCola = function(topology, cb) {
    var nodes = [];
    var links = [];

    iterItems(topology.nodes, function(n, k) {
        var i = nodes.length;
        //n.y = i;
        //n.x = +domainFromName(n.name);
        n.id = i;
        n.width = n.height = 2;
        nodes.push(n);
    });

    iterItems(topology.links, function(n) {
        n.source = topology.nodes[n.source];
        n.target = topology.nodes[n.target];
        links.push(n);
    });
    links.sort(function(a, b) {
        var ac = a.source.name.startsWith('client') || a.target.name.startsWith('client');
        var bc = b.source.name.startsWith('client') || b.target.name.startsWith('client');
        if (ac && bc) return 0;
        if (ac) return -1;
        if (bc) return 1;
        return 0;
    });

    var _c = new cola.Layout().
        nodes(nodes).
        links(links).
        linkDistance(50). /*function(link) {
            if (link.source.name.startsWith('client') || link.target.name.startsWith('client'))
                return 5;
            return 4.5;
        })*/
        avoidOverlaps(true).
        size([400, 400]);
        //jaccardLinkLengths(3.5);
        //symmetricDiffLinkLengths(1.4);

    // Placement finished
    _c.on('end', function() {
        nodes.forEach(function(n) {
            n.x /= 12;
            n.y /= 12;
        });
        cb(nodes, links);
    });

    _c.start(10,20,20);
};

var computeInitialPlacementD3 = function(topology, cb) {
    var nodes = [];
    var links = [];

    iterItems(topology.nodes, function(n, k) {
        var i = nodes.length;
        if (n.name.startsWith('client')) {
            n.x = +domainFromName(n.name);
            n.y = 0;
        }
        //n.x = +domainFromName(n.name);
        //n.y = (n.x / 10) | 0;
        n.id = i;
        n.width = n.height = 2;
        nodes.push(n);
    });

    iterItems(topology.links, function(n) {
        n.source = topology.nodes[n.source];
        n.target = topology.nodes[n.target];
        links.push(n);
    });

    var sim = d3.forceSimulation().
        force('collide', d3.forceCollide(3.4)).
        force('charge', d3.forceManyBody()).
        force('link', d3.forceLink().
            id(function(d) { return d.name; }).
            distance(function(d) {
                //if (d.source.kind === 'client' || d.target.kind === 'client')
                //    return 2.5;
                return 1;
            })
            .strength(3)).
        force('center', d3.forceCenter()).
        force('y', d3.forceY().strength(0.9)).
        force('x', d3.forceX().strength(0.5)).
        stop();

        /*size([22, 22]).
        linkDistance(function(link) {
            if (link.source.name.startsWith('client') || link.target.name.startsWith('client'))
                return 5;
            return 3;
        });*/
    //sim.on('tick', function() { cb(nodes, links); });

    // Placement finished
    var end = function() {
        cb(nodes, links, topology);
    };
    //sim.on('end', end);

    sim.nodes(nodes);
    sim.force('link').links(links);
    var n = Math.ceil(Math.log(sim.alphaMin()) / Math.log(1 - sim.alphaDecay()));
    for (var i = 0; i < n; ++i) {
        sim.tick();
    }
    end();
};

var computeInitialPlacement = computeInitialPlacementD3;

// Find path:
// - Never overlap corners
// - Minimize/no parallel overlap
// - Non-parallel overlap is OK
// - Minimize corners
// - Minimize segment length
var placePaths = function(nodes, links) {
    var BUILDING = 16;
    var grid = {}, paths = {};
    nodes.forEach((n) => {
        grid[pt2s(n)] = {c: 200, f: BUILDING};
    });

    var astarPass = function(pass) {
        return links.map(function(link, id) {
            //console.log('link order', link.name);
            var prev = paths[id];
            var path = astar(grid,
                {x: link.source.x, y: link.source.y},
                {x: link.target.x, y: link.target.y},
                prev);

            // TODO: mark path as active, but don't increase cost every
            // pass
            prev = paths[id] = {};
            path.forEach(function(v) {
                var vi = pt2s(v);
                var info = grid[vi] || {c: 0, f: 0};
                info.f |= v.d;
                grid[vi] = info;
                if (pass === 1) prev[vi] = true;
            });
            //link.path = pass === 2 ? compressPath(path) : path;
            //link.path = compressPath(path);
            link.path = path;
            return link;
        });
    };

    if (1) return astarPass(1);
    return links.map(function(link) {
        link.path = [
            {x: link.source.x, y: link.source.y},
            {x: link.target.x, y: link.target.y}
        ];
        return link;
    });
    //return astarPass(2);
};

var compressPath = function(path) {
    var at = {x: path[0].x, y: path[0].y};
    var cpath = [at];
    var xdir = 0, ydir = 0;

    for (var i = 1; i < path.length; i++) {
        var next = path[i];
        if (at.x + xdir == next.x && at.y + ydir == next.y) {
            at.x = next.x;
            at.y = next.y;
        } else {
            xdir = next.x - at.x;
            ydir = next.y - at.y;
            at = {x: next.x, y: next.y};
            cpath.push(at);
        }
    }
    return cpath;
};

/* After initial placement
 */
var drawLayers = function(nodes, links, topology) {
    var mx, my;
    var createFromNodes = function(ns) {
        // TODO: larger groups
        ns.forEach(function(n) {
            n.x = (n.x + 0.5) | 0;
            n.y = (n.y + 0.5) | 0;
            var s = iso2screen(n);
            if (mx === undefined) {
                mx = s.x;
                my = s.y;
            } else {
                if (s.x < mx) mx = s.x;
                if (s.y < my) my = s.y;
            }
        });
        return ns;
    };

    $render.setTopology(createFromNodes(nodes), placePaths(nodes, links));
    $input.forceCam(-mx + 10*UPSCALE*TILE_HALF_WIDTH, -my + 15*UPSCALE*TILE_HALF_HEIGHT);
    $attack.setTopology(topology); /////
};


var flag = function(e) {
    var fb = document.getElementById('flag-box');
    var br = e.target.getBoundingClientRect();
    fb.style.top = (((br.top + window.scrollY)|0) - 20) + 'px';
    fb.style.left = (((br.left + window.scrollX)|0) + 5) + 'px';
    var f = e.target.getAttributeNS(null, 'data-flag');
    if (f) {
        fb.textContent = f;
    }
};

var flowSort = function(left, right) {
    if (left.s < right.s) return -1;
    if (left.s > right.s) return 1;
    if (left.t < right.t) return -1;
    if (left.t > right.t) return 1;
    if (left.bps < right.bps) return -1;
    if (left.bps > right.bps) return 1;
    return 0;
};

var xdomain = function(pfx) {
    if (pfx.startsWith('ac1e'))
        return parseInt(pfx.substr(4), 16);
    return 0;
};

var insertFlow = function(list, e) {
    for (var i = 0; i < list.length; i++) {
        if (list[i].traf === e.traf && list[i].s === e.s) {
            list[i].bps += e.bps;
            return;
        }
    }
    list.push(e);
};

var buildFlowMap = function(ident, flows) {
    if (!flows || !flows.forEach)
        return [];

    var addSingleFlow = function(info, line) {
        var s = xdomain(line[0]);
        if (s === 0)
            return;
        var t = xdomain(line[1]);
        var d = line[2];
        var traf = parseInt(line[3], 16);
        var pps = parseInt(line[4]);
        var bps = parseInt(line[5]);
        var e = {bps: bps, traf: traf, s: s/*, t: t*/};
        if (d == 't') {
            insertFlow(info.tx, e);
        } else {
            insertFlow(info.rx, e);
        }
        // TODO: merge insert
        info.rx.sort(flowSort);
        info.tx.sort(flowSort);
    };

    var ifmac, iface, info, owner;
    var skip = false;
    var draw = [];

    flows.forEach(function(line) {
        line = line.split(/ /);
        var time = 1;
        switch (line[0]) {
            case 'm':
                time = 1;
                ifmac = line[1];
                iface = flowMacs[ifmac];
                if (!iface) {
                    skip = true;
                    return;
                }

                skip = false;
                owner = $topology.flow_owner[iface.link];
                if (owner === undefined) {
                    $topology.flow_owner[iface.link] = ident;
                } else if (owner !== ident) {
                    info = undefined;
                    skip = true;
                }
                if (!skip) {
                    info = {owner: ident,
                        left: iface.left, right: iface.right,
                        rx: [], tx: []};
                    draw.push(info);
                }
                break;
            case 't':
                console.log(line);
                break;
            case 'f':
                break;
            default:
                if (!skip) addSingleFlow(info, line);
        }
    });
    return draw;
};
