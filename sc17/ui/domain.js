/* jshint esnext: true */

var attackNames = {
    'sarnet/a/congestion': 'DDoS',
    'sarnet/a/password_bruteforce': 'password bruteforce',
};

/* Hungry hungry hippo, store all values with a window
 */
var activeDomain = 'does-not-exist';
var elements = {};
var observableList = document.querySelector('#observables .observable-list');
var domainTitle = document.getElementById('active-domain');
var logEntries = document.getElementById('log-entries');

var N = 30;
var _add = function(stor, k, val) {
    var ary = stor[k];
    if (ary === undefined)
        ary = stor[k] = [];
    ary.push(val);
    if (ary.length > N)
        ary.shift();
};

var getRedrawList = function(k) {
    return [];
};

var setNodeMetadata = function(name, kv, reset) {
    var eq = function(a, b) {
        var k;
        for (k in a) {
            if (k !== 'time' && a[k] != b[k])
                return false;
        }
        for (k in b) {
            if (k !== 'time' && a[k] != b[k])
                return false;
        }
        return true;
    };
    var a = $aux.get(name, reset);
    var pushLog = function(entry) {
        var log = a.metadata._log;
        if (log === undefined)
            a.metadata._log = log = [];
        if (entry.ka === true)
            return;
        entry.time = new Date();
        log.unshift(entry);
        if (log.length > 10) log.pop();
        if (name === activeDomain) {
            updateLogEntries(name, log);
        }
    };
    if (a.metrics === undefined) a.metrics = {};
    if (a.thresh === undefined) a.thresh = {};
    var update = {};
    for (var k in kv) {
        var v, n, parts = k.split('/');
        if (k === 'vm/ifmap') {
            a.ifmap = JSON.parse(kv[k]);
        } else if (k.startsWith('sarnet/o/')) {
            n = parts[parts.length - 1];
            a.observables[n] = JSON.parse(kv[k]);
            pushLog({observable: n, state: a.observables[n].state});
            if (name === activeDomain)
                drawObservable(a, n, a.observables[n]);
        } else if (k.startsWith('sarnet/a/')) {
            pushLog({attack: k, state: kv[k]});
        } else if (k.startsWith('sarnet/thresh/')) {
            n = parts[parts.length - 1];
            a.thresh[n] = +kv[k];
        } else if (k === 'sarnet/log') {
            pushLog(JSON.parse(kv[k]));
        } else if (k === 'vm/ns') {
            var ifaces = JSON.parse(kv[k]);
            // Need to reset unspecified interfaces
            iterItems(a.ifmap, function(_, k) {
                if (ifaces[k] === undefined)
                    ifaces[k] = {r: {b: 0}};
            });
            iterItems(ifaces, function(val, ifname) {
                _add(a.metrics, 'rate_rx@' + ifname, val.r.b);
                update['traffic_rate_rx@' + ifname] = true;
            });
        } else if (parts[0] === 'c') {
            v = kv[k];
            // TODO: use p[-1]@p[1]
            switch (parts[parts.length - 1]) {
                case 'logfails':
                    var s = 0, o = JSON.parse(v);
                    for (var ip in o) s += o[ip];
                    _add(a.metrics, k, s);
                    update['logfail_rate@' + parts[1]] = true;
                    break;
                case 'sales':
                    _add(a.metrics, k, +v);
                    update['sales_rate@' + parts[1]] = true;
                    break;
                case 'logins':
                    _add(a.metrics, k, +v);
                    update['logfail_rate@' + parts[1]] = true;
                    break;
            }
        }
    }
    if (name === activeDomain) {
        iterItems(update, function(_, n) {
            var e = elements[n];
            if (e !== undefined) {
                // XXX: threshold is per metric
                if (n.startsWith('sales_rate@'))
                    e.chart.line = a.thresh[n.replace('sales_rate@', 'sales@')];
                //if (a.thresh[n] !== undefined)
                e.chart.draw();
            }
        });
    }
};

var updateLogEntries = function(name, entries) {
    var ht = function(v) {
        return lpad(v.getHours(), 2) + ':' + lpad(v.getMinutes(), 2) + ':' + lpad(v.getSeconds(), 2);
    };
    var hl = function(v) {
        return '<span class="hl">' + v + '</span>';
    };
    var hn = function(v) {
        var c = v.indexOf('.');
        return v.substr(0, c);// + v.substr(c);
    };
    var log = entries || [];
    var t = '';
    for (const v of log) {
        let klass;
        let line, peer;
        if (v.kind === 'match_result') {
            peer = hn(v.peer) + ' →  ' + hn(name);
        } else {
            peer = hn(name);
            if (v.peer)
                peer += '→  ' + hn(v.peer);
        }
        if (v.kind === 'nfv-alive') {
            klass = 'log-see';
            line = 'NFV ' + v.nfv + ' is alive and receiving malicious traffic';
        //} else if (v.kind === '') {
        } else if (v.deploy === 'nfv') {
            // dst []src
            klass = 'log-deploy';
            line = 'Deploy NFV: ' + hl(v.nfv);
        } else if (v.deploy === 'filter') {
            // proto []src? dst
            klass = 'log-deploy';
            line = 'Deploy ingress filter: ';
            if (v.src)
                line += ' originating ' + hl(v.src);
            if (v.dst)
                line += ' destined ' + hl(v.dst);
            //+ ' on interface [TODO]';
        } else if (v.deploy === 'rate') {
            klass = 'log-deploy';
            line = 'Deploy rateup: ' + hl(v.rate ? v.rate : '100Mbit');// + ' on interface [TODO]';
        } else if (v.kind === 'redirect') {
            klass = 'log-deploy';
            line = 'Redirect traffic';
            if (v.src !== undefined && v.src.length > 0) {
                line += ' originating from ' + hl(v.src.join(', '));
            }
            line += ' destined to ' + hl(v.dst) + ' via ' + hl(hn(v.redir_to));
        } else if (v.kind === 'ask') {
            klass = 'log-ask';
            line = 'See traffic from/towards ' + hl(v.pattern.domain) + ' with type ' + hl(v.pattern.proto.toUpperCase()) + ' and rate at least ' + hl(v.pattern.rate) + '? ' + ' ' + v.path.map(hn).join(' >> ');
        } else if (v.kind === 'match_result' || v.kind === 'match') {
            klass = 'log-see';
            line = 'I see traffic';
            if (v.src.length) line += ' source=' + v.src.map(hn).map(hl).join(', ');
            if (v.dst.length) line += ' destination=' + v.dst.map(hn).map(hl).join(', ');
            if (v.other.length) line += ' other=' + v.other.map(hn).map(hl).join(', ');
        } else if (v.observable !== undefined) {
            klass = 'log-observable';
            line = 'Observable ' + hl(v.observable) + ' became ' +
                (v.state ? 'healthy' : hl('unhealthy'));
        } else if (v.attack !== undefined) {
            klass = 'log-attack';
            if (v.state === '') {
                line = 'Attack ' + hl(attackNames[v.attack]) + ' resolved';
            } else if (v.state === 'unhealthy') {
                line = 'Attack ' + hl(attackNames[v.attack]) + ' detected and affecting service!';
            } else if (v.state === 'healthy') {
                line = 'Attack ' + hl(attackNames[v.attack]) + ' successfully defended, but still on-going';
            } else {
                line = JSON.stringify(v);
            }
        } else {
            line = JSON.stringify(v);
        }
        t += '<p class="' + klass + '">' + ht(v.time) + ' | ' + peer + ': <span class="line">' + line + '</span>\n';
    }
    logEntries.innerHTML = t;
};

var drawObservable = function(a, name, state) {
    var v = elements[name];
    if (elements[name] === undefined) {
        v = elements[name] = {};
        v.elem = document.createElement('div');
        v.elem.className = 'observable';
        v.elem.dataset.name = name;
        v.caption = document.createElement('div');
        v.elem.appendChild(v.caption);
        insertSorted(observableList, v.elem, function(e) {
            return e.dataset.name;
        });

        v.svg = $svgSized(220, 200);
        var lines = [];
        genLines(name).forEach(function(n) {
            var v = a.metrics[n];
            if (v === undefined)
                v = a.metrics[n] = [];
            var l = {label: n.split('/').pop(), color: 'black', values: v};
            if (n.startsWith('rate_rx@'))
                l.vshow = (v) => speed(v*8);
            lines.push(l);
        });
        v.chart = ChartLines(v.svg, {vmin: 0, vmax: 1, line: -1000}, lines,
            N, 220, 200);
        v.elem.appendChild(v.svg);
    }
    v.caption.textContent = name;
    if (!state.state) {
        v.elem.classList.add('unhealthy');
    } else {
        v.elem.classList.remove('unhealthy');
    }
};

var genLines = function(n) {
    var parts = n.split('@');
    switch (parts[0]) {
        case 'traffic_rate_rx':
            return ['rate_rx@' + parts[1]];
        case 'sales_rate':
            return ['c/' + parts[1] + '/sales'];
        case 'logfail_rate':
            return ['c/' + parts[1] + '/logins',
                    'c/' + parts[1] + '/logfails'];
    }
    return [];
};

var redrawActiveDomain = function(name) {
    activeDomain = name;

    elements = {};
    observableList.innerHTML = '';
    var c = activeDomain.indexOf('.');
    domainTitle.textContent = activeDomain.substr(0, c);
    var e = document.createElement('span');
    e.className = 'subtitle';
    e.textContent = activeDomain.substr(c);
    domainTitle.appendChild(e);

    updateActiveDomain();
};

var updateActiveDomain = function() {
    var a = $aux.get(activeDomain);

    // Draw cached obervables
    iterItems(a.observables, function(v, k) {
        drawObservable(a, k, v);
    });

    // Draw cached log
    updateLogEntries(activeDomain, a.metadata._log);
};

redrawActiveDomain('service101.as101.sarnet-sc17-ciena');

var trackBroadcast = function(msg) {
    if (msg.kind == 'ad') {
        redrawActiveDomain(msg.data);
    } else if (msg.kind == 'reload-domain') {
        location.reload();
    }
};

var coms = (function() {
    var server = 'wss://' + window.location.hostname + ':8101/';
    var recon = false;
    var con = vnet.JSONStream(server, {
        onopen: function() {
            console.log('[com] Connection opened');
            var data = {version: 'sc17-domain/0.1',
                        agent: navigator.userAgent,
                        url: location.href};
            if (recon) data.reconnect = recon;
            send('identify', data);
            send('broadcast', {kind: 'ad?'}); // req. active-domain from UI
            recon = true;
        },
        onmessage: function(msg) {
            var mtype = msg['@'];
            var k;
            if (mtype == 't') {
            } else if (mtype == 'M') {
                for (k in msg.M) {
                    setNodeMetadata(k, msg.M[k], true);
                }
            } else if (mtype == 'm') {
                setNodeMetadata(msg.i, msg.m);
            } else if (mtype == 'n') {
            } else if (mtype == 'f') {
            } else if (mtype == 'B') {
                trackBroadcast(msg.B);
            } else {
                console.log(msg);
            }
        }
    }, 2000);

    var send = function(kind, data) {
        con.send({kind: kind, data: data});
    };
    var ctx = {send: send};
    setInterval(function() {
        send('ping');
    }, 30000);
    return ctx;
})();

function bcast(kind, data) {
    coms.send('broadcast', {kind: kind, data: data});
}

(function() {
    var elem = document.querySelector('#control-loop');
    var width = 600, height = 150;
    var r = height / 2;
    var arc = d3.arc().innerRadius(r - 20).outerRadius(r);
    var larc = d3.arc().innerRadius(r).outerRadius(r + 10);
    var svg = $svgSized(width, height + 30);
    elem.appendChild(svg);

    var g = document.createElementNS(SVG, 'g');
    g.setAttribute('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');
    g.setAttribute('class', 'control-loop');
    svg.appendChild(g);

    var data = [
        {name: 'Detect',  active: 1, dx: 10},
        {name: 'Analyze', active: 0, dx: 2},
        {name: 'Decide',  active: 0, dx: -23, dy: 8},
        {name: 'Respond', active: 0, dx: -72},
        {name: 'Learn',   active: 0, dx: -57},
    ];

    var arcs = d3.pie().value(1).padAngle(0.03);
    arcs(data).forEach(function(v) {
        v.data.startAngle = v.startAngle;
        v.data.endAngle = v.endAngle;
        v.data.padAngle = v.padAngle;
    });

    for (let v of data) {
        let grp = document.createElementNS(SVG, 'g');
        v._elem_g = grp;
        v._elem_arc = document.createElementNS(SVG, 'path');
        v._elem_text = document.createElementNS(SVG, 'text');
        v._elem_g.setAttribute('class', 'arc ' + v.name.toLowerCase());
        v._elem_arc.setAttribute('d', arc(v));
        var xy = larc.centroid(v);
        xy[0] += v.dx !== undefined ? v.dx : 0;
        xy[1] += v.dy !== undefined ? v.dy : 0;
        v._elem_text.setAttribute('transform', 'translate(' + xy + ')');
        v._elem_text.setAttribute('dy', '.35em');
        v._elem_text.textContent = v.name;
        grp.appendChild(v._elem_arc);
        grp.appendChild(v._elem_text);
        g.appendChild(grp);
    }

    var iter = -1;
    setInterval(function() {
        iter++;
        if (iter >= data.length) {
            if (iter < (data.length + 3))
                return;
            for (let v of data) {
                v._elem_g.classList.remove('active');
            }
            iter = -1;
        } else {
            data[iter]._elem_g.classList.add('active');
        }
    }, 200);
})();
