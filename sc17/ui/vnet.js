/* jshint esnext: true */

var $topology;
var flowMacs = {}; // map => node/neighbor


/* Communication channel
 */
var coms = (function() {
    /* Completely replace full topology
     * TODO: some old metadata may remain in memory
     * TODO: this requires some consistency checking
     */
    var setTopology = function(topology) {
        $topology = topology;
        $topology.flow_owner = {};
        $topology.stat_owner = {};
        computeInitialPlacement(topology, drawLayers);
        flowMacs = {};

        for (let k in topology.nodes) {
            let n = topology.nodes[k];
            if (!n.unit_url)
                continue;

            let a = $aux.get(n.name);
            a.iflink = {};
            a.ifmac = {};
            // ifstat?

            for (let iface of n.ifaces) {
                if (!iface.links || !iface.links.length)
                    continue;

                let linkName = iface.links[0];
                let link = topology.links[linkName];
                // E.g. mgmt interface
                if (!link)
                    continue;

                if (iface.ifname)
                    a.iflink[linkName] = iface.ifname;

                if (iface.mac) {
                    let mac = iface.mac.toLowerCase();
                    let nb = link.source.name === n.name ? link.target : link.source;

                    // For traffic stats
                    a.ifmac[mac] = linkName;

                    // For flow lookups
                    flowMacs[mac.replace(/:/g, '')] = {left: link.source,
                        right: link.target,
                        owner: n.name,
                        link: linkName};
                }
            }
        }
    };

    var isOwner = function(link) {
        var owner = $topology.stat_owner[link];
        if (owner === undefined) {
            $topology.stat_owner[link] = name;
            return true;
        }
        return owner === name;
    };

    /* TODO: compress
    */
    var setNodeNetstats = function(name, ifaces) {
        if (!$topology.nodes[name])
            return;
        var n = $aux.get(name);
        var stats = [];
        var updated = {};
        for (var k in ifaces) {
            let rx = ifaces[k].r.b;
            let tx = ifaces[k].t.b;
            let ifmac = n.iflink[k];
            let link = n.ifmac[ifmac];
            if (link === undefined) {
                continue;
            }
            if (!isOwner(link))
                continue;
            updated[link] = true;
            if ($topology.links[link].source.name === name)
                stats.push({name: link, rx: rx, tx: tx});
            else
                stats.push({name: link, rx: tx, tx: rx});
        }
        for (const iface of $topology.nodes[name].ifaces) {
            for (const link of iface.links) {
                if (!isOwner(link))
                    return;
                if (!updated[link]) {
                    updated[link] = true;
                    stats.push({name: link, rx: 0, tx: 0});
                }
            }
        }
        if ($render) $render.setLinkTraffic(stats);
    };

    var trackValue = function(kv, k, v) {
        if (kv === undefined) return;
        if (v) kv[k] = v;
        else delete kv[k];
    };

    /* Update node metadata
    */
    var setNodeMetadata = function(name, kv, reset) {
        var meta;
        var s = false, sl = false;

        if (kv === null) {
            $aux.del(name);
            meta = {};
            kv = {};
            reset = true;
        } else {
            meta = $aux.get(name);
        }

        if (reset) {
            meta.status = {};
            meta.linkStatus = {};
            s = sl = true;
        }
        if (meta.status === undefined) meta.status = {};
        if (meta.linkStatus === undefined) meta.linkStatus = {};

        // Things we should store:
        // - sarnet/a/*
        // - sarnet/o/*
        // - netctl stuff

        if (kv['vm/ns'] !== undefined) {
            // TODO: compress
            setNodeNetstats(name, JSON.parse(kv['vm/ns']));
            // <We don't expect anything else>
        }

        for (let k in kv) {
            let v = kv[k];
            if (k.startsWith('if/')) {
                let iface = JSON.parse(v);
                if (iface.mac && iface.iface) {
                    let mac = iface.mac.toLowerCase();
                    meta.iflink[iface.iface] = mac;
                    meta.iflink[meta.ifmac[mac]] = iface.iface;
                }
            } else if (k.startsWith('sarnet/a/')) {
                trackValue(meta.status, k, v ? v : false);
                s = true;
            } else if (k.startsWith('sarnet/o/')) {
                trackValue(meta.status, k, v !== '{"state": true}');
                s = true;
            } else {
                switch (k) {
                    case 'sarnet/log':
                        $render.drawNodeLog(name, JSON.parse(v));
                        break;
                    case 'vm/egress-policer':
                        meta.linkStatus.policer = JSON.parse(v);
                        sl = true;
                        break;
                    case 'sarnet/learn_mode':
                        trackValue(meta.status, k, v === 'true');
                        s = true;
                        break;
                    case 'cmd/host-netctl/iface-filter':
                    case 'cmd/host-netctl/sdn-redirect':
                    case 'cmd/host-netctl/nfv':
                        trackValue(meta.status, k, v !== '[]');
                        s = true;
                        break;
                    case 'vm/containers':
                        let atk = false;
                        for (let cname in JSON.parse(v)) {
                            if (cname.startsWith('va-')) {
                                atk = true;
                                break;
                            }
                        }
                        trackValue(meta.status, '$attacker', atk);
                        s = true;
                        break;
                }
            }
        }

        if (s) {
            $render.setNodeStatus(name, meta.status);
            $attack.trackNodeStatus(name, meta.status);
        }
        if (sl) {
            $render.setNodeLinkStatus(name, meta.linkStatus);
            //$attack.trackLinkStatus(name, meta.linkStatus);
        }
    };

    var renderCompressedFlowMap = function(mac_map) {
        var data = {};
        for (var mac in mac_map) {
            var flows = mac_map[mac];
            var info = flowMacs[mac];
            if (info === undefined) {
                return;
            }

            if (info.owner !== info.left.name) {
                $input.updateLinkFlows(info, flows[1], flows[0]);
                data[info.link] = {sender: info.left, target: info.right,
                    tx: flows[0], rx: flows[1]};
            } else {
                $input.updateLinkFlows(info, flows[0], flows[1]);
                data[info.link] = {sender: info.left, target: info.right,
                    rx: flows[0], tx: flows[1]};
            }
        }
        $render.setFlowsLinks(data);
    };

    return vnet.WSUI('wss://' + window.location.hostname + ':8101/', function(msg) {
        var mtype = msg['@'];
        var k;
        if (mtype == 't') {
            setTopology(/*msg.c, */msg.t);
        } else if (mtype == 'M') {
            var nodes = [];
            for (k in msg.M) {
                nodes.push(k);
                setNodeMetadata(k, msg.M[k], true);
            }
            nodes.sort();
            //console.log('M:', nodes.join(', '));
        } else if (mtype == 'm') {
            setNodeMetadata(msg.i, msg.m);
        } else if (mtype == 'f') {
            if (msg.f) renderCompressedFlowMap(msg.f);
        } else if (mtype == 'B') {
            if (msg.B.kind === 'reload-topo')
                location.reload();
        } else {
            console.log(JSON.stringify(msg));
        }
    });
})();
