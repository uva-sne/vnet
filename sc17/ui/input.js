/* jshint esnext: true */

/* Mouse/finger controls
 *
 * TODO: deal with multi-touch correctly (track touch.identifier)
 */
$input = (function() {
    var moving = false,
        pressing = false,
        time, last,
        pressTarget;
    var originX, originY;
    var rotationHold, updownHold;
    var holdX = 0, holdY = 0;
    var deltaX, deltaY;

    var linkModal = document.getElementById('link-modal');

    var clickHook = null,
        lastClick;

    var that = {
        camX: 0,
        camY: 500,
        activeLink: null,

        forceCam: function(x, y) {
            moving = false;
            deltaX = deltaY = 0;
            this.camX = x;
            this.camY = y;
            $render.setCamera(x, y);
        },

        cancel: function(e) {
            e.stopPropagation();
            moving = false;
        },

        updateLinkFlows: function(info, rx, tx) {
            if (info.link !== that.activeLink)
                return;
            let text = `${info.link}\n\nFrom: ${info.left.name}\n`;
            for (let flow of rx) {
                text += 'AS' + flow[0] + ' type ' + traffic_types[flow[1]] +
                    ' rate ' + speed(flow[2]) + ' pps ' + flow[3] + '\n';
            }
            text += `\nFrom: ${info.right.name}\n`;
            for (let flow of tx) {
                text += 'AS' + flow[0] + ' type ' + traffic_types[flow[1]] +
                    ' rate ' + speed(flow[2]) + ' pps ' + flow[3] + '\n';
            }
            linkModal.textContent = text;
        },

        click: function(e) {
            //// Never trigger on buttons
            //if (e.target && e.target.tagName == 'BUTTON')
            //    return;
            linkModal.style.display = 'none';
            that.activeLink = null;
        },

        linkClick: function(elem, name) {
            _events(elem, ['mousedown', 'touchstart'], function(e) {
                if (moving || pressing) return;
                e.stopPropagation();
                pressing = true;
                last = time = now();
                console.log('Link press start:', name);
                pressTarget = null;
                linkModal.style.display = 'block';
                linkModal.textContent = '...';
                that.activeLink = name;
            });
        },

        nodeClick: function(elem, name) {
            _events(elem, ['mousedown', 'touchstart'], function(e) {
                e.stopPropagation();
                if (moving || pressing) return;
                pressing = true;
                last = time = now();
                console.log('Press start:', name);
                pressTarget = name;
            });
        },

        hookNode: function(hook) {
            if (clickHook !== null) {
                console.error('Invalid click hook');
            }
            clickHook = hook;
        },
        unhookNode: function() {
            if (clickHook === null) {
                console.error('Invalid click unhook');
            }
            clickHook = null;
        }
    };

    _events(document.body, ['mousedown', 'touchstart'], function(e) {
        var t;
        if (pressing || moving) return true;
        if (e.type == 'mousedown') {
            // Only LMB
            if (e.button !== 0) return true;
            t = e;
        } else {
            // Only first touch? XXX
            t = e.touches[0];
        }
        e.preventDefault();
        moving = true;
        deltaX = deltaY = 0;
        originX = t.clientX;
        originY = t.clientY;
        holdX = that.camX;
        holdY = that.camY;
        last = time = now();
    }, {passive: false});

    _events(document.body, ['mousemove', 'touchmove'], function(e) {
        if (!moving) return;
        var t = e.type == 'touchmove' ? e.touches[0] : e;
        deltaX = t.clientX - originX;
        deltaY = t.clientY - originY;
        that.camX = holdX + deltaX;
        that.camY = holdY + deltaY;
        var n = now();
        if ((n - last) > 0.017) {
            last = n;
            $render.setCamera(that.camX, that.camY);
        }
    }, {passive: false});

    _events(document.body, ['mouseleave', 'mouseup', 'touchend'], function(e) {
        if (e.type === 'mouseup' && e.button !== 0) return; // XXX
        e.preventDefault();
        if (!moving && !pressing)
            return;
        let delta = now() - time;

        if (moving) {
            if (delta < 0.5 && (Math.abs(deltaX) + Math.abs(deltaY)) < 5) {
                that.click(e);
            } else {
                $render.setCamera(that.camX, that.camY);
            }
        } else if (pressTarget) {
            console.log('Press end:', pressTarget, delta);
            if (delta > 0.3 && clickHook !== null) {
                clickHook(pressTarget);
            } else {
                $render.nodeAnimate(pressTarget, 'lift');
                setActiveDomain(pressTarget);
            }
            /*
            return function(e) {
                that.cancel(e);
                    if (lastClick === name) {
                        return;
                    }
                    lastClick = name;
                }
            };*/
        }

        pressing = moving = false;
    }, {passive: false});

    // TODO: initial placement offset inside renderer
    $render.setCamera(that.camX, that.camY);
    return that;
})();

$hack = $input;
window.addEventListener('resize', $render.resize);


var setActiveDomain = function(name) {
    // TODO: store, recall
    coms.send('broadcast', {kind: 'ad', data: name});
};

forEach(document.querySelectorAll('#layer-box button'), function(btn) {
    _events(btn, ['click', 'touchstart'], function(e) {
        $input.cancel(e);
        btn.classList.toggle('active');
        $render.setLayerVisible(btn.dataset.layer,
            btn.classList.contains('active'));
    });
    //if (btn.dataset.layer == 'flows')
    //    btn.click();
});


// TODO: sync to *current* value
// TODO: block button if running scenario
var setCollaboration = (function() {
    var collabButtons = document.querySelectorAll('#sarnet-controls button');
    var collabLevel = '';

    function set(level) {
        // TODO: enforce this during disconnects
        console.log('Enforce collaboration:', level);
        syncButtons(level);
        coms.send({'@': 'pub', 'k': 'cmd/sarnet/collaboration', 'v': level,
            'r': true});
    }

    function setNewState(e) {
        $input.cancel(e);
        if (!layers.bg)
            return;
        set(e.target.dataset.collab);
    }

    function syncButtons(requested) {
        let old = collabLevel;
        layers.bg.classList.remove('col-' + old);
        collabLevel = requested;
        void layers.bg.offsetHeight; /* Triggers reflow to sync up animations */
        layers.bg.classList.add('col-' + collabLevel);

        for (let btn of collabButtons) {
            if (btn.dataset.collab === collabLevel)
                btn.classList.add('active');
            else
                btn.classList.remove('active');
        }
    }

    for (let btn of collabButtons)
        _events(btn, ['click', 'touchstart'], setNewState);

    return set;
})();
//setTimeout(() => setCollaboration('upstream'), 10);

var _timer = function(state, func, time) {
    if (time === false) {
        if (state !== undefined)
            clearTimeout(state);
        return undefined;
    }
    if (state === undefined) {
        return setTimeout(func, time);
    }
    return state;
};


/*
 */
var $attack = (function() {
    var results = document.getElementById('results');
    var important = document.getElementById('important');
    var currentStart, currentEnd, currentVictim;
    var topology;
    var attack = 'udp-ddos', rate = '80';
    var currentAttack = attack;
    var level = 'local';
    var running = false;

    function checkCleanup() {
        for (let name in topology.nodes) {
            let m = $aux[name];
            if (!m)
                continue;
            let ignore_obs = !!name.match(/service101/);
            for (let k in m.status) {
                if (ignore_obs && k.startsWith('sarnet/o/'))
                    continue;
                console.log('Not cleaned up:', name, k);
                return false;
            }
        }
        return true;
    }

    function stopAndThen(next) {
        return function() {
            // TODO: keep sending until no attack containers remain
            if (coms.connected) {
                coms.send({'@': 'attack', 'attack': 'stop'});
                currentState = next;
                setCollaboration(level);
            }
        };
    }

    /* Wait for resource cleanup, then activate next scenario
     */
    function stateCleanupForTransition() {
        important.style.display = 'block';
        if (!checkCleanup())
            return;
        if (coms.connected) {
            running = true;
            currentState = stateStart;
        }
    }

    function stateStart() {
        important.style.display = '';
        running = true;
        engage();
        currentState = stateNone;
    }

    /* Do nothing
     */
    function stateNone() {
    }

    function start() {
        results.style.display = '';

        if (!victim || !attackers.length) {
            randomize();
        }

        running = false;
        level = 'local';
        currentAttack = attack;
        currentVictim = victim;

        $results.setAttack(currentAttack);
        $results.setRowState('local', null, null, null);
        $results.setRowState('upstream', null, null, null);
        $results.setRowState('alliance', null, null, null);

        currentState = stopAndThen(stateCleanupForTransition);
    }

    function advance() {
        if (running)
            finalizeRow();
        running = false;
        if (level === 'local') {
            level = 'upstream';
        } else if (level === 'upstream') {
            level = 'alliance';
        } else {
            stop();
            return;
        }
        currentState = stopAndThen(stateCleanupForTransition);
    }

    function stop() {
        important.style.display = '';
        running = false;
        coms.send({'@': 'attack', 'attack': 'stop'});
        currentState = stateNone;
    }

    var currentState = stateNone;
    setInterval(function() {
        currentState();
    }, 1000);

    // --------

    var attackers = [], attackerPool = [];
    var victim, victimPool = [];


    function N(name) {
        let n = topology.nodes[name];
        if (n && n.unit_url)
            return url_to_ident(n.unit_url);
        return name;
    }

    /**/
    function finalizeRow() {
        if (currentEnd === undefined)
            currentEnd = false;
        $results.setRowState(level, null, currentStart, currentEnd);
        currentStart = null;
        currentEnd = undefined;
    }

    var $results = (function() {
        var namebox = document.getElementById('results-attack-scenario');
        var rows = {
            local: document.querySelectorAll('#results .row-local td'),
            upstream: document.querySelectorAll('#results .row-upstream td'),
            alliance: document.querySelectorAll('#results .row-alliance td'),
        };
        return {
            tick: function() {
                if (!running)
                    return;
                let end = currentEnd;
                if (end === undefined) {
                    end = now();
                }
                $results.setRowState(level, null, currentStart, end);
            },
            setRowState: function(row, solution, start, end) {
                var elems = rows[row];
                //if (solution) {
                if (end || end === false) {
                    //elems[1].textContent = solution;
                    elems[1].style.visibility = 'visible';
                } else {
                    elems[1].style.visibility = 'hidden';
                    //elems[1].textContent = '';
                }
                if (end === false) {
                    elems[2].innerHTML = '<span style="color:#ff4545">Failed</span>';
                } else if (end && start) {
                    elems[2].textContent = time(end - start);
                } else {
                    elems[2].textContent = '';
                }
            },
            setAttack: function(name) {
                namebox.textContent = name;
            }
        };
    })();
    setInterval($results.tick, 99);

    /**/
    function engage() {
        currentStart = now();
        currentEnd = undefined;

        let nrate = +rate;
        var d = {'@': 'attack', attack: 'cs', cs: {}};
        var vicDomain = domainFromName(victim);
        console.log('victim', victim, vicDomain);
        for (let a of attackers) {
            let localDomain = domainFromName(a);
            let env = {};
            let img;
            if (attack === 'pw') {
                img = 'vnet-client';
                env.CONTAINER_ARGS = '-mode,pw,-workers,3,172.30.' + vicDomain + '.64';
            } else if (attack === 'reflect') {
                console.log(localDomain, +localDomain < 13, (+localDomain) < 13);
                img = 'vnet-ddos-udp';
                env.SRC = '172.30.' + vicDomain + '.250';
                // TODO: need a reflector select mechanism
                env.DST = '172.30.' + (+localDomain < 13 ? '15' : '11') + '.63';
                env.RATE = String((nrate / (3 * attackers.length)) | 0);
                env.SIZE = '500';
            } else {
                img = 'vnet-ddos-udp';
                env.SRC = '172.30.' + localDomain + '.250';
                env.DST = '172.30.' + vicDomain + '.250';
                env.RATE = String(Math.min(95, nrate / attackers.length)|0);
                env.SIZE = '1460';
            }
            d.cs[N(a)] = [{name: 'va-attack', img: img, env: env}];
        }
        for (let a of attackerPool) {
            if (attackers.indexOf(a) < 0)
                d.cs[N(a)] = [];
        }
        coms.send(d);
    }

    function freerun() {
        results.style.display = 'none';
        if (!victim || !attackers.length) {
            randomize();
        }
        engage();
    }

    /*** node selection ***/
    var randomize = function() {
        if (victim !== undefined)
            deselect(victim);
        while (attackers.length)
            deselect(attackers.pop());
        shuffle(victimPool);
        shuffle(attackerPool);
        victim = victimPool[0];
        let n = 1 + (Math.random() * 3) | 0;
        for (let i = 0; i < n && i < attackerPool.length; i++)
            attackers.push(attackerPool[i]);
        console.log('[attack] Victim:', victim);
        console.log('[attack] Attackers:', attackers);
        reselect();
    };
    var select = function(name) { topology.nodes[name].box.classList.add('highlight'); };
    var deselect = function(name) { topology.nodes[name].box.classList.remove('highlight'); };
    var reselect = function() {
        if (victim) select(victim);
        for (let a of attackers)
            select(a);
    };
    var clickNode = function(name) {
        // if actuallyRunning: return;
        if (name.match(/service/)) {
            if (victim === name) {
                deselect(victim);
                victim = undefined;
                return;
            } else if (victim !== undefined) {
                deselect(victim);
            }
            victim = name;
            select(name);
        } else if (name.match(/client/)) {
            var exists = attackers.indexOf(name);
            if (exists >= 0) {
                attackers.splice(exists, 1);
                deselect(name);
            } else {
                attackers.push(name);
                select(name);
            }
        } else {
            $render.nodeAnimate(name, 'vibrate');
        }
    };

    /* Show/hide menu
     */
    //(function() {
    //    var aui = document.getElementById('control-overlay');
    //    _events(document.getElementById('attack-ui-toggle'),
    //        ['click', 'touchstart'], function(e) {
    //            $input.cancel(e);
    //            aui.classList.toggle('panel-disabled');
    //            if (aui.classList.contains('panel-disabled')) {
    //                // <restore layers>
    //                $input.unhookNode();
    //                layers.fg.classList.remove('show-hl');
    //            } else {
    //                // <hide layers>
    //                $input.hookNode(clickNode);
    //                layers.fg.classList.add('show-hl');
    //            }
    //        });
    //})();
    if (window.layers) layers.fg.classList.add('show-hl');
    $input.hookNode(clickNode);

    /* Attack type buttons
     */
    (function() {
        var btns = document.querySelectorAll('#attack-type button');
        var clickAttackType = function(e) {
            $input.cancel(e);
            attack = e.target.dataset.attack;
            for (let btn of btns) {
                if (btn.dataset.attack === attack)
                    btn.classList.add('active');
                else
                    btn.classList.remove('active');
            }
        };
        for (let btn of btns)
            _events(btn, ['mousedown', 'touchstart'], clickAttackType);
    })();

    /* Attack rate
     */
    (function() {
        var btns = document.querySelectorAll('#attack-intensity button');
        var clickAttackRate = function(e) {
            $input.cancel(e);
            rate = e.target.dataset.rate;
            for (let btn of btns) {
                if (btn.dataset.rate === rate)
                    btn.classList.add('active');
                else
                    btn.classList.remove('active');
            }
        };
        for (let btn of btns)
            _events(btn, ['mousedown', 'touchstart'], clickAttackRate);
    })();

    /* Control buttons
     * TODO: sync start/stop button states
     */
    (function() {
        var btns = document.querySelectorAll('#attack-act button');
        var clickAttackAct = function(e) {
            $input.cancel(e);
            switch (e.target.dataset.act) {
                case 'randomize': randomize(); break;
                case 'start': start(); break;
                case 'advance': advance(); break;
                case 'stop': stop(); break;
                case 'freerun': freerun(); break;
            }
        };
        for (let btn of btns)
            _events(btn, ['mousedown', 'touchstart'], clickAttackAct);
    })();

    return {
        trackNodeStatus: function(name, status) {
            if (!running)
                return;

            // <verify correct node/attack>
            console.log('[attack]', name, status);
            for (let k in status) {
                if (k.startsWith('sarnet/a/')) {
                    let v = status[k];
                    if (v === 'healthy')
                        currentEnd = now();
                    else
                        currentEnd = undefined;
                }
            }

            //if (state !== '') {
            //    if (state === 'healthy') {
            //        currentEnd = now();
            //    } else {
            //        currentEnd = undefined;
            //    }
            //} else {
            //    // <<Resolved>>
            //    if (currentEnd === undefined) // Skipped healthy? shouldn't happen
            //        currentEnd = now();
            //}
        },
        setTopology: function(topology_) {
            topology = topology_;
            victimPool = [];
            attackerPool = [];
            for (var name in topology.nodes) {
                if (name.match(/service/)) {
                    victimPool.push(name);
                } else if (name.match(/client/)) {
                    attackerPool.push(name);
                }
            }
            reselect();
        }
    };
})();

function client(src, dst, workers) {
    src = String(src);
    let origin;
    for (let n in $topology.nodes) {
        if (domainFromName(n) == src) {
            origin = url_to_ident($topology.nodes[n].unit_url);
            break;
        }
    }
    if (!origin) {
        console.warn('Unknown domain:', src);
        return;
    }
    if (!workers) workers = 3;
    let d = {'@': 'attack', attack: 'cs', cs: {}};
    if (dst !== 0 && !dst) {
        d.cs[origin] = [];
    } else {
        if (!(dst instanceof Array))
            dst = [dst];
        d.cs[origin] = dst.map(function(n) {
            let env = {};
            env.CONTAINER_ARGS = '-workers,'+workers+',172.30.' + dst + '.64';
            return {name: 'va-attack-' + n, img: 'vnet-client', env: env};
        });
    }
    coms.send(d);
}

function ddos(src, dst, rate) {
    src = String(src);
    let origin;
    for (let n in $topology.nodes) {
        if (domainFromName(n) == src) {
            origin = url_to_ident($topology.nodes[n].unit_url);
            break;
        }
    }
    if (!origin) {
        console.warn('Unknown domain:', src);
        return;
    }
    let d = {'@': 'attack', attack: 'cs', cs: {}};
    if (rate === 0 || (dst !== 0 && !dst)) {
        d.cs[origin] = [];
    } else {
        if (!(dst instanceof Array))
            dst = [dst];
        d.cs[origin] = dst.map(function(n) {
            let env = {};
            env.SRC = '172.30.' + src + '.250';
            env.DST = '172.30.' + n + '.250';
            env.RATE = rate === undefined ? '50' : String(rate);
            env.SIZE = '1460';
            return {name: 'va-attack-' + n, img: 'vnet-ddos-udp', env: env};
        });
    }
    coms.send(d);
}

function delay(d) {
    if (d === undefined)
        d = 0;
    coms.send({'@': 'pub',
               'k': 'cmd/sarnet/delay',
               'v': String(d),
               'r': true});
}

function bcast(kind, data) {
    coms.send('broadcast', {kind: kind, data: data});
}

setTimeout(function() {
    let flowbtn = document.querySelector('button[data-layer=flows]');
    if (flowbtn) flowbtn.click();
}, 50);
