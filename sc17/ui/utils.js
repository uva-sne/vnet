/* jshint esnext: true */
function clearAnim() {
    this.style.animation = '';
}
function autoClearAnim(elem) {
    elem.addEventListener('animationend', clearAnim);
}

var now = () => performance.now()/1000;
var clamp = (v, lo, hi) => v > hi ? hi : (v < lo ? lo : v);
var nearest2 = (v) => Math.pow(2, Math.ceil(Math.log(v) / Math.log(2)));
var _events = (onto, evts, func, args) => evts.forEach((e) => {
    onto.addEventListener(e, func, args);
});

function touch(elem, callback) {
    _events(elem, ['mousedown', 'touchstart'], callback);
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = (Math.random() * (i + 1)) | 0;
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
}
var any = function(lst, fn) {
    for (var i = 0; i < lst.length; i++) {
        if (fn(lst[i], i))
            return true;
    }
    return false;
};
var runInterval = (f, n) => {
    f();
    setInterval(f, n);
};
var forEach = function(obj, func) {
    if (!obj || !obj.forEach) return;
    return obj.forEach(func);
};
var iterItems = function(obj, func) {
    if (!obj) return;
    for (var k in obj) {
        func(obj[k], k);
    }
};
var fixMAC = function(s) {
    return (s.substr(0,2) + ':' + s.substr(2,2) + ':' + s.substr(4,2) + ':' +
        s.substr(6,2) + ':' + s.substr(8,2) + ':' + s.substr(10,2));
};
var lpad = function(v, n) {
    v = String(v);
    while (v.length < 2)
        v = '0' + v;
    return v;
};
var decimal = function(v) {
    var p = String(v).split('.');
    if (p.length <= 1)
        return '0';
    return p[p.length - 1];
};
var time = function(v) {
    var s = lpad(decimal(v).substr(0, 3), 3);
    v |= 0;
    s = lpad(v % 60, 2) + '.' + s;
    v = (v / 60) | 0;
    s = lpad(v % 60, 2) + ':' + s;
    return s;
};
var speed = function(v) {
    //v = v|0;
    if (v < 3248)
        return (v | 0) + 'bit/s';
    if (v < 1000000)
        return ((v / 1000) | 0) + 'kbit/s';
    if (v < 10000000000)
        return ((v / 1000000) | 0) + 'Mbit/s';
    return ((v / 1000000000) | 0) + 'Gbit/s';
};
///

var SVG = 'http://www.w3.org/2000/svg';
var H = (n) => document.createElement(n);
var E = (n) => document.createElementNS(SVG, n);

var $svg = function(elem) {
    return document.createElementNS(SVG, elem);
};

var $svgSized = function(w, h) {
    var svg = $svg('svg');
    svg.setAttribute('width', w);
    svg.setAttribute('height', h);
    return svg;
};

var pos = function(e, s) {
    e.style.position = 'absolute';
    e.style.left = s.x + 'px';
    e.style.top = s.y + 'px';
};

var insertAfter = function(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
};

var insertSorted = function(root, elem, comp) {
    var after;
    for (var i = 0; i < root.children.length; i++) {
        if (comp(root.children[i]) > comp(elem))
            break;
        after = root.children[i];
    }
    if (after !== undefined)
        insertAfter(elem, after);
    else
        root.insertBefore(elem, root.firstChild);
};

function svgJoinPoints(points, scale) {
    var d = '';
    for (let i = 0; i < points.length; i++) {
        d += (i === 0 ? 'M' : ' L') + (scale * points[i][0]) + ',' + (scale * points[i][1]);
    }
    return d;
}
