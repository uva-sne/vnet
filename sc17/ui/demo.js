var SVG = 'http://www.w3.org/2000/svg';

$status = {debug: function() {
    console.log(arguments);
}};
var eTopology = document.getElementById('topology');
var here = document.getElementById('here');
var elems = {};
var netstat = {};
var pad = function(v) {
    while (v.length < 12) {
        v += ' ';
    }
    return v;
};
var trunc = function(v) {
    v = String(v);
    var c = v.indexOf('.');
    if (c >= 0) {
        v = v.substring(0, c + 2);
    }
    return v;
};
var bps = function(v) {
    v *= 8;
    if (v > 1e9) {
        return trunc(v / 1e9) + 'Gb/s';
    } else if (v > 1e6) {
        return trunc(v / 1e6) + 'Mb/s';
    } else if (v > 1024) {
        return trunc(v / 1e3) + 'kb/s';
    }
    return v + 'b/s';
};
var memory = {};
var formatMemory = function(m) {
    var t = '';
    for (var i in m) {
        t += i + ':\n';
        for (var k in m[i]) {
            //if (!k.match(/^cmd/)) continue;
            t += ' - ' + k + ' = ' + JSON.stringify(m[i][k]) + '\n';
        }
    }
    return t;
};

var netstats = function(memory, ident, ifaces) {
    var m = memory[ident];
    if (m === undefined) {
        m = memory[ident] = {};
    }

    // TODO: set all non-reported ifaces to 0

    for (var iface in ifaces) {
        var i = ifaces[iface];
        //if (m[iface] === undefined)
        m[iface] = i;
    }
};

var graph = document.getElementById('topology');

var setTopology = function(/*counter, */topology) {
    var size = [400, 400];
    var svg = document.createElementNS(SVG, 'svg');
    svg.setAttribute('width', size[0]);
    svg.setAttribute('height', size[1]);

    var nodes = [];
    var links = [];
    var lut = {};

    iterItems(topology.nodes, function(n) {
        var i = nodes.length;
        nodes[i] = n;
        n.id = i;
        lut[n.name] = i;
    });

    iterItems(topology.links, function(n) {
        links.push({source: lut[n.source],
                    target: lut[n.target]});
    });

    var gpad = 0.01;
    var _c = new cola.Layout().
        nodes(nodes).
        links(links).
        linkDistance(50).
        avoidOverlaps(true).
        size(size);
        //jaccardLinkLengths(50);
        //symmetricDiffLinkLengths(50, 0.2);
    _c.on('tick', function() {
        links.forEach(function(n) {
            n.elem.setAttribute('x1', n.source.x);
            n.elem.setAttribute('y1', n.source.y);
            n.elem.setAttribute('x2', n.target.x);
            n.elem.setAttribute('y2', n.target.y);
        });
        nodes.forEach(function(n) {
            n.elem.setAttribute('x', n.x - 10);
            n.elem.setAttribute('y', n.y - 10);
        });
    });

    links.forEach(function(n) {
        var r = document.createElementNS(SVG, 'line');
        r.setAttribute('stroke', '#444444');
        r.setAttribute('stroke-width', '2px');
        n.elem = r;
        svg.appendChild(r);
    });

    nodes.forEach(function(n, i) {
        var r = document.createElementNS(SVG, 'rect');
        r.setAttribute('title', n.name);
        r.setAttribute('width', 20);
        r.setAttribute('height', 20);
        r.setAttribute('stroke', '#666666');
        if (n.name.match(/client/))
            r.setAttribute('fill', '#997777');
        else if (n.name.match(/service/))
            r.setAttribute('fill', '#779977');
        else if (n.name.match(/nfv/))
            r.setAttribute('fill', '#997799');
        else if (n.name.match(/surf/))
            r.setAttribute('fill', 'gold');
        else
            r.setAttribute('fill', '#999999');
        n.elem = r;
        svg.appendChild(r);
    });


    _c.start(10);

    graph.innerHTML = '';
    graph.appendChild(svg);

    //eTopology.textContent = JSON.stringify(topology);
};

var msg = function(msg) {
    //console.log(JSON.stringify(msg));
    var mtype = msg['@'];
    if (mtype == 't') {
        setTopology(/*msg.c, */msg.t);
    } else if (mtype == 'M') {
        memory = msg.M;
        here.textContent = formatMemory(memory);
    } else if (mtype == 'm') {
        var kind, data, ident;
        ident = msg.i;
        data = msg.m;
        var m = memory[ident];
        if (m === undefined) {
            m = memory[ident] = {};
        }
        for (var k in data) {
            m[k] = data[k];
        }
        here.textContent = formatMemory(memory);
    } else if (mtype == 'n') {
        netstats(memory, msg.i, msg.n);
    } else {
        console.log(msg);
    }
};

var server = 'wss://' + window.location.hostname + ':8101/';
vnet.WSUI(server, msg);
