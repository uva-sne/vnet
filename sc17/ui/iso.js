/* jshint esnext: true */

/* SARNET -- isometric SVG edition
 */
var WA = 0.3, WB = 0.7;

var iso2screenF = function(xy) {
    return {x: ((xy.x - xy.y) * TILE_HALF_WIDTH * UPSCALE),
            y: ((xy.x +  xy.y) * TILE_HALF_HEIGHT * UPSCALE)};
};
var iso2screen = function(xy) {
    return {x: ((xy.x - xy.y) * TILE_HALF_WIDTH * UPSCALE)|0,
            y: ((xy.x +  xy.y) * TILE_HALF_HEIGHT * UPSCALE)|0};
};

var TILE_DEPTH = 2*TILE_HALF_WIDTH;
var iso2screenDepth = function(xy, depth) {
    return {x: ((xy.x - xy.y) * TILE_HALF_WIDTH * UPSCALE),
            y: (((xy.x +  xy.y) * TILE_HALF_HEIGHT - TILE_DEPTH * depth) * UPSCALE)};
};

var MATRIX;

var nodeInterfacePos = function(name) {
    var n = $topology.nodes[name];
    if (n === undefined) return undefined;
    var xy = iso2screen(n);
    return {x: xy.x, y: xy.y};
};

var $stop = false;
var ZERO = {x: 0, y: 0};

var layers = {};

function _draw3Box(x, y, w, h, d, offzd, hack) {
    if (offzd === undefined)
        offzd = 0;
    // TODO: allow flippable
    let fa = iso2screenDepth({x: x + w, y: y - h}, offzd);
    let fb = iso2screenDepth({x: x + w, y: y + h}, offzd);
    let fc = iso2screenDepth({x: x - w, y: y + h}, offzd);
    let fd = iso2screenDepth({x: x - w, y: y - h}, offzd);
    let ca = iso2screenDepth({x: x + w, y: y - h}, offzd+d);
    let cb = iso2screenDepth({x: x + w, y: y + h}, offzd+d);
    let cc = iso2screenDepth({x: x - w, y: y + h}, offzd+d);
    let cd = iso2screenDepth({x: x - w, y: y - h}, offzd+d);
    let r = E('path');
    let p = `M${ca.x} ${ca.y} L${cb.x} ${cb.y} L${cc.x} ${cc.y} L${cc.x} ${cc.y} L${cd.x} ${cd.y} L${ca.x} ${ca.y}`+
        ` L${fa.x} ${fa.y} L${fb.x} ${fb.y} L${fc.x} ${fc.y} L${cc.x} ${cc.y}`+
        ` M${cb.x} ${cb.y} L${fb.x} ${fb.y}`;
    if (hack === 'door') {
        let dfl = iso2screenDepth({x: x + 0.4*w, y: y + h}, offzd);
        let dfr = iso2screenDepth({x: x - 0.4*w, y: y + h}, offzd);
        let dcl = iso2screenDepth({x: x + 0.4*w, y: y + h}, offzd+0.6*d);
        let dcr = iso2screenDepth({x: x - 0.4*w, y: y + h}, offzd+0.6*d);
        p +=  ` M${dfl.x} ${dfl.y} L${dcl.x} ${dcl.y}`;
        p +=  ` M${dcl.x} ${dcl.y} L${dcr.x} ${dcr.y}`;
        p +=  ` M${dcr.x} ${dcr.y} L${dfr.x} ${dfr.y}`;
    } else if (hack === 'surf-stripe') {
        let sa = iso2screenDepth({x: x + w, y: y - h}, offzd+0.7*d);
        let sb = iso2screenDepth({x: x + w, y: y + h}, offzd+0.7*d);
        let sc = iso2screenDepth({x: x - w, y: y + h}, offzd+0.7*d);
        p +=  ` M${sa.x} ${sa.y} L${sb.x} ${sb.y}`;
        p +=  ` M${sb.x} ${sb.y} L${sc.x} ${sc.y}`;
    }
    r.setAttribute('d', p);
    return r;
}

/* ----------------------------------------------------------------------------
 */

/* Draw link paths, with spacing between parallel paths
 * FIXME
 */
var isoDrawPaths = function(elem, icons, links) {
    var d = function(path) {
        var p = '', xy;
        for (var i = 0; i < path.length; i++) {
            xy = iso2screen(path[i]);
            p += (i === 0 ? 'M' : ' L') + xy.x + ' ' + xy.y;
        }
        return p;
    };

    function routerBox(a, b, ciena) {
        let _pos = {x: ((WA * a.x + WB * b.x)),
                    y: ((WA * a.y + WB * b.y))};
        if (ciena) {
            let sz = UPSCALE * 0.035;
            let w = sz * 0.22;
            let h = sz * 0.26;
            let d = sz * 0.13;
            let red = _draw3Box(_pos.x, _pos.y, w, h, d);
            red.setAttribute('class', 'ciena-floor');
            let white = _draw3Box(_pos.x, _pos.y, w, h, d*1.2, d);
            white.setAttribute('class', 'ciena-top');
            routers.appendChild(red);
            routers.appendChild(white);
            return;
        }

        let sz = UPSCALE * 0.4;
        let depth = 4;
        let s = iso2screenF(_pos);
        //s.x = s.x - sz/2;
        //s.y = s.y - sz/2 - (depth / 2);
        s.y = s.y - (depth / 2);
        let w = TILE_HALF_WIDTH * sz;
        let h = TILE_HALF_HEIGHT * sz;
        let r = E('path');
        r.setAttribute('class', 'router-box');
        r.setAttribute('d',
            //'M' + x + ',' + y +
            //' L' + x + ',' + (y + sz) +
            //' L' + (x + sz) + ',' + (y + sz) +
            //' L' + (x + sz) + ',' + y + ' Z' +
            //' M' + x + ',' + y + ' L' + (x + sz) + ',' + (y + sz) +
            //' M' + (x + sz) + ',' + y + ' L' + x + ',' + (y + sz) +
            //' L' + (x + 3) + ',' + (y + depth) +
            //' L' + (x + 3 + sz) + ',' + (y + depth) +
            //' L' + (x + sz) + ',' + (y + 3) +
            //' L' + (x + sz) + ',' + (y)
            `
            M${s.x} ${s.y - h} L${s.x} ${s.y + h}
            M${s.x + w} ${s.y} L${s.x - w} ${s.y}
            L${s.x} ${s.y + h} L${s.x + w} ${s.y} L${s.x} ${s.y - h}
            L${s.x - w} ${s.y}
            L${s.x - w} ${s.y + depth}
            L${s.x} ${s.y + h + depth}
            L${s.x + w} ${s.y + depth}
            L${s.x + w} ${s.y}
            `

            /*
            ' M' + (s.x - w) + ' ' + s.y +
            ' L' + s.x       + ' ' + (s.y + h) +
            ' L' + (s.x + w) + ' ' + s.y +
            ' L' + (s.x + w) + ' ' + (s.y + depth) +
            ' L' +  s.x      + ' ' + (s.y + h + depth) +
            ' L' + (s.x - w) + ' ' + (s.y + depth) +
            'Z'*/
        );
        routers.appendChild(r);
    }

    var nlinks = {};
    let routers = E('g');
    //routers.setAttribute('transform', MATRIX);
    for (let link of links) {
        if (!link.path || link.path.length <= 1) {
            console.log('b0rk', link);
            return;
        }

        var v = nlinks[link.name] = {};
        var lr = paraline(link.path);
        if (lr.left.length >= 3)   {
            lr.left[0] = {x: WA * lr.left[0].x + WB * lr.left[1].x, y: WA * lr.left[0].y + WB * lr.left[1].y};
            lr.right[0] = {x: WA * lr.right[0].x + WB * lr.right[1].x, y: WA * lr.right[0].y + WB * lr.right[1].y};
            lr.left[lr.left.length - 1] = {x: WA * lr.left[lr.left.length - 1].x + WB * lr.left[lr.left.length - 2].x, y: WA * lr.left[lr.left.length - 1].y + WB * lr.left[lr.left.length - 2].y};
            lr.right[lr.left.length - 1] = {x: WA * lr.right[lr.left.length - 1].x + WB * lr.right[lr.left.length - 2].x, y: WA * lr.right[lr.left.length - 1].y + WB * lr.right[lr.left.length - 2].y};
        }
        v.rxpool = [];
        v.txpool = [];
        v.rxparticles = [];
        v.txparticles = [];
        v.left = lr.left;
        v.right = lr.right;

        v.clicker = E('path');
        v.clicker.setAttribute('fill', 'none');
        v.clicker.setAttribute('stroke', 'transparent');
        v.clicker.setAttribute('stroke-width', '30');
        v.clicker.setAttribute('d', d(link.path));
        v.clicker.style.pointerEvents = 'initial';
        $input.linkClick(v.clicker, link.name);
        elem.appendChild(v.clicker);

        v.rx = E('path');
        v.rx.dataset.link = link.name;
        v.rx.setAttribute('class', 'link-traffic-rx');
        //v.rx.setAttribute('d', d(link.path));
        v.rx.setAttribute('d', d(lr.left));
        if (lr.swap) v.rx.style.strokeWidth = '5';
        elem.appendChild(v.rx);

        v.tx = E('path');
        v.tx.dataset.link = link.name;
        v.tx.setAttribute('class', 'link-traffic-tx');
        //v.tx.setAttribute('d', parallel(link.path, -1));
        v.tx.setAttribute('d', d(lr.right));
        elem.appendChild(v.tx);

        v.icons = H('div');
        v.icons.style.background = 'white';
        v.icons.style.color = 'black';
        v.icons.style.display = 'none';
        v.icons.style.textAlign = 'center';
        v.icons.style.border = '2px solid black';
        v.icons.style.outline = '1px solid white';
        v.icons.style.borderRadius = '8%';
        v.icons.style.fontWeight = 'bold';
        v.icons.style.opacity = '0.8';
        v.icons.style.padding = '2px';
        v.icons.style.width = '18px';
        v.icons.style.height = '16px';
        let i = ((link.path.length - 1) / 2) | 0;
        //let p = {x: (link.path[i].x + link.path[i + 1].x) / 2,
        //         y: (link.path[i].y + link.path[i + 1].y) / 2};
        pos(v.icons, iso2screenF(link.path[i]));
        icons.appendChild(v.icons);

        //let ciena = link.source.name.endsWith('.ciena') || link.target.name.endsWith('.ciena');
        let ciena = link.source.name.startsWith('ciena') || link.target.name.startsWith('ciena');
        routerBox(link.path[0], link.path[1], ciena);
        routerBox(link.path[link.path.length - 1], link.path[link.path.length - 2],
            ciena);
    }
    layers.boxes.appendChild(routers);
    return nlinks;
};

/* ----------------------------------------------------------------------------
 */

var flowTicker = (function() {
    var _lastAnim = now();
    var _position = 0;
    var _lastCreated = 0;

    var elem, nlinks, timer;

    // V8 optim
    function Particle(domain, pos, size) {
        this.c = domain;
        this.p = pos;
        this.s = size;
    }

    var particleCircles = new Array(1000);
    var particlePool;
    (function() {
        for (var _i = 0; _i < 1000; _i++)
            particleCircles[_i] = new Particle(0, 0);//E('circle');
    })();

    var spawnParticle = function(particles, pool, progress) {
        for (i = 0; i < pool.length; i++) {
            pool[i].next -= progress;
            if (pool[i].next < 0) {
                pool[i].next = pool[i].ival + 0.3 * Math.random() - 0.15;
                if (particlePool.length) {
                    var p = particlePool.pop();
                    p.c = pool[i].c;
                    p.p = 0;
                    p.s = pool[i].s;
                    particles.unshift(p); //new Particle(pool[i].c, 0));
                }
            }
        }
    };

    var _spawnParticle = function(particles, pool, progress) {
        var i;
        for (i = 0; i < pool.length; i++) {
            pool[i].next -= progress;
        }
        for (i = 0; i < pool.length; i++) {
            if (pool[i].next < 0) {
                pool[i].next = pool[i].ival;
                if (particlePool.length) {
                    var p = particlePool.pop();
                    p.setAttribute('r', '2');
                    p._progress = 0; // Perf?
                    p.style.fill = flowColor(pool[i].c);
                    particles.unshift(p);
                    elem.appendChild(p);
                    break;
                }
            }
        }
    };

    // TODO: ignores segment length
    var ctx, displaceX, displaceY;
    var ba = false;
    var advance = function(path, particles, progress, invert) {
        if (particles.length > mp) {
            mp = particles.length;
        }
        var p;
        var e = path.length;
        var i = 0;
        for (; i < particles.length; i++) {
            p = particles[i];
            p.p += progress;
            if (p.p >= (e - 1))
                break;
            var seg = p.p | 0;
            var interseg = p.p - seg;
            if (invert) {
                seg = e - seg - 2;
                interseg = 1 - interseg;
            }
            var start = path[seg];
            var end = path[seg + 1];
            var x = start.x + (interseg * (end.x - start.x));
            var y = start.y + (interseg * (end.y - start.y));
            ctx.fillStyle = flowColor(p.c);
            var xx = displaceX + UPSCALE * (x - y) * TILE_HALF_WIDTH;
            var yy = displaceY + UPSCALE * (x + y) * TILE_HALF_HEIGHT;
            var sz = p.s * 2;
            ctx.fillRect(xx-(sz/2), yy-(sz/2), sz, sz);
        }
        while (i < particles.length) {
            p = particles.pop();
            particlePool.push(p);
            i++;
        }
    };

    var mp = 0;

    var _advance = function(path, particles, progress, invert) {
        var p;
        var e = path.length;
        var i = 0;
        for (; i < particles.length; i++) {
            p = particles[i];
            p._progress += progress;
            if (p._progress >= (e - 1))
                break;
            var seg = p._progress | 0;
            var interseg = p._progress - seg;
            if (invert) {
                seg = e - seg - 2;
                interseg = 1 - interseg;
            }
            var start = path[seg];
            var end = path[seg + 1];
            var x = start.x + (interseg * (end.x - start.x));
            var y = start.y + (interseg * (end.y - start.y));
            // consider translate
            p.setAttribute('cx', (x - y) * TILE_HALF_WIDTH);
            p.setAttribute('cy', (x + y) * TILE_HALF_HEIGHT);
        }
        while (i < particles.length) {
            p = particles.pop();
            particlePool.push(p);
            p.remove();
            i++;
        }
    };

    var draw = function() {
        displaceX = +elem.dataset.x || 0;
        displaceY = +elem.dataset.y || 0;
        ctx.clearRect(0, 0, elem.width, elem.height);

        var interval = 0.05; // Space between particles

        var n = now();
        var progress = (n - _lastAnim) * 2;
        _lastAnim = n;

        if (progress > 0.5) progress = 0.5; // Cap for e.g. inactive screens
        _position += progress;

        var spawn = false;
        var numParticles = 0;
        var gap = _position - _lastCreated;

        if (gap >= interval) {
            // We'll have at least "progress" worth of space to spawn particles
            numParticles = (gap / interval) | 0;
            //_lastPosition = _position + (numParticles * interval);
            spawn = true;
            _lastCreated = _position; // + interval;
        }
        for (var k in nlinks) {
            var link = nlinks[k];
            if (spawn) {
                spawnParticle(link.rxparticles, link.rxpool, gap);
                spawnParticle(link.txparticles, link.txpool, gap);
            }
            advance(link.left, link.rxparticles, progress, false);
            advance(link.right, link.txparticles, progress, true);
        }
        //requestAnimationFrame(flowTicker);
    };
    return function(_elem, _nlinks) {
        elem = _elem;
        nlinks = _nlinks;
        if (elem.tagName === 'CANVAS') {
            ctx = elem.getContext('2d');
        } else {
            elem.innerHTML = '';
        }
        particlePool = particleCircles.slice();
        if (timer === undefined) {
            timer = setInterval(draw, 30);
        }
    };
})();

/* ----------------------------------------------------------------------------
 */

/*
 */
function _scaleTail(value, exp) {
    if (value < 0) value = 0;
    else if (value > 1) value = 1;
    return Math.pow(value, exp);
}

function flowScale(v) {
    v = v / 10000;
    if (v > 1) v = 1;
    else if (v < 0) v = 0;
    return 0.1 + 2 - 2 * v;
}

var _max_pps = 0;
var _low_rate = Infinity;
// Assign cum. probability based on flow rate vs max rate
var createFlowPool = function(flows, curPool, link) {
    //if (link.match(/stitch/)) {
    //    console.log(link, 'pps',
    //        flows.map(function(v) {
    //            return v[0] + '=' + v[3];
    //        }).join(' ')
    //    );
    //}
    var line_rate = 12500000;
    var pool = [];
    var _pps = 0;
    for (let flow of flows) {
        // 0 1 2 3
        // s t b p
        _pps += flow[3];
        let sz = 2 + 5 * _scaleTail(flow[2] / line_rate, 2);
        let ival = flowScale(flow[3]);
        if (ival < _low_rate) {
            _low_rate = ival;
        }
        let next;
        for (let i = 0; i < curPool.length; i++) {
            if (curPool[i].c === flow[0] && curPool[i].t === flow[1]) {
                next = curPool[i].next;
                break;
            }
        }
        // Space out flows arriving at same time
        if (next === undefined)
            next = ival + Math.random();
        pool.push({c: flow[0],
                   t: flow[1],
                   s: sz,
                   ival: ival,
                   next: next});
    }
    if (flows.length) {
        let _x = _pps / flows.length;
        if (_x > _max_pps) {
            //console.log('PPS:', _x);
            _max_pps = _x;
        }
    }
    //if (link.match(/stitch/)) {
    //    console.log(link, 'pool:',
    //        pool.map(function(v) {
    //            return v.c + '/' + v.t + '=' + v.ival.toFixed(2);
    //        }).join(' ')
    //    );
    //}
    return pool;
};

/* ----------------------------------------------------------------------------
 */

/*
 */
var $render = (function() {
    var root = document.getElementById('topology');
    var scale = window.devicePixelRatio;
    if (scale !== 1) {
        scale = 1;
        //root.style.transform = 'scale(' + (1 / scale) + ')';
        //TILE_HALF_WIDTH /= scale;
        //TILE_HALF_HEIGHT /= scale;
    }
    MATRIX = 'matrix(' + TILE_HALF_WIDTH + ' ' + TILE_HALF_HEIGHT + ' ' +
    (-TILE_HALF_WIDTH) + ' ' + TILE_HALF_HEIGHT + ' 0 0)';

    var S = function() {
        var s = E('svg');
        pos(s, ZERO);
        root.appendChild(s);
        return s;
    };
    var C = function() {
        var s = document.createElement('canvas');
        pos(s, ZERO);
        root.appendChild(s);
        return s;
    };
    var htmlLayer = function() {
        var s = document.createElement('div');
        pos(s, ZERO);
        root.appendChild(s);
        return s;
    };
    var offset = function(layer, depth) {
        layer.dataset.bx = depth * 4;
        layer.dataset.by = -depth * 15;
        return layer;
    };

    var camera = {x: 0, y: 0};
    var resize = function() {
        var x = camera.x;
        var y = camera.y;
        var w = window.innerWidth * scale;
        var h = window.innerHeight * scale;
        root.style.width = w + 'px';
        root.style.height = h + 'px';
        iterItems(layers, function(s) {
            var bx = +s.dataset.bx || 0;
            var by = +s.dataset.by || 0;
            s.setAttribute('width', w);
            s.setAttribute('height', h);
            if (s.tagName == 'CANVAS') {
                // A NUUUU CHEEKI BREEKI IV DAMKE
                s.dataset.x = (x + bx);
                s.dataset.y = (y + by);
            } else if (s.tagName == 'DIV') {
                s.style.transform = 'translate(' + (x + bx) + 'px,' + (y + by) + 'px)';
            } else {
                //s.setAttribute('transform',
                //    'translate(' + x + ', ' + y + ')');
                s.setAttribute('viewBox',
                    (-(x + bx)) + ' ' + (-(y + by)) + ' ' + w + ' ' + h);
            }
        });
        //root.style.backgroundPosition = '-' + (1000 + x/8) + 'px -' + (1000 + y/8) + 'px';
        //root.style.backgroundPosition = (-100 + x/8) + 'px ' + (900 - y/8) + 'px';
    };

    // Avoid unnecessary render work by using different layers
    layers.bg = S();
    layers.net = S();
    layers.particles = C();
    layers.icons = htmlLayer();
    layers.flow = S();
    let _OFF = 0;
    layers.boxes = offset(S(), _OFF);
    layers.fg = offset(htmlLayer(), _OFF);
    layers.figures = S();
    layers.status = offset(htmlLayer(), _OFF);
    layers.netico = htmlLayer();
    layers.coms = offset(S(), _OFF);

    var statusIcons = {holders: {}, icons: {}};
    var icon_map = {
        '$attacker': 'img/attacker.gif',
        'sarnet/learn_mode': 'img/learn_mode.svg',
        'sarnet/a/congestion': 'img/fire.gif', //'img/congestion.svg',
        'sarnet/a/password_bruteforce': 'img/password.svg',
        'cmd/host-netctl/iface-filter': 'img/firewall.svg',
        'cmd/host-netctl/sdn-redirect': 'img/redirect.svg',
        'cmd/host-netctl/nfv': 'img/nfv.png'
    };
    var iconHolder = function(name) {
        // TODO: sync pos for robustness?
        let holder = statusIcons[name];
        if (holder !== undefined)
            return holder;
        let node = $topology.nodes[name];
        if (node === undefined)
            return;
        let s = iso2screen(node);
        holder = H('div');
        pos(holder, s);
        holder.style.display = 'flex';
        holder.style.transform = 'translate(-50%, -64px)';
        statusIcons[name] = holder;
        layers.status.appendChild(holder);
        return holder;
    };
    var createIcon = function(img) {
        var e = H('img');
        if (img === 'img/learn_mode.svg') {
            e.style.border = '2px solid orange';
            e.style.boxShadow = '0 0 5px gold';
        } else {
            e.style.boxShadow = '0 0 5px black';
        }
        e.src = img;
        return e;
    };
    var syncIcons = function(pool, name, values) {
        let fresh = false;
        let icons = pool[name];
        if (icons === undefined) {
            if (values.length === 0) return;
            else icons = pool[name] = {};
        }
        for (let k in icons) {
            if (values[k] === undefined) {
                icons[k].remove();
                delete icons[k];
            }
        }
        if (values.length === 0)
            return false;
        let elem = iconHolder(name);
        for (let k in values) {
            if (icons[k] === undefined) {
                let ico = icon_map[k];
                if (ico !== undefined) {
                    icons[k] = createIcon(ico);
                    elem.appendChild(icons[k]);
                    if (k !== '$attacker') {
                        //console.log(name, 'is fresh', JSON.stringify(k));
                        fresh = true;
                    }
                } else if (k === '$attacker') {
                    let n = $topology.nodes[name];
                    icons[k] = E('image');
                    icons[k].setAttribute('href', 'img/sneaky-beaky.gif');
                    icons[k].setAttribute('width',  4*UPSCALE);
                    icons[k].setAttribute('height', 4*UPSCALE);
                    let p = iso2screen(n);
                    icons[k].setAttribute('x', p.x);
                    icons[k].setAttribute('y', p.y);
                    layers.figures.appendChild(icons[k]);
                }
            }
        }
        return fresh;
    };

    let zIndex = 1;
    iterItems(layers, function(layer, k) {
        layer.classList.add('layer-' + k);
        layer.style.pointerEvents = 'none';
        layer.style.zIndex = zIndex;
        zIndex += 1;
    });

    var phys = layers.fg;
    var l3flow = layers.flow;

    var neighs = [[0, 0], [-1, 0], [1, 0], [0, 1], [0, -1]];

    var drawFlag = function(s, n, label) {
        let x = s.x;
        let y = s.y - TILE_HALF_HEIGHT;
        let w = label.length * 6 + 8;
        let h = 70, hb = h - 20;
        let g = E('g');
        let outline = E('path');
        outline.setAttribute('d',
            'M' + x + ',' + y +
            ' L' + x +        ',' + (y - h) +
            ' L' + (x + w) + ',' + (y - h)+
            ' L' + (x + w) + ',' + (y - hb)+
            ' L' + (x) +      ',' + (y - hb)
        );
        let t = E('text');
        t.setAttribute('stroke', 'none');
        t.setAttribute('x', x + 8);
        t.setAttribute('y', y - h + 14);
        t.setAttribute('fill', 'white');
        t.textContent = label;
        g.appendChild(outline);
        g.appendChild(t);
        return g;
    };

    var drawBox = function(s, n) {
        if (0) {
            let r = E('rect');
            r.setAttribute('transform', MATRIX + ' translate(' + ((n.x - 0.5) * UPSCALE) + ',' + ((n.y - 0.5) * UPSCALE) + ')');
            r.setAttribute('width', UPSCALE);
            r.setAttribute('height', UPSCALE);
            return r;
        }
        //   ' M' + (UPSCALE * (s.x - TILE_HALF_WIDTH)) + ' ' + (UPSCALE * s.y) +
        //   ' L' + (UPSCALE *  s.x                   ) + ' ' + (UPSCALE * (s.y + TILE_HALF_HEIGHT)) +
        //   ' L' + (UPSCALE * (s.x + TILE_HALF_WIDTH)) + ' ' + (UPSCALE * s.y) +
        //   ' L' + (UPSCALE *  s.x                   ) + ' ' + (UPSCALE * (s.y - TILE_HALF_HEIGHT)) +
        //   'Z'
        let e = E('path');
        e.setAttribute('transform', MATRIX + ' translate(' + (UPSCALE * n.x) + ',' + (UPSCALE * n.y) + ') scale(' + (UPSCALE * 1.2) + ')');
        e.setAttribute('d', cloudPath);
        return e;
    };

    function draw3Box(x, y, w, h, d, offzd, hack) {
        if (offzd === undefined)
            offzd = 0;
        // TODO: allow flippable
        let fa = iso2screenDepth({x: x + w, y: y - h}, offzd);
        let fb = iso2screenDepth({x: x + w, y: y + h}, offzd);
        let fc = iso2screenDepth({x: x - w, y: y + h}, offzd);
        let fd = iso2screenDepth({x: x - w, y: y - h}, offzd);
        let ca = iso2screenDepth({x: x + w, y: y - h}, offzd+d);
        let cb = iso2screenDepth({x: x + w, y: y + h}, offzd+d);
        let cc = iso2screenDepth({x: x - w, y: y + h}, offzd+d);
        let cd = iso2screenDepth({x: x - w, y: y - h}, offzd+d);
        let r = E('path');
        let p = `M${ca.x} ${ca.y} L${cb.x} ${cb.y} L${cc.x} ${cc.y} L${cc.x} ${cc.y} L${cd.x} ${cd.y} L${ca.x} ${ca.y}`+
            ` L${fa.x} ${fa.y} L${fb.x} ${fb.y} L${fc.x} ${fc.y} L${cc.x} ${cc.y}`+
            ` M${cb.x} ${cb.y} L${fb.x} ${fb.y}`;
        if (hack === 'door') {
            let dfl = iso2screenDepth({x: x + 0.4*w, y: y + h}, offzd);
            let dfr = iso2screenDepth({x: x - 0.4*w, y: y + h}, offzd);
            let dcl = iso2screenDepth({x: x + 0.4*w, y: y + h}, offzd+0.6*d);
            let dcr = iso2screenDepth({x: x - 0.4*w, y: y + h}, offzd+0.6*d);
            p +=  ` M${dfl.x} ${dfl.y} L${dcl.x} ${dcl.y}`;
            p +=  ` M${dcl.x} ${dcl.y} L${dcr.x} ${dcr.y}`;
            p +=  ` M${dcr.x} ${dcr.y} L${dfr.x} ${dfr.y}`;
        } else if (hack === 'surf-stripe') {
            let sa = iso2screenDepth({x: x + w, y: y - h}, offzd+0.7*d);
            let sb = iso2screenDepth({x: x + w, y: y + h}, offzd+0.7*d);
            let sc = iso2screenDepth({x: x - w, y: y + h}, offzd+0.7*d);
            p +=  ` M${sa.x} ${sa.y} L${sb.x} ${sb.y}`;
            p +=  ` M${sb.x} ${sb.y} L${sc.x} ${sc.y}`;
        }
        r.setAttribute('d', p);
        layers.figures.appendChild(r);
        return r;
    }

    function drawShop(n, dx, dy) {
        let sz = UPSCALE * 0.05;
        let w = sz * 0.22;
        let h = sz * 0.22;
        let d = sz * 0.2;
        draw3Box(n.x + dx, n.y + dy, w, d, h, undefined, 'door').setAttribute('class', 'shop-wall');
        draw3Box(n.x + dx, n.y + dy+0.015, w, d+0.03, 0.03, h).setAttribute('class', 'shop-roof');
    }

    function drawSurfBooth(n, dx, dy) {
        let sz = UPSCALE * 0.05;
        let w = sz * 0.2;
        let h = sz * 0.3;
        draw3Box(n.x + dx, n.y + dy, w, w, h, undefined, 'surf-stripe').setAttribute('class', 'surf-tower');
    }

    function drawContainer(n, dx, dy) {
        let sz = UPSCALE * 0.05;
        let w = sz * 0.1;
        let h = sz * 0.2;
        let d = sz * 0.12;
        draw3Box(n.x + dx, n.y + dy, w, h, d).setAttribute('class', 'shipping-container');
    }

    function drawHouse(n, dx, dy) {
        let _w = 0.2, _h = 0.15;
        let flip = Math.random() < 0.5;
        let sz = UPSCALE * 0.05;
        let w = sz * (flip ? _h : _w);
        let h = sz * (flip ? _w : _h);
        let d = sz * 0.1;
        let rp = sz * 0.22;

        let x = n.x + dx, y = n.y + dy;
        let fa = iso2screenF({x: x + w, y: y - h});
        let fb = iso2screenF({x: x + w, y: y + h});
        let fc = iso2screenF({x: x - w, y: y + h});
        let fd = iso2screenF({x: x - w, y: y - h});
        let ca = iso2screenDepth({x: x + w, y: y - h}, d);
        let cb = iso2screenDepth({x: x + w, y: y + h}, d);
        let cc = iso2screenDepth({x: x - w, y: y + h}, d);
        let cd = iso2screenDepth({x: x - w, y: y - h}, d);

        let r = E('path');
        r.setAttribute('d',
            `M${cc.x} ${cc.y} L${cb.x} ${cb.y} L${ca.x} ${ca.y}`+
            `L${fa.x} ${fa.y} L${fb.x} ${fb.y} L${fc.x} ${fc.y} L${cc.x} ${cc.y}`+
            `M${cb.x} ${cb.y} L${fb.x} ${fb.y}`
        );
        r.setAttribute('class', 'house-wall');
        layers.figures.appendChild(r);

        if (h > w) {
            let ra = iso2screenDepth({x: x, y: y - h}, rp);
            let rb = iso2screenDepth({x: x, y: y + h}, rp);
            r = E('path');
            r.setAttribute('class', 'house-roof');
            r.setAttribute('d',
                `M${ca.x} ${ca.y} L${ra.x} ${ra.y}`+
                `L${rb.x} ${rb.y} L${cc.x} ${cc.y}`+
                `L${cb.x} ${cb.y} L${ca.x} ${ca.y}`+
                `M${cb.x} ${cb.y} L${rb.x} ${rb.y}`
            );
            layers.figures.appendChild(r);
        } else {
            let ra = iso2screenDepth({x: x + w, y: y}, rp);
            let rb = iso2screenDepth({x: x - w, y: y}, rp);
            r = E('path');
            r.setAttribute('class', 'house-roof');
            r.setAttribute('d',
                `M${ca.x} ${ca.y} L${ra.x} ${ra.y}`+
                `L${rb.x} ${rb.y} L${cc.x} ${cc.y}`+
                `L${cb.x} ${cb.y} L${ca.x} ${ca.y}`+
                `M${cb.x} ${cb.y} L${ra.x} ${ra.y}`
            );
            layers.figures.appendChild(r);
        }
        /*

        let hw = w/2, rh = 1.7*depth;
        r = E('path');
        r.setAttribute('d', `
            M${s.x - w} ${s.y - depth}
            L${s.x} ${s.y}
            L${d.x} ${d.y}
            L${d.x - hw} ${d.y - rh}
            L${s.x - hw} ${s.y - rh}
            L${s.x - w} ${s.y - depth}
            M${s.x} ${s.y} L${s.x - hw} ${s.y - rh}
            `);
            //L${s.x - hw} ${s.y - rh}
            //L${s.x - hw} ${s.y - 2*depth}
            //L${s.x + w} ${s.y - depth}
        */
        //layers.figures.appendChild(r);
    }

    var drawNodes = function(nodes) {
        layers.bg.innerHTML = '';
        layers.icons.innerHTML = '';
        phys.innerHTML = '';

        let backdrops = E('g');
        let infiLines = E('g');
        let outlines = E('g');
        let flags = E('g');
        backdrops.setAttribute('transform', MATRIX);
        backdrops.setAttribute('class', 'backdrops');
        infiLines.setAttribute('transform', MATRIX);
        infiLines.setAttribute('class', 'line-grid');
        outlines.setAttribute('transform', MATRIX);
        outlines.setAttribute('class', 'outlines');
        layers.bg.appendChild(backdrops);
        layers.bg.appendChild(infiLines);
        layers.bg.appendChild(outlines);
        layers.boxes.appendChild(flags);

        let line = function(a, b, start, end) {
            for (let i = start; i <= end; i++) {
                let l = E('line');
                l.setAttribute(a + '1', UPSCALE * (i + 0.5));
                l.setAttribute(a + '2', UPSCALE * (i + 0.5));
                l.setAttribute(b + '1', UPSCALE * (start - 0.5));
                l.setAttribute(b + '2', UPSCALE * (end + 1.5));
                infiLines.appendChild(l);
            }
        };
        line('x', 'y', -50, 50);
        line('y', 'x', -50, 50);

        var neigh = function(a, c) {
            for (var i = 0; i < neighs.length; i++) {
                var k = tileCoord[pt2s({x: a.x + neighs[i][1],
                    y: a.y + neighs[i][0]})];
                if (k) {
                    var l = (neighs[i][0] === 0 && neighs[i][1] === 0) ? '70' : '90';
                    k.style.fill = 'hsl(' + c + ', 60%, ' + l + '%)';
                }
            }
        };

        var coords = [
            [0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1],
            [-1, 0], [-1, 1],
            [-1, 2], [0, 2], [1, 2],
            [2, 1], [2, 0], [2, -1],
            [1, -2], [0, -2], [-1, -2],
            [-2, -1], [-2, 0], [-2, 1] /*,
            [-2, -2], [2, 2], [2, -2], [-2, 2]*/
        ];
        var backdrop = {};
        nodes.forEach(function(n) {
            var s = iso2screen(n);
            var classes = 'cell';
            if (n.kind === 'client') {
            } else if (n.kind === 'service') {
                classes += ' cell-local cell-upstream cell-alliance';
            } else {
                var link;
                for (var i = 0; i < n.ifaces.length; i++) {
                    link = $topology.links[n.ifaces[i].links[0]];
                    if (!link) {
                        //console.log(n.ifaces[i].links);
                        continue;
                    }
                    if (link.source.kind === 'service' || link.target.kind === 'service') {
                        classes += ' cell-upstream';
                        break;
                    }
                }
                classes += ' cell-alliance';
            }
            if (1) {
                // TODO: dynamic path
                let bd = E('path');
                bd.setAttribute('transform', 'translate(' + (UPSCALE * n.x) + ',' + (UPSCALE * n.y) + ')');
                var flange = [
                    [1.5,-1.5],
                    [1.5,-2.5], [-1.5,-2.5], [-1.5,-1.5],
                    [-2.5,-1.5], [-2.5,1.5], [-1.5,1.5],
                    [-1.5,2.5], [1.5,2.5], [1.5,1.5],
                    [2.5,1.5], [2.5,-1.5], [1.5,-1.5]
                ];
                // Flanges around the -1.5,-1.5 1.5,1.5 square
                bd.setAttribute('d', svgJoinPoints(flange, UPSCALE));
                let ol = bd.cloneNode();
                bd.setAttribute('fill', 'hsl(' + domainHue(n) + ',50%,93%)');
                ol.setAttribute('fill', 'none');
                ol.setAttribute('stroke', 'hsl(' + domainHue(n) + ',50%,87%)');
                ol.setAttribute('class', classes);
                n.outline = ol;
                autoClearAnim(ol);
                backdrops.appendChild(bd);
                outlines.appendChild(ol);
            }

            // TODO: we could define the background as a clip path and use a
            // circle for attention-animation (expanding r)
            if (0) {
                let e = E('circle');
                e.setAttribute('cx', n.x);
                e.setAttribute('cy', n.y);
                e.setAttribute('r', '1.8');
                e.setAttribute('transform', MATRIX);
                e.setAttribute('class', classes);
                layers.bg.appendChild(e);
            }

            switch (n.kind) {
                case 'client':
                    drawHouse(n, 1.5, -0.2);
                    //drawHouse(n, 0, -1);
                    drawHouse(n, 1, 1);
                    drawHouse(n, -1, -0.5);
                    break;
                case 'service':
                    drawShop(n, 1, -1);
                    break;
                case 'transit':
                    if (n.name.endsWith('.surf')) {
                        drawSurfBooth(n, 1, -1);
                        drawContainer(n, -1, -1.5);
                        drawContainer(n, -0.7, -1.5);
                    }
                    break;
                case 'nfv':
                    if (n.name.match(/62/)) {
                        drawContainer(n, 1,   1);
                        drawContainer(n, 0.7, 1);
                        drawContainer(n, 0.4, 1);
                    } else {
                        drawContainer(n, -1, -1);
                        drawContainer(n, -0.7, -1);
                        drawContainer(n, -0.4, -1);
                    }
                    break;
            }

            var g = E('g');
            g.dataset.node = n.name;
            g.className = 'box-holder';
            n.box = g;
            autoClearAnim(g);
            var colorize = true;

            if (0) {
                let e = drawSide(s, 3);
                if (colorize) e.style.fill = domainColor(n);
                g.appendChild(e);
            }
            if (1) {
                // << box >>
                let e = drawBox(s, n);
                e.setAttribute('class', 'cell');
                if (colorize) e.style.fill = domainColor(n);
                g.appendChild(e);
            }
            if (1) {
                let e = E('text');
                e.setAttribute('font-size', 0.5 * UPSCALE);
                e.setAttribute('transform', MATRIX);
                e.setAttribute('x', UPSCALE * (n.x - 2.35));
                e.setAttribute('y', UPSCALE * (n.y + 1.35));
                e.textContent = n.name.replace(/\..+/, '');
                flags.appendChild(e);
            }
            if (0) {
                let e = drawFlag(s, n, n.name.replace(/\..+/, ''));
                e.setAttribute('class', 'flag');
                e.setAttribute('stroke', '#999');
                e.setAttribute('fill', domainColor(n));
                flags.appendChild(e);
            }
            layers.boxes.insertBefore(g, layers.boxes.firstChild);
            if (1) {
                let e = H('div');
                e.className = 'node node-' + n.kind;
                pos(e, s);
                if (0) e.textContent = n.name.replace(/\..+/, '');
                e.style.pointerEvents = 'initial';
                $input.nodeClick(e, n.name);
                n.elem = e;
                e.addEventListener('animationend', () => n.elem.style.animation = '');
                phys.appendChild(e);
            }
            if (1) {
                let icon;
                if (n.kind === 'client') {
                    //icon = 'img/client.svg';
                } else if (n.kind === 'service') {
                    icon = 'img/cart.svg';
                }
                if (icon) {
                    let e = H('img');
                    e.src = icon;
                    e.height = UPSCALE * TILE_HALF_HEIGHT + 8;
                    n.elem.appendChild(e);
                }
            }
            if (0 && n.kind === 'service') {
                let icon = 'img/plane.svg';
                let e = H('img');
                e.src = icon;
                e.height = TILE_HALF_WIDTH;
                if (icon == 'img/surf.png') {
                    e.height = 32;
                    pos(e, {x: s.x-TILE_HALF_WIDTH, y: s.y+TILE_HALF_HEIGHT});
                    //e.style.transform = 'rotate(-28deg)';
                    e.style.opacity = 0.4;
                } else {
                    pos(e, s);
                }
                layers.icons.appendChild(e);
            }
        });
    };

    var drawSide = function(s, depth) {
        // TODO: only for edge cells
        var e = E('path');
        e.setAttribute('class', 'side');
        e.setAttribute('d',
            ' M' + (s.x - TILE_HALF_WIDTH) + ' ' + s.y +
            ' L' + s.x                     + ' ' + (s.y + TILE_HALF_HEIGHT) +
            ' L' + (s.x + TILE_HALF_WIDTH) + ' ' + s.y +
            ' L' + (s.x + TILE_HALF_WIDTH) + ' ' + (s.y + depth) +
            ' L' +  s.x                    + ' ' + (s.y + TILE_HALF_HEIGHT + depth) +
            ' L' + (s.x - TILE_HALF_WIDTH) + ' ' + (s.y + depth) +
            'Z'
        );
        return e;
    };

    var drawArrow = function(src, kind, dst, ka) {
        var words = {
            'ask': 'Ask',
            'match': 'Match',
            'deploy': 'Deploy'
        };
        var word = words[kind] || kind;
        var color = logColor[kind];
        var g = E('g');
        if (color === undefined) return g;
        if (ka) {
            g.style.opacity = '0.1';
        }
        var e = E('path');
        var ssrc = iso2screen(src),
            sdst = iso2screen(dst);
        var dx = ssrc.x - sdst.x;
        var dy = ssrc.y - sdst.y;
        var snMag = Math.sqrt(dy * dy + dx * dx) || 0.00000001;
        var nx = dx / snMag;
        var ny = dy / snMag;
        //ssrc.x -= 32 * nx;
        //ssrc.y -= 32 * ny;
        sdst.x += 32 * nx;
        sdst.y += 32 * ny;
        var c = 128; //kind === 'deploy' ? 256 : 128;
        var zx = (c*snMag*0.002) * (-dy / snMag);
        var zy = (c*snMag*0.002) * (dx / snMag);
        var qx = (ssrc.x + sdst.x) / 2 + zx;
        var qy = (ssrc.y + sdst.y) / 2 + zy;
        var d = 'M' + ssrc.x + ' ' + ssrc.y +
            ' Q' + qx + ' ' + qy +
            ' ' + sdst.x + ' ' + sdst.y;

        e.setAttribute('d', d);
        e.setAttribute('marker-end', 'url(#arrow-' + color + ')');
        e.setAttribute('fill', 'none');
        e.setAttribute('stroke', color);
        e.style.stroke = color;
        e.setAttribute('stroke-width', '10');
        g.appendChild(e);
        if (1) {
            let t = E('text');
            t.textContent = word;
            t.setAttribute('font-family', 'monospace');
            t.setAttribute('x', 0.1*ssrc.x + 0.9*sdst.x);
            t.setAttribute('y', 0.1*ssrc.y + 0.9*sdst.y);
            let r = E('rect');
            r.setAttribute('x', t.getAttribute('x') - 4);
            r.setAttribute('y', t.getAttribute('y') - 14);
            r.setAttribute('width', word.length*8 + 8);
            r.setAttribute('height', 20);
            r.setAttribute('fill', 'white');
            r.setAttribute('stroke', 'silver');
            g.appendChild(r);
            g.appendChild(t);
        }
        return g;
    };

    var nodeLog = {};
    var logColor = {'ask': 'blue',
                    'match': 'crimson',
                    'nfv-alive': 'crimson',
                    'deploy': 'green',
                    'redirect': 'green'};
    var expireNodeLog = function() {
        for (var k in nodeLog) {
            if (--nodeLog[k].time < 0) {
                layers.coms.removeChild(nodeLog[k].elem);
                delete nodeLog[k];
            }
        }
    };
    setInterval(expireNodeLog, 500);

    var nlinks = {}, flowElem = {};

    /* ----------------------------------------------------------------------------
     */

    resize();

    return {
        setCamera: function(x, y) {
            camera.x = x;
            camera.y = y;
            resize();
        },
        resize: resize,
        setTopology: function(nodes, links) {
            // TODO: this is finnicky {{{
            iterItems(layers, function(e) {
                e.innerHTML = '';
            });
            flowElem = {};
            nodeLog = {};
            statusIcons = {holders: {}, icons: {}};
            // }}}

            nlinks = isoDrawPaths(layers.net, layers.netico, links);
            flowTicker(layers.particles, nlinks);
            drawNodes(nodes);
        },
        setNodeStatus: function(name, status) {
            var BAD_PULSE = 'infinite alternate stroke-pulse-red';
            var fresh = syncIcons(statusIcons.icons, name, status);
            // <attack indicator vs unhealthy indicator>
            let flash = false, unhealthy = false;
            for (let k in status) {
                if (k.startsWith('sarnet/o/')) {
                    unhealthy = true;
                } else if (k.startsWith('sarnet/a/')) {
                    flash = true;
                }
            }
            let n = $topology.nodes[name];
            if (!n) {
                console.log('setNodeStatus?', name, status);
                return;
            }
            if (flash && unhealthy) {
                n.outline.style.animation = BAD_PULSE;
            } else if (fresh) {
                n.outline.style.animation = '0.5s 4 alternate linear stroke-pulse-green';
                //console.log(name, 'is fresh');
            } else if (n.outline.style.animation === BAD_PULSE) {
                console.log(name, 'bad clear');
                n.outline.style.animation = '';
            }
            //if (n.outline.style.animation.length > 0) {
            //    console.log(name,
            //        n.outline.style.animation,
            //        n.outline.style.animation === BAD_PULSE);
            //}
            n.elem.classList[unhealthy ? 'add' : 'remove']('unhealthy');
        },
        setNodeLinkStatus: function(name, links) {
            let info = $aux[name];
            if (info === undefined) // Something wrong?
                return;
            let policer = links.policer || {};
            let node = $topology.nodes[name];
            if (node === undefined) {
                console.log(name);
                return;
            }
            for (let iface of node.ifaces) {
                if (!iface.links)
                    continue;
                let link = $topology.links[iface.links[0]];
                if (link === undefined)
                    continue;
                if (link.source.kind !== 'service' && link.target.kind !== 'service') {
                    continue;
                }
                console.log('>', node.name);
                if (node.kind === 'service') {
                    console.log('service link', link.name, JSON.stringify(policer));
                    // TODO: there's no guarantee we get iface info before
                    // netctl?
                    let lname = link.name;
                    if (lname === 'link-xo-stitch-3300') {
                        /// Aaaaah
                        p = policer['enp3s0f0.3300'];
                    //} else if (lname === 'link-xo-stitch-521') {
                    //    p = policer.eth2 || policer['stitch-521']; // XXX
                    } else {
                        p = policer[info.iflink[link.name]];
                    }
                    console.log('.. policer', lname, policer, p);
                    let i = nlinks[link.name].icons;
                    if (p === undefined) {
                        i.style.display = 'none';
                    } else {
                        i.style.display = '';
                        i.textContent = p === '' ? '100' : p.replace('Mbit', '');
                        i.style.filter = i.textContent === '100' ? 'invert(100%)' : '';
                    }
                }
            }
        },

        // ----
        setLayerVisible: function(layer, visible) {
            var obj;
            if (layer == 'phys') obj = layers.net;
            else if (layer == 'flows') obj = layers.flow;
            else return;
            obj.style.visibility = visible ? 'visible' : 'hidden';
        },
        setLinkTraffic: function(links) {
            if ($stop) return;
            links.forEach(function(link) {
                var obj = nlinks[link.name];
                if (!obj)
                    return;
                obj.tx.style.stroke = trafficolor(link.tx, max_bandwidth);
                obj.rx.style.stroke = trafficolor(link.rx, max_bandwidth);
            });
        },
        setFlowsLinks: function(links) {
            for (let link in links) {
                let data = links[link];
                let nlink = nlinks[link];
                nlink.rxpool = createFlowPool(data.rx, nlink.rxpool, link);
                nlink.txpool = createFlowPool(data.tx, nlink.txpool, link);
                let state = flowElem[link];
                if (state === undefined) {
                    flowElem[link] = state = {elem: E('g'), paths: {}};
                    state.elem.dataset.link = link;
                    l3flow.appendChild(state.elem);
                }
                state.keep = {};
                drawFlowsSVG(state, data.sender, data.target, data.rx, data.tx);
                for (var k in state.paths) {
                    var z = state.paths[k];
                    if (state.keep[k] === undefined)
                        state.elem.removeChild(z);
                }
                state.paths = state.keep;
            }
        },
        setFlows: function(name, flows) {
            // TODO: we now get flows per client, so this should be optimized
            var v = flowElem[name];
            if (v === undefined) {
                flowElem[name] = v = {elem: E('g'),
                    paths: {}};
                v.elem.dataset.link = name;
                l3flow.appendChild(v.elem);
            }
            if ($stop) return;
            v.keep = {};
            //v.elem.innerHTML = '';
            for (let flow of flows) {
                drawFlowsSVGEx(v, flow);
            }
            for (let k in v.paths) {
                if (v.keep[k] === undefined)
                    v.paths[k].remove();
            }
            v.paths = v.keep;
        },
        nodeAnimate: function(name, animation) {
            var n = $topology.nodes[name];
            n.elem.style.animation = '0.7s 1 alternate ah-' + animation + ' linear';
            n.box.style.animation = '0.7s 1 alternate as-' + animation + ' linear';
        },
        drawNodeLog: function(name, log) {
            if (log.peer === undefined)
                return;
            var node = $topology.nodes[name];
            var key = node.name + ';' + log.peer + ';' + log.kind;
            var arrow = nodeLog[key];
            if (arrow === undefined) {
                var e = drawArrow(node, log.kind, $topology.nodes[log.peer], log.ka === true);
                nodeLog[key] = {elem: e, time: 3};
                layers.coms.appendChild(e);
            } else {
                arrow.time = 3; //
            }
        }
    };
})();


/*
 */
var svgShapes = {
    'blocks': function() {
        return 'M10 10 H20 V20 H10 L10 10 ' +
               'M30 10 H40 V20 H30 L30 10 ' +
               'M30 30 H40 V40 H30 L30 30 ' +
               'M10 30 H20 V40 H10 L10 30';
    },
    'cross': function() {
        return 'M10 10 H40 V40 H10 L10 10 ' +
               'M10 10 L40 40 M10 40 L40 10';
    },
    'tower': function() {
        // TODO: for fake 3D effect we actually have to mess around a bit,
        // give two edge lines a larger stroke-width
        return 'M10 10 H 40 V 40 H 10 L 10 10 ' +
               'M22 22 H 28 V 28 H 22 L 22 22';
    }
};
svgShapes.castle = svgShapes.blocks;

/* Draw flows between two nodes
 * TODO: refactor
 */
var drawFlowsSVG = function(state, sender, target, rx_flows, tx_flows) {
    var s = iso2screen(sender);
    var t = iso2screen(target);

    // Line normal
    var mul = 8;
    var dx = s.x - t.x;
    var dy = s.y - t.y;
    var snMag = Math.sqrt(dy * dy + dx * dx) || 0.00000001;
    var zx = -dy / snMag;
    var zy = dx / snMag;
    var fx = mul * zx;
    var fy = mul * zy;
    zx *= 3;
    zy *= 3;
    var updateFlow = function(key, path, curc, s) {
        var e;
        if (state.keep[key] !== undefined) {
            console.log('Double definition:', key,
                sender.name,
                target.name);
            return;
        } else if (state.paths[key] !== undefined) {
            e = state.keep[key] = state.paths[key];
        } else {
            e = document.createElementNS(SVG, 'path');
            e.dataset.key = key;
            e.style.stroke = flowColor(s);
            var n = 150;
            var t = (Math.random() * n)|0;
            var sz = 2;
            e.style.strokeDasharray = t + ' ' + sz + ' ' + (n - sz - t) + ' 0';
            state.keep[key] = e;
            state.elem.appendChild(e);
        }
        if (e.getAttribute('d') !== path)
            e.setAttribute('d', path);
        e.style.strokeWidth = String(Math.max(curc, 0.8)) + 'px';
    };

    var drawFlowBundle = function(init, start, dest, flows) {
        var c = init;
        // 0 1 2
        // s t b
        flows.forEach(function(flow, i) {
            var s = flow[0];
            var traf = flow[1];
            var bps = flow[2];

            if (bps > _maxbw) _maxbw = bps;
            var curc = Math.max(20 * Math.sqrt(bps / _maxbw), 2);

            c += Math.sqrt(curc);
            var ox = zx * i; // c
            var oy = zy * i;
            var cx = (dest[0] + start[0]) / 2 + ox + (fx * c);
            var cy = (dest[1] + start[1]) / 2 + oy + (fy * c);
            var path = ('M' + (start[0] + ox) + ',' + (start[1] + oy) + ' ' +
                        'Q' + cx + ',' + cy + ' ' +
                        (dest[0] + ox)  + ',' + (dest[1] + oy));
            updateFlow(prefix + traf + '#' + s, path, curc, s);
        });
    };

    // TODO
    var p = sender.name + '#' + target.name + '#';
    var prefix = p + 'rx#';
    drawFlowBundle(1, [s.x, s.y], [t.x, t.y], rx_flows);
    zx = -zx; fx = -fx; zy = -zy; fy = -fy;
    prefix = p + 'tx#';
    drawFlowBundle(1, [t.x, t.y], [s.x, s.y], tx_flows);
};

var drawFlowsSVGEx = function(state, flow) {
    var s = iso2screen(flow.sender);
    var t = iso2screen(flow.target);
    var rx_flows = flow.rx;
    var tx_flows = flow.tx;

    // Line normal
    var mul = 8;
    var dx = s.x - t.x;
    var dy = s.y - t.y;
    var snMag = Math.sqrt(dy * dy + dx * dx) || 0.00000001;
    var zx = -dy / snMag;
    var zy = dx / snMag;
    var fx = mul * zx;
    var fy = mul * zy;
    zx *= 3;
    zy *= 3;
    var updateFlow = function(key, path, curc, _flow) {
        var e;
        if (state.keep[key] !== undefined) {
            console.log('Double definition:', key,
                flow.sender.name,
                flow.target.name);
            return;
        } else if (state.paths[key] !== undefined) {
            e = state.keep[key] = state.paths[key];
        } else {
            e = document.createElementNS(SVG, 'path');
            e.dataset.key = key;
            e.style.stroke = flowColor(_flow);
            e.style.fill = 'transparent';
            var n = 150;
            var t = (Math.random() * n)|0;
            var sz = 2;
            e.style.strokeDasharray = t + ' ' + sz + ' ' + (n - sz - t) + ' 0';
            state.keep[key] = e;
            state.elem.appendChild(e);
        }
        if (e.getAttribute('d') !== path)
            e.setAttribute('d', path);
        e.style.strokeWidth = String(Math.max(curc, 0.8)) + 'px';
    };

    var drawFlowBundle = function(init, start, dest, flows) {
        var c = init;
        flows.forEach(function(flow, i) {
            var curc = 20 * flow.bps /_maxbw;
            curc = Math.max(curc, 2);
            if (flow.bps > _maxbw) _maxbw = flow.bps;
            c += Math.sqrt(curc);
            var ox = zx * i; // c
            var oy = zy * i;
            var cx = (dest[0] + start[0]) / 2 + ox + (fx * c);
            var cy = (dest[1] + start[1]) / 2 + oy + (fy * c);
            var path = ('M' + (start[0] + ox) + ',' + (start[1] + oy) + ' ' +
                        'Q' + cx + ',' + cy + ' ' +
                        (dest[0] + ox)  + ',' + (dest[1] + oy));
            updateFlow(prefix + flow.traf + '#' + flow.s, path, curc, flow);
        });
    };

    // TODO
    var p = flow.sender.name + '#' + flow.target.name + '#';
    var prefix = p + 'rx#';
    drawFlowBundle(1, [s.x, s.y], [t.x, t.y], rx_flows);
    zx = -zx; fx = -fx; zy = -zy; fy = -fy;
    prefix = p + 'tx#';
    drawFlowBundle(1, [t.x, t.y], [s.x, s.y], tx_flows);
};

/*var _blur = document.querySelector('#glow feGaussianBlur');
setInterval(function() {
    var t = now();
    var d = 2 + Math.cos(t);
    _blur.setAttribute('stdDeviation', d.toFixed(2));
}, 100);*/
