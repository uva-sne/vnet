/* jshint esnext: true */
// https://stackoverflow.com/questions/7985805/three-js-mesh-group-example-three-object3d-advanced

function iso2screen(pt) {
    return {x: pt.x, y: pt.y};
}

// TODO:
// - Create "bordered-top box" geometry

var container, stats;
var camera, scene, renderer, objects_ = [];

var BG_COLOR_GL = 0x334d79;
var BG_COLOR = '#ffffff';

var $camera = {
    rotation: 0.875,
    updown: Math.PI/3,
    lastInteraction: 0,
    camX: 0,
    camZ: -1500,
};

var boxTexture = (function() {
    var canvas = document.createElement('canvas');
    canvas.width = 256;
    canvas.height = 256;
    var context = canvas.getContext('2d');
    var texture = new THREE.Texture(canvas);
    //texture.generateMipmaps = false;
    //texture.anisotropy = 8;
    var edge = 8;
    context.fillStyle = '#eeeeee';
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.fillStyle = '#ffffff';
    context.fillRect(edge, edge,
        canvas.width - 2*edge, canvas.height - 2*edge);
    texture.needsUpdate = true;
    return texture;
})();

var raycaster = new THREE.Raycaster();
function click(e) {
    var m = new THREE.Vector2((e.clientX / window.innerWidth) * 2 - 1,
        -(e.clientY / window.innerHeight) * 2 + 1);
    raycaster.setFromCamera(m, camera);
    var intersects = raycaster.intersectObjects(scene.children);

    if (intersects.length > 0) {
        var obj = intersects[0].object;
        var x = obj.position.x / 50;
        var y = obj.position.z / 50;
        if (obj.material.emissive) {
            if (obj.material.emissive.getHex() !== 0)
                obj.material.emissive.setHex(0);
            else
                obj.material.emissive.setHex(0xff0000);
        }
        //console.log(x, ',', y, 'cost', grid[x + ',' + y]);
    }
}

var now = () => performance.now() / 1000;

function setupMovement(c) {
    var moving = false;
    var originX, originY;
    var rotationHold, updownHold;
    var holdX, holdZ;
    var deltaX, deltaY;

    var time;

    document.body.addEventListener('mousedown', (e) => {
        if (moving) return;
        e.preventDefault();
        time = now();
        moving = true;
        originX = e.clientX;
        originY = e.clientY;
        holdX = $camera.camX;
        holdZ = $camera.camZ;
        rotationHold = $camera.rotation;
        updownHold = $camera.updown;
        $camera.lastInteraction = now();
    });

    document.body.addEventListener('mousemove', (e) => {
        if (!moving) return;
        deltaX = e.clientX - originX;
        deltaY = e.clientY - originY;
        $camera.camX = holdX + deltaX;
        $camera.camZ = holdZ + deltaY;
        //$camera.rotation = rotationHold + deltaX / 1000;
        //$camera.updown = clamp(updownHold - deltaY / 1000, 0.1, Math.PI/2);
        $camera.lastInteraction = now();
    });

    _events(document.body, ['mouseleave', 'mouseup'], (e) => {
        if (!moving) return;
        if ((now() - time) < 1.0 && (deltaX + deltaY) < 5) {
            click(e);
        }
        moving = false;
        $camera.lastInteraction = now();
    });
}

var shapes = {
    router: () => 'router',
    tower: () => 'tower',
    'switch': () => 'switch',
};

var $layers = {};

// Prepare a cube layer
var createLayer = function(n) {
    $layers[n] = {
        level: n,
        nodes: [],
        paths: [],
    };
    return $layers[n];
};


// Draw path elements onto layer
var drawPaths = function(layer, links) {
    var sz = 2;

    var doMess = function(path, mat) {
        /*
        var x1, y1;
        var x2, y2;
        var d;
        var space = 0.05;

        x1 = a.x;
        x2 = b.x;
        y1 = a.y;
        y2 = b.y;

        if (a.x === b.x) {
            d = a.y < b.y ? 1 : -1;
            x1 += s * space * d;
            x2 += s * space * d;
            if (d === s) {
                y1 += space;
                y2 += space;
            } else {
                y1 -= space;
                y2 -= space;
            }
        } else {
            d = a.x < b.x ? 1 : -1;
            y1 += s * space * d;
            y2 += s * space * d;
            if (d === s) {
                x1 += space;
                x2 += space;
            } else {
                x1 -= space;
                x2 -= space;
            }
        }

        var w = Math.abs(x1 - x2) * CELL;
        var h = Math.abs(y1 - y2) * CELL;
        var geometry = new THREE.BoxGeometry(w ? w : sz, sz, h ? h : sz);
        var pipe = new THREE.Mesh(geometry, mat);
        pipe.position.y = layer.level * 15 + 0.25 + sz;
        pipe.position.x = Math.min(x1, x2) * CELL + w/2;
        pipe.position.z = Math.min(y1, y2) * CELL + h/2;
        scene.add(pipe);
        */
        for (var i = 1; i < path.length; i++) {
            var x1 = path[i - 1].x, x2 = path[i].x;
            var y1 = path[i - 1].y, y2 = path[i].y;
            var w = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2)) * CELL;
            var h = sz;
            var geometry = new THREE.BoxGeometry(w, h ? h : sz, sz);
            var pipe = new THREE.Mesh(geometry, mat);
            pipe.position.x = x1*CELL;
            pipe.position.y = 0;
            pipe.position.z = y1*CELL;
            pipe.lookAt(new THREE.Vector3(y2*CELL, 0, x2*CELL));
            pipe.position.x = (x1 + x2) / 2 * CELL + w/2;
            pipe.position.y = 0;
            pipe.position.z = (y1 + y2) / 2 * CELL + h/2;
            scene.add(pipe);
        }
    };

    console.log(links);
    for (let link of links) {
        var path = link.path;
        var rx = new THREE.MeshLambertMaterial({color: 0x111111});
        var tx = new THREE.MeshLambertMaterial({color: 0x111111});
        _link_map[link.name] = {rx: rx, tx: tx};
        // Multipipe
        var lr = paraline(link.path);
        doMess(lr.left, rx);
        doMess(lr.right, tx);
    }
};

var _link_map = {};
var _link_max = 20000000;

var shapeSideMaterial = new THREE.MeshLambertMaterial({color: 0x5599CC});
//var shapeMaterial = new THREE.MeshLambertMaterial({color: 0xFFFFFF});
var shapeMaterial = new THREE.MeshLambertMaterial({map: boxTexture});

var addShape = function(node, cube) {
    var geometry = new THREE.BoxGeometry(20, 3, 20);
    var s/* = new THREE.Mesh(geometry, [
        shapeSideMaterial,
        shapeSideMaterial,
        shapeMaterial,
        shapeSideMaterial,
        shapeSideMaterial,
        shapeSideMaterial,
    ])*/;
    s = new THREE.Mesh(geometry, shapeMaterial);
    s.position.x = 0;
    s.position.y = 1.5 + 1.5;
    s.position.z = 0;
    cube.add(s);
};


// TODO: BufferGeometry

var drawNodes = function(layer, nodes) {
    layer.nodes = [];

    var c;
    switch (layer.level) {
        case 0: c = 0x394b69; break;
        case 1: c = 0x6396ee; break;
        default: c = 0xffffff; break;
    }

    var geometry;
    var rotate = false;
    if (layer.level === 0) {
        geometry = new THREE.PlaneGeometry(CELL, CELL);
        rotate = true;
    } else {
        geometry = new THREE.BoxGeometry(CELL, 3, CELL);
    }

    iterItems(nodes, function(n) {
        var material = layer.level >= 2 ?
            new THREE.MeshLambertMaterial({map: boxTexture}) :
            new THREE.MeshLambertMaterial({color: c});
        var cube = new THREE.Mesh(geometry, material);
        var bx = (n.x|0) * CELL;
        var bz = (n.y|0) * CELL;
        cube.position.y = layer.level * 15;
        cube.position.x = bx;
        cube.position.z = bz;
        if (rotate) cube.rotation.x = -Math.PI/2;

        if (n.shape !== undefined)
            addShape(n, cube);

        // TODO: remove old crap from scene
        scene.add(cube);
        if (layer.level >= 2)
            objects_.push(cube);
        nodes.push(cube);
    });
};

var $render = (function() {
    var onWindowResize;

    container = document.createElement('div');
    document.body.appendChild(container);

    //setupMovement(container);

    if (0) {
        var frustum = 1000;
        var aspect = window.innerWidth / window.innerHeight;
        camera = new THREE.OrthographicCamera(frustum * aspect / - 2,
                                              frustum * aspect / 2,
                                              frustum / 2,
                                              frustum / - 2,
                                              1, frustum);
        onWindowResize = () => {
            var aspect = window.innerWidth / window.innerHeight;
            camera.left   = - frustum * aspect / 2;
            camera.right  =   frustum * aspect / 2;
            camera.top    =   frustum / 2;
            camera.bottom = - frustum / 2;
            camera.updateProjectionMatrix();
            renderer.setSize(window.innerWidth, window.innerHeight);
        };
    } else {
        camera = new THREE.PerspectiveCamera(45,
            window.innerWidth / window.innerHeight,
            1, 5000);
        onWindowResize = () => {
            camera.aspect = window.innerWidth / window.innerHeight;
            camera.updateProjectionMatrix();
            renderer.setSize(window.innerWidth, window.innerHeight);
        };
    }

    scene = new THREE.Scene();

    // Create arbitrary link
    var links = [];

    var stringify = (v) => {
        v = String(v);
        if (!/\./.test(v))
            v += '.0';
        return v;
    };

    var shaderPlane = () => {
        var blocks = stringify((size * 2) / step);
        var blocksize = stringify(step);
        var material = new THREE.ShaderMaterial({
            vertexShader: document.getElementById('traffic-shader-vert').textContent,
            fragmentShader:
                '#define BLOCKS '+ blocks + '\n' +
                '#define BLOCKSIZE '+ blocksize + '\n' +
                document.getElementById('traffic-shader-frag').textContent,
        });
        var geometry = new THREE.PlaneGeometry(size2, size2);
        var mesh = new THREE.Mesh(geometry, material);
        mesh.position.x = 0;
        mesh.position.y = 0;
        mesh.position.z = 0;
        mesh.rotation.x = Math.PI / 2;
        return mesh;
    };

    //scene.add(canvasPlane());

    // Lights
    var ambientLight = new THREE.AmbientLight(0x202020);
    scene.add(ambientLight);

    var directionalLight = new THREE.DirectionalLight(0xDDDDDD);
    directionalLight.position.x = 0.2;
    directionalLight.position.y = 0.8;
    directionalLight.position.z = 0.0;
    directionalLight.position.normalize();
    scene.add(directionalLight);

    renderer = new THREE.WebGLRenderer({antialias: true});
    console.log('Max anisotropy:', renderer.getMaxAnisotropy());
    renderer.setClearColor(BG_COLOR_GL);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);

    window.addEventListener('resize', onWindowResize, false);
    animate();

    var layer = createLayer(1);
    var ll = createLayer(0);

    return {
        setCamera: function(x, y) {
            $camera.camX = x - 960;
            $camera.camZ = y - 1400;
        },
        setTopology: function(nodes, links) {
            drawNodes(layer, nodes);
            drawPaths(ll, links);
        },
        setNodeStatus: function() {
        },
        setNodeLinkStatus: function() {
        },
        setFlowsLinks: function() {
        },
        setLinkTraffic: function(links) {
            links.forEach(function(link) {
                var texs = _link_map[link.name];
                if (!texs) return;
                _link_max = Math.max(link.tx, _link_max);
                _link_max = Math.max(link.rx, _link_max);
                texs.tx.color.setHSL(trafficolor(link.tx, _link_max), 0.5, 0.5);
                texs.rx.color.setHSL(trafficolor(link.rx, _link_max), 0.5, 0.5);
            });
        },
        setFlows: function() {
        },
        setNodeMetadata: function() {
        }
    };
})();


function animate() {
    requestAnimationFrame(animate);
    render();
}


function render() {
    // Actually, the X and Z need to move under a fixed angle
    // TODO; use for lookAt
    var ang = 135*Math.PI/180;
    var s = 0.7;

    var lookAt = new THREE.Vector3(0, 0, 0);
    lookAt.x = camera.position.x = s * ($camera.camX * Math.cos(ang) - $camera.camZ * Math.sin(ang));
    lookAt.z = camera.position.z = s * ($camera.camX * Math.sin(ang) + $camera.camZ * Math.cos(ang));
    camera.position.y = 800;
    lookAt.y = camera.position.y - 70;
    lookAt.x -= 50;
    lookAt.z -= 50;
    //lookAt.y = -100;
    camera.lookAt(lookAt);
    camera.updateMatrixWorld();
    renderer.render(scene, camera);
}
