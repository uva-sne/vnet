* Remove explicit uva allocation from sc16 slice JSON
* Fix site name inheritance in sc16 slice JSON
    * Ask or set xo-request default site
* Remove source map option from npm compress
