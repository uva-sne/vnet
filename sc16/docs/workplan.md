% SC16 workplan
% Ben de Graaff; Ralph Koning

This document describes the goals and planning for the SARNET SC16 demo.


# SARNET goals


## Data visualization

A multiscreen visualization, using a tablet for user interaction
and display(s) for additional/supporting visualizations.

Part of the visualization will focus on traffic flow aggregates.
We believe flow information will help the user (at least visually)
*and* the SARNET agent to better trace the origin within the network
of certain attacks.
Traffic flows will be aggregated based on the flow subnet ("domain")
and kind (ICMP/UDP/TCP).
Additional visualization will be added for supporting metrics -- not
necessarily pertaining to the network state --
such as CPU utilization,
and relevant application metrics.

With the demo focusing much more on automated responses,
it is not yet clear what kind of user interaction is useful (and engaging)
for the long term.
We will focus on allowing the user to control (parameterized) attacks
and selecting *potential* solutions (e.g. a user controlled mapping
between detected "attack types" and solutions),
and then having an automated agent
choose the appropriate solution.
For more information, see the scenarios section.

Topology visualization:

* Flows and bandwidth
* Show the various domains
    * Edge/uncontrollable domains shown as "cloud" with limited information
    * Optionally limit visualization to "active domain" only

<!--
* TBD, side-by-side view of physical topology and "flow based" topology?
-->

Network statistics:

* Throughput (total and per aggregate flow)
* Domain
* Traffic type
* Utilization (%)
* Packet loss (Hard to observe on ExoGENI?)
* Latency (?)

Security statistics:

* IDS detected events
* CPU usage/thresholds
* Honeypot metrics (see scenarios)
* Filter statistics (?)


## Network topology

The network consists of two primary domains: a service domain running
an application and a provider domain running routers.
The routing domain is connected to additional domains, consisting
of routers with numerous peers (simulated on the node).

The service domain consists of nodes linked to a switch in a star topology.
The switch runs OpenVSwitch and is controlled using OpenFlow.
One of the nodes runs a container "cluster", which is explained in more detail
below.
The OVS node will export sFlow data to the VNET controller.

The routers will run (TBD) Quagga for BGP and host-sflow to export flow
information.
If it is feasible to build a UI for this, the user will be allowed to influence
routing decisions.

This topology will be deployed as a slice on a single ExoGENI site.

<!--
Each OVS node will connect to our OpenFlow controller.

Each OVS node (and router?) will export sFlow data to the VNET controller.
-->




*Notes*:
we cannot currently use ExoGENI's OpenFlow functionality because it uses
(an old version of) FlowVisor which requires some subtle changes to flow
table management and packet parsing in the controller (also making it
incompatible with ONOS).
In addition, the OpenFlow capable switch at UvA only supports OF1.0.

We'll have to experiment with the sample rate for sFlow.

TBD: maximum link bandwidth between nodes and OVS


### OpenFlow controller

We will build an OpenFlow controller to manage the service topology.
It will be instructed with basic topology information when
it is started, such as the IP addresses and names of all nodes in the network,
and the (initial) network topology.

Basic functionality:

* Create flow entries for host-to-host communication
* Handle ARP traffic in the controller
* Traffic engineering: allow flows to be mirrored to or redirected through a
  node (container NFV)
    * Encapsulate in a VLAN
    * Dynamically add/remove these redirections


The controller itself has two phases:

1. Discovery: learn OpenFlow ports, IP <-> MAC relations
2. Normal operation: create/maintain OpenFlow rules for normal traffic


Flow rules will be MAC based to allow for IP spoofing between nodes.


*Note*:
In the future this could be replaced with e.g. ONOS or another provisioning
system,
but because the traffic redirection rules
can be hard to implement this is currently considered as future work.


## Container NFV

A container "cluster" that contains multiple containers that perform some
NFV.
Traffic to this cluster node will be tagged with a particular VLAN,
with each container having a unique VLAN ID.

* TBD: Container for traffic analysis (alternative: OpenFlow sample rules)
    * Traffic mirroring

* Container for traffic scrubbing
    * Requires transparent traffic redirection through a node (XXX: transparent
      redirection is difficult to implement)
    * Can be implemented such that the user must first complete a CAPTCHA,
      after which a secure cookie will be set; traffic without this cookie will
      not pass through

In theory these NFV functions can be applied per domain.

<!--
* Run OVS on the container host and do OpenFlow there (complexity?)
-->

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

# Scenarios

## CPU load attack

The application running in the service domain has a set of pages with varying
CPU cost.
The attacker continuously requests high CPU cost pages.

* Metric exposed by node: CPU utilization

Solution:

* Block/rate limit traffic from attacker IP (upstream)
* CAPTCHA


## Password bruteforce

* Initial metric exposed by application: password attempts/second
* Attach IDS to get insight into attack (dictionary attack, linear guessing?)
    * Determine attacker IP

Solution:

* Redirect *attacker* to honeypot

Note:

* CAPTCHA might work here as well, but one could argue that this would
  unnecessarily negatively impact "customer satisfaction"


## Application crash

Based on the recent BIND nameserver single-packet DoS attack.
Attacker will send crash packets which can be seen by the application having
small periods of downtime in its metrics graph.

Solution:

* Block attacker IP
* Filter bad packets


## DDoS

Existing attack scenario, like last year.



# Automated responses

The data gathered within this SARNET is sufficient for some basic automated
responses.
It is up the to SARNET agent to detect the type of attack (scenario),
which nodes are performing the attack,
and what the appropriate solution is.

TBD: Ralph?


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

# Planning

Intended deadlines per component.


**Oct 10 - Oct 14**

* Confirm/adjust goals
* Finalize scenarios

**Oct 17 - Oct 21**

* OpenFlow controller fully functional
* Container NFV networking operational
* Start exporting sFlow

**Oct 24 - Oct 28**

* Flow collection and visualization

**Oct 31 - Nov 4**

* Container NFV fully functional
* Traffic generator fully functional

**Nov 7 - Nov 11**

* Demo packaging/container done
