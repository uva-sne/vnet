#!/bin/sh
cat <<VNET_CONFIG > /etc/.vnet-config
vnetmon_server=wss://{{ controller_fqdn }}:8500/
openflow_controller=tcp:{{ controller_ip }}:6653
{% if '.edge' in node.name %}
edge_ips=1
{% endif %}
{% if '.core.' in node.name %}
hsflowd=1
{% endif %}
sflow_ip={{ controller_ip }}
sflow_port=8503
vnet_node_id={{ node.id }}
{% for v in node.config %}
{{ v }}
{% endfor %}
VNET_CONFIG
mv /etc/.vnet-config /etc/vnet-config
vnet-vm-install
