/*! tuiojs:
 * Copyright (c) 2009 Fajran Iman Rusadi <fajran@gmail.com>
 * https://github.com/fajran/tuiojs
 *
 * Modified by:
 * Copyright (c) 2015 Ben de Graaff <ben@nx3d.org>
 *
 * Available under the MIT license.
 */
var tuio_callback = (function() {
    var TOUCH_START = 3;
    var TOUCH_MOVE = 4;
    var TOUCH_END = 5;

    var cursors = [];
    var touches = {};

    var createEvent = function(name, touch) {
        var evt = document.createEvent('CustomEvent');
        evt.initEvent(name, true, true);
        evt.touches = cursors;
        evt.targetTouches = getTargetTouches(touch.target);
        evt.changedTouches = [touch];
        evt.pageX = evt.screenX = touch.screenX;
        evt.pageY = evt.screenY = touch.screenY;

        if (touch.target) {
            touch.target.dispatchEvent(evt);
        } else {
            document.dispatchEvent(evt);
        }
    };

    var getTargetTouches = function(element) {
        var targetTouches = [];
        cursors.forEach(function(touch) {
            if (touch.target == element) {
                targetTouches.push(touch);
            }
        });
        return targetTouches;
    };

    var cursor = function() {
        var c = document.createElement('div');
        c.style.position = 'absolute';
        c.style.height = '2px';
        c.style.width = '2px';
        c.style.background = '#222';
        //c.style.background = 'yellow';
        return c;
    };

    var cursorMove = function(c, x, y) {
        //console.log([x,y]);
        c.style.left = String(x) + 'px';
        c.style.top = String(y) + 'px';
    };

    return function(type, sid, fid, x, y, angle) {
        var data;

        if (type == TOUCH_START) {
            touches[sid] = data = {
                sid: sid,
                fid: fid,
                identifier: sid,
                cursor1: cursor(),
                cursor2: cursor(),
            };

            document.body.appendChild(data.cursor1);
            document.body.appendChild(data.cursor2);
        } else {
            data = touches[sid];
            if (!data)
                return;
        }

        data.screenX = (window.screen.width * x)|0;
        data.screenY = (window.screen.height * y)|0;

        // TODO: page includes scroll offset
        data.clientX = data.pageX = data.screenX + window.screenX;
        data.clientY = data.pageY = data.screenY + window.screenY;

        cursorMove(data.cursor1, data.clientX, data.clientY - 28);
        cursorMove(data.cursor2, data.clientX, data.clientY + 28);

        switch (type) {
            case TOUCH_START:
                data.target = document.elementFromPoint(data.pageX, data.pageY);
                cursors.push(data);
                createEvent('touchstart', data);
                break;

            case TOUCH_MOVE:
                createEvent('touchmove', data);
                break;

            case TOUCH_END:
                cursors.splice(cursors.indexOf(data), 1);
                createEvent('touchend', data);
                break;

            default:
                break;
        }

        //if (type == TOUCH_START) {
        //    console.log('Touch', sid, data.target);
        //}

        if (type == TOUCH_END) {
            try { document.body.removeChild(data.cursor1); } catch (e) {console.log(e); }
            try { document.body.removeChild(data.cursor2); } catch (e) {console.log(e); }
            delete touches[TOUCH_END];
        }
    };
})();

/*
(function() {
    setTimeout(function() { tuio_callback(3, 100, 100, 0.5, 0.5, 0); },  500);
    setTimeout(function() { tuio_callback(4, 100, 100, 0.5, 0.3, 0); }, 1000);
    setTimeout(function() { tuio_callback(4, 100, 100, 0.5, 0.4, 0); }, 1500);
    setTimeout(function() { tuio_callback(4, 100, 100, 0.5, 0.3, 0); }, 2000);
    setTimeout(function() { tuio_callback(4, 100, 100, 0.6, 0.3, 0); }, 2500);
    setTimeout(function() { tuio_callback(4, 100, 100, 0.6, 0.4, 0); }, 3000);
    setTimeout(function() { tuio_callback(5, 100, 100, 0.5, 0.3, 0); }, 3500);
})();
*/

var IS_TUIO_CLIENT = false;
(function() {
    if (!navigator.plugins['Tuio Client']) {
        return;
    }

    IS_TUIO_CLIENT = true;
    console.log('[tuio] Initializing', window.screen);
    var body = document.querySelector('body');
    body.style.cursor = 'none';

    var obj = document.createElement('object');
    obj.type = 'application/x-tuio';
    obj.style.position = 'absolute';
    obj.style.visibility = 'hidden';
    obj.style.height = '0';
    obj.style.width = '0';
    obj.style.left = '-100px';
    obj.style.top = '-100px';
    document.body.appendChild(obj);
})();
