var $composer = (function() {
    var composer = document.getElementById('ui-orchestrate');
    var area = composer.querySelector('.compose-area');
    var submit = document.getElementById('btn-orchestrate-submit');

    var tpl_builder_list = /*Handlebars.compile(
            '<h3>{{desc}}</h3>' +
            '<div class="enum-list">' +
            '{{#each values}}<div data-key="{{../name}}" data-option="{{name}}" class="enum-option">{{desc}}' +
            '{{#if color}} <span style="color: {{color}}; float: right;">•</span>{{/if}}' +
            '</div>{{/each}}' +
            '</div>')*/1;

    var props = [
        {'desc': 'Location',
         'name': 'location',
         'values': [
            {'color': 'uva', 'name': 'uva', 'desc': 'University of Amsterdam (Amsterdam, NL)'},
            {'color': 'ciena', 'name': 'ciena', 'desc': 'Ciena (Ottawa, CA)'},
            {'color': 'starlight', 'name': 'starlight', 'desc': 'StarLight (Chicago, US)'}
         ]},
        {'desc': 'Type',
         'name': 'type',
         'values': [
            {'name': 'router', 'desc': 'Router'},
            {'name': 'switch', 'desc': 'Switch'},
            /*{'name': 'node', 'desc': 'App server'}
         ]},
        {'desc': 'Apps',
         'name': 'apps',
         'change': function(state) {
             if (state.type == 'app') state.apps = [];
         },
         'select': function(state) {
             if (state.type == 'app') return true;
             return false;
         },
         'values': [*/
            {'name': 'screen', 'desc': 'Tile display'},
            {'name': 'sage2', 'desc': 'SAGE2 server'},
            {'name': 'iperf', 'desc': 'iPerf'},
            //{'name': 'gridftp', 'desc': 'GridFTP'}
         ]}
    ];

    //////
    var selection = null;
    var handler = {
        selectNode: function(layer, node, elem) {
            if (selection === null) {
                selection = node;
                layer.view.selectNames([node.name]);
            } else {
                var valid_select = true;
                if (node.name == selection.name) {
                    valid_select = false;
                } else {
                    var sne = selection.kind == 'switch' || selection.kind == 'router';
                    var tne = node.kind == 'switch' || node.kind == 'router';

                    if (sne && tne) {
                        // ok
                    } else if (selection.group != node.group) {
                        console.log('Group mismatch');
                        valid_select = false;
                    } else if (!sne && !tne) {
                        console.log('VM2VM');
                        valid_select = false;
                    }
                }
                if (!valid_select) {
                    selection = null;
                    layer.view.selectNames([]);
                    return;
                }
                var link = {'name': node.name < selection.name ? node.name + '/' + selection.name : selection.name + '/' + node.name,
                            'status': 'active',
                            'source': selection.name,
                            'target': node.name};
                selection = null;
                layer.view.selectNames([]);
                if (!_$.find(graph.links, 'name', link.name)) {
                    graph.links.push(link);
                    layer.data.updateLayer(_$.deepcopy(graph));
                    layer.view.redraw();
                }
            }
        },
        clickNode: function() {},
        clickLink: function() {}
    };

    var menu_cor = 400;
    var size = {width: window.innerWidth - menu_cor, height: window.innerHeight};
    var graph = {name: 'x-compose-layer',
                 description: 'Virtual layer for composing new topologies',
                 nodes: [],
                 links: []};

    var layer = {
        data: vnet.LayerData(graph),
        btn: null,
    };
    layer.view = LayerView(graph.name, area, layer.data, layer, $size, handler);

    var node_id = 1;
    layer.view._svg.on('click', function() {
        console.log(d3.event);
        if (d3.event.target.tagName != 'svg')
            return;
        if (d3.event.x < menu_cor)
            return; // XXX: 
        if (!state.location) return;
        if (!state.type) return;
        console.log('Thing', arguments, d3.event, d3.event.x, d3.event.y, d3.event.target.tagName);

        var kind = 'node';
        var name = 'Host';
        if (state.type == 'switch') { kind = 'switch'; name = 'Switch'; }
        else if (state.type == 'router') { kind = 'router'; name = 'Router'; }
        else if (state.type == 'sage2') { name = 'Sage'; }
        var n = {'name': name + node_id,
                 'group': state.location,
                 'kind': kind,
                 'image': state.type,
                 'sx': (d3.event.pageX ? d3.event.pageX : d3.event.x) - menu_cor,
                 'sy': (d3.event.pageY ? d3.event.pageY : d3.event.y),
                 'metadata': {
                     'location': state.location,
                     'type': state.type}};
        node_id += 1;
        console.log(n);
        graph.nodes.push(n);
        layer.data.updateLayer(_$.deepcopy(graph));
        layer.view.redraw();
        //state.location = undefined;
        state.type = undefined;
        sync_options();
    }, 'click', 'touchstart');
    //////
    _$.events(submit, function() {
        if (graph.nodes.length)
            $com.send('layer:orchestrate', graph);
        /* TODO: wait for confirm */
        $ui_toggler.graph();
    }, 'click', 'touchstart');
    //////
    var builder_menu = composer.querySelector('.builder-menu');
    var builder_list = '';
    props.forEach(function(v) {
        builder_list += tpl_builder_list(v);
    });
    builder_menu.innerHTML = builder_list;
    var options = builder_menu.querySelectorAll('.enum-option');
    var state = {};
    var sync_options = function() {
        _$.forEachObj(state, function(v, k) {
            var p = _$.find(props, 'name', k);
            var items = builder_menu.querySelectorAll('[data-key=' + k + ']');
            _$.forEach(items, function(o) {
                var a = o.getAttribute('data-option');
                if (state[k] == a)
                    o.classList.add('enum-option-selected');
                else
                    o.classList.remove('enum-option-selected');
            });
        });
    };
    var clicker = function(e) {
        console.log('Event', e.type);
        var key = e.target.getAttribute('data-key');
        var option = e.target.getAttribute('data-option');
        var p = _$.find(props, 'name', key);
        state[key] = option;
        sync_options();
    };
    for (var i = 0; i < options.length; i++) {
        _$.events(options[i], clicker, 'click', 'touchstart');
    }
    return {
        reset: function() {
            if (!graph.nodes.length && !graph.links.length)
                return;

            node_id = 1;
            graph.nodes = [];
            graph.links = [];
            layer.data.updateLayer(_$.deepcopy(graph));
            layer.view.redraw();
        }
    };
})();
