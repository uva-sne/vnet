/* Debug status indicators
 */
var $status = (function() {
    var err = document.getElementById('error-label');
    var label = document.getElementById('status-label');

    _$.events(err, function(e) {
        e.preventDefault();
        err.innerHTML = '';
        err.classList.add('hidden');
    }, 'click', 'touchstart');

    var i = 0;
    _$.events(label, function(e) {
        e.preventDefault();
        if (++i > 5) {
            window.location.reload();
            label.style.background = '#cc0000';
        }
    }, 'click', 'touchstart');

    setInterval(function() {
        if (i > 0) i--;
    }, 1000);

    return {
        debug: function() {
            var args = [];
            args.push.apply(args, arguments);
            label.textContent = args.join(' ');
        },
        error: function(msg) {
            var p = document.createElement('p');
            p.textContent = msg;
            err.appendChild(p);
            err.classList.remove('hidden');
        }
    };
})();

/**
 * Logo rotator
 */
/*(function() {
    var delay = 0, i = 0;
    var logos = ['static/logo/uva-dark.jpg',
        'static/logo/ciena-small.png',
        'static/logo/klm-small.png',
        'static/logo/tno-small.png'
    ];
    var pos = ['33% 99%', '11% 99%', '11% 99%', '11% 99%'];
    var barleft = document.getElementById('bar-left');
    setInterval(function() {
        barleft.style.backgroundImage = 'url(' + logos[i] + ')';
        barleft.style.backgroundPosition = pos[i];
        var maxdelay = (i === 0) ? 3 : 2;
        delay += 1;
        if (delay < maxdelay)
            return;
        i = (i + 1) % logos.length;
        delay = 0;
    }, 3000);
    var _logos = document.getElementById('logos');
    _$.events(_logos, function() {
        logos.style.display = 'none';
    }, 'click', 'touchstart');
})();*/

/**
 * Primary UI toggle
 */
/*var $ui_toggler = (function() {
    var ui_compose = document.getElementById('ui-orchestrate');
    var ui_graph = document.getElementById('ui');

    var show_graph = function() {
        ui_graph.classList.remove('hidden');
        ui_compose.classList.add('hidden');
    };
    _$.events(document.getElementById('btn-ui-orchestrate'), function() {
        $composer.reset();
        ui_graph.classList.add('hidden');
        ui_compose.classList.remove('hidden');
    }, 'click', 'touchstart');
    _$.events(document.getElementById('btn-ui-graph'), show_graph, 'click', 'touchstart');
    return {graph: show_graph};
})();*/


/* --- */

var stats_table = function(keys, values) {
    var t = document.getElementById('stats-table');
    if (!t) {
        t = document.createElement('div');
        t.style.height = '300px';
        t.style.overflow = 'auto';
        t.style.position = 'absolute';
        t.style.fontSize = '12px';
        t.style.left = '10px';
        t.style.top = '0';
        t.style.background = '#000';
        t.style.padding = '4px';
        t.id = 'stats-table';
        document.body.appendChild(t);
    }
    var h = '';
    h += '<table border="1" cellspacing=0 cellpadding=0>';
    var header = ['<tr>'];
    for (var i = 0; i < keys.length; i++) {
        header += '<td>'+keys[i]+'</td>';
    }
    header += '</tr>';
    var rows = [];
    for (var k in values) {
        var vrow = values[k];
        var row = '<tr>';
        for (i = 0; i < vrow.length; i++) {
            var v = vrow[i];
            if (keys[i].substr(-1,1) == 'x') v = vnet.utils.speed(v*8);
            row += '<td style="min-width:5em;text-align:right;">'+v+'</td>';
        }
        row += '</tr>';
        rows.push(row);
    }
    rows.sort();
    h += header + rows.join("\n");
    h += '</table>';
    t.innerHTML = h;
};

/* --- */

var $revenue_chart = null;
(function() {
    var N = 64;
    var w = 360;
    var h = 200;

    $revenue_chart = (function() {
        var values1 = _$.zeroes(N);
        var values2 = _$.zeroes(N);
        var cfg = {};
        var chart = vnet.StackChart(document.querySelector('#sc15-revenue'),
                cfg,
                [{color: 'hsl(200, 70%, 50%)',
                  fill:  'hsl(200, 70%, 20%)',
                  label: 'Server 2',
                  values: values2},
                 {color: 'hsl(280, 70%, 50%)',
                  fill:  'hsl(280, 70%, 20%)',
                  label: 'Server 1',
                  values: values1}],
                  N, w, h);
        return function(v1, v2) {
            var v = v1 + v2;
            if (v > cfg.vmax) cfg.vmax = v;
            values1.shift();
            values1.push(v1);
            values2.shift();
            values2.push(v2);
            chart();
        };
    })();
})/*()*/;

/* --- */
var $slice_summary = document.getElementById('slice-summary');
setInterval(function() {
    var stats = (function() {
        var layer = $layers.getActiveLayer();
        if (!layer)
            return;

        var MBIT = 1000000;
        var sum = 0,
            nf_cost = 0,
            usage = 0,
            loss = 0;
        layer.data.graph.links.forEach(function(v) {
            if (v._is_up || v._is_up === undefined) {
                var cr = v._cost_rate;
                if (!cr) cr = vnet.DEFAULT_LINK_RATE;
                sum += cr;
                usage += v._latest_rx + v._latest_tx;
                loss += v._loss_rx + v._loss_tx;
            }
            if (v.metadata.filter) {
                nf_cost += 1;
            }
        });

        return {
            sales: String(layer.data.service_revenues.reduce(function(a, b) {
                return a + b;
            }) | 0),
            default_cost: 4 * 100 + (layer.data.graph.links.length - 4) * 50,
            cost: sum / MBIT + nf_cost * 500,
            bandwidth: sum * 2,
            usage: usage,
            loss: loss
        };
    })();
    if (!stats) return;

    var extra_cost = '';
    if (stats.default_cost != stats.cost) {
        if (stats.cost > stats.default_cost) {
            extra_cost = ' + $' + String(stats.cost - stats.default_cost);
        } else {
            extra_cost = ' - $' + String(stats.default_cost - stats.cost);
        }
    }
    var summary = Handlebars.templates.sliceSummary({
        'Service revenue': stats.sales + ' (sales per second)',
        'Network cost': '$' + (stats.default_cost * 10) + extra_cost,
        Bandwidth: vnet.utils.speed(stats.bandwidth),
        Usage: vnet.utils.speed(stats.usage),
        Loss:  vnet.utils.speed(stats.loss)
    });
    $slice_summary.innerHTML = summary;
}, 1500);

//vnet.TUIO();

var lastMessage = performance.now();
setInterval(function() {
    var n = performance.now() - lastMessage;
    if (n > 750) {
        $status.debug('Timeout ' + (n|0));
        //$com.close();
    }
}, 500);

var _h = true;
var $com = vnet.WSUI(vnet.config.server, function(kind, data) {
    lastMessage = performance.now();
    var parseOwner = function(p) {
        p = p.split(':');
        return {layer: p[1], node: p[2].replace(/^node=/, '')};
    };
    switch (kind) {
    case 'layer:stats':
    case 'link:stats':
        //stats_table(data.table, data.values);
        $layers.run('stats', data);
        $infoDisplay.updateStats();

        //if (_h) {
        //    var layer = $layers.getActiveLayer();
        //    $mode.clickLink(layer, layer.data.getLink('Link25'));
        //    _h = false;
        //}
        break;

    case 'layer:metadata':
        $layers.run('metadata', data);
        $infoDisplay.updateMetadata();
        //var rvs = $layers.getActiveLayer().data.service_revenues;
        //$revenue_chart.apply($revenue_chart, rvs);
        break;

    case 'link:update':
        $layers.run('updateLink', data);
        break;

    case 'node:update':
        $layers.run('updateNode', data);
        break;

    case 'layer:update':
    case 'topology:layer':
        $layers.updateLayers([data]);
        break;

    case 'layer:scenario':
        //$layers.updateScenario(data.layer, data.scenario, data.state);
        //$scenarioManager.setState(data.layer, data.scenario, data.state, data.revenue);
        break;

    case 'layers:refresh':
        $layers.updateLayers(data.layers);
        break;

    case 'error':
        console.log('Server reports error:', data.msg);
        $status.error(data.msg);
        break;

    case 'refresh':
        window.location.reload();
        break;

    case 'flows':
        var layer = $layers.getActiveLayer();
        if (layer) layer.view.flows(data || []);
        break;

    case 'meta':
        var objects = [];
        _$.forEachObj(data, function(v, k) {
            var owner = parseOwner(k);
            var keys = [];
            var values = [];
            _$.forEachObj(v, function(v, k) {
                keys.push(k);
                values.push(v);
            });
            var layer = $layers.getLayer(owner.layer);
            if (layer) layer.data.meta(owner.node, keys, values);
            objects.push(owner);
        });
        $infoDisplay.updateMetadata(objects);
        break;

    case 'm':
        var owner = parseOwner(data.id);
        var keys = data.k;
        var values = data.v;
        layer = $layers.getLayer(owner.layer);
        objects = [];
        if (layer) {
            layer.data.meta(owner.node, keys, values);
            $infoDisplay.updateMetadata([owner]);
        }
        break;

    case 'broadcast':
        console.log('[bc]', data);
        break;

    default:
        console.log('Unknown command:', kind, data);
        break;
    }
});

document.addEventListener('touchmove', function(e) {
    e.preventDefault();
});
