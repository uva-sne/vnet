var SVG = 'http://www.w3.org/2000/svg';
var XLINK = 'http://www.w3.org/1999/xlink';

var goarray = function(s) {
    s = s.replace(/^\[|\]$/g, '');
    if (s == '') return [];
    return s.split(' ');
};

vnet.utils = {};
vnet.utils.speed = function(v) {
    //v = v|0;
    if (v < 3248)
        return (v | 0) + 'bit/s';
    if (v < 1000000)
        return ((v / 1000) | 0) + 'kbit/s';
    if (v < 10000000000)
        return ((v / 1000000) | 0) + 'Mbit/s';
    return ((v / 1000000000) | 0) + 'Gbit/s';
};
var _$ = {
    // For non-Array types
    forEach: function(v, f) {
        for (var i = 0; i < v.length; i++)
            f(v[i], i);
    },
    forEachObj: function(obj, f) {
        for (var k in obj) {
            if (obj.hasOwnProperty(k))
                f(obj[k], k);
        }
    },
    isEmptyObject: function(obj) {
        for (var k in obj) {
            if (obj.hasOwnProperty(k))
                return false;
        }
        return true;
    },
    any: function(v, f) {
        if (typeof v === 'undefined') throw 'v undefined';
        for (var i = 0; i < v.length; i++)
            if (f(v[i], i))
                return true;
        return false;
    },
    map: function(v, f) {
        var r = [];
        for (var i = 0; i < v.length; i++)
            r.push(f(v[i], i));
        return r;
    },
    extend: function(into, values) {
        for (var i = 0; i < values.length; i++)
            into.push(values[i]);
        return into;
    },
    addAll: function(into, values) {
        for (var k in values)
            into[k] = values[k];
        return into;
    },
    addOnce: function(lst, v) {
        for (var i = 0; i < lst.length; i++)
            if (lst[i] == v)
                return false;
        lst.push(v);
        return true;
    },
    keys: function(obj) {
        var ks = [];
        for (var k in obj)
            ks.push(k);
        return ks;
    },
    find: function(lst, k, src) {
        for (var i = 0; i < lst.length; i++) {
            if (lst[i][k] == src)
                return lst[i];
        }
    },
    clearChildren: function(p) {
        _$.forEach(p.childNodes, function(c) {
            p.removeChild(c);
        });
    },
    choice: function(lst) {
        var i = (Math.random() * lst.length) | 0;
        return lst[i];
    },
    zeroes: function(n) {
        var lst = new Array(n);
        for (var i = 0; i < n; i++) {
            lst[i] = 0;
        }
        return lst;
    },
    events: function() {
        for (var i = 2; i < arguments.length; i++) {
            arguments[0].addEventListener(arguments[i], arguments[1]);
        }
    },
    deepcopy: function(obj) {
        return JSON.parse(JSON.stringify(obj));
    },
    arraysEqual: function(a, b) {
        if (a === b) return true;
        if (a.length != b.length) return false;

        for (var i = 0; i < a.length; ++i) {
            if (a[i] !== b[i]) return false;
        }
        return true;
    },
    fold: function(lst, initial, func) {
        var r = initial;
        _$.forEach(lst, function(v) {
            r = func(r, v);
        });
        return r;
    }
};

var unpack_table_row = function(keys, row) {
    var ret = {};
    keys.forEach(function(k, i) {
        ret[k] = row[i];
    });
    return ret;
};
