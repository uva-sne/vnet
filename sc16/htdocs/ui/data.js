/* Graph/topology data
 * ===================
 *
 * Types of data updates in the graph:
 *
 * - Relatively stable: nodes, links, RDF metadata
 * - Low frequency: link state
 * - Medium/high frequency: dynamic metadata
 * - High frequency: bandwidth statistics
 *
 * TODO:
 * - Figure out which properties should be metadata and which properties should
 *   have first-class support.
 */
vnet.DEFAULT_LINK_RATE = 100000000;

/* Layer data
 *
 * Keeps track of all the data in a layer and triggers events on
 * update.
 */
vnet.LayerData = (function() {
    "use strict";
    var DEPTH = 16;

    // Fixed properties from D3.js
    var NODE_PROPERTIES = ['x', 'y', 'px', 'py', 'fx', 'fy', 'weight'];

    var _merge = function(map, key, vals) {
        var lst = map[key];
        if (lst === undefined) {
            map[key] = vals;
            return;
        }
        lst.splice(0, lst.length);
        for (var i = 0; i < vals.length; i++)
            lst.push(vals[i]);
    };

    var nodeAddress = function(node, link) {
        var ret = [];
        if (!node.ifaces)
            return ret;
        for (var i = 0; i < node.ifaces.length; i++) {
            var iface = node.ifaces[i];
            if (!link || (iface.links && iface.links.indexOf(link) > -1)) {
                for (var j = 0; j < iface.ipv4.length; j++) {
                    ret.push(iface.ipv4[j]);
                }
            }
        }
        return ret;
    };

    var unpack_table_row = function(keys, row) {
        var ret = {};
        keys.forEach(function(k, i) {
            ret[k] = row[i];
        });
        return ret;
    };

    var bits2num = function(rate) {
        var scale = {
            'Kbit': 1000,
            'Mbit': 1000000,
            'Gbit': 1000000000,
            'Tbit': 1000000000000
        };
        var parts = rate.split(/(\D+)/);
        return (+parts[0]) * scale[parts[1]];
    };
    var _pct = function(l, r) {
        if (l < 0) return 0.1;
        if (l > r) return 100;
        return 100 * (l / r);
    };

    var keep_stats = function(link, rx, tx) {
        var buf;

        if (!link.stats)
            link.stats = {capacity: rx};

        buf = link.stats.rx;
        if (!buf) buf = link.stats.rx = _$.zeroes(DEPTH);
        buf.shift();
        buf.push(rx);
        if (rx > link.stats.capacity) link.stats.capacity = rx;

        buf = link.stats.tx;
        if (!buf) buf = link.stats.tx = _$.zeroes(DEPTH);
        buf.shift();
        buf.push(tx);
        if (tx > link.stats.capacity) link.stats.capacity = tx;
    };

    var merge_metadata = function(graph, table) {
        var service_revenues = {};

        var keys = table.table;
        var values = table.values;
        if (!keys) {
            return;
        }
        values.forEach(function(table_row) {
            var row = unpack_table_row(keys, table_row);
            var node = lookup[row.node];
            if (!node || !row.metadata) return;
            node.dynamic_metadata = row.metadata;
            // TODO: this misses unsets
            for (var k in row.metadata) {
                var v = row.metadata[k];
                node.metadata[k] = v;
            }
            if (row.metadata['service-sales']) {
                //service_revenue += +row.metadata['service-server-counter'];
                service_revenues[node.name] = +row.metadata['service-sales'];
            }
            // TODO:
            //node.metadata.node_id = node.name.replace(/^.*-(\d+)$/, '$1');
            //node.metadata.node_v4 = '10.0.0.' + node.metadata.node_id;
        });

        // FIXME
        graph.service_revenues = [
            service_revenues['s1.services.as100'] || 0,
            service_revenues['s2.services.as100'] || 0];
    };

    var merge_stats = function(graph, table) {
        var keys = table.table;
        var values = table.values;
        if (!keys) {
            return;
        }
        if (keys[0] != 'link') return;
        values.forEach(function(table_row) {
            var row = unpack_table_row(keys, table_row);
            var link = graph._lookup[row.link];
            if (!link) return;

            var srate = row.src_rate ? bits2num(row.src_rate) : 0;
            var drate = row.dst_rate ? bits2num(row.dst_rate) : 0;

            // TODO: should highlight link
            link._rate_error = (srate != drate && srate && drate);

            var guess_limit = (srate && drate) ? Math.min(srate, drate) :
                (srate ? srate : (drate ? drate : vnet.DEFAULT_LINK_RATE));

            // Reported in bytes, not bits
            // RX/TX swap!
            var rate_rx = Math.max(row.src_rx, row.dst_tx) * 8;
            var rate_tx = Math.max(row.src_tx, row.dst_rx) * 8;

            link._latest_rx = rate_rx;
            link._latest_tx = rate_tx;

            var loss_rx = Math.abs(row.src_rx - row.dst_tx) * 8;
            var loss_tx = Math.abs(row.src_tx - row.dst_rx) * 8;
            link._loss_rx = loss_rx;
            link._loss_tx = loss_tx;

            var cost_rate = (srate && drate) ? Math.max(srate, drate) :
                (srate ? srate : (drate ? drate : vnet.DEFAULT_LINK_RATE));
            link._cost_rate = cost_rate;
            link.metadata.rate = vnet.utils.speed(link._cost_rate);
            link._is_up = (!row.src_state || row.src_state == 'up') &&
                (!row.dst_state || row.dst_state == 'up');
            link.metadata.state = link._is_up ? 'up' : 'down';
            link.metadata.filter = row.src_filter == row.dst_filter ? row.src_filter : (row.src_filter + ';' + row.dst_filter);

            keep_stats(link, rate_rx, rate_tx);
        });
    };

    var lookup = {};
    var lookupMAC = {};

    var LayerData = function(graph) {
        var data = {
            _lookup: lookup,

            updateLayer: function(graph) {
                var that = this;
                var nodes = [];
                var links = [];

                _$.forEachObj(graph, function(v, k) {
                    if (k == 'nodes' || k == 'links')
                        return;

                    that.graph[k] = v;
                });

                graph.nodes.forEach(function(v, i) {
                    var n = lookup[v.name];
                    if (n !== undefined) {
                        // Merge values
                        for (var p = 0; p < NODE_PROPERTIES.length; p++) {
                            var prop = NODE_PROPERTIES[p];
                            if (n.hasOwnProperty(prop)) v[prop] = n[prop];
                        }
                        _$.forEachObj(v, function(_k, _v) {
                            n[_k] = _v;
                        });
                        v = n;
                    } else {
                        lookup[v.name] = v;
                    }

                    v.ifaces.forEach(function(iface) {
                        lookupMAC[iface.mac.toUpperCase()] = {node: v.name, link: iface.links[0]};
                    });

                    delete v.index;
                    nodes.push(v);
                });

                graph.links.forEach(function(v, i) {
                    var n = lookup[v.name];
                    if (n === undefined) {
                        n = v;
                    }
                    if (typeof v.source === 'string') n.source = lookup[v.source];
                    if (typeof v.target === 'string') n.target = lookup[v.target];
                    lookup[n.name] = n;
                    links.push(n);
                });

                _merge(this.graph, 'nodes', nodes);
                _merge(this.graph, 'links', links);
                this.graph.slice_name = graph.slice_name;
            },

            nodeAddresses: nodeAddress,
            linkAddresses: function(name) {
                var source, target;
                for (var i = 0; i < this.graph.links.length; i++) {
                    var link = this.graph.links[i];
                    if (link.name == name) {
                        return [nodeAddress(link.source, name),
                                nodeAddress(link.target, name)];
                    }
                }
                return [];
            },

            updateNode: function() {},
            updateLink: function() {},

            meta: function(name, keys, values) {
                var d = lookup[name], m;
                if (!d)
                    return;
                for (var i = 0; i < keys.length; i++) {
                    var k = keys[i], v = values[i];
                    if ((m = k.match(/^mac\.(.*)$/))) {
                        storeMAC(v, d, m[1]);
                    }
                    d.metadata[k] = v;
                }
            },

            metadata: function(table) {
                merge_metadata(this, table);
            },
            stats: function(table) {
                merge_stats(this, table);
            },
            getLink: function(n) {
                for (var i = 0; i < this.graph.links.length; i++) {
                    if (this.graph.links[i].name == n)
                        return this.graph.links[i];
                }
            },

            // Data
            lookupMAC: lookupMAC,
            graph: {
                nodes: [],
                links: [],
                scenarios: [],
                scenario: 0
            },
            service_revenues: [0, 0]
        };

        data.updateLayer(graph);
        return data;
    };

    return LayerData;
})();

// Fixed properties from D3.js
var NODE_PROPERTIES = ['x', 'y', 'px', 'py', 'fx', 'fy', 'weight'];

vnet.Graph = (function() {
    var _merge = function(map, key, vals) {
        var lst = map[key];
        if (lst === undefined) {
            map[key] = vals;
            return;
        }
        lst.splice(0, lst.length);
        for (var i = 0; i < vals.length; i++)
            lst.push(vals[i]);
    };

    var keep_stats = function(link, rx, tx) {
        var buf;

        if (!link.stats)
            link.stats = {capacity: rx};

        buf = link.stats.rx;
        if (!buf) buf = link.stats.rx = _$.zeroes(DEPTH);
        buf.shift();
        buf.push(rx);
        if (rx > link.stats.capacity) link.stats.capacity = rx;

        buf = link.stats.tx;
        if (!buf) buf = link.stats.tx = _$.zeroes(DEPTH);
        buf.shift();
        buf.push(tx);
        if (tx > link.stats.capacity) link.stats.capacity = tx;
    };

    var merge_metadata = function(graph, table) {
        var service_revenues = {};

        var keys = table.table;
        var values = table.values;
        if (!keys) {
            return;
        }
        values.forEach(function(table_row) {
            var row = unpack_table_row(keys, table_row);
            var node = lookup[row.node];
            if (!node || !row.metadata) return;
            node.dynamic_metadata = row.metadata;
            // TODO: this misses unsets
            for (var k in row.metadata) {
                var v = row.metadata[k];
                node.metadata[k] = v;
            }
            if (row.metadata['service-sales']) {
                //service_revenue += +row.metadata['service-server-counter'];
                service_revenues[node.name] = +row.metadata['service-sales'];
            }
            // TODO:
            //node.metadata.node_id = node.name.replace(/^.*-(\d+)$/, '$1');
            //node.metadata.node_v4 = '10.0.0.' + node.metadata.node_id;
        });

        // FIXME
        graph.service_revenues = [
            service_revenues['s1.services.as100'] || 0,
            service_revenues['s2.services.as100'] || 0];
    };

    var merge_stats = function(graph, table) {
        var keys = table.table;
        var values = table.values;
        if (!keys) {
            return;
        }
        if (keys[0] != 'link') return;
        values.forEach(function(table_row) {
            var row = unpack_table_row(keys, table_row);
            var link = graph._lookup[row.link];
            if (!link) return;

            var srate = row.src_rate ? bits2num(row.src_rate) : 0;
            var drate = row.dst_rate ? bits2num(row.dst_rate) : 0;

            // TODO: should highlight link
            link._rate_error = (srate != drate && srate && drate);

            var guess_limit = (srate && drate) ? Math.min(srate, drate) :
                (srate ? srate : (drate ? drate : vnet.DEFAULT_LINK_RATE));

            // Reported in bytes, not bits
            // RX/TX swap!
            var rate_rx = Math.max(row.src_rx, row.dst_tx) * 8;
            var rate_tx = Math.max(row.src_tx, row.dst_rx) * 8;

            link._latest_rx = rate_rx;
            link._latest_tx = rate_tx;

            var loss_rx = Math.abs(row.src_rx - row.dst_tx) * 8;
            var loss_tx = Math.abs(row.src_tx - row.dst_rx) * 8;
            link._loss_rx = loss_rx;
            link._loss_tx = loss_tx;

            var cost_rate = (srate && drate) ? Math.max(srate, drate) :
                (srate ? srate : (drate ? drate : vnet.DEFAULT_LINK_RATE));
            link._cost_rate = cost_rate;
            link.metadata.rate = vnet.utils.speed(link._cost_rate);
            link._is_up = (!row.src_state || row.src_state == 'up') &&
                (!row.dst_state || row.dst_state == 'up');
            link.metadata.state = link._is_up ? 'up' : 'down';
            link.metadata.filter = row.src_filter == row.dst_filter ? row.src_filter : (row.src_filter + ';' + row.dst_filter);

            keep_stats(link, rate_rx, rate_tx);
        });
    };

    var lookup = {};
    var lookupMAC = {};

    var graph = {
        // Set active graph element
        updateLayer: function(graph) {
        },


        // Helpers
        _lookup: lookup,
        _lookupMAC: lookupMAC,

        getHostByMAC: function(mac) {
            var n = lookupMAC[mac];
            return n ? lookup[n.node] : undefined;
        },

        getLinkByMAC: function(mac) {
            var n = lookupMAC[mac];
            // How about source vs target, iface?
            return n ? lookup[n.link] : undefined;
        },

        // Currently active elements
        nodes: [],
        links: [],

        name: null
    };

    var LayerData = function(graph) {
        var data = {
            _lookup: lookup,

            updateLayer: function(graph) {
                var that = this;
                var nodes = [];
                var links = [];

                _$.forEachObj(graph, function(v, k) {
                    if (k == 'nodes' || k == 'links')
                        return;

                    that.graph[k] = v;
                });

                graph.nodes.forEach(function(v, i) {
                    var n = lookup[v.name];
                    if (n !== undefined) {
                        // Merge values
                        for (var p = 0; p < NODE_PROPERTIES.length; p++) {
                            var prop = NODE_PROPERTIES[p];
                            if (n.hasOwnProperty(prop)) v[prop] = n[prop];
                        }
                        _$.forEachObj(v, function(_k, _v) {
                            n[_k] = _v;
                        });
                        v = n;
                    } else {
                        lookup[v.name] = v;
                    }

                    v.ifaces.forEach(function(iface) {
                        lookupMAC[iface.mac.toUpperCase()] = {node: v.name, link: iface.links[0]};
                    });

                    delete v.index;
                    nodes.push(v);
                });

                graph.links.forEach(function(v, i) {
                    var n = lookup[v.name];
                    if (n === undefined) {
                        n = v;
                    }
                    if (typeof v.source === 'string') n.source = lookup[v.source];
                    if (typeof v.target === 'string') n.target = lookup[v.target];
                    lookup[n.name] = n;
                    links.push(n);
                });

                _merge(this.graph, 'nodes', nodes);
                _merge(this.graph, 'links', links);
                this.graph.slice_name = graph.slice_name;
            },

            nodeAddresses: nodeAddress,
            linkAddresses: function(name) {
                var source, target;
                for (var i = 0; i < this.graph.links.length; i++) {
                    var link = this.graph.links[i];
                    if (link.name == name) {
                        return [nodeAddress(link.source, name),
                                nodeAddress(link.target, name)];
                    }
                }
                return [];
            },

            updateNode: function() {},
            updateLink: function() {},

            meta: function(name, keys, values) {
                var d = lookup[name], m;
                if (!d)
                    return;
                for (var i = 0; i < keys.length; i++) {
                    var k = keys[i], v = values[i];
                    if ((m = k.match(/^mac\.(.*)$/))) {
                        storeMAC(v, d, m[1]);
                    }
                    d.metadata[k] = v;
                }
            },

            metadata: function(table) {
                merge_metadata(this, table);
            },
            stats: function(table) {
                merge_stats(this, table);
            },
            getLink: function(n) {
                for (var i = 0; i < this.graph.links.length; i++) {
                    if (this.graph.links[i].name == n)
                        return this.graph.links[i];
                }
            },

            // Data
            lookupMAC: lookupMAC,
            graph: {
                nodes: [],
                links: [],
                scenarios: [],
                scenario: 0
            },
            service_revenues: [0, 0]
        };

        data.updateLayer(graph);
        return data;
    };

    return graph;
})();
