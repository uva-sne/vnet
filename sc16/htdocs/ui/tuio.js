/**
 * TUIOProxy WebSocket client
 */
vnet.TUIO = function() {
    var cursors = [];
    var cursordata = {};

    ////
    var cursor = function() {
        var c = document.createElement('div');
        c.style.position = 'absolute';
        c.style.height = '2px';
        c.style.width = '2px';
        c.style.background = 'yellow';
        return c;
    };

    var cursorMove = function(c, x, y) {
        c.style.left = String(x) + 'px';
        c.style.top = String(y) + 'px';
    };
    ////

    var getTargetTouches = function(element) {
        var targetTouches = [];
        cursors.forEach(function(touch) {
            if (touch.target == element) {
                targetTouches.push(touch);
            }
        });
        return targetTouches;
    };

    var createEvent = function(name, sid) {
        var data = cursordata[sid];
        var evt = document.createEvent('CustomEvent');
        evt.initEvent(name, true, true);
        evt.touches = cursors;
        evt.targetTouches = getTargetTouches(data.target);
        evt.changedTouches = [data];
        evt.pageX = evt.screenX = data.screenX;
        evt.pageY = evt.screenY = data.screenY;

        if (data.target) {
            data.target.dispatchEvent(evt);
        } else {
            document.dispatchEvent(evt);
        }
    };

    var alive = function(sids) {
        var data;
        for (var sid in cursordata) {
            if (sids.indexOf(sid) < 0) {
                createEvent('touchend', sid);

                data = cursordata[sid];

                try { document.body.removeChild(data.cursor1); } catch (e) {console.log(e); }
                try { document.body.removeChild(data.cursor2); } catch (e) {console.log(e); }

                delete cursordata[sid];
                cursors.splice(cursors.indexOf(data), 1);
            }
        }
    };

    var set = function(sid, x, y) {
        var evt, data;
        var sx = (window.screen.width * x)|0;
        var sy = (window.screen.height * y)|0;

        // TODO: page includes scroll offset
        var px = sx + window.screenX;
        var py = sy + window.screenY;

        if ((data = cursordata[sid])) {
            evt = 'touchmove';
        } else {
            data = {
                sid: sid,
                identifier: sid,
                target: document.elementFromPoint(px, py),

                cursor1: cursor(),
                cursor2: cursor()
            };
            evt = 'touchstart';
            cursors.push(data);
            cursordata[sid] = data;

            document.body.appendChild(data.cursor1);
            document.body.appendChild(data.cursor2);
        }

        data.x = x;
        data.y = y;
        data.screenX = sx;
        data.screenY = sy;
        data.clientX = data.pageX = px;
        data.clientY = data.pageY = py;
        createEvent(evt, sid);

        cursorMove(data.cursor1, data.clientX, data.clientY - 28);
        cursorMove(data.cursor2, data.clientX, data.clientY + 28);
    };

    // TODO: detect if touchtable
    return vnet.JSONStream('ws://127.0.0.1:5000/', {
        onopen: function() {
            document.body.style.cursor = 'none';
        },
        onmessage: function(msg) {
            msg.forEach(function(v) {
                switch (v.n) {
                case 'a':
                    alive((v.s || []).map(String));
                    break;
                case 's':
                    set(String(v.s), v.x, v.y);
                    break;
                }
            });
        }
    }, 1000, true);
};
