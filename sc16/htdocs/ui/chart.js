/**
 * Line chart helper
 *
 * The caller retains control over the values and configuration.
 */
vnet.LineChart = function(root, config, lines, N, w, h) {
    var elem = document.createElement('div');
    root.innerHTML = '';
    root.appendChild(elem);
    var chart = d3.select(elem)
            .append('svg')
            .attr('width', w)
            .attr('height', h * lines.length)
            .style('border-top', '1px solid #191919')
            .style('border-bottom', '1px solid #191919');

    var yfunc = config.y ? config.y : d3.scaleLinear;
    config.vmin = 0;
    config.vmax = 100;

    var x = d3.scaleLinear().domain([0, N - 1]).range([-1, w + 0.75]);
    var y = yfunc().domain([config.vmax, config.vmin]).range([2, h - 2]);

    var make_line = function(datum, i) {
        datum.y_offset = i * h;

        datum.area = d3.area()
            /*.interpolate('linear')*/
            .x(function(_, i) { return x(i); });

        datum.line = d3.line()
            /*.interpolate('linear')*/
            .x(function(_, i) { return x(i); });

        if (!datum.invert) {
            datum.area
                .y0(datum.y_offset + h)
                .y1(function(d) { return datum.y_offset + y(d); });
            datum.line.y(function(d) { return datum.y_offset + y(d); });
        } else {
            datum.area
                .y0(datum.y_offset)
                .y1(function(d) { return datum.y_offset + (h - y(d)); });
            datum.line.y(function(d) { return datum.y_offset + (h - y(d)); });
        }
    };

    lines.forEach(make_line);
    var g = chart.selectAll('.tline').data(lines);

    var tline = g.enter().append('g').attr('class', 'tline');
    tline.append('path')
        .style('fill', function(d) { return d.fill; });
    tline.append('path')
        .style('fill', 'transparent')
        .style('stroke', function(d) { return d.color; })
        .style('stroke-width', '2px');
    tline.append('text')
        .style('stroke-width', '1px')
        .attr('x', 0)
        .attr('y', function(d) {
            return d.y_offset + 20;
        });

    var paths = tline.selectAll('path');
    var texts = tline.selectAll('text');

    return function() {
        y.domain([config.vmax, config.vmin]);
        paths.attr('d', function(d, n) {
            if (n == 0) return d.area(d.values);
            return d.line(d.values);
        });
        texts.text(function(d) {
            var v = d.values[d.values.length - 1];
            if (config.vshow)
                v = config.vshow(v);
            else
                v = String((v + 0.5) | 0);
            return d.label + ': ' + (v);
        });
    };
};

vnet.svg = function(w, h) {
    var svg = document.createElementNS(SVG, 'svg');
    svg.setAttribute('width', w);
    svg.setAttribute('height', h);
    return svg;
};


/**
 */
vnet.ChartLines = function(root, config, lines, N, w, h) {
    var elem = vnet.svg(w, h);
    root.innerHTML = '';
    root.appendChild(elem);

    elem.setAttribute('class', 'chart chart-lines');

    var tline = null;
    if (config.line) {
        tline = document.createElementNS(SVG, 'line');
        tline.style.stroke = '#633';
        elem.appendChild(tline);
    }

    lines.forEach(function(line, i) {
        line.elem = document.createElementNS(SVG, 'path');
        line.elem.setAttribute('stroke', line.color);
        line.elem.setAttribute('fill', 'transparent');
        line.elemLabel = document.createElementNS(SVG, 'text');
        line.elemLabel.setAttribute('stroke-width', '0.4px');
        line.elemLabel.setAttribute('fill', line.color);
        line.elemLabel.setAttribute('stroke', line.color);
        line.elemLabel.setAttribute('x', '0');
        line.elemLabel.setAttribute('y', String(i * 20 + 20));
        elem.appendChild(line.elem);
        elem.appendChild(line.elemLabel);
    });

    if (config.vmin === undefined) config.vmin = 0;
    if (config.vmax === undefined) config.vmax = 100;
    var yfunc = config.y ? config.y : d3.scaleLinear;
    var y = yfunc().range([20, h - 2]).domain([config.vmax, config.vmin]);
    var x = d3.scaleLinear().domain([0, N - 1]).range([-1, w + 0.75]);

    var lgen = d3.line()
        .curve(d3.curveMonotoneX)
        .x(function(_, i) { return x(i); })
        .y(function(v) { return y(v); });

    return function(latest) {
        if (latest !== undefined) {
            if (latest > config.vmax)
                config.vmax = latest;
            if (latest < config.vmin)
                config.vmin = latest;
            y.domain([config.vmax, config.vmin]);
        }
        var func = config.vshow ? config.vshow : function(v) { return String((v + 0.5) | 0); };
        if (tline) {
            tline.setAttribute('x1', x(0));
            tline.setAttribute('x2', x(w));
            tline.setAttribute('y1', y(config.line));
            tline.setAttribute('y2', y(config.line));
        }
        lines.forEach(function(line) {
            line.elem.setAttribute('d', lgen(line.values));
            var text = line.label + ': ' + func(line.values[line.values.length - 1]);
            line.elemLabel.textContent = text;
        });

    //return function() {
    //    paths.attr('d', function(d, n) {
    //        if (n == 0) return d.area(d.values);
    //        return d.line(d.values);
    //    });
    //    texts.text(function(d) {
    //        var v = d.values[d.values.length - 1];
    //        if (config.vshow)
    //            v = config.vshow(v);
    //        else
    //            v = String((v + 0.5) | 0);
    //        return d.label + ': ' + (v);
    //    });
    //};
    };

    //var make_line = function(datum, i) {
    //    datum.y_offset = i * h;

    //    datum.area = d3.area()
    //        /*.interpolate('linear')*/
    //        .x(function(_, i) { return x(i); });

    //    datum.line = d3.line()
    //        /*.interpolate('linear')*/
    //        .x(function(_, i) { return x(i); });

    //    if (!datum.invert) {
    //        datum.area
    //            .y0(datum.y_offset + h)
    //            .y1(function(d) { return datum.y_offset + y(d); });
    //        datum.line.y(function(d) { return datum.y_offset + y(d); });
    //    } else {
    //        datum.area
    //            .y0(datum.y_offset)
    //            .y1(function(d) { return datum.y_offset + (h - y(d)); });
    //        datum.line.y(function(d) { return datum.y_offset + (h - y(d)); });
    //    }
    //};
};


/**
 */
vnet.StackChart = function(root, config, lines, N, w, h) {
    var elem = document.createElement('div');
    root.innerHTML = '';
    root.appendChild(elem);
    var chart = d3.select(elem)
            .append('svg')
            .attr('width', w)
            .attr('height', h)
            .style('border-top', '1px solid #191919')
            .style('border-bottom', '1px solid #191919');

    var yfunc = config.y ? config.y : d3.scaleLinear;
    config.vmin = 0;
    config.vmax = config.vmax || 100;
    var x = d3.scaleLinear().domain([0, N - 1]).range([-1, w + 0.75]);
    var y = yfunc().domain([config.vmax, config.vmin]).range([2, h - 2]);

    var make_line = function(datum, i) {
        var y_offset;
        var Z = -2;
        if (i > 0) { y_offset = lines[i - 1]; }

        datum.area = d3.area()
            /*.interpolate('linear')*/
            .x(function(d, i) { return x(i); })
            .y0(h)
            .y1(function(d, i) {
                var v;
                if (y_offset) { v = y(d + y_offset.values[i]) + Z; }
                else { v = y(d); }
                return v;
            });

        datum.line = d3.line()
            /*.interpolate('linear')*/
            .x(function(d, i) { return x(i); })
            .y(function(d, i) {
                var v;
                if (y_offset) { v = y(d + y_offset.values[i]) + Z; }
                else { v = y(d); }
                return v;
            });
    };
    lines.forEach(make_line);
    var g = chart.selectAll('.tline').data(lines);

    var tline = g.enter().insert('g', ':first-child').attr('class', 'tline');
    tline.append('path')
        .style('fill', function(d) { return d.fill; });
    tline.append('path')
        .style('fill', 'transparent')
        .style('stroke', function(d) { return d.color; })
        .style('stroke-width', '2px');
    var paths = tline.selectAll('path');

    var rrrr = function() {
        y.domain([config.vmax, config.vmin]);
        paths.attr('d', function(d, n) {
            if (n === 0) return d.area(d.values);
            return d.line(d.values);
        });
    };
    rrrr();
    return rrrr;
};
