/**
 * Layer list UI manager
 *
 * TODO: maintain layer-to-layer relations
 * TODO: deal with element resize
 */

var $layers = (function() {
    var elem = document.getElementById('graphs');
    var layerBar = document.getElementById('layer-bar');

    var first = true;
    var drag = true;

    var layers = {};
    var activeLayer = null;

    var createChild = function() {
        var child = document.createElement('div');
        child.className = 'graph-layer graph-hidden';
        elem.appendChild(child);
        return child;
    };

    var createLayerButton = function(name, desc) {
        var btn = document.createElement('button');
        btn.textContent = desc || name;
        _$.events(btn, function() {
            selectLayer(name);
        }, 'click', 'touchstart');
        layerBar.appendChild(btn);
        return btn;
    };

    var layerAdd = function(name, graph) {
        if (layers[name] !== undefined)
            return layers[name];

        // TODO: there needs to be a hook somewhere to update the description
        var child = createChild();
        var btn = createLayerButton(name, graph.description);
        var layer = {
            name: name,
            data: vnet.LayerData(graph),
            btn: btn
        };
        var size = {width: elem.offsetWidth,
                    height: elem.offsetHeight};
        layer.view = LayerView(name, child, layer.data, layer, size);
        layers[name] = layer;
        return layer;
    };

    var selectLayer = function(name) {
        if (location.hash != '#' + name) {
            var url = location.pathname + location.search + '#' + name;
            history.replaceState({}, document.title, url);
        }

        _$.forEachObj(layers, function(n, k) {
            if (k == name) {
                n.view.activate();
                n.btn.className = 'layer-selected';
                activeLayer = name;
            } else {
                n.view.deactivate();
                n.btn.className = '';
            }
        });

        // Notify
        $mode.selectLayer(); // TODO
        $com.selectLayer(name);
    };

    var _selectFirst = function(fname) {
        if (first && !_$.isEmptyObject(layers)) {
            var layer = location.hash.substr(1);
            if (layer && layers[layer]) {
                selectLayer(layer);
            } else {
                selectLayer(fname);
            }
            first = false;
        }
    };

    return {
        layers: layers,

        selectLayer: selectLayer,
        getActiveLayer: function() {
            return layers[activeLayer];
        },
        getLayer: function(name) {
            return layers[name];
        },

        resize: function() {
            // TODO
            //$size.width = window.innerWidth;
            //$size.height = window.innerHeight;
            //_$.forEachObj(layers, function(v) {
            //    v.view.resize();
            //});
        },

        // Incoming commands
        // -----------------

        updateScenario: function(name, scenario, state) {
            var layer = this.getLayer(name);
            if (!layer) return;
            if (scenario != layer.data.scenario) {
                console.log('Transition to scenario', scenario);
                layer.data.scenario = scenario;
                layer.view.redraw();
            }
        },

        // Update multiple layers
        updateLayers: function(_layers) {
            var that = this;
            _$.forEach(_layers, function(data) {
                var layer;
                if (layers[data.name]) {
                    layer = layers[data.name];
                    layer.data.updateLayer(data);
                } else {
                    layer = layerAdd(data.name, data);
                }
                layer.view.redraw();
            });

            if (_layers.length > 0)
                _selectFirst(_layers[0].name);
        },

        run: function(func, data) {
            var layer = this.getLayer(data.layer);
            if (!layer) return;
            layer.data[func](data);
            if (func == 'stats' || func == 'metadata')
                layer.view.redraw_properties();
            else
                layer.view.redraw();
        }
    };
})();
window.addEventListener('resize', $layers.resize);

// ----------------------------------------------------------------------------

var KIND_NODE = 1, KIND_LINK = 2, KIND_IP = -1;
var _task_name = function(n) {
    if (n == KIND_NODE) return 'node:task';
    if (n == KIND_LINK) return 'link:task';
};

var _task_ident = function(layer, kind) {
    var names = [].splice.call(arguments, 2);
    var g = $layers.getLayer(layer);
    if (!g) return;

    var prefix = 'vnet:' + g.data.graph.slice_name + ':';
    if (kind == KIND_NODE)
        return names.map(function(v) { return prefix + 'node=' + v; });

    var ret;
    if (kind == KIND_IP) {
        return [g.data._lookup[names[0]].metadata.node_v4];
        //names.map(function(name) {
        //ret = [];
        //names.map(function(name) {
        //    g.data.nodeAddresses(g.data._lookup[name]).map(function(v) {
        //        ret.push(String(v).replace(/\/\d+$/, ''));
        //    });
        //});
        ////return ret;
        //return [ret[0]];
    } else if (kind == KIND_LINK) {
        ret = [];
        names.map(function(name) {
            g.data.linkAddresses(name).map(function(v) {
                ret.push(prefix + 'ipv4=' + v);
            });
        });
        return ret;
    }
};

var toolImplementations = {
    'source-dest': {
        ok: function(tool, dialog, selection) {
            if (selection.items.length != 2)
                return false;

            console.log('Requesting task', tool.name,
                        'from', selection.items[0],
                        'to', selection.items[1]);

            $com.send(_task_name(selection.kind),
                      {task: tool.name,
                       target: _task_ident(selection.layer, selection.kind, selection.items[0]),
                       args: _task_ident(selection.layer, KIND_IP, selection.items[1])});
            return true;
        },
        postRender: function(tool, dialog, dialogConfirm) {
            var ok = dialog.querySelector('button.tool-ok');
            _$.events(ok, dialogConfirm, 'click', 'touchstart');
            //ok.disabled = selection.items.length < 2;
        }
    }, 'enum': {
        ok: function(tool, dialog, selection) {
            var option = dialog.querySelector('.enum-option-selected');
            if (!option)
                return false;
            option = String(option.getAttribute('data-option'));
            console.log('Requesting task', tool.name,
                        'on', selection.items[0],
                        'using', option);
            $com.send(_task_name(selection.kind),
                      {task: tool.name,
                       target: _task_ident(selection.layer, selection.kind, selection.items[0]),
                       args: [option]});
            return true;
        },
        postRender: function(tool, dialog, dialogConfirm) {
            var opts = dialog.querySelectorAll('.enum-option');
            var select = function() {
                var opt = this;
                tool.selected = this.getAttribute('data-option');
                _$.forEach(opts, function(v) {
                    if (v == opt) {
                        v.classList.add('enum-option-selected');
                    } else {
                        v.classList.remove('enum-option-selected');
                    }
                });
            };
            _$.forEach(opts, function(v) {
                v.addEventListener('click', select);
                v.addEventListener('touchstart', select);
            });
            var ok = dialog.querySelector('button.tool-ok');
            ok.addEventListener('click', dialogConfirm);
            ok.addEventListener('touchstart', dialogConfirm);
        }
    }, 'simple': {
        ok: function(tool, dialog, selection) {
            console.log('Requesting task', tool.name,
                        'on', selection.items[0]);
            $com.send(_task_name(selection.kind),
                      {task: tool.name,
                       target: _task_ident(selection.layer, selection.kind, selection.items[0])});
            return true;
        },
        postRender: function(tool, dialog, dialogConfirm) {
            var ok = dialog.querySelector('button.tool-ok');
            ok.addEventListener('click', dialogConfirm);
            ok.addEventListener('touchstart', dialogConfirm);
        }
    }
};


/**
 * Mode management
 */
var $mode = (function() {
    var elem = document.getElementById('select-tools');
    var info = elem.querySelector('.select-tools-info');
    var toolbox = elem.querySelector('.select-tools-list');
    var dialog = elem.querySelector('.select-tools-dialog');
    dialog.style.display = 'none';

    var selectRelated = function(layer, e) {
        if (layer.parent) {
            if (e.parents)
                layer.parent.view.selectRelated(e.parents);
        }
        if (!layer.children) return;
        layer.children.forEach(function(v) {
            var child = $layers.getLayer(v);
            var related = child.data.parents_inverse[e.name];
            if (related) {
                child.view.selectRelated(related);
                child.view.selectNames([]);
            }
        });
    };


    var emptySelection = function(s) {
        s.layer = null;
        s.kind = null;
        s.limit = null;
        s.max = 1;
        s.items = [];
    };
    var postAction = function(s) {
        s.items = [s.items[0]];
    };

    var currentTool = null;
    var selection = {};
    emptySelection(selection);

    //var dialogHide = function() {
    //    dialog.innerHTML = '';
    //    dialog.style.display = 'none';
    //    emptySelection(selection);
    //};

    var dialogConfirm = function() {
        console.log('Dialog confirmed.');
        var impl = toolImplementations[currentTool.kind];
        if (!impl || impl.ok(currentTool, dialog, selection, this)) {
            var layer = $layers.getLayer(selection.layer);
            resetDialog();
            if (layer) layer.view.selectNames(selection.items);
        }
    };

    // TODO: DOM updates aren't really necessary
    var dialogUpdate = function(n) {
        dialog.style.display = 'block';
        var view = {tool: currentTool, selection: selection};

        switch (currentTool.kind) {
        case 'source-dest':
            selection.limit = KIND_NODE;
            dialog.innerHTML = Handlebars.templates.toolSourceDest(view);
            break;

        case 'enum':
            view.options = [];
            currentTool.selected = null;
            if (currentTool.name == 'link-egress') {
                currentTool.selected = n.metadata.rate.replace(/\/s$/, '');
            } else if (currentTool.name == 'link-status') {
                currentTool.selected = n.metadata.state == 'down' ? 'Down' : 'Up';
            } else if (currentTool.name == 'link-filter') {
                if (n.metadata.filter == 'tcp')
                    currentTool.selected = 'TCP';
                else if (n.metadata.filter == 'udp')
                    currentTool.selected = 'UDP';
                else if (!n.metadata.filter)
                    currentTool.selected = 'None';
            }
            console.log('T', currentTool.name, 'V', currentTool.selected);

            for (var i = 0; i < currentTool.options.length; i++) {
                view.options.push({
                    value: currentTool.options[i],
                    selected: currentTool.options[i] == currentTool.selected
                });
            }
            dialog.innerHTML = Handlebars.templates.toolEnum(view);
            break;

        default:
            dialog.innerHTML = Handlebars.templates.toolSimple(view);
            break;
        }

        var impl = toolImplementations[currentTool.kind];
        if (impl && impl.postRender)
            impl.postRender(currentTool, dialog, dialogConfirm);
    };

    var drawDialogBox = function(toolset, toolfilter) {
        return function(layer, e) {
            info.innerHTML = Handlebars.templates.toolInfo(e);

            var tools = {};
            _$.forEachObj(toolset, function(v, k) {
                if (!toolfilter || toolfilter(e, v, k))
                    tools[k] = v;
            });

            toolbox.innerHTML = Handlebars.templates.toolList({tools: tools});
            var click = function(evt) {
                evt.preventDefault();

                var oldTool = currentTool;
                currentTool = toolset[this.getAttribute('data-tool')];
                if (oldTool && oldTool == currentTool) {
                    resetDialog();
                    // Reset selection?
                    var layer = $layers.getLayer(selection.layer);
                    if (layer) layer.view.selectNames(selection.items);
                    return;
                }

                dialogUpdate(e);
            };
            _$.forEach(toolbox.querySelectorAll('button'), function(v) {
                _$.events(v, click, 'click', 'touchstart');
            });
        };
    };
    var drawDialogNode = drawDialogBox(vnet.config.nodeTools);
    var drawDialogLink = drawDialogBox(vnet.config.linkTools, function(link, _, tool) {
        if (tool == 'link-status' && (link.source.kind == 'node' || link.target.kind == 'node'))
            return false;
        return true;
    });

    // Aggressive reset
    var dialogHide = function(noskipselect) {
        info.innerHTML = '';
        toolbox.innerHTML = '';
        dialog.innerHTML = '';
        resetDialog();
        if (!noskipselect)
            emptySelection(selection);
    };

    var resetDialog = function() {
        currentTool = null;
        dialog.innerHTML = '';
        dialog.style.display = 'none';
        selection.limit = null;
        selection.max = 1;
        if (selection.items.length > 0)
            selection.items = [selection.items[0]];
    };

    return {
        // Proxy functions
        // ---------------

        selectLayer: function() {
            // TODO: support state per layer
            dialogHide();
            emptySelection(selection);
        },

        selectNode: function(layer, node, elem) {
            selectRelated(layer, node);

            if (selection.limit && selection.limit != KIND_NODE)
                return;

            if (selection.limit == KIND_NODE) {
                if (node.name == selection.items[0])
                    return;

                // XXX
                //if (!node.tools || !node.tools.iperf) return;

                selection.items[1] = node.name;
                layer.view.selectNames(selection.items);
                dialogUpdate();
            } else {
                resetDialog();
                selection.kind = KIND_NODE;
                selection.limit = null;
                selection.layer = layer.name;
                selection.items = [node.name];
                //drawDialogNode(layer, node);
                dialogHide(true);
                layer.view.selectNames(selection.items);
            }
        },

        clickNode: function(layer, node, elem) {
            $infoDisplay.node(node);

            if (node.name.match(/\.edge/)) {
                $attack.setOrigin(layer.name, node.name);
            }
        },

        clickLink: function(layer, link, elem) {
            $infoDisplay.link(link);

            if (selection.limit && selection.limit != KIND_LINK)
                return;

            resetDialog();
            selection.kind = KIND_LINK;
            selection.limit = null;
            selection.layer = layer.name;
            selection.items = [link.name];
            layer.view.selectNames(selection.items);
            selectRelated(layer, link);
            drawDialogLink(layer, link);
        }
    };
})();

// ----------------------------------------------------------------------------

var $attack = (function() {
    var _task_ident = function(la, na) {
        return ['vnet:' + la + ':node=' + na];
    };
    var originLabel = document.getElementById('attack-origin');
    var attackers = ['e3.edge3.as500', 'e2.edge2.as400', 'e1.edge1.as300'];
    var origin = null, originLayer = function() {
        return $layers.getActiveLayer().name;
    };

    var buttons = document.getElementById('attack-btns').getElementsByTagName('button');
    var doAttack = function(e) {
        e.preventDefault();
        var atk = this.dataset.attack;
        var clearall = atk == 'none';

        var scenarioReset = [];
        var iperfReset = [];

        if (origin === null && atk !== 'udp' && atk != 'none') {
            origin = _$.choice(attackers);
        }

        if (atk == 'none') {
            scenarioReset = attackers;
            iperfReset = attackers;
        } else if (atk == 'udp') {
            scenarioReset = attackers;
        } else {
            iperfReset = attackers;
            scenarioReset = attackers.filter(function(n) { return n !== origin; });
        }

        _$.forEach(buttons, function(btn) {
            if (btn.dataset.attack == atk) {
                btn.classList.add('selected');
            } else {
                btn.classList.remove('selected');
            }
        });
        iperfReset.forEach(function(n) {
            $com.send('node:task',
                      {task: 'attack-stop',
                       target: _task_ident(originLayer(), n),
                       args: []});
        });
        scenarioReset.forEach(function(n) {
            $com.send('node:task',
                      {task: 'attack-scenario',
                       target: _task_ident(originLayer(), n),
                       args: ['none']});
        });
        if (atk == 'udp') {
            // TODO send from 2 domains
            attackers.forEach(function(n, i) {
                $com.send('node:task',
                          {task: 'attack',
                           target: _task_ident(originLayer(), n),
                           args: ['10.100.1.' + String(i % 2 + 1), 'udp', '90M', '3600']}); // TODO
            });
        } else if (origin !== null) {
            $com.send('node:task',
                      {task: 'attack-scenario',
                       target: _task_ident(originLayer(), origin),
                       args: [atk, atk == 'cpu' ? '10' : '3',
                           atk == 'pw' ? '3' : '6']});
        }
    };
    _$.forEach(buttons, function(btn) {
        _$.events(btn, doAttack, 'click', 'touchstart');
    });

    return {
        setOrigin: function(layer, n) {
            originLabel.textContent = origin = n;
        },
    };
})();

// ----------------------------------------------------------------------------

var $infoDisplay = (function() {
    var N = 16;

    var chart_elem = document.querySelector('#traffic-chart');
    var cfg = {/*y: d3.scale.sqrt, */vshow: vnet.utils.speed};
    var chart_data = [
        {label: 'RX',
         fill:  '#002204',
         color: '#009911',
         values: [], invert: false},
        {label: 'TX',
         fill: '#000422',
         color: '#001199',  values: [], invert: true}
    ];

    var traffic_chart = vnet.LineChart(chart_elem, cfg, chart_data, N, 325, 80);
    chart_elem.style.visibility = 'hidden';

    var sync = function() {
        if (!cur) return;
        if (!cur.stats) return;
        if (!cur.stats.rx) return;
        if (!cur.stats.tx) return;
        cfg.vmax = (cur.stats.capacity > vnet.DEFAULT_LINK_RATE) ? cur.stats.capacity : vnet.DEFAULT_LINK_RATE;
        //(!cur.stats.capacity || cur.stats.capacity < MIN) ? cur.stats.capacity : 100;
        chart_data[0].values = cur.stats.rx;
        chart_data[1].values = cur.stats.tx;
        traffic_chart();
    };

    var nodeInfo = function(node, isnew) {
        //if (node.group == 'uva')
        //    node.logo = 'uva.jpg';
        //else if (node.group == 'ciena' || node.group == 'ciena-hanover')
        //    node.logo = 'ciena.png';
        //else if (node.group == 'starlight')
        //    node.logo = 'icair.gif';
        //else
            node.logo = null;
        sidebar.innerHTML = Handlebars.templates.nodeInfo(node);
        chart_elem.style.visibility = 'hidden';
        if (isnew) {
            curInfo = nodeInfo;
            cur = node;
        }
    };

    var linkInfo = function(link, isnew) {
        sidebar.innerHTML = Handlebars.templates.linkInfo(link);
        chart_elem.style.visibility = 'visible';

        if (isnew) {
            curInfo = linkInfo;
            cur = link;
            sync();
        }
    };

    var sidebar = document.querySelector('#sidebar-content');
    var cur = null, curInfo = null, last = null;
    return {
        node: function(e) {
            nodeInfo(e, true);
        },
        // REMOVE {
        updateNode: function(e) { if (cur == e) { nodeInfo(e, false); } },
        updateLink: function(e) { if (cur == e) { linkInfo(e, false); } },
        // }
        link: function(e) {
            linkInfo(e, true);
        },
        updateMetadata: function(objects) {
            if (cur && objects && !_$.any(objects, function(v) {
                return v.node == cur.name;
            })) {
                return;
            }
            if (cur) curInfo(cur, false);
        },
        updateStats: function() {
            sync();
        }
    };
})();

// ----------------------------------------------------------------------------

(function() {
    var phys = document.getElementById('btn-links-physical');
    var flow = document.getElementById('btn-links-flows');

    _$.events(phys, function(e) {
        e.preventDefault();
        var state = phys.classList.toggle('selected') ? 'visible' : 'hidden';
        _$.forEachObj($layers.layers, function(layer) {
            layer.view._links.style.visibility = state;
            // Hack
            var sw = layer.data._lookup['switch.services.as100'];
            if (sw) sw._elem.style.visibility = state;
        });
    }, 'click', 'touchstart');
    _$.events(flow, function(e) {
        e.preventDefault();
        var state = flow.classList.toggle('selected') ? 'visible' : 'hidden';
        _$.forEachObj($layers.layers, function(layer) {
            layer.view._flows.style.visibility = state;
        });
    }, 'click', 'touchstart');
})();

// ----------------------------------------------------------------------------

var $scenarioManager = (function() {
    var time_limit = 4 * 60 * 1000;

    var state = 0,
        revenue = 0,
        scenario = 0;
    var buttons = document.getElementById('scenario-buttons'),
        timer = document.getElementById('scenario-timer'),
        label = document.getElementById('scenario-label');

    var tid = null;
    var tstart = null, tend = null;

    var pad = function(n, ln) {
        n = String(n);
        while (n.length < ln) {
            n = '0' + n;
        }
        return n;
    };

    var format = function(t) {
        var ret;
        t = (t / 100) | 0;
        ret = String(t % 10);
        t = (t / 10) | 0;
        ret = pad(t % 60, 2) + '.' + ret;
        t = (t / 60) | 0;
        ret = pad(t, 2) + ':' + ret;
        return ret;
    };

    var startTiming = function() {
        if (tid !== null) {
            clearInterval(tid);
        }
        tstart = Date.now();
        tend = null;
        tid = setInterval(function() {
            var n = Date.now();
            var t = time_limit - (n - tstart);
            if (t < 0) t = 0;
            colorFormat(t);
        }, 99);
    };

    var resetTiming = function() {
        if (tid !== null) {
            clearInterval(tid);
            tid = null;
        }
        timer.style.color = 'lightblue';
        timer.innerHTML = format(time_limit);
    };

    var colorFormat = function(remain) {
        if (remain < (5 * 1000)) timer.style.color = 'red';
        else if (remain < (60 * 1000)) timer.style.color = 'orange';
        else timer.style.color = 'lightblue';
        timer.innerHTML = format(remain);
    };

    var stopTiming = function(rev) {
        if (tid !== null) {
            clearInterval(tid);
            tid = null;
        }
        if (tstart !== null && tend === null) {
            tend = Date.now();
        }
        if (tstart && tend || (!tstart && !tend)) {
            if (rev) {
                timer.style.color = 'lightblue';
                timer.innerHTML = 'Score: ' + ((rev+0.5)|0) + '<br>' + format(tend - tstart);
            } else {
                var remain = time_limit - (tend - tstart);
                if (remain < 0) remain = 0;
                colorFormat(remain);
            }
        }
    };

    var click = function(e) {
        var act = e.target.getAttribute('data-action');
        $com.send('sc15:scenario:action', {'layer': $layers.getActiveLayer().name,
                                           'action': act});
    };

    var render = function() {
        switch (state) {
        case 0:
            resetTiming();
            buttons.innerHTML = 'Preparing...';
            break;
        case 1:
            resetTiming();
            buttons.innerHTML = Handlebars.templates.scenarioStart();
            break;
        case 2:
            startTiming();
            buttons.innerHTML = Handlebars.templates.scenarioStop();
            break;
        case 3:
            stopTiming(null);
            buttons.innerHTML = Handlebars.templates.scenarioSubmit();
            break;
        case 4:
            stopTiming(revenue);
            buttons.innerHTML = Handlebars.templates.scenarioNext();
            break;
        default:
            buttons.innerHTML = 'State ' + state;
        }

        var lds = $layers.getActiveLayer().data.scenarios[scenario];
        label.innerHTML = 'Scenario: ' + lds.name;

        _$.forEach(buttons.querySelectorAll('button'), function(b) {
            _$.events(b, click, 'click', 'touchstart');
        });
    };

    return {
        setState: function(_, scen, s, r) {
            console.log('Set scenario state', s, r);
            scenario = scen;
            state = s;
            revenue = r;
            render();
        }
    };
})/*()*/;

/////

(function() {
    var range = document.getElementById('loop-speed');
    var rangeMinus = document.getElementById('loop-minus');
    var rangePlus = document.getElementById('loop-plus');
    var last = -1;
    var send = function() {
        if (last === range.value)
            return;
        $com.send('broadcast', {'control-loop-speed': +range.value});
    };
    var keep = null;
    var adjust = function(dir) {
        return function(e) {
            e.preventDefault();
            if (keep)
                return;
            keep = setInterval(function() {
                range.value = +range.value + dir;
                send();
            }, 200);
            //(e.target.max / e.target.offsetWidth) * (e.gesture.touches[0].screenX - e.target.offsetLeft);
        };
    };
    var stop = function() {
        if (keep) {
            clearInterval(keep);
            keep = null;
        }
    };
    _$.events(range, send, 'input', 'change');
    _$.events(rangePlus, adjust(1), 'touchstart');
    _$.events(rangeMinus, adjust(-1), 'touchstart');
    _$.events(rangePlus, stop, 'touchend', 'touchcancel');
    _$.events(rangeMinus, stop, 'touchend', 'touchcancel');
    setTimeout(send, 1000);
})();
