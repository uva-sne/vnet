/* Graph layer rendering
 * =====================
 */


/* Utilities
 * ---------
 */


var FIXED = 1;
var VSCALE = 1;
var y2s, x2s; y2s = x2s = function(o) { return o * VSCALE; };
var s2y, s2x; s2y = s2x = function(o) { return o / VSCALE; };

var trafficolor = function(v, max) {
    v = v * 265 / max;
    if (v < 0) v = 0;
    if (v > 265) v = 265;
    var p = 40;
    if (v < 10) p = 20 + v * 2;
    return 'hsl(' + (265 - v) + ', 50%, ' + p + '%)';
};

var percentClass = function(pct) {
    if (!pct || pct <= 0)
        return 'zero';
    else if (pct <= 2)
        return 'low';
    else if (pct <= 20)
        return 'medium';
    else if (pct <= 50)
        return 'high';
    return 'very-high';
};

var valueClass = function(val) {
    if (!val || val <= 0)
        return 'zero';
    else if (val <= 11e6)
        return 'low';
    else if (val <= 31e6)
        return 'medium';
    else if (val <= 68e6)
        return 'high';
    return 'very-high';
};

var $shapes = {
    rect: function(size) {
        //var ry = Math.sqrt(size / (2 * tan30)), rx = ry * tan30;
        //var rx = Math.sqrt(size / 2), ry = rx;
        var rx = Math.sqrt(size / 3), ry = rx;
        return ('M' + -rx + ',' +  ry + 'L' +
                       rx + ',' +  ry + ' ' +
                       rx + ',' + -ry + ' ' +
                      -rx + ',' + -rx + 'Z');
    },
    cloud: function(size) {
        var r = Math.sqrt(size / 80);
        return ('M ' + (r * -5) + ',' + (r * -3) + ' ' +
            'a ' + (r * 4) + ',' + (r * 4) + ' 1 0,0 ' + (r * 0) + ',' + (r * 8) + ' h ' + (r * 10) + ' ' +
            'a ' + (r * 4) + ',' + (r * 4) + ' 1 0,0 ' + (r * 0) + ',' + (r * -8) + ' ' +
            'a ' + (r * 2) + ',' + (r * 2) + ' 1 0,0 ' + (r * -3) + ',' + (r * -2) + ' ' +
            'a ' + (r * 3) + ',' + (r * 3) + ' 1 0,0 ' + (r * -7) + ',' + (r * 2) + ' z');

            //'a 20,20 1 0,0 0,40 h 50 ' +
            //'a 20,20 1 0,0 0,-40 ' +
            //'a 10,10 1 0,0 -15,-10 ' +
            //'a 15,15 1 0,0 -35,10 z';
    }
};

var SIZEN = 768;
var _default_circle = d3.symbol().type(d3.symbolCircle).size(SIZEN);

var nodeShape = function(d, sz) {
    if (d.name.match(/edge/)) {
        return $shapes.cloud(SIZEN);
    }

    if (d.kind == 'switch' || d.kind == 'ovs') {
        return $shapes.rect(SIZEN);
    }

    if (sz !== undefined) {
        return d3.symbol().type(d3.symbolCircle).size(SIZEN * sz)();
    }

    return _default_circle();
};

var nodeGroup = function(d) {
    return d.name.replace(/^.+\.as/, 'as');
};

var linkIcon = function(d) {
    return 'static/icons/attacker.gif';
};

var nodeIcon = function(d) {
    if (d.name.match(/^attacker/)) {
        return 'static/icons/attacker.gif';
    } else if (d.name.match(/^(server|s[0-9]\.)/)) {
        return 'static/icons/shopping.png';
    } else if (d.name.match(/^(client|host)/)) {
        return 'static/icons/user.png';
    }

    var img = (d.image || '').replace(/^img-/, '');
    switch (img) {
        case 'screen':
        case 'video':
        case 'sage2':
        case 'iperf':
        case 'router':
        case 'switch':
            return 'static/icons/' + img + '.png';
    }

    switch (d.kind) {
        case 'ovs':
            return 'static/icons/switch.png';
        case 'switch':
        case 'router':
            return 'static/icons/' + d.kind + '.png';
    }
    return '';
};

var nodeQoS = function(n) {
    var s = n.metadata['service-sales'];
    if (s < 10) return 'bad';
    else if (s < 20) return 'poor';
    else if (s < 35) return 'ok';
    return 'good';
};


/* Input management
 * ----------------
 */

var DragControl = function(dragroot, force, layer, handler, _size) {
    var need_resume = false;
    var counter = 0;

    var resume = function(chg) {
        need_resume = !!counter;
        counter += chg;
        //if (need_resume)
        force.alpha(0.6).restart();
    };
    var pointers = {};
    var endDrag = function(ident) {
        var d = pointers[ident];
        if (d === undefined)
            return false;
        pointers[ident] = undefined;
        var end = performance.now() - d._start;
        if (end < 300) {
            // Short click
            handler.clickNode(layer, d, this);
        }

        var dist = Math.abs(d._ix - d.x) + Math.abs(d._iy - d.y);
        if (dist < 20) {
            if (end < 300) {
                // Not a real move, preserve fixedness
                handler.selectNode(layer, d, this);
            } else {
                // Press to unfix
                delete d.fx;
                delete d.fy;
            }
        } else {
            // Drag fix
            d.fx = d.x;
            d.fy = d.y;
        }
        resume(-1);
        return true;
    };
    var move = function(ident, x, y) {
        var d = pointers[ident];
        if (d === undefined)
            return false;
        d.x = d.fx = x;
        d.y = d.fy = y;
        resume(0);
        return true;
    };

    return {
        start: function(e) {
            e.preventDefault();
            var d = e.currentTarget._node;
            if (!d || d._pointer !== undefined) return;
            var pointer = -1;
            if (e.targetTouches) {
                pointer = e.targetTouches[0].identifier;
            }
            d._start = performance.now();
            d._ix = d.x;
            d._iy = d.y;
            pointers[pointer] = d;
            resume(1);
        },
        end: function(e) {
            var any;
            if (e.type == 'mouseup')
                any = endDrag(-1);
            else
                any = _$.fold(e.changedTouches,
                    false,
                    function(v, touch) {
                        var r = endDrag(touch.identifier);
                        return v || r;
                    });
            if (!any)
                return;
            e.preventDefault();
            return;

        },
        drag: function(e) {
            var ox = dragroot.parentElement.offsetLeft;
            var oy = dragroot.parentElement.offsetTop;
            if (e.type == 'mousemove') {
                move(-1, s2x(c(e.pageX - ox, _size.width)),
                    s2y(c(e.pageY - oy, _size.height)));
            } else {
                _$.forEach(e.changedTouches, function(p) {
                    move(p.identifier, s2x(c(p.pageX - ox, _size.width)),
                        s2y(c(p.pageY - oy, _size.height)));
                });
            }
        }
    };
};


/* View management
 * ---------------
 */
var c = function(v, r) {
    var N = 8;
    var nv = (v < N ? N : (v > (r - N) ? (r - N) : v));
    return isNaN(nv) ? 0 : nv;
};

/* Data rendering
 */
var LayerView = function(name, elem, data, layer, _size, _handler) {
    if (!_handler) _handler = $mode;

    var svg = document.createElementNS(SVG, 'svg');
    svg.setAttribute('width', _size.width);
    svg.setAttribute('height', _size.height);
    elem.appendChild(svg);

    var gflows = document.createElementNS(SVG, 'g');
    gflows.setAttribute('class', 'flows');
    svg.appendChild(gflows);
    var glinks = document.createElementNS(SVG, 'g');
    glinks.setAttribute('class', 'links');
    svg.appendChild(glinks);
    var gnodes = document.createElementNS(SVG, 'g');
    gnodes.setAttribute('class', 'nodes');
    svg.appendChild(gnodes);

    var cx = _size.width / (2 * VSCALE);
    var cy = _size.height / (2 * VSCALE);

    var force = d3.forceSimulation()
        //.force('x', d3.forceX(0.1))
        //.force('y', d3.forceY(0.5).y(function(d) { return d.y; }))
        .force('link', d3.forceLink().id(function(d) { return d.name; })
            .distance(function(link) {
                //if (link.source.kind == 'node' || link.target.kind == 'node')
                //    return 30;
                return 80; }))
        .force('charge', d3.forceManyBody()
            .strength(function() { return -150; })
        )
        .force('center', d3.forceCenter(cx, cy));

    var need_remove_center = true;
    force.on('tick', function() {
        // We only use the center force for initial positioning
        if (need_remove_center && force.alpha() < 0.1) {
            force.force('center', null);
            need_remove_center = false;
        }

        //node.attr('cx', function(d) { return d.x * VSCALE; })
        //    .attr('cy', function(d) { return d.y * VSCALE; });
        data.graph.nodes.forEach(function(d) {
            if (!d._elem)
                return;

            d.x = c(d.x, _size.width);
            d.y = c(d.y, _size.height);
            d._elem.setAttribute('transform', 'translate(' + x2s(d.x) +
                        ',' + y2s(d.y) + ')');
        });

        data.graph.links.forEach(function(link) {
            if (!link._elems)
                return;
            var x2 = x2s(c(link.source.x, _size.width));
            var y2 = y2s(c(link.source.y, _size.height));
            var x1 = y2s(c(link.target.x, _size.width));
            var y1 = y2s(c(link.target.y, _size.height));

            var mul = 1;
            var dx = link.source.x - link.target.x,
                dy = link.source.y - link.target.y;
            var snMag = Math.sqrt(dy * dy + dx * dx);
            if (snMag === 0) return '';

            link._elems.forEach(function(e) {
                e.setAttribute('x1', x1);
                e.setAttribute('y1', y1);
                e.setAttribute('x2', x2);
                e.setAttribute('y2', y2);
                if (e.attributes.vnet_line_offset) {
                    var mul = e.attributes.vnet_line_offset|0;
                    var tx = mul * -dy / snMag;
                    var ty = mul * dx / snMag;
                    e.setAttribute('transform', 'translate(' + (tx * VSCALE) + ',' +
                                (ty * VSCALE) + ')');
                }
            });

            link._elem_filter.setAttribute('x', (x1 + x2) / 2);
            link._elem_filter.setAttribute('y', (y1 + y2) / 2);
        });

        redrawFlows();
    });

    var dcontrol = DragControl(svg, force, layer, _handler, _size);
    _$.events(window, dcontrol.drag, 'mousemove', 'touchmove');
    _$.events(window, dcontrol.end, 'mouseup', 'touchend');

    var sync = function() {
        /* Roll-your-own
         * TODO: support for inserting virtual elements
         * TODO: support for virtual remove/add
         *
         * Gets worse when adding flows:
         *  link._line_hl
         *  link._line_rx
         *  link._line_tx
         *
         * Placement: node._elem
         * Substyle:
         *  node._icon (scenario support only)
         */

        data.graph.nodes.forEach(function(node) {
            if (node._elem)
                return;
            //if (node.name.match(/^nfv/))
            //    return;
            node.x = cx;
            node.y = cy;

            // TODO: dragging
            node._elem = document.createElementNS(SVG, 'g');
            node._elem._node = node;
            node._elem.setAttribute('class', 'node');
            node._elem.setAttribute('data-group', nodeGroup(node));

            _$.events(node._elem, dcontrol.start, 'mousedown', 'touchstart');

            //// Click thing
            //ng.append('path')
            //    //.attr('d', d3.symbol().type('circle').size(1600))
            //    .attr('d', $shapes.rect(64))
            //    .attr('style', 'fill: transparent')
            //    .on('mouseover', function(d) {
            //        d.fixed |= 4;
            //        d.px = d.x;
            //        d.py = d.y;
            //    })
            //    .on('mouseout', function(d) {
            //        d.fixed &= ~4;
            //    });

            var c = document.createElementNS(SVG, 'path');
            c.setAttribute('class', 'node-shape');
            c.setAttribute('d', nodeShape(node));
            node._elem_shape = c;
            node._elem.appendChild(c);

            var iconUrl = nodeIcon(node);
            if (iconUrl !== '') {
                var i = document.createElementNS(SVG, 'image');
                i.setAttribute('class', 'node-icon');
                i.setAttribute('x', -8);
                i.setAttribute('y', -8);
                i.setAttribute('height', 16);
                i.setAttribute('width', 16);
                i.setAttributeNS(XLINK, 'xlink:href', nodeIcon(node));
                node._elem.appendChild(i);
            }

            var tg = document.createElementNS(SVG, 'g');
            tg.setAttribute('class', 'node-label');
            var tt = document.createElementNS(SVG, 'text');
            tt.setAttribute('x', 20);
            tt.setAttribute('y', 3);
            if (node.name.match(/^s1/)) {
                tt.textContent = 'webshop1';
            } else if (node.name.match(/^s2/)) {
                tt.textContent = 'webshop2';
            } else if (node.name.match(/edge/)) {
                tt.textContent = node.name.replace(/.*\.([^\.]+)$/, '$1');
            } else {
                tt.textContent = node.name.replace(/^([^\.]+)\..*/, '$1');
            }
            node._elem_label = tt;
            var tr = document.createElementNS(SVG, 'rect');
            tr.setAttribute('x', 18);
            tr.setAttribute('y', -9);
            tr.setAttribute('width', tt.textContent.length * 7);
            tr.setAttribute('height', 16);
            tg.appendChild(tr);
            tg.appendChild(tt);
            node._elem_labelbg = tr;
            node._elem.appendChild(tg);
            gnodes.appendChild(node._elem);
        });

        data.graph.links.forEach(function(link) {
            if (link._elems)
                return;

            //if (link.source.name.match(/^nfv/) || link.target.name.match(/^nfv/))
            //    return;

            // TODO: make sure updates work properly
            var group = document.createElementNS(SVG, 'g');
            var link_hl = document.createElementNS(SVG, 'line');
            var link_rx = document.createElementNS(SVG, 'line');
            var link_tx = document.createElementNS(SVG, 'line');
            var link_click = document.createElementNS(SVG, 'line');
            var link_filter = document.createElementNS(SVG, 'image');
            link_filter.style.display = 'none';

            link_hl.setAttribute('class', 'link-hl');
            link_rx.setAttribute('class', 'link-traffic-rx');
            link_rx.attributes.vnet_line_offset = 1;
            link_tx.setAttribute('class', 'link-traffic-tx');
            link_tx.attributes.vnet_line_offset = -1;
            link_click.setAttribute('class', 'link-click');
            link_filter.setAttribute('width', '16');
            link_filter.setAttribute('height', '16');

            _$.events(link_click, function() { _handler.clickLink(layer, link, this); },
                'click', 'touchstart');
            link._elem = group;
            link._elems = [link_hl, link_rx, link_tx, link_click];
            link._elem_filter = link_filter;
            group.appendChild(link_hl);
            group.appendChild(link_rx);
            group.appendChild(link_tx);
            group.appendChild(link_filter);
            group.appendChild(link_click);
            glinks.appendChild(group);
        });

        force.nodes(data.graph.nodes);
        force.force('link').links(data.graph.links);

        redraw_properties();
    };

    var redraw_properties = function() {
        data.graph.links.forEach(function(link) {
            if (!link._elems)
                return;

            link._elems[1].style.stroke = (function(d) {
                if (d._rate_error) return 'white';
                if (!d._latest_rx || d._is_up === false || !d._cost_rate)
                    return '';
                return trafficolor(d._latest_rx, d._cost_rate);
            })(link);
            link._elems[2].style.stroke = (function(d) {
                if (d._rate_error) return 'white';
                if (!d._latest_tx || d._is_up === false || !d._cost_rate) {
                    return '';
                }
                return trafficolor(d._latest_tx, d._cost_rate);
            })(link);

            link._elem.setAttribute('data-traffic-tx', (function(d) {
                if (d._latest_tx === 0 && d._is_up === false)
                    return 'off';
                return valueClass(d._latest_tx);
            })(link));

            link._elem.setAttribute('data-traffic-rx', (function(d) {
                if (d._latest_rx === 0 && d._is_up === false)
                    return 'off';
                return valueClass(d._latest_rx);
            })(link));

            link._elem_filter.style.display = link.metadata.filter == '' ? 'none' : '';
            link._elem_filter.setAttributeNS(XLINK, 'xlink:href', linkIcon(link));
        });

        data.graph.nodes.forEach(function(node) {
            if (!node._elem)
                return;

            if (node.metadata['service-sales'] !== undefined) {
                node._elem.setAttribute('data-qos', nodeQoS(node));
            }

            if (node.metadata['nfv-chain'] !== undefined) {
                var chain = goarray(node.metadata['nfv-chain']);
                var p;
                if (chain.length)
                    node._elem_label.textContent = chain.map(function(v) {
                        return v.replace(/:.*$/, '');
                    }).join(' → ');
                else
                    node._elem_label.textContent = 'nfv-cluster';
                node._elem_labelbg.setAttribute('x',
                    18 + (chain.length <= 1 ? 0 : (chain.length * 8))
                );
                node._elem_label.setAttribute('x',
                    20 + (chain.length <= 1 ? 0 : (chain.length * 8))
                );
                node._elem_shape.setAttribute('d',
                    nodeShape(node, Math.max(1, 1.5 * chain.length)));
                if (node._elem_icons === undefined)
                    node._elem_icons = [];
                while (node._elem_icons.length > chain.length) {
                    p = node._elem_icons.pop();
                    node._elem.removeChild(p);
                }
                while (chain.length > node._elem_icons.length) {
                    p = document.createElementNS(SVG, 'image');
                    p.setAttribute('class', 'node-icon');
                    p.setAttribute('y', -8);
                    p.setAttribute('height', 16);
                    p.setAttribute('width', 16);
                    node._elem_icons.push(p);
                    node._elem.appendChild(p);
                }
                var start = /*chain.length == 1 ? -8 : -10*/-8;
                var dist = chain.length == 1 ? 16 : 18;
                for (p = 0; p < chain.length; p++) {
                    node._elem_icons[p].setAttribute('x',
                        (start * chain.length) + (p * dist));
                    node._elem_icons[p].setAttributeNS(XLINK, 'xlink:href',
                        'static/icons/' + chain[p].replace(/:.*$/, '') + '.png');
                }
            }
        });
    };

    var force_started = false;
    var flowmap = {};
    var redrawFlows = function() {
        gflows.textContent = '';
        _$.forEachObj(flowmap, function(v) {
            return drawFlowsSVG(data, gflows, v);
        });
    };

    return {
        _svg: svg,
        _links: glinks,
        _flows: gflows,

        redraw: function() {
            sync();
            redrawFlows();
        },
        flows: function(flows) {
            // TODO: add similar flows
            // TODO: cache, by origin
            flowmap = buildFlowMap(gflows, data, flows);
            redrawFlows();
        },
        redraw_properties: redraw_properties,
        resize: function() {},
        activate: function() {
            elem.classList.remove('graph-hidden');
        },
        deactivate: function() {
            elem.classList.add('graph-hidden');
        },
        selectNames: function(items) {
            data.graph.nodes.forEach(function(node) {
                var r = _$.any(items, function(e) { return node.name == e; });
                if (node._elem)
                node._elem.classList[r ? 'add' : 'remove']('selected');
            });
            data.graph.links.forEach(function(link) {
                var r = _$.any(items, function(e) { return link.name == e; });
                if (link._elem)
                link._elem.classList[r ? 'add' : 'remove']('selected');
            });
            //nodes.classed('selected', function(d) {
            //    var r = _$.any(items, function(e) { return d.name == e; });
            //    return r;
            //});
            //links.classed('selected', function(d) {
            //    var r = _$.any(items, function(e) { return d.name == e; });
            //    return r;
            //});
        }
    };
};

/**
 * Flows
 */
//var _canvas = document.getElementById('links');
//var _links = _canvas.getContext('2d');
//var _links = document.getElementById('links');

var buildFlowMap = function(gflows, data, flows) {
    // First we need a mapping of flows to links (rx/tx)

    // 0 <origin>
    // 1 <sender mac> <sender domain>
    // 3 <target mac> <target domain>
    // 5 <proto> 6 <bytes> 7 <pkts>

    // TODO: cache, not here
    var flowmap = {};

    flows.forEach(function(v) {
        if (v[1] == '.') return;
        var sender = data.lookupMAC[v[1]];
        var target = data.lookupMAC[v[3]];
        if (!sender || !target) {
            console.log('flow?', sender, v[1], target, v[3]);
            return;
        }
        sender = sender.node;
        target = target.node;

        var key, entry, swap;
        if (sender > target) { key = target + ' ' + sender; swap = true; }
        else { key = sender + ' ' + target; swap = false; }

        entry = flowmap[key];
        if (entry === undefined) {
            flowmap[key] = entry = {
                authority: v[0],
                sender: swap ? target : sender,
                target: swap ? sender : target,
                rx: [], tx: []};
        } else if (entry.authority != v[0])
            return;

        if (swap) {
            entry.rx.push(v);
        } else {
            entry.tx.push(v);
        }
    });

    return flowmap;

};

var flowColor = function(flow) {
    switch (flow[2]) {
        case '1': return 'blue';
        case '3': return '#91e467';
        case '4': return '#9167e4';
        case '5': return '#a09daf';
    }
    return 'yellow';
};

var drawFlowsSVG = function(data, gflows, link) {
    var _maxbw = 1000000;
    var rx_flows = link.rx;
    var tx_flows = link.tx;
    //var line = document.createElementNS(SVG, 'line');
    var source = data._lookup[link.sender];
    var target = data._lookup[link.target];
    var sx = x2s(source.x); // + 10
    var sy = y2s(source.y);
    var tx = y2s(target.x);
    var ty = y2s(target.y);

    // Line normal
    var mul = 8;
    var dx = sx - tx;
    var dy = sy - ty;
    var snMag = Math.sqrt(dy * dy + dx * dx) || 0.00000001;
    var zx = -dy / snMag;
    var zy = dx / snMag;
    var fx = mul * zx;
    var fy = mul * zy;
    zx *= 3;
    zy *= 3;

    // Adjust center points
    if (0) {
    var uvx = dx / snMag;
    var uvy = dy / snMag;
    sx -= uvx * 41;
    sy -= uvy * 41;
    tx += uvx * 41;
    ty += uvy * 41;
    }

    var bundle = document.createElementNS(SVG, 'g');
    bundle.link = link.name;

    var drawFlowBundle = function(init, start, dest, flows) {
        var c = init;

        //var totalbw = flows.reduce(function(a, b) {
        //    return a + +b[6];
        //}, 0);

        // 6 = bytes
        flows.forEach(function(flow, i) {
            var curc = (+flow[6])/5000;
            c += Math.sqrt(curc);

            var ox = zx * i; // c
            var oy = zy * i;

            var cx = (dest[0] + start[0]) / 2 + ox + (fx * c);
            var cy = (dest[1] + start[1]) / 2 + oy + (fy * c);
            if (flow > _maxbw) _maxbw = flow;

            var p = document.createElementNS(SVG, 'path');
            var path = ('M' + (start[0] + ox) + ',' + (start[1] + oy) + ' ' +
                        'Q' + cx + ',' + cy + ' ' +
                        (dest[0] + ox)  + ',' + (dest[1] + oy));
            p.setAttributeNS(null, 'd', path);
            p.style.strokeWidth = String(Math.max(curc, 0.8)) + 'px';
            p.style.stroke = flowColor(flow);
            p.style.fill = 'transparent';
            bundle.appendChild(p);

        });
    };

    var p;
    var dpath = ('M' + sx + ',' + sy + ' ' +
                 tx + ',' + ty);

    // TODO: this is not triggered
    if (!rx_flows.length && !tx_flows.length) {
        p = document.createElementNS(SVG, 'path');
        p.setAttributeNS(null, 'd', dpath);
        p.style.strokeWidth = '2px';
        p.style.stroke = '#333';
        bundle.appendChild(p);
    }

    if (1) {
        p = document.createElementNS(SVG, 'path');
        p.setAttributeNS(null, 'd', dpath);
        p.style.strokeWidth = '12px';
        p.style.stroke = 'transparent';
        bundle.appendChild(p);
    }

    drawFlowBundle(1, [sx, sy], [tx, ty], rx_flows);
    zx = -zx;
    fx = -fx;
    zy = -zy;
    fy = -fy;
    drawFlowBundle(1, [tx, ty], [sx, sy], tx_flows);

    gflows.appendChild(bundle);
    return bundle;
};
