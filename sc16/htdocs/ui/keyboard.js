/* Create a virtual keyboard handler
 */
vnet.KeyboardHandler = function(keyb_elem, send_events_to) {
    var BACKSPACE = '⇐';
    var keyboard_input = function(e) {
        console.log('Event:', e, e.target, 'Text:', e.textContent);
        if (e.textContent == BACKSPACE) {
            send_events_to(false);
        } else if (e.textContent == 'OK') {
            send_events_to(true);
        } else {
            send_events_to(e.textContent);
        }
    };

    //keyb_elem.innerHTML = Handlebars.templates.keyboard();
};

/*
 */
vnet.HighscoreInput = function(elem) {
    elem.innerHTML = Handlebars.templates.highscoreInput();

    _$.events(elem.querySelector('keyboard'), keyboard_input,
              'click', 'touchstart');
};
