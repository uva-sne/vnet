/* jshint esnext: true */

var TAU = 2 * Math.PI;
var SLICE_NAME = '';

var $loop = function(elem) {
    var width = 500, height = 150;
    var r = height / 2;
    var arc = d3.arc().innerRadius(r - 20).outerRadius(r);
    var larc = d3.arc().innerRadius(r).outerRadius(r + 10);
    var svg = vnet.svg(width, height + 30);
    elem.appendChild(svg);

    var g = document.createElementNS(SVG, 'g');
    g.setAttribute('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');
    g.setAttribute('class', 'control-loop');
    svg.appendChild(g);

    var data = [
        {name: 'Detect',  active: 1, dx: 10},
        {name: 'Analyze', active: 0, dx: 2},
        {name: 'Decide',  active: 0, dx: -23, dy: 8},
        {name: 'Respond', active: 0, dx: -72},
        {name: 'Learn',   active: 0, dx: -57},
    ];

    var arcs = d3.pie().value(1).padAngle(0.03);
    arcs(data).forEach(function(v) {
        v.data.startAngle = v.startAngle;
        v.data.endAngle = v.endAngle;
        v.data.padAngle = v.padAngle;
    });

    data.forEach(function(v) {
        var grp = document.createElementNS(SVG, 'g');

        v._elem_g = grp;
        v._elem_arc = document.createElementNS(SVG, 'path');
        v._elem_text = document.createElementNS(SVG, 'text');

        v._elem_g.setAttribute('class', 'arc ' + v.name.toLowerCase());

        v._elem_arc.setAttribute('d', arc(v));

        var xy = larc.centroid(v);
        xy[0] += v.dx !== undefined ? v.dx : 0;
        xy[1] += v.dy !== undefined ? v.dy : 0;
        v._elem_text.setAttribute('transform', 'translate(' + xy + ')');
        v._elem_text.setAttribute('dy', '.35em');
        v._elem_text.textContent = v.name;

        grp.appendChild(v._elem_arc);
        grp.appendChild(v._elem_text);
        g.appendChild(grp);
    });

    var style = function() {
        data.forEach(function(v) {
            if (v.active) {
                v._elem_g.classList.add('active');
            } else {
                v._elem_g.classList.remove('active');
            }
            //.attr('class', (d) => d.data.name.toLowerCase())
            //.attr('d', arc);
        });
    };
    style();
    return {data: data, style: style};
}(document.querySelector('#control-loop'));

var $hack = {};

(function() {
    var rand = (n) => {
        var d = 50;
        var ary = new Array(n);
        for (var i = 0; i < n; i++) {
            d += (10 * Math.random() - 5) | 0;
            if (d < 0) d = 0;
            else if (d > 30) d = 30;
            ary[i] = d;
        }
        return ary;
    };

    var vshowPct = (n) => String(n) + '%';
    var vshowSpeed = vnet.utils.speed;
    var vshowRev = null;
    var N = 30;


    var apph = 220;

    // CPU
    $hack.cpu1 = rand(N);
    $hack.cpu2 = rand(N);
    $hack.cpu = vnet.ChartLines(document.querySelector('#cpu'), {vmax: 100,
        vshow: vshowPct, line: 85},
            [{label: 'Webshop 1', color: 'hsl(200, 70%, 40%)', values: $hack.cpu1},
             {label: 'Webshop 2', color: 'hsl(280, 70%, 40%)', values: $hack.cpu2}],
             N, 480, apph);


    // Revenue
    $hack.revenue1 = rand(N);
    $hack.revenue2 = rand(N);
    $hack.revenue = vnet.ChartLines(document.querySelector('#revenue'), {vmax: 100,
        vshow: vshowRev,
        line: 120},
            [{label: 'Webshop 1', color: 'hsl(200, 70%, 40%)', values: $hack.revenue1},
             {label: 'Webshop 2', color: 'hsl(280, 70%, 40%)', values: $hack.revenue2}],
            N, 480, apph);

    // Logins
    $hack.s1g = 0;
    $hack.s1b = 0;
    $hack.s2g = 0;
    $hack.s2b = 0;
    $hack.logins1 = rand(N);
    $hack.logins2 = rand(N);
    $hack.logins = vnet.ChartLines(document.querySelector('#logins'), {vmax: 100,
        vshow: null, line: 20},
            [{label: 'Successful', color: 'hsl(100, 70%, 40%)', values: $hack.logins1},
             {label: 'Failed', color: 'hsl(320, 70%, 40%)', values: $hack.logins2}],
            N, 480, apph);

    // Bandwidth
    $hack.bandwidth1 = rand(N);
    $hack.bandwidth = vnet.ChartLines(document.querySelector('#bandwidth'), {vmax: 100,
        vshow: vshowSpeed, y: d3.scalePow},
            [{label: 'Utilized', color: 'hsl(0, 0%, 40%)', values: $hack.bandwidth1}],
            N, 480, 300);

    // Flows
    $hack.flows1 = rand(N/2);
    $hack.flows2 = rand(N/2);
    $hack.flows = vnet.ChartLines(document.querySelector('#flows'), {vmax: 100,
        vshow: null},
            [{label: 'TCP', color: 'hsl(40, 70%, 40%)', values: $hack.flows1},
             {label: 'UDP', color: 'hsl(180, 70%, 40%)', values: $hack.flows2}],
            N/2, 480, 300);
})();

var $detect = (function() {
    var depth = 8;
    var values = {};
    var ring = {};
    var elem = document.getElementById('detect-alert');
    var elems = {};
    var active = {};
    var activeCount = 0;
    //var detected = function(key, onoff) {
    //    if (!elems[key])
    //        return;
    //    if (onoff) {
    //        if (!active[key]) {
    //            active[key] = true;
    //            activeCount += 1;
    //            elem.style.display = 'block';
    //            elems[key].style.display = 'block';
    //            //document.getElementById('cloop').classList.add('active');
    //        }
    //    } else {
    //        if (active[key]) {
    //            active[key] = false;
    //            activeCount -= 1;
    //            if (!activeCount) {
    //                elem.style.display = 'none';
    //                //document.getElementById('cloop').classList.remove('active');
    //            }
    //            elems[key].style.display = 'none';
    //        }
    //    }
    //};
    var last = function(key) {
        return values[key][ring[key]];
    };
    var threshold = function(key, maxval) {
        var t = 0;
        values[key].forEach(function(v) {
            if (v > maxval)
                t += 1;
        });
        return t > 3;
    };
    var bd = document.getElementById('box-detect');
    var detects = [
        {sv: 'cpu_threshold',      t: 'CPU utilization too high'},
        {sv: 'logfail_threshold',  t: 'Abnormal login failure rate'},
        {sv: 'revenue_impacted',   t: 'Revenue below threshold'},
        {sv: 'abnormal_udp_flows', t: 'Abnormal UDP flows detected'}
    ];
    var detectBox = function() {
        var text = '';
        detects.forEach(function(v) {
            if ($agent[v.sv]) {
                text += v.t + '\n';
            }
        });
        bd.textContent = text;
    };

    var detect = function(key) {
        var onoff = false;
        switch (key) {
            case 'logfails':
                onoff =
                    last('logfails') > last('logins') &&
                    last('logfails') > 20;
                $agent.logfail_threshold = onoff;
                break;
            case 'cpu':
                onoff = threshold('cpu', 85);
                $agent.cpu_threshold = onoff;
                break;
            case 'revenue':
                // TODO
                onoff = last('revenue') < 120;
                $agent.revenue_impacted = onoff;
                break;
        }
        detectBox();
    };
    return {
        push: function(key, value) {
            if (values[key] === undefined) {
                values[key] = _$.zeroes(depth);
                ring[key] = 0;
                elems[key] = document.getElementById('detect-alert-' + key);
            }
            var r = ring[key];
            r = (r + 1) % depth;
            values[key][r] = value;
            ring[key] = r;
            // TODO: only after all values were pushed
            detect(key);
        }
    };
})();

var store_macs = function(node_id, kvs) {
    console.log(node_id, kvs);
};

var $com = null;
(function() {
    var fadd = function(ary, v, l) {
        if (isNaN(v)) v = 0;
        ary.shift();
        ary.push(v);
        if (l > v)
            return l;
        return v;
    };
    var nrand = (n) => (n * Math.random()) | 0;
    var sumBw = function(data) {
        var keys = data.table;
        var values = data.values;
        var bw = 0;
        values.forEach(function(table_row) {
            var row = unpack_table_row(keys, table_row);
            bw += Math.max(row.src_rx, row.dst_tx);
            bw += Math.max(row.src_tx, row.dst_rx);
        });
        return bw * 8;
    };
    var kvize = function(ks, vs) {
        var kv = {};
        for (var i = 0; i < ks.length; i++) {
            kv[ks[i]] = vs[i];
        }
        return kv;
    };
    var flows = function(flows) {
        var known_mac_senders = {};
        var udp, tcp, bad_domains = {}, result = [];
        udp = tcp = 0;
        flows.forEach(function(f) {
            //if (known_mac_senders[f[1]] !== undefined) {
            //    if (known_mac_senders[f[1]] !== f[0]) {
            //        return;
            //    }
            //} else {
            //    known_mac_senders[f[1]] = f[0];
            //}
            // TODO: filter dupes
            if (f[5] == 'udp') {
                udp += +f[7];
                // TODO: at least require some threshold
                // TODO: base this on origin MAC, not IP
                if (!bad_domains[f[2]]) {
                    bad_domains[f[2]] = true;
                    result.push(f[2]);
                }
            }
            else if (f[5] == 'tcp') tcp += +f[7];
        });
        tmp = fadd($hack.flows1, tcp,
            fadd($hack.flows2, udp, 0));
        $hack.flows(tmp);
        result.sort();
        $agent.abnormal_udp_flows = result.length > 0;
        $agent.known_ddos_domains = result;
    };
    var respondBox = function(elem, chain) {
        var text = '';
        goarray(chain).forEach(function(v) {
            text += '* ' + v + '\n';
        });
        if (text !== '') {
            text = 'Deployed NFV chain:\n' + text;
        }
        elem.textContent = text;
    };
    var passwordBox = function(elem, pws) {
        var text = '';
        if (canShowPasswords && pws.length > 0) {
            text += 'Latest password attempts:\n* ' + pws.join('\n* ') + '\n';
        }
        elem.textContent = text;
    };
    var server = 'wss://' + window.location.hostname + ':8501/';
    var cpu, revenue, logins, logfails, tmp, tdf, tf, kv;
    var ws = vnet.WSUI(server, function(kind, data) {
        switch (kind) {
        case 'layers:refresh':
            if (data.layers.length > 0) {
                SLICE_NAME = data.layers[0].name;
                ws.send('layer:select', {layer: SLICE_NAME});
                $topology = data.layers[0];
            } else {
                console.log('?', data.layers);
            }
            break;
        case 'layer:update':
            if ($topology === null) {
                SLICE_NAME = data.name;
                ws.send('layer:select', {layer: SLICE_NAME});
            }
            $topology = data;
            break;

        case 'layer:stats':
            $hack.bandwidth(fadd($hack.bandwidth1, sumBw(data), 0));
            break;

        case 'broadcast':
            console.log('bc', data);
            if (data['control-loop-speed'] !== undefined) {
                loopSpeed = data['control-loop-speed'] * 100;
            }
            break;

        case 'm':
            kv = kvize(data.k, data.v);
            if ((tmp = data.id.match(/^vnet:.*:node=s(1|2)/))) {
                if (kv['cpu-pct']) {
                    $hack.cpu(
                        fadd($hack['cpu' + tmp[1]], +kv['cpu-pct'])
                    );
                    // TODO
                    if (tmp[1] == '1') $detect.push('cpu', +kv['cpu-pct']);
                } else if (kv['service-sales']) {
                    $hack.revenue(
                        fadd($hack['revenue' + tmp[1]], +kv['service-sales'])
                    );
                    // TODO
                    if (tmp[1] == '1') $detect.push('revenue', +kv['service-sales']);
                    $hack['s' + tmp[1] + 'g'] = +kv['service-logins'];
                    $hack['s' + tmp[1] + 'b'] = +kv['service-auth-fails'];
                    if (tmp[1] == '1') {
                        tmp = fadd($hack.logins1, $hack.s1g + $hack.s2g,
                            fadd($hack.logins2, $hack.s1b + $hack.s2b));
                        $hack.logins(tmp);
                        $detect.push('logins', $hack.s1g + $hack.s2g);
                        $detect.push('logfails', $hack.s1b + $hack.s2b);
                    }
                }
            } else if (data.id.match(/node=nfv/)) {
                if (kv['ids.cpu'] !== undefined && kv['ids.pw'] !== undefined) {
                    $agent.known_cpu_attackers = goarray(kv['ids.cpu']);
                    $agent.known_password_crackers = goarray(kv['ids.pw']);
                }
                if (kv['nfv-chain'] !== undefined) {
                    respondBox(document.getElementById('box-respond'), kv['nfv-chain']);
                }
                if (kv['honeypot.pws'] !== undefined) {
                    passwordBox(document.getElementById('box-analyze-passwords'),
                        goarray(kv['honeypot.pws']));
                }
            }
            break;

        case 'layer:metadata':
            cpu = 0;
            revenue = 0;
            logins = 0;
            logfails = 0;
            data.values.forEach(function(v) {
                if (v[0].match(/^s1/)) {
                    revenue = fadd($hack.revenue1, +v[1]['service-sales'], revenue);
                    logins += +v[1]['service-logins'];
                    logfails += +v[1]['service-auth-fails'];
                } else if (v[0].match(/^s2/)) {
                    cpu = fadd($hack.cpu2, +v[1]['cpu-pct'], cpu);
                    revenue = fadd($hack.revenue2, +v[1]['service-sales'], revenue);
                    logins += +v[1]['service-logins'];
                    logfails += +v[1]['service-auth-fails'];
                }
            });
            $hack.cpu(cpu);
            $detect.push('cpu', cpu);
            $hack.revenue(revenue);
            $detect.push('revenue', revenue);
            tmp = fadd($hack.logins1, logins,
                          fadd($hack.logins2, logfails, 0));
            $hack.logins(tmp);
            $detect.push('logins', logins);
            $detect.push('logfails', logfails);
            break;

        case 'flows':
            flows(data || []);
            break;

        default:
            console.log(kind, data);
        }
    });
    $com = ws;
})();

var ND = function(v) {
    return 'vnet:' + SLICE_NAME + ':node=' + v;
};
var loopSpeed = 1200;
var canShowPasswords = false;

var $topology = null;
var $agent = (function() {
    var timestamp = () => (new Date()).getTime();
    var response_started_at = null;
    var marked = false;
    var TIME_UNTIL_REMOVAL = 20000;

    var sdn = 'switch.services.as100';
    var nfv = 'nfv.services.as100';

    var nfv_mac = '01:01:01:01:01:01';
    var domain_edges = {};

    var solutions = {

        // Note that this is not really correct, it's just that the IDS
        // is always there
        'ids': {
            apply: function() {
                $com.send('node:task', {target: [ND(sdn)], task: 'sdn', args: [nfv_mac]});
            },
            clear: function() {
                $com.send('node:task', {target: [ND(sdn)], task: 'sdn', args: []});
            }
        },
        'udp-filter': {
            apply: function(s) {
                var targets = [];
                s.params.forEach(function(v) {
                    targets.push(domain_edges[v]);
                });
                if (targets.length)
                    $com.send('link:task', {target: targets,
                        task: 'link-filter', args: ['udp']});
            },
            clear: function() {
                var targets = [];
                _$.forEachObj(domain_edges, function(v) {
                    targets.push(v);
                });
                $com.send('link:task', {target: targets,
                    task: 'link-filter-flush', args: []});
            }
        }
    };

    var activeSolutions = {};
    var activeChain = [];
    var formatIP = function(ips) {
        return ips.reduce(function(root, v) {
            return root + ':' + v.replace(/^10\.100\./, '');
        }, '');
    };
    var formatMask = function(ips) {
        var known = {};
        return ips.reduce(function(root, v) {
            var domain = v.split('.')[2];
            if (known[domain] === undefined) {
                known[domain] = true;
                return root + ':' + domain + '.0/24';
            }
            return root;
        }, '');
    };

    var agent = {
        respond: function() {
            _$.forEachObj(activeSolutions, function(v, k) {
                if (!v.apply)
                    return;
                v.apply = false;
                console.log('apply', k);
                var s = solutions[k];
                if (s && s.apply) s.apply(v);
            });

            agent.tick_down();

            var nfv_chain = [];
            if (activeSolutions.ids)
                nfv_chain.push('ids');
            if (activeSolutions.honeypot)
                nfv_chain.push('honeypot' + formatIP(activeSolutions.honeypot.params));
            if (activeSolutions.captcha)
                nfv_chain.push('captcha' + formatMask(activeSolutions.captcha.params));

            if (!_$.arraysEqual(nfv_chain, activeChain)) {
                activeChain = nfv_chain;
                $com.send('node:task', {target: [ND(nfv)], task: 'chain',
                    args: nfv_chain});
            }

            canShowPasswords = activeSolutions.honeypot !== undefined;
        },

        tick_down: function() {
            var now = timestamp();

            _$.forEachObj(activeSolutions, function(v, k) {
                // TODO: non-fixed interval
                var delta = now - v.activeAt;
                if (delta < TIME_UNTIL_REMOVAL)
                    return;
                console.log('erase', k);
                delete activeSolutions[k];
                var s = solutions[k];
                if (s && s.clear) s.clear(v);
            });
        },

        consider: function(solution, params) {
            marked = true;
            var apply = true;

            if (activeSolutions[solution]) {
                if (params && typeof params == 'object') {
                    apply = !_$.arraysEqual(params, activeSolutions[solution].params);
                } else {
                    apply = params != activeSolutions[solution].params;
                }
            }

            //console.log('consider', solution, params, apply);
            activeSolutions[solution] = {
                activeAt: timestamp(),
                apply: apply,
                params: params
            };
        }
    };

    var state = {
        revenue_impacted: false,
        cpu_threshold: false,
        logfail_threshold: false,
        abnormal_udp_flows: false,
        known_cpu_attackers: [],
        known_password_crackers: [],
        known_ddos_domains: []
    };

    var decide = function(state) {
        // Future work: cost of response vs lost revenue
        marked = false;

        //document.getElementById('agent-state').textContent = (
        //    JSON.stringify(state, undefined, 4) + '\n' +
        //    JSON.stringify(activeSolutions, undefined, 4));

        // TODO:
        // - Future work: upstream should tell us this
        if (state.abnormal_udp_flows)
            agent.consider('udp-filter', state.known_ddos_domains);

        // XXX: Be quite liberal in when to deploy IDS, it should self-correct
        var need_ids = false;
        if (state.cpu_threshold || state.logfail_threshold || state.known_cpu_attackers.length || state.known_password_crackers.length)
            agent.consider('ids');

        if (activeSolutions.ids) {
            if (state.known_cpu_attackers.length)
                agent.consider('captcha', state.known_cpu_attackers);

            if (state.known_password_crackers.length)
                agent.consider('honeypot', state.known_password_crackers);
        } else if (need_ids) {
            agent.consider('ids');
        }
    };
    var formatDomains = function(dmns) {
        return dmns.map(function(v) {
            return 'AS' + v + '00';
        }).join(', ');
    };

    var analyzeBox = function(elem) {
        var text = '';
        var needids = state.logfail_threshold || state.cpu_threshold;
        if (state.known_cpu_attackers.length > 0) {
            text += 'Known CPU attackers: ' + state.known_cpu_attackers.join(', ') + '\n';
            needids = false;
        }
        if (state.known_password_crackers.length > 0) {
            text += 'Known crackers: ' + state.known_password_crackers.join(', ') + '\n';
            needids = false;
        }
        if (state.known_ddos_domains.length > 0) {
            text += 'DDoS domains: ' + formatDomains(state.known_ddos_domains) + '\n';
        }
        if (needids) {
            text += 'Insufficient information.\n';
        }
        elem.textContent = text;
        return _$.deepcopy(state);
    };

    var decideBoxText = {
        'ids': 'Deploy IDS to gather additional data',
        'honeypot': 'Deploy honeypot to divert and capture attack',
        'captcha': 'Deploy mandatory CAPTCHA to filter non-humans',
        'udp-filter': 'Filter UDP traffic at edge domains',
    };
    var decideBox = function(elem) {
        var text = '';
        _$.forEachObj(activeSolutions, function(v, k) {
            var remain = TIME_UNTIL_REMOVAL - (timestamp() - v.activeAt);
            var postfix = '';
            if ((remain / TIME_UNTIL_REMOVAL) < 0.8)
                postfix = ' (remaining active for ' + String(Math.max(0, (remain / 1000) | 0)) + 's)';
            text += decideBoxText[k] + postfix + '\n';
        });
        //
        elem.textContent = text;
    };

    var macForHost = function(name) {
        var mac;
        $topology.nodes.forEach(function(n) {
            if (n.name !== name)
                return;
            mac = n.ifaces[0].mac;
        });
        console.log('MAC for', name, '->', mac);
        return mac;
    };
    var linkID = function(name) {
        var ip;
        // TODO: zomg-optimize
        $topology.nodes.forEach(function(n) {
            if (n.name !== name)
                return;
            // Actually.. get neighbour IP
            var ours = n.ifaces[0].ipv4;
            var link = n.ifaces[0].links[0];
            var neigh;
            $topology.links.forEach(function(l) {
                if (l.name != link)
                    return;
                neigh = l.source == name ? l.target : l.source;
            });
            $topology.nodes.forEach(function(nn) {
                if (nn.name != neigh)
                    return;
                // Wew
                nn.ifaces.forEach(function(i) {
                    if (i.links[0] == link)
                        ip = i.ipv4[0];
                });
            });
        });
        ip = 'vnet:' + $topology.name + ':ipv4=' + ip;
        console.log('IP for', name, '->', ip);
        return ip;
    };

    var init = function() {
        nfv_mac = macForHost('nfv.services.as100');
        domain_edges = {
            '3': linkID('e1.edge1.as300'),
            '4': linkID('e2.edge2.as400'),
            '5': linkID('e3.edge3.as500'),
        };
    };

    var loopState = 0;
    var actingState = {};
    var didInit = false;
    var controlLoop = function() {
        setTimeout(controlLoop, loopSpeed);

        // Don't do anything if there's no topology
        if ($topology === null)
            return;
        if (!didInit) {
            init();
            didInit = true;
        }

        switch (loopState) {
        case 0:
            $loop.data.forEach((v) => v.active = 0);
            break;

        case 1:
            // Show detected events
            actingState = analyzeBox(document.getElementById('box-analyze'));
            break;

        case 2:
            decide(actingState);
            decideBox(document.getElementById('box-decide'));
            break;

        case 3:
            agent.respond();
            //respondBox(document.getElementById('box-respond'));
            //// TODO: show applied responses
            break;
        }

        $loop.data[loopState].active = 1;
        $loop.style();
        loopState = (loopState + 1) % 5;
    };

    setTimeout(controlLoop, loopSpeed);
    return state;
})();
