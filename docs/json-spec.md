% VNET JSON specification


## Graph

The graph is the top level object and has the following properties:

Property    Description
----------- ------------------------------------------
`name`      Name of the graph (usually slice name)
`parent`    Name of the graph parent's graph
`nodes`     List of nodes
`links`     List of links


## Common object properties

Property    Description
----------- ------------------------------------------
`name`      Friendly name, must be unique within the slice
`url`       Unique identifier in URL/URN form
`guid`      Unique GUID
`metadata`  Key/value pairs that describe the object
`parent`    Element in the parent layer that is directly related to this element
`parents`   List of zero or more elements in the parent layer that are directly related to this element


## Nodes

Property    Description
----------- ------------------------------------------
`kind`      Type of node (e.g. for styling and icons)
`group`     Physical location/ExoGENI cluster
`image`     Type of disk image
`ifaces`    List of interfaces that should be available on the device


## Links

Note that links are bidirectional, so the reverse order link is
equivalent.

Property    Description
----------- ------------------------------------------
`source`    The source node
`target`    The target node



## TODO/request for feedback

* Add support for VLANs
* Add support for stich ports
