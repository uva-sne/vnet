## Tools

General:

* Start/stop attack (scripted or random targets)
  * Default option
  * Enum: type of attack
* Reset link states (?)

Attack host:

* Start attack
  * Select target
  * Enum: type of attack


* Link bandwidth slider
* Link enable/disable

* NF: block all
* NF: block protocol

- For NF, select specific node/link/site as target?
- Show nodes with NFs active

(Re)structuring:

* Event system
* Sidebar (or multiple bars) with flexible components
* "Modal"/popover UIs?

Bonesi deps:

* libpcap0.8
* libnet1


## Demo

* Initialize client links to 50Mbit/s


# SC15 demo monitoring

* Session ID
* Attack label
* Revenue
* Cost


Commands:

    START SESSION
    => Select scenario
    => Create session_id

        START RUN
        => Start timer
        => Execute next attack

        SUBMIT
        => Enforce minimal running time

        VERIFY
        => Lock network/attack state
        => Record average behavior over N seconds

        STORE

    STOP SESSION



UI.Connect
    -> Layers
       -> Configs           // Fixed per layer
       -> ActiveConfig      // From scenario manager

       alt:
       -> Config            // Get active scenario and find related config


ScenarioManager.SetScenario(name)
    -> Notify all UI

ScenarioManager.Reset()
ScenarioManager.Start()
ScenarioManager.Submit()


Server commands:

* layer:configs layer {
   $name: {
    visible-attacker-icon,
    use-custom-colors,
    control-disable,
    ...}}
* layer:config $name

* sc15:scenario:state {timer: 123, running: true}

Client commands:

* sc15:scenario:reset -- hard reset
* sc15:scenario:start -- start scenario
* sc15:scenario:submit
* sc15:scenario:retry

