DOC_PATH = /srv/www/vnet/htdocs/docs
RST2HTML = rst2html --stylesheet=docs/simple.css

MDFLAGS = -s -t html5 -N -c /docs/simple.css

DOC_SOURCES = $(DOC_PATH)/json-spec.html \
	$(DOC_PATH)/tuio.html \
	$(DOC_PATH)/container.html \
	$(DOC_PATH)/sarnet.html \
	$(DOC_PATH)/scenario.html \
	$(DOC_PATH)/sc15-workplan.html \
	$(DOC_PATH)/sc16/workplan.html

.PHONY: docs
docs: $(DOC_PATH)/index.html $(DOC_PATH)/simple.css | $(DOC_SOURCES)

$(DOC_PATH)/simple.css: docs/simple.css
	cp $^ $@

$(DOC_PATH)/index.html: | $(DOC_SOURCES)
	# TODO
	echo > $@
	#tree -D -h -H . docs -P '*.html' -I index.html -o $@

$(DOC_PATH)/%.html: docs/%.md | docs/simple.css
	mkdir -p $(dir $@)
	pandoc $(MDFLAGS) -o $@ $^

$(DOC_PATH)/%.html: docs/%.rst | docs/simple.css
	$(RST2HTML) $^ > $@
