=============
SC15 workplan
=============

.. |date| date:: %Y-%m-%d %H:%M (Amsterdam)

:Updated: |date|

.. contents::

Demo takeaways
==============

Key takeaways:

* Defending against cyberattacks is hard
* The general steps we take in defending against cyberattacks are:
  detect (classify, analyze) ->
  risk assessment ->
  react ->
  learn (measure effects).
  This is what the SARNET control loop is based on.
* Tools are needed to optimize security responses for the best response speed
  and lowest cost of response. We are going to build this!

Touch table interactions:

* Visualize network state
* Graph with "service revenue" (how many requests are reaching the service)
* Graph with "network cost" (current configured bandwidth capacity)
* Allow user to use defensive features listed above


Milestone 1: the basics
=======================

✓ Denial of service
-------------------

* **Done:** Touch table controller can be used to issue commands
* **Done/partial:** Ingress and egress limiting via OS commandline
* *Postponed:* Ingress and egress limiting via OVS commandline
* *Skipped:* Ingress and egress limiting via OpenFlow

NB: all denial of service attacks share requirements.

Status:

* We can do egress filtering using ``tc qdisc .. tbf ..`` on both ends of the
  links, which gives us the performance we want.
  Ingress policing does not work well or requires more tuning/research.

  *This requires that the topology controller must control both ends of the
  link in order to apply limiting.*

* Filling the link using iperf gives the expected performance degradation.

✓ Link enable/disable
----------------------

* **Done:** Enable/disable ports via OS tools
* **Done/Failed:** Enable/disable ports via OVS commandline
* *Optional:* Enable/disable ports via OpenFlow

Status:

* Setting link state and admin state on the interface via the OVS commandline
  **does not** actually affect the status of the link. Not usable!

  Removing and then adding a port adds a not-insignificat amount of wait time
  before the link is usable again.

* Disabling the interface on the system works as expected. (Needs adjustments
  of neuca and OVS scripts on the current images that reenable the interface.)


✓ Link rate
-----------

* **Done:** need basic control over link rate (e.g. set link rate to
  10, 20, ..., 100mbit/s)

  We need working ingress and egress limiting on nodes to simulate link
  capacity so that we can actually perform a denial of service.

Status:

* We use ExoGENI's link capacity, which means only ingress limiting and
  no further control.
* Preliminary experiments with tc were not succesful (we were unable to
  tune it to get the expected link rates)


Additional work done
--------------------

Interesting/important unplanned work:

* **Done:** Update TUIO library for TUIO browser plugin and debug/improve
  implementation.

* **Done:** Add configuration for VNET ports so that multiple instances
  can be run at the same time more easily.



Milestone 2: data gathering & visualization
===========================================

✓ Image updates
---------------

* **Done:** Update and deploy existing monitoring tools on the nodes for
  bandwidth monitoring.

* **Done:** Allow per-slice configuration of monitoring/log/&c controllers.

* **Done:** Deploy additional logging/statistics aggregation tools 


✓ Topology controller
----------------------

* **Done:** Fix metrics collection and network interface detection.
  Review bandwidth monitoring code (currently depends on fixed IP allocations
  per ethernet interface because mapping interfaces to the links in the graphs
  is difficult)
* **Done:** Add support for RPC commands that have to be sent to multiple
  nodes at the same time (for link control)
* **Done:** Add support for link state reporting by switches
* If necessary, add/verify support for joining multiple slices


✓ UI controller/monitor
-----------------------

* **Done:** update statistics to track bandwidth of network
  elements, properly taking into account packet/rate loss.

* **Done:** fix tracking of monitoring provided dynamic metadata.


Additional work done
--------------------

Interesting/important unplanned work:

* Rewrote most controller code to better deal with nodes
  connecting/disconnecting and topology changes.

* Refactored UI code.

* Investigate if traffic shaping works on VMs: it works as expected.


New issues
----------

* A purely switch based topology leads to suboptimal traffic paths as
  expected (due to spanning tree, mostly). 

  We may need to write an OpenFlow controller to do basic routing.



Milestone 3: SARNET
===================


✓ Demo/demo controller
----------------------

* Automatically start tracking "revenue" of new services that connect to the
  monitoring agent.


✓ Simulation configuration
--------------------------

* Tools to (pre)configure the client-server connections that are used in the
  simulation


✓ Running scenarios
-------------------

Tools to configure the topology for different attack scenarios .


* **Done:** Touch table controller needs to be able to coordinate
  starting
  scripts on multiple nodes, reconfiguring the state of the network at the
  start of a scenario
* **In progress:** allow hiding of certain topology elements depending on which
  scenario is active.
  Also various other options such as colors and available tools/control

* *Done:* Nodes randomly enter and exit attack state to make detection
  more difficult.

* **Done:** Investigate and package best tools to perform traffic generation
  (iperf, bonesi)


Unknown requirements as of yet: Behavior/attack scripting?

* Script to start (coordinated D)DoS on client VM nodes


Status:

* We'll continue using iperf for bandwidth generation for now, since e.g. the
  bonesi tool is meant more for resource exhaustion.
  For example, we have no way of tracking the number of "active"/attempted TCP
  connections on the machine right now, so we can't visualize e.g. SYN floods.



Additional work done
--------------------

* Write WebSocket TUIO proxy, since plugin has lots of issues.



Milestone *n*: final stretch
============================

Stability testing
-----------------

* Make sure all required OVS functionality actually works as expected.
  (Stability, actions has the desired effects, configured bandwidth can be
  attained.)



Backlog
=======

Not yet planned, but (non-OpenFlow things) will be done before SC15.




"Low bandwidth" DDoS
--------------------

In this scenario the link between service and network is easily congested.
E.g. it can only handle 10mbit/s while the network handles 100mbit/s.

This is primarily to demonstrate that you can deploy NFs across the
network to filter/shape traffic.

* **Required:** Document: what kind of filtering and shaping can OVS do that is
  relevant for SC15?
* *Experiment:* can we defend from this effectively? What are effective parameters?

* **Required:** Apply filters/shaping via OVS commandline
* *Optional:* Apply filters/shaping via OpenFlow



Traffic filtering/shaping
-------------------------

This will be represented as a *network function* that we can deploy in our
network.

* Our "legit" traffic is TCP and will run over a fixed port (e.g. 80 or
  something similar)
* Different types of attacks can directly attack the service, or use something
  that is easier to filter (depending on what we can get working)


Extra features
--------------

* Investigate and add support for stitch ports

* Investigate if OpenFlow can be used to implement the functionality we
  implement using OVS/OS tools.


* Investigate if we can use OVS to create a sort of "load balancer" when
  redundant links are enabled.
  If not, investigate if router VMs are needed for routing traffic over
  redundant paths.


* *Backup:* Investigate if we need IP tunnels to gain additional flexibility
  that the ExoGENI network might not be able to provide [Depends on whether the
  OVS requirements are doable with just the ExoGENI network setup or not]



External dependencies
=====================

NB: These are soft dependencies, are not required, and do not block our work.


Requested effort from Ciena:

* Investigate and document (basic) filtering/QoS in Open vSwitch that can help
  mitigate a DoS. (Both in OpenFlow and non-OpenFlow mode.)
  E.g. IP or port filters.

* Investigate if some Ciena NF functionality can be integrated into the demo
  topology (must run on ExoGENI or otherwise be directly compatible with the
  network we're building)



Planned to not do
=================

Things we (UvA) won't be doing, mostly due to time constraints:

* Working on ExoGENI itself (codebase is rather complex and we're not Java
  experts)

* 8700 driver (for now, see above); waiting for NETCONF implementation

* Adding support for slice modification (see above)
