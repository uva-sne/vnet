# Issues


* (?) Link states as topology property: "active", "inactive" (potential
  connection)

  * Substates: "killing"/"killed"


* UI: Maybe preposition graph nodes instead of using a force layout.

  Moving around the elements doesn't really mean anything, and you could do
  it fully automatically.

  Makes it easier to stop using D3(?)


* Make charge only apply to connected nodes(?); mostly an orchestration UI
  problem


* Faster rendering

  * Canvas
  * ... but clipping
  * Level of detail


* Zooming/scrolling/viewport changes seem to be horribly broken.


* Node shows graphs for all links
* Detailed link graph:
  * Bandwidth per protocol

# Topology discovery

Simple broadcast protocol:

* On each link, send UDP packet to broadcast address


# TODO

* Sync complete state: scenario, state, time, score
* Hide elements

Robustness:

* vnet-routing as postup?

* Signal when all vnetmon clients for a given topology have connected

* Stateful tasks

  "egress" => "50Mbit"

* State sync task e.g.:
  node: attack=
  link: egress=50Mbit state=up filter=


* Automatically add ExoGENI slice if vnetmon reporting is detected

  * Tricky if cross-domain (geni controller)
  * Would be better if we had a production VNET controller so that test slices
    don't accidentally get added to it

* Slice monitoring from within the topology controller

* Remove useless metadata from ndl2json

* Nicer tools to work with graphs!


* "Announce" values: clean solution for key/value pairs that the UI will
  broadcast on update and on connect

