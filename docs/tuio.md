% TUIO multi-touch support for VNET

# TUIO support

In an ideal world (i.e. Linux) multi-touch devices are exposed as a pointer
device to the operating system and are directly supported by the browser as
touch events.

In the real world we have to deal with TUIO.  With TUIO, multi-touch devices
are exposed over a UDP service running on the machine that is connected to the
device. In the past touch events were bridged to the browser using a browser
plugin, but maintaining a plugin over multiple platforms and browsers is
difficult and the NPAPI plugin standard has been deprecated.

In order to solve these issues, we instead decided to bridge TUIO events over
a standard protocol supported by all modern browsers: WebSockets. This allows
application code running inside the browser to directly handle TUIO events,
for example by translating them to touch events.


## Preparation

Make sure the following software is installed:

* Go 1.5+ (older version may work, YMMV)
* <https://bitbucket.org/uva-sne/tuio-proxy>


## Configuration

* Set up the TUIO software for your multi-touch device.
  It must be available on port 3333.

* Run `tuioproxy`



### "Insecure" setup

The VNET interface runs over HTTPS whereas by default TUIO proxy does not.
While access to the TUIO proxy is limited to the local machine, browsers do
not allow mixing of insecure and secure content.

To work around this issue you must bypass (i.e. disable) this security check
in your browser.

Start Chrome from the commandline (you must kill **all** other instances
first):

     /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --allow-running-insecure-content


### Secure setup

Another option is to run the TUIO proxy server with TLS enabled:

    tuioproxy -cert cert.pem -key key.pem

The certificate must be valid for `tuiproxy.sarnet.uvalight.net`
(Add `tuiproxy.sarnet.uvalight.net 127.0.0.1` to `/etc/hosts`)
and accepted by the browser.

*TODO: not yet supported by VNET*

*TODO: make host configurable*
