% Overview of the SARNET implementation


# SARNET

Diagram:

              --- Moni. <-> Topology          ---- UI
            /                        \      /
      Network Ctrl                     NOS
            \                        /      \
              ---- Controller ------          ---- SARNET solver


* Network state
    * Network controllers (OpenFlow, ONOS, ..)
    * Dynamic configuration

* Topology
    * Deployment platform (ExoGENI, ONOS, ..)
    * Current state of the network/resources

* Observer
    * Receives (and maybe preprocess) network/service state
    * Nodes

* Interaction
    * Modify network/service configuration (state)
    * UI
    * SARNET agent


## UI

* Receives topology updates

* Broadcasting "unlabeled" messages: e.g. topology changes
* Filtering/activating label messages: heavier data, e.g. subscribe to
  "realtime" layer statistics



## Controller values

Basic properties that can be set on a link:

    vnet:link:state    {up, down}
    vnet:link:rate     {10Mbit/s, 50Mbit/s, ...}
    vnet:link:filter   {udp, tcp}


Advanced properties:

* Complex filters
* Network flow modification
* Traffic redirection; routing weights, routes, anycast
* QoS


Resource control:

* Add/remove/migrate VMs
* Add/remove (virtual) links
* VLANs, VPNs, other random things


## SARNET input

* List of VM images



# Topology

The topology controller manages one or more slices and keeps track of the
overall state of the resulting topology.

Requests to topology controller:

* Start monitoring resource
* Refresh topology
* Orchestrate new topology

Events created by topology:

* Received topology update



# Observer

The observer monitors values provided by nodes on the network.

Simple metadata related to nodes is scoped as such:

    (node-id, key) => value


Metadata related to links is less straight forward, because often the node is
not aware of the link identifier as used in the topology (i.e. only has MAC/IP
address, local interface name) and multiple nodes may publish conflicting data
(in value or in time) regarding the same link.

Example:

    (send! 'template:host:iface-stats
        node-id local-link-name
        (iface1 pkts bytes)
        ..
        (ifaceN pkts bytes))

In some cases (e.g. flow information from a switch) only a single source of
information exists.

The OpenFlow controller will learn a mapping between the OpenFlow port (TODO:
check out `ofp_port.name`) and the node (ARP/MAC based, or custom protocol).

The OF controller knows which host is on which port. It should request flow
statistics and then send the following template:

    (send! 'template:switch:port2host
        switch-id
        (port1 host1)
        ..
        (portN hostN))

    (send! 'template:switch:flow-counters
        switch-id
        (port1-a port1-b pkts bytes)
        ..
        (portN-a portN-b pkts bytes))




TBD:

* Broadcasting metadata updates to clients
    * Batching (bandwidth tables)
    * Or IPFIX-like templates?

* Packet loss
* Events

* Dealing with virtual links:
    * Interfaces, especially on the switch, may be used for multiple "virtual"
      links; measure based on flow stats (port-to-port)

* Annoyance: fixed metadata from topology


## Connecting monitoring to the topology

For ExoGENI resources, we use global identifiers for lookup in UI and in
controllers.

Nodes in the topology can be identified using the following scheme:

* ``vnet:$slice:node=$node``

Because we cannot identify links by name on the VM, we use IPv4 addresses
(always unique within the slice):

* ``vnet:$slice:ipv4=$v4``

The downside of this approach is that the topology must also be aware of these
IPv4 addresses, and communicate them to the UI. (Not a problem in the ExoGENI
case.)

We currently do not have any other resources, but the proposed solution is
to either use URNs or URLs.


TODO(?):

* Instruct VM with IPv4-to-link mapping and resolve on machine

* Actions on links may either go to OVS or to the endpoints


## Node behavior

* Nodes self-identify
* Device shares interface information
* Topology controller maps to topology information based on IP address

Link monitoring:

    link_rate = min(source_rate, target_rate)
    loss = max(source_rate, target_rate) - link_rate

Issues with link monitoring:

* Statistics reporting is not time synchronized, so will never be completely
  accurate.
  * Maybe timestamps should be used to shift data points.
  * Clock sync and measure at specific times.
