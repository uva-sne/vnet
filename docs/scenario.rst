==============
Demo scenarios
==============

.. sectnum::

.. contents::
   :backlinks: none

Introduction
============

The general idea is to simulate one or more types of attacks on a service
over a period of time.
The user decides the best defensive response
to maintain the highest degree of availability.
The response is either chosen directly or from a number of (scripted)
responses as determined by a "SARNET agent".

This will be demonstrated on a touch table
and should be part of the ongoing menu built for SC15.

The simulated service will be a sales services (web shop/portal) for a
large company, with compute resources distributed over
two or more sites for typical load balancing/failover purposes.
The distributed nature of the service gives the user multiple choices in
their response, in contrast with an all-or-nothing response that is more likely
with a single site.

Some nodes in the topology will act as traffic generators, to simulate
clients or customers in the network.
They will be constantly sending or receiving traffic, or performing
transactions (depending on the type of service we choose).

The demo focuses not only on defensive measures the user can take within
their own domain (i.e. by exerting control over their own resources),
but also on measures that require cooperation with other domains (upstream
service providers).


Attack scenarios
================

The demo should feature one or more of the attack scenarios described below.

.. Care should be taken to choose attack scenarios that are both realistic (i.e.
   the kind of attacks people are familiar with)
   and can actually be mitigated.

Note that some attacks are hard to differentiate from very high demand:
a much larger than normal number of sales could cause the service to be
disrupted.

We will initially focus on attacks that affect bandwidth usage and network
availability,
and leave attacks that cause excessive compute resource usage as future work.


Denial of service:
    A DoS by one attacker or a small number of attackers.
    This is detected by a large amount of (invalid) traffic from a single
    host (compared to average/normal traffic).

Distributed denial of service:
    A DDoS with traceable traffic origin.
    The source IP addresses in this scenario are not spoofed,
    so they can be blocked at various points in the topology (depending on
    what is most effective.)

Spoofed DDoS:
    A DDoS from spoofed or reflected origin.
    The source IP addresses in this scenario are not (or less)
    predictable so countering this attack will require more subtle defenses
    such as upstream rate limiting or traffic filtering.


Amplified DDoS:
    An amplified DDoS attack. The attacker uses protocols which use a small
    request but generate a large response, and spoofs the requester address to
    the victim.
    (`See also <https://en.wikipedia.org/wiki/Denial-of-service_attack#Reflected_.2F_spoofed_attack>`_)

"Low bandwidth" DDoS:
    A DDoS attack to a low bandwidth customer.
    The upstream provider has sufficient bandwidth capacity but the customer
    makes limited use of it (e.g. due to financial constraints) and is
    effectively unreachable.

0day exploit:
    Attacker actively breaking into a system.
    For example, behavioral anomalies are detected by an IDS.




Defensive strategies
====================

In order to stop or mitigate attacks, the user has the following defensive
options:

* Add extra compute resources or bandwidth.
  The amount of resources that can be added depend on the maximum available
  resources defined by a site.
* Migrate a compute resource to another site that has sufficient bandwidth
  capacity.
* Blackhole IP ranges upstream.
* Block or rate limit protocols that are vulnerable to amplification attacks
  (DNS/NTP) at the provider side.
* Insert a traffic limiting NF upstream.
  The user places a virtual router before their link using NFV at the
  provider side. Since it is in the provider network it is capable of
  blocking/filtering traffic before it reaches the (lower bandwidth) link.
* *Divide and conquer* strategies, e.g. segmenting the network to maximize the
  availability for all users while preventing a single site from being
  overburdened by traffic coming from many sources.
* Shut down misbehaving compute resources.

*More to be defined?*


Side effects:

* Indiscriminate blackholing/filtering causes the service to be unavailable for
  legitimate users.

.. * [Probably requires cooperation with upstream providers to better trace
   the actual origin of the attack]
.. * The customer places a DDoS filter NF on their link. This filter NF has to
   be somehow managed by the provider and could use several strategies.

TODO: consider what SCinet is doing for SC15.
They have a security team that deals with this at all the SC events.


Simulated service
==================

The simulated service will be a simple HTTP based web portal.
It simulates two actions:
rendering a product catalog (transferring a large data body)
and performing a sale (high CPU usage).

A customer will always first fetch the catalog and then attempt to buy
a product.
If the customer is unable to perform either of these two steps, no sale
is recorded.


The service must remains available and *profitable* in order to consider the
defensive measures to be successful.


Economic aspects
================

Since compute resources and bandwidth are not free [#]_, and have a direct
effect on the type of defensive choices a user can or would make,
we propose a simple financial model for the demo.

.. [#] If adding resources costs nothing, the obvious solution will always be
   "add more resources until it works"...


If the service is unavailable (not enough bandwidth, packet loss, no route
to the customer, ...) then no sale can be recorded.
Additional criteria such as a maximum response time can also be added
to simulate lost sales from suboptimal service performance.

The basic simulated "economy" to motivate user decisions should include:

  * Cost of resources
  * Cost of bandwidth ($/GB)
  * Income from sales [#]_
  * Sales reductions from loss of site availability

  * Resulting revenue (score)



.. [#] If we're going to make the sites geographically seperate, then a basic
   time simulation affecting number of sales may be an option. (E.g.
   ``sales = (1 + sin(t)) * site_sales_volume``)

*Aspects listed below will not be included in the SC15 demo.*

Defense costs:

* Bandwidth capacity upgrades
* Redundancy of paths
* Unusual traffic paths ("new" peering agreements)
* Compute capacity upgrades (number of physical or virtual nodes)
* Service downtime or partial availability (lost sales)

  * Anything else that negatively affects user experience, like increased load
    time or intermittent failures can be a cause of lost sales to a varying
    degree.


Attacker cost (not sure if this should be factored into the demo):

* Pay a botnet owner to get access to hacked machines




Topology
========

The topology should meet these requirements:

Service node:
    Runs the service software that has to be defended.
    At least two nodes attached to a switch or router at different points in
    the graph.

    Future work will be services as *sites*: multiple related nodes will be
    connected to a switch or router and will cooperate to handle the service
    requests (e.g. load balancing).


Traffic node:
    Acts as traffic generators.
    At least five nodes attached to different points in the graph,
    but not directly to the service nodes.
    They represent large "customer" networks (i.e. a regular ISP) that
    generate the traffic to the service.

    These nodes will run software that can simulates the customers
    **and** the attackers.

    Can also be seen as subtopologies without a complete topology
    description (network clouds/black boxes).


Network elements:
    Simulates any kind of element or link that you would find in a real
    production network.
    From the demo defender's point of view these elements belong to an
    upstream provider (so they may have limited control).

    The majority of these elements should be redundantly connected, though
    ideally in a way that prevents links from overlapping in the demo
    rendering.

    The graph of these elements will be what makes the demo network visually
    appealing.

    (See also: traffic shaping below.)


Ideally the topology should be easily extensible with new service or traffic
nodes.


ExoGENI limitations
^^^^^^^^^^^^^^^^^^^

* Preallocate sufficient bandwidth for all links (i.e. the maximum amount of
  bandwidth you would like a link to use), but scale them down in
  software.

  We will do traffic shaping to control the "actual" capacity (e.g. using
  ``tc`` on the node) from the demo software,
  using a simple remote control application.

* Be wary of slice boot times; keep the slice small enough to have an acceptable
  boot time for now.


SC15 demo features
==================

Stuff the demo should be able to do:

* Control over the attack (behind the scenes or in the demo)

  Source of the attack, type of attack, total bandwidth, ...

* Attack classes:

  * Basic attack that can easily be detected but only has a small impact on
    the service.

    Should be easily blocked.

  * High intensity attack where you can't defend *everything* anymore.
    Highlights that complete mitigation isn't always an option.

    In this scenario a person should choose to get the highest acceptable
    availability.

* Show a log of what is happening behind the scenes so people understand
  the (algorithmic) process that is happening.


* Visualization of the SARNET control loop.

* Visualization of the service revenue over time.
  This illustrates the effects of the attack and how effective the defensive
  measures are.

* Visualization of cost of network over time.
  This illustrates how efficient the defensive measures are.

* Ability to activate and deactivate existing links or paths

  * Realism: the defender may not be allowed activate some *links* in the
    network, for example between two network devices which are owned by some
    upstream provider. (Would the upstream allow that much direct
    control in a real network?)

    Instead the user can request their total required bandwidth capacity
    (future: or some other kind of QoS setting)
    to which the upstream providers *react* by activating new links/paths that
    can meet the capacity needs.

* Bandwidth capacity sliders for the links where this is allowed/possible.


Tools to implement:

* Very simple automatic responder that reacts to bandwidth capacity problems,
  as a "starting point" for the SARNET software.

* Simulated service.
  Needs to report back the sales statistics to the demo software.

* Simulated customer.
  A client that triggers sales on the service.
  Preconfigure or remotely control frequency of outgoing requests.

* Attack simulator: TBD. Figure out what kind of attacks are needed.

* Monitoring/network control tool:

  * We need basic statistics from every element in the topology to visualize
    the network state (e.g. bandwidth, packet loss, latency)

  * Control over some network elements to apply routing rules, firewall rules,
    or traffic filters

* Monitoring of bare metal/VM health (using ExoGENI's analytics?)



Stuff Ciena would like to show:

* High bandwidth usage/capacity


Future features
===============

These features will not be implemented for SC15, but are provided for reference.

* Ability to add or remove compute resources to the sites, or to enable
  additional sites (from a predetermined list of available sites): maybe, if
  time permits.

* (Semi-)automatic SARNET response: this will not be included because no
  reasoning software exists yet.

* Measuring effects on response time and availability on sales.

* Additional control over links, such as packet loss and latency.
