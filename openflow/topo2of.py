"""

A   B
 \ /
  X
  |   M
   ------ E
  |
  V
 / \
C   D


500     SRC:A DST:B     -> OUT:B
500     SRC:B DST:A     -> OUT:A
500     SRC:C DST:D     -> OUT:D
500     SRC:D DST:C     -> OUT:C
500     SRC:V DST:C     -> OUT:C
500     SRC:V DST:D     -> OUT:D
500     SRC:V DST:E     -> OUT:E,M
500     SRC:X DST:A     -> OUT:A
500     SRC:X DST:B     -> OUT:B
500     SRC:X DST:E     -> OUT:E,M

500     SRC:A DST:C     -> OUT:X
500     SRC:A DST:D     -> OUT:X
500     SRC:A DST:E     -> OUT:X
500     SRC:B DST:C     -> OUT:X
500     SRC:B DST:D     -> OUT:X
500     SRC:B DST:E     -> OUT:X
500     SRC:E DST:A     -> OUT:X,M
500     SRC:E DST:B     -> OUT:X,M
500     SRC:V DST:A     -> OUT:X
500     SRC:V DST:B     -> OUT:X

500     SRC:C DST:A     -> OUT:V
500     SRC:C DST:B     -> OUT:V
500     SRC:C DST:E     -> OUT:V
500     SRC:D DST:A     -> OUT:V
500     SRC:D DST:B     -> OUT:V
500     SRC:D DST:E     -> OUT:V
500     SRC:E DST:C     -> OUT:V,M
500     SRC:E DST:D     -> OUT:V,M
500     SRC:X DST:C     -> OUT:V
500     SRC:X DST:D     -> OUT:V

---

500     SRC:A -> goto net1
500     SRC:B -> goto net1
500     SRC:C -> goto net2
500     SRC:D -> goto net2
500     SRC:E -> OUT:M goto net3

net1:
500     DST:A -> goto direct
500     DST:B -> goto direct
500     DST:C -> OUT:X*
500     DST:D -> OUT:X*
500     DST:E -> OUT:X*

net2:
500     DST:A -> OUT:V*
500     DST:B -> OUT:V*
500     DST:C -> goto direct
500     DST:D -> goto direct
500     DST:E -> OUT:V*

net3:
500     DST:A -> OUT:X
500     DST:B -> OUT:X
500     DST:C -> OUT:V
500     DST:D -> OUT:V
500     DST:E -> goto direct


direct:
500     DST:A     -> OUT:A
500     DST:B     -> OUT:B
500     DST:C     -> OUT:C
500     DST:D     -> OUT:D
500     DST:E     -> OUT:E,M

---

500     SRC:A TAG:1 -> goto net1
500     SRC:B TAG:1 -> goto net1
500     SRC:C TAG:2 -> goto net1
500     SRC:D TAG:2 -> goto net1
500     SRC:E TAG:3 -> OUT:M goto net1

net1:
500     TAG:1 DST:A -> goto direct
500     TAG:1 DST:B -> goto direct
500     TAG:2 DST:C -> goto direct
500     TAG:2 DST:D -> goto direct
500     TAG:3 DST:E -> goto direct

net2:
500     DST:A -> OUT:V*
500     DST:B -> OUT:V*
500     DST:C -> goto direct
500     DST:D -> goto direct
500     DST:E -> OUT:V*

net3:
500     DST:A -> OUT:X
500     DST:B -> OUT:X
500     DST:C -> OUT:V
500     DST:D -> OUT:V
500     DST:E -> goto direct


direct:
500     DST:A     -> OUT:A
500     DST:B     -> OUT:B
500     DST:C     -> OUT:C
500     DST:D     -> OUT:D
500     DST:E     -> OUT:E,M



"""

from pprint import pprint


def topo2of(nodes, links):
    graph_nodes = {}
    for link in links:
        lnodes = link['nodes']
        if len(lnodes) == 2:
            v1 = graph_nodes.setdefault(lnodes[0], {})
            v2 = graph_nodes.setdefault(lnodes[1], {})
            v1[lnodes[1]] = link
            v2[lnodes[0]] = link

    def find_linked_path(src, dst):
        searched = {src}
        queue = [[(src, None)]]
        while queue:
            path = queue.pop(0)
            searched.add(path[-1][0])
            for nb, data in graph_nodes[path[-1][0]].items():
                if nb == dst:
                    return path + [(nb, data)]
                elif nb not in searched:
                    queue.append(path + [(nb, data)])

    rules = []
    for n, info in sorted(nodes.items()):
        for nb, nb_info in sorted(nodes.items()):
            if n == nb:
                continue
            elif nb_info.get('routable') is False:
                continue
            path = find_linked_path(n, nb)
            actions = []
            for p, p_info in path:
                if p_info is None or p not in nodes:
                    continue
                elif p_info['type'] == 'mirror':
                    actions.append(('mirror', p))
                else:
                    actions.append(('output', p))
                    break

            print(n, nb, '=>', actions or path)
            rules.append((nodes[n]['mac'], nodes[nb]['mac'], actions))

    return None
    return rules

if __name__ == '__main__':
    pprint(topo2of({
        'A': {'mac': '02:00:01:00:00:01'},
        'B': {'mac': '02:00:01:00:00:02'},
        'C': {'mac': '02:00:01:00:00:03'},
        'D': {'mac': '02:00:01:00:00:04'},
        'E': {'mac': '02:00:01:00:00:05'},
        'X': {'mac': '02:00:01:00:FF:11', 'routable': False},
        'M': {'mac': '02:00:01:00:FF:22', 'routable': False},
    }, [
        {'nodes': ['A', 'switch1'], 'type': 'switch'},
        {'nodes': ['B', 'switch1'], 'type': 'switch'},
        {'nodes': ['X', 'switch1'], 'type': 'transparent'},
        {'nodes': ['X', 'switch2'], 'type': 'transparent'},
        {'nodes': ['C', 'switch2'], 'type': 'switch'},
        {'nodes': ['D', 'switch2'], 'type': 'switch'},
        {'nodes': ['E', 'M'], 'type': 'mirror'},
        {'nodes': ['M', 'switch2'], 'type': 'mirror'},
        {'nodes': ['E'], 'type': 'edge'},
    ]))
