from dpkt.ethernet import Ethernet, ETH_TYPE_8021Q
#, VLANtag8021Q


def frame_packet(src, dst, original_frame):
    # vlan_tags=[VLANtag8021Q(pri=1,cfi=1,id=1)]

    if original_frame.type == ETH_TYPE_8021Q:
        vlan_tags = original_frame.vlan_tags
    else:
        vlan_tags = []

    return Ethernet(src=src, dst=dst,
                    type=original_frame.type,
                    vlan_tags=vlan_tags)
