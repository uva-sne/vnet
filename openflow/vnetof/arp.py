"""
ARP
---

* Maintains an ARP table for the topology
* Intercept

Information we know: Host/IP
Information we learn: MAC/port

TODO: implement full ARP with flooding
"""
import logging
from dpkt.ethernet import Ethernet, ETH_TYPE_ARP
from dpkt.arp import ARP, ARP_OP_REQUEST, ARP_OP_REPLY

ip_to_mac = {}
mac_to_port = {}
mac_to_host = {}
host_to_mac = {}


def arp_learn(port, ip, mac, host):
    logging.info('%s.%s> Learned %s: %r = %r', port[0], port[1], host, ip, mac)
    ip_to_mac[ip] = mac
    mac_to_port[mac] = port[1]
    mac_to_host[mac] = host
    host_to_mac[host] = mac


def arp_request(ip):
    return ip_to_mac.get(ip)


def arp_name(mac):
    if mac == '\xff\xff\xff\xff\xff\xff':
        return 'BROADCAST'
    return mac_to_host.get(mac)


def arp_table():
    """ Return a table consisting of host to port """
    table = {}
    for host, mac in host_to_mac.items():
        table[host] = {'port': mac_to_port[mac], 'mac': mac}
    return table


def arp_intercept(in_port, frame, payload):
    """ Intercept ARP requests and respond if appropriate """

    if payload.op != ARP_OP_REQUEST:
        return

    logging.info('%r | ARP request for %r', payload.spa, payload.tpa)

    if payload.tpa not in ip_to_mac:
        return None

    hwdst = ip_to_mac[payload.tpa]
    logging.info('ARP response: %r', hwdst)

    return ARP(op=ARP_OP_REPLY,
               sha=hwdst,
               spa=payload.tpa,
               tha=payload.sha,
               tpa=payload.spa)


def build_arp_request(host, origin_ip, origin_mac):
    frame = Ethernet(dst='\xff\xff\xff\xff\xff\xff',
                     src=origin_mac,
                     type=ETH_TYPE_ARP)
    frame.data = ARP(op=ARP_OP_REQUEST,
                     sha=origin_mac,
                     spa=origin_ip,
                     tha='\xff\xff\xff\xff\xff\xff',
                     tpa=host)
    return frame
