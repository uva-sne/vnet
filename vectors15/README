.. vim:set ft=rst:

=========
Vectors15
=========

Tools and scripts for the Vectors15 demo.


Boot up demo slice
==================

Copy/paste commands below, changing a

.. code::

    cd /srv/www/skynet/htdocs

    # **Always** run this first!
    export PYTHONPATH=. PATH=bin:$PATH


    # **Verify** if ciena-demo slice is running
    geni-rpc -u geni list-slices


    # **Start**:
    # ALL steps are required

    # Build and submit request XML
    orca-request-xml < vectors15/resources/ciena-demo.json > vectors15/resources/ciena-request.xml
    geni-rpc -u geni request ciena-demo vectors15/resources/ciena-request.xml

    # Manually poll latest manifest until all nodes are up
    geni-rpc -u geni rdf ciena-demo > vectors15/resources/ciena-manifest.xml

    # When the ciena-demo slice is up, you can restart the demo!
    # Beware, restarting the demo will ALWAYS delete the ciena-demo-live slice

    # Old images:
    install_iperf geni ciena-demo

    # **Stop**:
    geni-rpc -u geni del ciena-demo


    # Old images, orchestrated slice: (wait until they're up)
    install_iperf ciena ciena-demo-live
    # Warning: it may take a while before iperf actually starts working
