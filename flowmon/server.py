"""

* Multiple devices
    * IP based? Domain/point ID based?

* Collect windowed flow counters per protocol/subnet


Ralph-query:
* Has flow been seen on this device at timestamp X?



flowid => time



"""

from socket import socket, AF_INET, SOCK_DGRAM
from io import BytesIO
from traceback import print_exc
from datetime import datetime
from ipfix import parse_ipfix


FLOW_INGRESS = 0
FLOW_EGRESS = 0


protocols = {1: 'icmp',
             6: 'tcp',
             17: 'udp',
             58: 'icmpv6',
             132: 'sctp'}

bucket = {}


def track_bucket(key, packets, octets):
    v = bucket.get(key, (0, 0))
    bucket[key] = (v[0] + packets, v[1] + octets)


def speed(n):
    n *= 8
    if n < 2000:
        return '%.1f bit/s' % (n,)
    n /= 1000
    if n < 2000:
        return '%.1f kbit/s' % (n,)
    n /= 1000
    return '%.1f Mbit/s' % (n,)


def emit_bucket(t):
    global bucket
    if bucket:
        total = float(sum(v[1] for v in bucket.values()))
        for k, v in sorted(bucket.items()):
            print('%-6s  %-4s  %-10s  %s' % (k, v[0], speed(v[1] / t),
                                             '%.1f%%' % (100. * v[1] / total)))
    bucket = {}


def classify_traffic(domain, point, src_addr, dst_addr):
    src_domain = domain_from_subnet(src_addr)
    dst_domain = domain_from_subnet(dst_addr)

    if src_domain != domain and dst_domain != domain:
        return 'TrafficTransit'

    elif src_domain == dst_domain:
        return 'PerPortTraffic'

    elif domain == src_domain:
        return 'EgressFromHost'

    return 'IngressToHost'


def _time(a, ms):
    t = a - (ms / 1000000.0)
    return str(datetime.fromtimestamp(t))


def handle_flow(d, flow):
    proto = protocols.get(flow['protocolIdentifier'])
    if not proto:
        print('Skipped flow with protocol', flow['protocolIdentifier'])
        return

    t = d['time']
    ts = _time(t, flow.get('flowStartDeltaMicroseconds', 0))
    te = _time(t, flow.get('flowEndDeltaMicroseconds', 0))

    if flow['ipVersion'] == 4:
        src_addr = flow['sourceIPv4Address']
        dst_addr = flow['destinationIPv4Address']
    elif flow['ipVersion'] == 6:
        src_addr = flow['sourceIPv6Address']
        dst_addr = flow['destinationIPv6Address']
    else:
        print('Skipped flow with IP version', flow['ipVersion'])
        return

    # VNETMON discovery
    if proto == 'udp' and dst_addr == '255.255.255.255':
        return

    flow_key_lhs = '%s:%s' % (src_addr,
                              flow.get('sourceTransportPort', 0))
    flow_key_rhs = '%s:%s' % (dst_addr,
                              flow.get('destinationTransportPort', 0))

    # Bad sorting
    #if flow_key_lhs > flow_key_rhs:
    #    flow_key_lhs, flow_key_rhs = flow_key_rhs, flow_key_lhs
    key = flow_key_lhs + ' ' + flow_key_rhs

    print(d['sequence'], flow['flowDirection'], ts, te, proto, key,
          flow['packetDeltaCount'],
          flow['octetDeltaCount'])

    if flow['flowDirection'] == FLOW_EGRESS:
        track_bucket(proto, flow['packetDeltaCount'], flow['octetDeltaCount'])


def run_udp_server(port):
    sock = socket(AF_INET, SOCK_DGRAM)
    sock.bind(('0.0.0.0', port))

    last_time = 0

    templates = {}
    while True:
        data, _ = sock.recvfrom(2048)
        d = parse_ipfix(BytesIO(data), templates)

        if last_time != d['time']:
            delta_time = float(d['time'] - last_time)
            print('DELTA', delta_time)
            emit_bucket(delta_time)
            last_time = d['time']

        for flow in d['flows']:
            #print(sorted(flow.keys()))
            try:
                handle_flow(d, flow)
            except:
                print('Failed to handle:')
                print(flow)
                print_exc()


if __name__ == '__main__':
    run_udp_server(2055)
