ipfix_fields = {
    1:   {'name': 'octetDeltaCount', 'kind': 'unsigned64'},
    2:   {'name': 'packetDeltaCount', 'kind': 'unsigned64'},
    3:   {'name': 'deltaFlowCount', 'kind': 'unsigned64'},
    4:   {'name': 'protocolIdentifier', 'kind': 'unsigned8'},
    5:   {'name': 'ipClassOfService', 'kind': 'unsigned8'},
    6:   {'name': 'tcpControlBits', 'kind': 'unsigned16'},
    7:   {'name': 'sourceTransportPort', 'kind': 'unsigned16'},
    8:   {'name': 'sourceIPv4Address', 'kind': 'ipv4Address'},
    9:   {'name': 'sourceIPv4PrefixLength', 'kind': 'unsigned8'},
    10:  {'name': 'ingressInterface', 'kind': 'unsigned32'},
    11:  {'name': 'destinationTransportPort', 'kind': 'unsigned16'},
    12:  {'name': 'destinationIPv4Address', 'kind': 'ipv4Address'},
    13:  {'name': 'destinationIPv4PrefixLength', 'kind': 'unsigned8'},
    14:  {'name': 'egressInterface', 'kind': 'unsigned32'},
    15:  {'name': 'ipNextHopIPv4Address', 'kind': 'ipv4Address'},
    16:  {'name': 'bgpSourceAsNumber', 'kind': 'unsigned32'},
    17:  {'name': 'bgpDestinationAsNumber', 'kind': 'unsigned32'},
    18:  {'name': 'bgpNextHopIPv4Address', 'kind': 'ipv4Address'},
    19:  {'name': 'postMCastPacketDeltaCount', 'kind': 'unsigned64'},
    20:  {'name': 'postMCastOctetDeltaCount', 'kind': 'unsigned64'},
    21:  {'name': 'flowEndSysUpTime', 'kind': 'unsigned32'},
    22:  {'name': 'flowStartSysUpTime', 'kind': 'unsigned32'},
    23:  {'name': 'postOctetDeltaCount', 'kind': 'unsigned64'},
    24:  {'name': 'postPacketDeltaCount', 'kind': 'unsigned64'},
    25:  {'name': 'minimumIpTotalLength', 'kind': 'unsigned64'},
    26:  {'name': 'maximumIpTotalLength', 'kind': 'unsigned64'},
    27:  {'name': 'sourceIPv6Address', 'kind': 'ipv6Address'},
    28:  {'name': 'destinationIPv6Address', 'kind': 'ipv6Address'},
    29:  {'name': 'sourceIPv6PrefixLength', 'kind': 'unsigned8'},
    30:  {'name': 'destinationIPv6PrefixLength', 'kind': 'unsigned8'},
    31:  {'name': 'flowLabelIPv6', 'kind': 'unsigned32'},
    32:  {'name': 'icmpTypeCodeIPv4', 'kind': 'unsigned16'},
    33:  {'name': 'igmpType', 'kind': 'unsigned8'},
    34:  {'name': 'samplingInterval', 'kind': 'unsigned32'},
    35:  {'name': 'samplingAlgorithm', 'kind': 'unsigned8'},
    36:  {'name': 'flowActiveTimeout', 'kind': 'unsigned16'},
    37:  {'name': 'flowIdleTimeout', 'kind': 'unsigned16'},
    38:  {'name': 'engineType', 'kind': 'unsigned8'},
    39:  {'name': 'engineId', 'kind': 'unsigned8'},
    40:  {'name': 'exportedOctetTotalCount', 'kind': 'unsigned64'},
    41:  {'name': 'exportedMessageTotalCount', 'kind': 'unsigned64'},
    42:  {'name': 'exportedFlowRecordTotalCount', 'kind': 'unsigned64'},
    43:  {'name': 'ipv4RouterSc', 'kind': 'ipv4Address'},
    44:  {'name': 'sourceIPv4Prefix', 'kind': 'ipv4Address'},
    45:  {'name': 'destinationIPv4Prefix', 'kind': 'ipv4Address'},
    46:  {'name': 'mplsTopLabelType', 'kind': 'unsigned8'},
    47:  {'name': 'mplsTopLabelIPv4Address', 'kind': 'ipv4Address'},
    48:  {'name': 'samplerId', 'kind': 'unsigned8'},
    49:  {'name': 'samplerMode', 'kind': 'unsigned8'},
    50:  {'name': 'samplerRandomInterval', 'kind': 'unsigned32'},
    51:  {'name': 'classId', 'kind': 'unsigned8'},
    52:  {'name': 'minimumTTL', 'kind': 'unsigned8'},
    53:  {'name': 'maximumTTL', 'kind': 'unsigned8'},
    54:  {'name': 'fragmentIdentification', 'kind': 'unsigned32'},
    55:  {'name': 'postIpClassOfService', 'kind': 'unsigned8'},
    56:  {'name': 'sourceMacAddress', 'kind': 'macAddress'},
    57:  {'name': 'postDestinationMacAddress', 'kind': 'macAddress'},
    58:  {'name': 'vlanId', 'kind': 'unsigned16'},
    59:  {'name': 'postVlanId', 'kind': 'unsigned16'},
    60:  {'name': 'ipVersion', 'kind': 'unsigned8'},
    61:  {'name': 'flowDirection', 'kind': 'unsigned8'},
    62:  {'name': 'ipNextHopIPv6Address', 'kind': 'ipv6Address'},
    63:  {'name': 'bgpNextHopIPv6Address', 'kind': 'ipv6Address'},
    64:  {'name': 'ipv6ExtensionHeaders', 'kind': 'unsigned32'},
    70:  {'name': 'mplsTopLabelStackSection', 'kind': 'octetArray'},
    71:  {'name': 'mplsLabelStackSection2', 'kind': 'octetArray'},
    72:  {'name': 'mplsLabelStackSection3', 'kind': 'octetArray'},
    73:  {'name': 'mplsLabelStackSection4', 'kind': 'octetArray'},
    74:  {'name': 'mplsLabelStackSection5', 'kind': 'octetArray'},
    75:  {'name': 'mplsLabelStackSection6', 'kind': 'octetArray'},
    76:  {'name': 'mplsLabelStackSection7', 'kind': 'octetArray'},
    77:  {'name': 'mplsLabelStackSection8', 'kind': 'octetArray'},
    78:  {'name': 'mplsLabelStackSection9', 'kind': 'octetArray'},
    79:  {'name': 'mplsLabelStackSection10', 'kind': 'octetArray'},
    80:  {'name': 'destinationMacAddress', 'kind': 'macAddress'},
    81:  {'name': 'postSourceMacAddress', 'kind': 'macAddress'},
    82:  {'name': 'interfaceName', 'kind': 'string'},
    83:  {'name': 'interfaceDescription', 'kind': 'string'},
    84:  {'name': 'samplerName', 'kind': 'string'},
    85:  {'name': 'octetTotalCount', 'kind': 'unsigned64'},
    86:  {'name': 'packetTotalCount', 'kind': 'unsigned64'},
    87:  {'name': 'flagsAndSamplerId', 'kind': 'unsigned32'},
    88:  {'name': 'fragmentOffset', 'kind': 'unsigned16'},
    89:  {'name': 'forwardingStatus', 'kind': 'unsigned32'},
    90:  {'name': 'mplsVpnRouteDistinguisher', 'kind': 'octetArray'},
    91:  {'name': 'mplsTopLabelPrefixLength', 'kind': 'unsigned8'},
    92:  {'name': 'srcTrafficIndex', 'kind': 'unsigned32'},
    93:  {'name': 'dstTrafficIndex', 'kind': 'unsigned32'},
    94:  {'name': 'applicationDescription', 'kind': 'string'},
    95:  {'name': 'applicationId', 'kind': 'octetArray'},
    96:  {'name': 'applicationName', 'kind': 'string'},
    98:  {'name': 'postIpDiffServCodePoint', 'kind': 'unsigned8'},
    99:  {'name': 'multicastReplicationFactor', 'kind': 'unsigned32'},
    100: {'name': 'className', 'kind': 'string'},
    101: {'name': 'classificationEngineId', 'kind': 'unsigned8'},
    102: {'name': 'layer2packetSectionOffset', 'kind': 'unsigned16'},
    103: {'name': 'layer2packetSectionSize', 'kind': 'unsigned16'},
    104: {'name': 'layer2packetSectionData', 'kind': 'octetArray'},
    128: {'name': 'bgpNextAdjacentAsNumber', 'kind': 'unsigned32'},
    129: {'name': 'bgpPrevAdjacentAsNumber', 'kind': 'unsigned32'},
    130: {'name': 'exporterIPv4Address', 'kind': 'ipv4Address'},
    131: {'name': 'exporterIPv6Address', 'kind': 'ipv6Address'},
    132: {'name': 'droppedOctetDeltaCount', 'kind': 'unsigned64'},
    133: {'name': 'droppedPacketDeltaCount', 'kind': 'unsigned64'},
    134: {'name': 'droppedOctetTotalCount', 'kind': 'unsigned64'},
    135: {'name': 'droppedPacketTotalCount', 'kind': 'unsigned64'},
    136: {'name': 'flowEndReason', 'kind': 'unsigned8'},
    137: {'name': 'commonPropertiesId', 'kind': 'unsigned64'},
    138: {'name': 'observationPointId', 'kind': 'unsigned64'},
    139: {'name': 'icmpTypeCodeIPv6', 'kind': 'unsigned16'},
    140: {'name': 'mplsTopLabelIPv6Address', 'kind': 'ipv6Address'},
    141: {'name': 'lineCardId', 'kind': 'unsigned32'},
    142: {'name': 'portId', 'kind': 'unsigned32'},
    143: {'name': 'meteringProcessId', 'kind': 'unsigned32'},
    144: {'name': 'exportingProcessId', 'kind': 'unsigned32'},
    145: {'name': 'templateId', 'kind': 'unsigned16'},
    146: {'name': 'wlanChannelId', 'kind': 'unsigned8'},
    147: {'name': 'wlanSSID', 'kind': 'string'},
    148: {'name': 'flowId', 'kind': 'unsigned64'},
    149: {'name': 'observationDomainId', 'kind': 'unsigned32'},
    150: {'name': 'flowStartSeconds', 'kind': 'dateTimeSeconds'},
    151: {'name': 'flowEndSeconds', 'kind': 'dateTimeSeconds'},
    152: {'name': 'flowStartMilliseconds', 'kind': 'dateTimeMilliseconds'},
    153: {'name': 'flowEndMilliseconds', 'kind': 'dateTimeMilliseconds'},
    154: {'name': 'flowStartMicroseconds', 'kind': 'dateTimeMicroseconds'},
    155: {'name': 'flowEndMicroseconds', 'kind': 'dateTimeMicroseconds'},
    156: {'name': 'flowStartNanoseconds', 'kind': 'dateTimeNanoseconds'},
    157: {'name': 'flowEndNanoseconds', 'kind': 'dateTimeNanoseconds'},
    158: {'name': 'flowStartDeltaMicroseconds', 'kind': 'unsigned32'},
    159: {'name': 'flowEndDeltaMicroseconds', 'kind': 'unsigned32'},
    160: {'name': 'systemInitTimeMilliseconds', 'kind': 'dateTimeMilliseconds'},
    161: {'name': 'flowDurationMilliseconds', 'kind': 'unsigned32'},
    162: {'name': 'flowDurationMicroseconds', 'kind': 'unsigned32'},
    163: {'name': 'observedFlowTotalCount', 'kind': 'unsigned64'},
    164: {'name': 'ignoredPacketTotalCount', 'kind': 'unsigned64'},
    165: {'name': 'ignoredOctetTotalCount', 'kind': 'unsigned64'},
    166: {'name': 'notSentFlowTotalCount', 'kind': 'unsigned64'},
    167: {'name': 'notSentPacketTotalCount', 'kind': 'unsigned64'},
    168: {'name': 'notSentOctetTotalCount', 'kind': 'unsigned64'},
    169: {'name': 'destinationIPv6Prefix', 'kind': 'ipv6Address'},
    170: {'name': 'sourceIPv6Prefix', 'kind': 'ipv6Address'},
    171: {'name': 'postOctetTotalCount', 'kind': 'unsigned64'},
    172: {'name': 'postPacketTotalCount', 'kind': 'unsigned64'},
    173: {'name': 'flowKeyIndicator', 'kind': 'unsigned64'},
    174: {'name': 'postMCastPacketTotalCount', 'kind': 'unsigned64'},
    175: {'name': 'postMCastOctetTotalCount', 'kind': 'unsigned64'},
    176: {'name': 'icmpTypeIPv4', 'kind': 'unsigned8'},
    177: {'name': 'icmpCodeIPv4', 'kind': 'unsigned8'},
    178: {'name': 'icmpTypeIPv6', 'kind': 'unsigned8'},
    179: {'name': 'icmpCodeIPv6', 'kind': 'unsigned8'},
    180: {'name': 'udpSourcePort', 'kind': 'unsigned16'},
    181: {'name': 'udpDestinationPort', 'kind': 'unsigned16'},
    182: {'name': 'tcpSourcePort', 'kind': 'unsigned16'},
    183: {'name': 'tcpDestinationPort', 'kind': 'unsigned16'},
    184: {'name': 'tcpSequenceNumber', 'kind': 'unsigned32'},
    185: {'name': 'tcpAcknowledgementNumber', 'kind': 'unsigned32'},
    186: {'name': 'tcpWindowSize', 'kind': 'unsigned16'},
    187: {'name': 'tcpUrgentPointer', 'kind': 'unsigned16'},
    188: {'name': 'tcpHeaderLength', 'kind': 'unsigned8'},
    189: {'name': 'ipHeaderLength', 'kind': 'unsigned8'},
    190: {'name': 'totalLengthIPv4', 'kind': 'unsigned16'},
    191: {'name': 'payloadLengthIPv6', 'kind': 'unsigned16'},
    192: {'name': 'ipTTL', 'kind': 'unsigned8'},
    193: {'name': 'nextHeaderIPv6', 'kind': 'unsigned8'},
    194: {'name': 'mplsPayloadLength', 'kind': 'unsigned32'},
    195: {'name': 'ipDiffServCodePoint', 'kind': 'unsigned8'},
    196: {'name': 'ipPrecedence', 'kind': 'unsigned8'},
    197: {'name': 'fragmentFlags', 'kind': 'unsigned8'},
    198: {'name': 'octetDeltaSumOfSquares', 'kind': 'unsigned64'},
    199: {'name': 'octetTotalSumOfSquares', 'kind': 'unsigned64'},
    200: {'name': 'mplsTopLabelTTL', 'kind': 'unsigned8'},
    201: {'name': 'mplsLabelStackLength', 'kind': 'unsigned32'},
    202: {'name': 'mplsLabelStackDepth', 'kind': 'unsigned32'},
    203: {'name': 'mplsTopLabelExp', 'kind': 'unsigned8'},
    204: {'name': 'ipPayloadLength', 'kind': 'unsigned32'},
    205: {'name': 'udpMessageLength', 'kind': 'unsigned16'},
    206: {'name': 'isMulticast', 'kind': 'unsigned8'},
    207: {'name': 'ipv4IHL', 'kind': 'unsigned8'},
    208: {'name': 'ipv4Options', 'kind': 'unsigned32'},
    209: {'name': 'tcpOptions', 'kind': 'unsigned64'},
    210: {'name': 'paddingOctets', 'kind': 'octetArray'},
    211: {'name': 'collectorIPv4Address', 'kind': 'ipv4Address'},
    212: {'name': 'collectorIPv6Address', 'kind': 'ipv6Address'},
    213: {'name': 'exportInterface', 'kind': 'unsigned32'},
    214: {'name': 'exportProtocolVersion', 'kind': 'unsigned8'},
    215: {'name': 'exportTransportProtocol', 'kind': 'unsigned8'},
    216: {'name': 'collectorTransportPort', 'kind': 'unsigned16'},
    217: {'name': 'exporterTransportPort', 'kind': 'unsigned16'},
    218: {'name': 'tcpSynTotalCount', 'kind': 'unsigned64'},
    219: {'name': 'tcpFinTotalCount', 'kind': 'unsigned64'},
    220: {'name': 'tcpRstTotalCount', 'kind': 'unsigned64'},
    221: {'name': 'tcpPshTotalCount', 'kind': 'unsigned64'},
    222: {'name': 'tcpAckTotalCount', 'kind': 'unsigned64'},
    223: {'name': 'tcpUrgTotalCount', 'kind': 'unsigned64'},
    224: {'name': 'ipTotalLength', 'kind': 'unsigned64'},
    225: {'name': 'postNATSourceIPv4Address', 'kind': 'ipv4Address'},
    226: {'name': 'postNATDestinationIPv4Address', 'kind': 'ipv4Address'},
    227: {'name': 'postNAPTSourceTransportPort', 'kind': 'unsigned16'},
    228: {'name': 'postNAPTDestinationTransportPort', 'kind': 'unsigned16'},
    229: {'name': 'natOriginatingAddressRealm', 'kind': 'unsigned8'},
    230: {'name': 'natEvent', 'kind': 'unsigned8'},
    231: {'name': 'initiatorOctets', 'kind': 'unsigned64'},
    232: {'name': 'responderOctets', 'kind': 'unsigned64'},
    233: {'name': 'firewallEvent', 'kind': 'unsigned8'},
    234: {'name': 'ingressVRFID', 'kind': 'unsigned32'},
    235: {'name': 'egressVRFID', 'kind': 'unsigned32'},
    236: {'name': 'VRFname', 'kind': 'string'},
    237: {'name': 'postMplsTopLabelExp', 'kind': 'unsigned8'},
    238: {'name': 'tcpWindowScale', 'kind': 'unsigned16'},
    239: {'name': 'biflowDirection', 'kind': 'unsigned8'},
    240: {'name': 'ethernetHeaderLength', 'kind': 'unsigned8'},
    241: {'name': 'ethernetPayloadLength', 'kind': 'unsigned16'},
    242: {'name': 'ethernetTotalLength', 'kind': 'unsigned16'},
    243: {'name': 'dot1qVlanId', 'kind': 'unsigned16'},
    244: {'name': 'dot1qPriority', 'kind': 'unsigned8'},
    245: {'name': 'dot1qCustomerVlanId', 'kind': 'unsigned16'},
    246: {'name': 'dot1qCustomerPriority', 'kind': 'unsigned8'},
    247: {'name': 'metroEvcId', 'kind': 'string'},
    248: {'name': 'metroEvcType', 'kind': 'unsigned8'},
    249: {'name': 'pseudoWireId', 'kind': 'unsigned32'},
    250: {'name': 'pseudoWireType', 'kind': 'unsigned16'},
    251: {'name': 'pseudoWireControlWord', 'kind': 'unsigned32'},
    252: {'name': 'ingressPhysicalInterface', 'kind': 'unsigned32'},
    253: {'name': 'egressPhysicalInterface', 'kind': 'unsigned32'},
    254: {'name': 'postDot1qVlanId', 'kind': 'unsigned16'},
    255: {'name': 'postDot1qCustomerVlanId', 'kind': 'unsigned16'},
    256: {'name': 'ethernetType', 'kind': 'unsigned16'},
    257: {'name': 'postIpPrecedence', 'kind': 'unsigned8'},
    258: {'name': 'collectionTimeMilliseconds', 'kind': 'dateTimeMilliseconds'},
    259: {'name': 'exportSctpStreamId', 'kind': 'unsigned16'},
    260: {'name': 'maxExportSeconds', 'kind': 'dateTimeSeconds'},
    261: {'name': 'maxFlowEndSeconds', 'kind': 'dateTimeSeconds'},
    262: {'name': 'messageMD5Checksum', 'kind': 'octetArray'},
    263: {'name': 'messageScope', 'kind': 'unsigned8'},
    264: {'name': 'minExportSeconds', 'kind': 'dateTimeSeconds'},
    265: {'name': 'minFlowStartSeconds', 'kind': 'dateTimeSeconds'},
    266: {'name': 'opaqueOctets', 'kind': 'octetArray'},
    267: {'name': 'sessionScope', 'kind': 'unsigned8'},
    268: {'name': 'maxFlowEndMicroseconds', 'kind': 'dateTimeMicroseconds'},
    269: {'name': 'maxFlowEndMilliseconds', 'kind': 'dateTimeMilliseconds'},
    270: {'name': 'maxFlowEndNanoseconds', 'kind': 'dateTimeNanoseconds'},
    271: {'name': 'minFlowStartMicroseconds', 'kind': 'dateTimeMicroseconds'},
    272: {'name': 'minFlowStartMilliseconds', 'kind': 'dateTimeMilliseconds'},
    273: {'name': 'minFlowStartNanoseconds', 'kind': 'dateTimeNanoseconds'},
    274: {'name': 'collectorCertificate', 'kind': 'octetArray'},
    275: {'name': 'exporterCertificate', 'kind': 'octetArray'},
    276: {'name': 'dataRecordsReliability', 'kind': 'boolean'},
    277: {'name': 'observationPointType', 'kind': 'unsigned8'},
    278: {'name': 'newConnectionDeltaCount', 'kind': 'unsigned32'},
    279: {'name': 'connectionSumDurationSeconds', 'kind': 'unsigned64'},
    280: {'name': 'connectionTransactionId', 'kind': 'unsigned64'},
    281: {'name': 'postNATSourceIPv6Address', 'kind': 'ipv6Address'},
    282: {'name': 'postNATDestinationIPv6Address', 'kind': 'ipv6Address'},
    283: {'name': 'natPoolId', 'kind': 'unsigned32'},
    284: {'name': 'natPoolName', 'kind': 'string'},
    285: {'name': 'anonymizationFlags', 'kind': 'unsigned16'},
    286: {'name': 'anonymizationTechnique', 'kind': 'unsigned16'},
    287: {'name': 'informationElementIndex', 'kind': 'unsigned16'},
    288: {'name': 'p2pTechnology', 'kind': 'string'},
    289: {'name': 'tunnelTechnology', 'kind': 'string'},
    290: {'name': 'encryptedTechnology', 'kind': 'string'},
    291: {'name': 'basicList', 'kind': 'basicList'},
    292: {'name': 'subTemplateList', 'kind': 'subTemplateList'},
    293: {'name': 'subTemplateMultiList', 'kind': 'subTemplateMultiList'},
    294: {'name': 'bgpValidityState', 'kind': 'unsigned8'},
    295: {'name': 'IPSecSPI', 'kind': 'unsigned32'},
    296: {'name': 'greKey', 'kind': 'unsigned32'},
    297: {'name': 'natType', 'kind': 'unsigned8'},
    298: {'name': 'initiatorPackets', 'kind': 'unsigned64'},
    299: {'name': 'responderPackets', 'kind': 'unsigned64'},
    300: {'name': 'observationDomainName', 'kind': 'string'},
    301: {'name': 'selectionSequenceId', 'kind': 'unsigned64'},
    302: {'name': 'selectorId', 'kind': 'unsigned64'},
    303: {'name': 'informationElementId', 'kind': 'unsigned16'},
    304: {'name': 'selectorAlgorithm', 'kind': 'unsigned16'},
    305: {'name': 'samplingPacketInterval', 'kind': 'unsigned32'},
    306: {'name': 'samplingPacketSpace', 'kind': 'unsigned32'},
    307: {'name': 'samplingTimeInterval', 'kind': 'unsigned32'},
    308: {'name': 'samplingTimeSpace', 'kind': 'unsigned32'},
    309: {'name': 'samplingSize', 'kind': 'unsigned32'},
    310: {'name': 'samplingPopulation', 'kind': 'unsigned32'},
    311: {'name': 'samplingProbability', 'kind': 'float64'},
    312: {'name': 'dataLinkFrameSize', 'kind': 'unsigned16'},
    313: {'name': 'ipHeaderPacketSection', 'kind': 'octetArray'},
    314: {'name': 'ipPayloadPacketSection', 'kind': 'octetArray'},
    315: {'name': 'dataLinkFrameSection', 'kind': 'octetArray'},
    316: {'name': 'mplsLabelStackSection', 'kind': 'octetArray'},
    317: {'name': 'mplsPayloadPacketSection', 'kind': 'octetArray'},
    318: {'name': 'selectorIdTotalPktsObserved', 'kind': 'unsigned64'},
    319: {'name': 'selectorIdTotalPktsSelected', 'kind': 'unsigned64'},
    320: {'name': 'absoluteError', 'kind': 'float64'},
    321: {'name': 'relativeError', 'kind': 'float64'},
    322: {'name': 'observationTimeSeconds', 'kind': 'dateTimeSeconds'},
    323: {'name': 'observationTimeMilliseconds', 'kind': 'dateTimeMilliseconds'},
    324: {'name': 'observationTimeMicroseconds', 'kind': 'dateTimeMicroseconds'},
    325: {'name': 'observationTimeNanoseconds', 'kind': 'dateTimeNanoseconds'},
    326: {'name': 'digestHashValue', 'kind': 'unsigned64'},
    327: {'name': 'hashIPPayloadOffset', 'kind': 'unsigned64'},
    328: {'name': 'hashIPPayloadSize', 'kind': 'unsigned64'},
    329: {'name': 'hashOutputRangeMin', 'kind': 'unsigned64'},
    330: {'name': 'hashOutputRangeMax', 'kind': 'unsigned64'},
    331: {'name': 'hashSelectedRangeMin', 'kind': 'unsigned64'},
    332: {'name': 'hashSelectedRangeMax', 'kind': 'unsigned64'},
    333: {'name': 'hashDigestOutput', 'kind': 'boolean'},
    334: {'name': 'hashInitialiserValue', 'kind': 'unsigned64'},
    335: {'name': 'selectorName', 'kind': 'string'},
    336: {'name': 'upperCILimit', 'kind': 'float64'},
    337: {'name': 'lowerCILimit', 'kind': 'float64'},
    338: {'name': 'confidenceLevel', 'kind': 'float64'},
    339: {'name': 'informationElementDataType', 'kind': 'unsigned8'},
    340: {'name': 'informationElementDescription', 'kind': 'string'},
    341: {'name': 'informationElementName', 'kind': 'string'},
    342: {'name': 'informationElementRangeBegin', 'kind': 'unsigned64'},
    343: {'name': 'informationElementRangeEnd', 'kind': 'unsigned64'},
    344: {'name': 'informationElementSemantics', 'kind': 'unsigned8'},
    345: {'name': 'informationElementUnits', 'kind': 'unsigned16'},
    346: {'name': 'privateEnterpriseNumber', 'kind': 'unsigned32'},
    347: {'name': 'virtualStationInterfaceId', 'kind': 'octetArray'},
    348: {'name': 'virtualStationInterfaceName', 'kind': 'string'},
    349: {'name': 'virtualStationUUID', 'kind': 'octetArray'},
    350: {'name': 'virtualStationName', 'kind': 'string'},
    351: {'name': 'layer2SegmentId', 'kind': 'unsigned64'},
    352: {'name': 'layer2OctetDeltaCount', 'kind': 'unsigned64'},
    353: {'name': 'layer2OctetTotalCount', 'kind': 'unsigned64'},
    354: {'name': 'ingressUnicastPacketTotalCount', 'kind': 'unsigned64'},
    355: {'name': 'ingressMulticastPacketTotalCount', 'kind': 'unsigned64'},
    356: {'name': 'ingressBroadcastPacketTotalCount', 'kind': 'unsigned64'},
    357: {'name': 'egressUnicastPacketTotalCount', 'kind': 'unsigned64'},
    358: {'name': 'egressBroadcastPacketTotalCount', 'kind': 'unsigned64'},
    359: {'name': 'monitoringIntervalStartMilliSeconds', 'kind': 'dateTimeMilliseconds'},
    360: {'name': 'monitoringIntervalEndMilliSeconds', 'kind': 'dateTimeMilliseconds'},
    361: {'name': 'portRangeStart', 'kind': 'unsigned16'},
    362: {'name': 'portRangeEnd', 'kind': 'unsigned16'},
    363: {'name': 'portRangeStepSize', 'kind': 'unsigned16'},
    364: {'name': 'portRangeNumPorts', 'kind': 'unsigned16'},
    365: {'name': 'staMacAddress', 'kind': 'macAddress'},
    366: {'name': 'staIPv4Address', 'kind': 'ipv4Address'},
    367: {'name': 'wtpMacAddress', 'kind': 'macAddress'},
    368: {'name': 'ingressInterfaceType', 'kind': 'unsigned32'},
    369: {'name': 'egressInterfaceType', 'kind': 'unsigned32'},
    370: {'name': 'rtpSequenceNumber', 'kind': 'unsigned16'},
    371: {'name': 'userName', 'kind': 'string'},
    372: {'name': 'applicationCategoryName', 'kind': 'string'},
    373: {'name': 'applicationSubCategoryName', 'kind': 'string'},
    374: {'name': 'applicationGroupName', 'kind': 'string'},
    375: {'name': 'originalFlowsPresent', 'kind': 'unsigned64'},
    376: {'name': 'originalFlowsInitiated', 'kind': 'unsigned64'},
    377: {'name': 'originalFlowsCompleted', 'kind': 'unsigned64'},
    378: {'name': 'distinctCountOfSourceIPAddress', 'kind': 'unsigned64'},
    379: {'name': 'distinctCountOfDestinationIPAddress', 'kind': 'unsigned64'},
    380: {'name': 'distinctCountOfSourceIPv4Address', 'kind': 'unsigned32'},
    381: {'name': 'distinctCountOfDestinationIPv4Address', 'kind': 'unsigned32'},
    382: {'name': 'distinctCountOfSourceIPv6Address', 'kind': 'unsigned64'},
    383: {'name': 'distinctCountOfDestinationIPv6Address', 'kind': 'unsigned64'},
    384: {'name': 'valueDistributionMethod', 'kind': 'unsigned8'},
    385: {'name': 'rfc3550JitterMilliseconds', 'kind': 'unsigned32'},
    386: {'name': 'rfc3550JitterMicroseconds', 'kind': 'unsigned32'},
    387: {'name': 'rfc3550JitterNanoseconds', 'kind': 'unsigned32'},
    388: {'name': 'dot1qDEI', 'kind': 'boolean'},
    389: {'name': 'dot1qCustomerDEI', 'kind': 'boolean'},
    390: {'name': 'flowSelectorAlgorithm', 'kind': 'unsigned16'},
    391: {'name': 'flowSelectedOctetDeltaCount', 'kind': 'unsigned64'},
    392: {'name': 'flowSelectedPacketDeltaCount', 'kind': 'unsigned64'},
    393: {'name': 'flowSelectedFlowDeltaCount', 'kind': 'unsigned64'},
    394: {'name': 'selectorIDTotalFlowsObserved', 'kind': 'unsigned64'},
    395: {'name': 'selectorIDTotalFlowsSelected', 'kind': 'unsigned64'},
    396: {'name': 'samplingFlowInterval', 'kind': 'unsigned64'},
    397: {'name': 'samplingFlowSpacing', 'kind': 'unsigned64'},
    398: {'name': 'flowSamplingTimeInterval', 'kind': 'unsigned64'},
    399: {'name': 'flowSamplingTimeSpacing', 'kind': 'unsigned64'},
    400: {'name': 'hashFlowDomain', 'kind': 'unsigned16'},
    401: {'name': 'transportOctetDeltaCount', 'kind': 'unsigned64'},
    402: {'name': 'transportPacketDeltaCount', 'kind': 'unsigned64'},
    403: {'name': 'originalExporterIPv4Address', 'kind': 'ipv4Address'},
    404: {'name': 'originalExporterIPv6Address', 'kind': 'ipv6Address'},
    405: {'name': 'originalObservationDomainId', 'kind': 'unsigned32'},
    406: {'name': 'intermediateProcessId', 'kind': 'unsigned32'},
    407: {'name': 'ignoredDataRecordTotalCount', 'kind': 'unsigned64'},
    408: {'name': 'dataLinkFrameType', 'kind': 'unsigned16'},
    409: {'name': 'sectionOffset', 'kind': 'unsigned16'},
    410: {'name': 'sectionExportedOctets', 'kind': 'unsigned16'},
    411: {'name': 'dot1qServiceInstanceTag', 'kind': 'octetArray'},
    412: {'name': 'dot1qServiceInstanceId', 'kind': 'unsigned32'},
    413: {'name': 'dot1qServiceInstancePriority', 'kind': 'unsigned8'},
    414: {'name': 'dot1qCustomerSourceMacAddress', 'kind': 'macAddress'},
    415: {'name': 'dot1qCustomerDestinationMacAddress', 'kind': 'macAddress'},
    417: {'name': 'postLayer2OctetDeltaCount', 'kind': 'unsigned64'},
    418: {'name': 'postMCastLayer2OctetDeltaCount', 'kind': 'unsigned64'},
    420: {'name': 'postLayer2OctetTotalCount', 'kind': 'unsigned64'},
    421: {'name': 'postMCastLayer2OctetTotalCount', 'kind': 'unsigned64'},
    422: {'name': 'minimumLayer2TotalLength', 'kind': 'unsigned64'},
    423: {'name': 'maximumLayer2TotalLength', 'kind': 'unsigned64'},
    424: {'name': 'droppedLayer2OctetDeltaCount', 'kind': 'unsigned64'},
    425: {'name': 'droppedLayer2OctetTotalCount', 'kind': 'unsigned64'},
    426: {'name': 'ignoredLayer2OctetTotalCount', 'kind': 'unsigned64'},
    427: {'name': 'notSentLayer2OctetTotalCount', 'kind': 'unsigned64'},
    428: {'name': 'layer2OctetDeltaSumOfSquares', 'kind': 'unsigned64'},
    429: {'name': 'layer2OctetTotalSumOfSquares', 'kind': 'unsigned64'},
    430: {'name': 'layer2FrameDeltaCount', 'kind': 'unsigned64'},
    431: {'name': 'layer2FrameTotalCount', 'kind': 'unsigned64'},
    432: {'name': 'pseudoWireDestinationIPv4Address', 'kind': 'ipv4Address'},
    433: {'name': 'ignoredLayer2FrameTotalCount', 'kind': 'unsigned64'},
}
