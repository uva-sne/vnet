#!/usr/bin/python3 -u
from __future__ import print_function
from binascii import hexlify
from socket import socket, AF_INET, SOCK_DGRAM, timeout, SOL_SOCKET, SO_REUSEADDR
from xdrlib import Unpacker
from time import time
from json import dumps

from sys import stderr
from pypacker.layer12.ethernet import Ethernet

SFLOW_FLOW = 1
SFLOW_COUNTERS = 2

SFLOW_HP_ETHERNET = 1

PROTO_MAP = {6: 'tcp', 17: 'udp', '1': 'icmp'}

SAMPLE_INTERVAL = 2

learnings = {}
ifnames = {}
port_stats = {}
pub_stats = {}
can_refresh_ns = False


def unpack_charset_str(d):
    print('charset:', d.unpack_uint())
    print('str:', d.unpack_opaque())


def sflow_counters(sender, d):
    sequence = d.unpack_uint()  # NOQA
    source_id = d.unpack_uint()  # NOQA

    now = time()
    records = d.unpack_uint()

    for _ in range(records):
        data_format = d.unpack_uint()
        raw_data = d.unpack_opaque()
        data = Unpacker(raw_data)
        if data_format == 1:
            index = data.unpack_uint()  # NOQA
            if_type = data.unpack_uint()  # NOQA
            speed = data.unpack_uhyper()  # NOQA
            direction = data.unpack_uint()  # NOQA
            status = data.unpack_uint()  # NOQA
            in_octets = data.unpack_uhyper()  # NOQA
            in_ucasts = data.unpack_uint()  # NOQA
            in_mcasts = data.unpack_uint()  # NOQA
            in_bcasts = data.unpack_uint()  # NOQA
            in_discards = data.unpack_uint()  # NOQA
            in_errors = data.unpack_uint()  # NOQA
            in_unknown_protos = data.unpack_uint()  # NOQA
            out_octets = data.unpack_uhyper()  # NOQA
            out_ucasts = data.unpack_uint()  # NOQA
            out_mcasts = data.unpack_uint()  # NOQA
            out_bcasts = data.unpack_uint()  # NOQA
            out_discards = data.unpack_uint()  # NOQA
            out_errors = data.unpack_uint()  # NOQA
            promiscuous_mode = data.unpack_uint()  # NOQA
            #if sender.endswith('.4'):
            #    from time import asctime
            #    print(asctime(), sender, 'index', index, 'seq', sequence,
            #          'in_octets', in_octets, 'out_octets', out_octets)
            key = (sender, index)
            ifname = ifnames.get(key)
            info = port_stats.get(key)
            if info:
                samp = info[0]
                rx = info[1]
                tx = info[2]
                wait = info[3]
            if ifname and info:
                # Juniper....
                if in_octets != rx or out_octets != tx or wait > 7:
                    global can_refresh_ns
                    can_refresh_ns = True
                    t = time() - samp
                    # TODO: deal with wrapping
                    rbps = int(float(in_octets - rx) / t)
                    tbps = int(float(out_octets - tx) / t)
                    pub_stats[ifname] = {'r': {'b': rbps}, 't': {'b': tbps},
                                         'time': '%.2f' % (t,)}
                    info = None
            if not info:
                samp = now
                rx = in_octets
                tx = out_octets
                wait = 0
            port_stats[key] = (samp, rx, tx, wait + 1)
        elif data_format == 1004:
            # of_dpid = data.unpack_uhyper()
            # of_port = data.unpack_uint()
            pass
        elif data_format == 1005:
            # port_name = data.unpack_string().decode('utf8')
            pass
        elif data_format in (2203, 2207):
            # Don't care
            pass
        elif data_format == 2:
            continue
            # Don't really care
            dot3StatsAlignmentErrors = data.unpack_uint()  # NOQA
            dot3StatsFCSErrors = data.unpack_uint()  # NOQA
            dot3StatsSingleCollisionFrames = data.unpack_uint()  # NOQA
            dot3StatsMultipleCollisionFrames = data.unpack_uint()  # NOQA
            dot3StatsSQETestErrors = data.unpack_uint()  # NOQA
            dot3StatsDeferredTransmissions = data.unpack_uint()  # NOQA
            dot3StatsLateCollisions = data.unpack_uint()  # NOQA
            dot3StatsExcessiveCollisions = data.unpack_uint()  # NOQA
            dot3StatsInternalMacTransmitErrors = data.unpack_uint()  # NOQA
            dot3StatsCarrierSenseErrors = data.unpack_uint()  # NOQA
            dot3StatsFrameTooLongs = data.unpack_uint()  # NOQA
            dot3StatsInternalMacReceiveErrors = data.unpack_uint()  # NOQA
            dot3StatsSymbolErrors = data.unpack_uint()  # NOQA
        else:
            print('Unhandled counter format:', data_format)


def sflow_flow(sender, d, flow_state, delta):
    sequence = d.unpack_uint()  # NOQA
    source_id = d.unpack_uint()  # NOQA
    sample_rate = d.unpack_uint()  # NOQA
    sample_pool = d.unpack_uint()  # NOQA
    drops = d.unpack_uint()  # NOQA
    ipo = d.unpack_uint()
    opo = d.unpack_uint()

    # FROM AS51 ATTACHED TO ROUTER4
    # FROM AS101 ATTACHED TO ROUTER1

    learn_key = (sender, ipo)
    dir = learnings.get(learn_key)

    # TODO: count flows

    _sip = _dip = None

    flows = d.unpack_uint()
    for _ in range(flows):
        flow_type = d.unpack_uint()
        data = Unpacker(d.unpack_opaque())

        if flow_type == 1:
            proto = data.unpack_uint()
            orig_len = data.unpack_uint()
            data.unpack_uint()  # ??
            sample = data.unpack_opaque()
            if proto != SFLOW_HP_ETHERNET:
                continue

            sample += b'\x00' * (orig_len - len(sample))  # ..
            pkt = Ethernet(sample)

            if pkt.type == 2048:
                _sip = pkt.ip.src[:3]
                _dip = pkt.ip.dst[:3]
                if dir:
                    key = (pkt.ip.src[:3] + pkt.ip.dst[:3] + dir +
                           bytes([pkt.ip.p]))
                    val = flow_state.get(key) or (0, 0)
                    flow_state[key] = (val[0] + 1, val[1] + orig_len)

        elif flow_type == 1001:
            continue

        else:
            print('Unhandled flow type:', flow_type, file=stderr)

    if _sip and learn_key not in learnings:
        if _sip == b'\xac\x1ee':
            print('Learned', sender, 'is TX on in-port', ipo, 'out-port', opo)
            learnings[learn_key] = b't'
        elif _dip == b'\xac\x1ee':
            print('Learned', sender, 'is RX on in-port', ipo, 'out-port', opo)
            learnings[learn_key] = b'r'
        else:
            return
        if sender.endswith('.4'):
            ifnames[learn_key] = 'stitch-521'
        elif sender.endswith('.1'):
            ifnames[learn_key] = 'stitch-520'


def sflow_handle(sender, data, flow_state):
    d = Unpacker(data)

    packet = {'version': d.unpack_int(),
              'agent_address_fam': d.unpack_uint(),
              'agent_address_addr': d.unpack_uint(),
              'sub_agent_id': d.unpack_uint(),
              'sequence': d.unpack_uint(),
              'uptime': d.unpack_uint()}

    samples = d.unpack_uint()

    for _ in range(samples):
        sample_type = d.unpack_uint()
        sample_data = Unpacker(d.unpack_opaque())

        if sample_type == SFLOW_FLOW:
            sflow_flow(sender, sample_data, flow_state, 0)
        elif sample_type == SFLOW_COUNTERS:
            sflow_counters(sender, sample_data)
            #if 'iface' not in c:
            #    continue
            #print('%-4s  %-5s  %10s %10s' % (c['iface']['index'], '-',
            #                                 c['iface']['in_octets'],
            #                                 c['iface']['out_octets']),
            #      c['iface']['index'])
        else:
            print('Unknown type', sample_type, file=stderr)

    if 0:
        print(packet)


def print_state(sender, state):
    had_some = False
    for k, v in state['flows'].items():
        print(sender, *k + v)
        had_some = True
    return had_some


def sweep_states(flow_states):
    now = time()

    for sender, state in flow_states.items():
        if (now - state['last']) < SAMPLE_INTERVAL:
            # Not timed out yet
            # Sync...
            return

    publish = []
    for sender, state in flow_states.items():
        if sender.endswith('.4'):
            iface = 'stitch-521'
        elif sender.endswith('.1'):
            iface = 'stitch-520'
        else:
            # Bad IP...
            continue
        publish.append('m %s %s' % (sender, iface))
        for k, v in state['flows'].items():
            h = hexlify(k).decode('utf8')
            publish.append('%s %s %c %s %s %s' % (h[:6], h[6:12], k[6],
                                                  h[14:16], v[0], v[1]))
        state['last'] = now

    loop.run_until_complete(mqtt.publish('vm/fs', dumps(publish).encode('utf8')))

    global can_refresh_ns
    if can_refresh_ns:
        loop.run_until_complete(mqtt.publish('vm/ns', dumps(pub_stats).encode('utf8')))
        can_refresh_ns = False


def run_udp_server(port):
    flow_states = {}
    sock = socket(AF_INET, SOCK_DGRAM)
    sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    sock.bind(('0.0.0.0', port))
    sock.settimeout(0.4)

    while True:
        data = None
        try:
            data, rfrom = sock.recvfrom(65000)
        except timeout:
            pass

        if data:
            sender = rfrom[0]
            if sender not in flow_states:
                flow_states[sender] = {'last': 0, 'flows': {}}
            flow_state = flow_states[sender]
            sflow_handle(sender, data, flow_state['flows'])

        sweep_states(flow_states)


if __name__ == '__main__':
    from hbmqtt.client import MQTTClient
    from asyncio import get_event_loop

    loop = get_event_loop()

    mqtt = MQTTClient()
    loop.run_until_complete(mqtt.connect('mqtt://127.0.0.1/'))
    run_udp_server(8503)
