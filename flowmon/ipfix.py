from struct import unpack

from ipfix_fields import ipfix_fields

IPFIX_TEMPLATE_SET = 2
IPFIX_OPTIONS_TEMPLATE_SET = 3


def parse_template_set(buf, buf_length, templates):
    while buf_length:
        if buf_length < 4:
            raise ValueError

        template_id, field_count = unpack('!HH', buf.read(4))
        buf_length -= 4
        fields = []

        for _ in range(field_count):
            if buf_length < 4:
                raise ValueError
            field_id, length = unpack('!HH', buf.read(4))
            buf_length -= 4
            field = {'field_id': field_id & 0x7FFF, 'length': length}
            if field_id & 0x8000:
                if buf_length < 4:
                    raise ValueError
                field['enterprise_id'] = unpack('!I', buf.read(4))
                buf_length -= 4
            fields.append(field)

        templates[template_id] = fields


def parse_data_set(buf, length, template):
    rows = []
    while length:
        row = {}
        for t in template:
            if length < t['length']:
                raise ValueError
            length -= t['length']
            datum = buf.read(t['length'])

            tf = ipfix_fields.get(t['field_id'])
            if tf:
                name = tf['name']
                parser = field_parsers.get(tf['kind'])
                if parser:
                    datum = parser(datum)
            else:
                name = t['field_id']
            row[name] = datum
        rows.append(row)
    return rows


def set_iterator(buf, buf_length, templates):
    while buf_length >= 4:
        header = buf.read(4)
        if not header:
            break

        set_id, length = unpack('!HH', header)
        if length < 4 or length > buf_length:
            raise ValueError((length, buf_length))

        buf_length -= length
        length -= 4

        if set_id == IPFIX_TEMPLATE_SET:
            parse_template_set(buf, length, templates)

        elif set_id >= 256 and set_id in templates:
            for row in parse_data_set(buf, length, templates[set_id]):
                yield row

        else:
            print('Unknown template:', set_id)
            #yield set_id, buf.read(length)


def parse_ipfix(buf, templates):
    version, length, export_time, sequence, obs_domain_id = \
        unpack('!HHIII', buf.read(16))

    # TODO: verify buf length
    # TODO: pass in set of desired fields?
    length -= 16

    return {'domain_id': obs_domain_id,
            'sequence': sequence,
            'time': export_time,
            'flows': list(set_iterator(buf, length, templates))}


def parse_unsigned(n):
    return unpack('!Q', b'\x00' * (8 - len(n)) + n)[0]


def parse_ipv4(n):
    return '%d.%d.%d.%d' % tuple(n)


def parse_ipv6(n):
    return ('%02x%02x:%02x%02x:%02x%02x:%02x%02x:'
            '%02x%02x:%02x%02x:%02x%02x:%02x%02x') % tuple(n)


def parse_mac(n):
    return '%02x:%02x:%02x:%02x:%02x:%02x' % tuple(n)


field_parsers = {
    'unsigned8': parse_unsigned,
    'unsigned16': parse_unsigned,
    'unsigned32': parse_unsigned,
    'unsigned64': parse_unsigned,
    'ipv4Address': parse_ipv4,
    'ipv6Address': parse_ipv6,
    'macAddress': parse_mac,
}
