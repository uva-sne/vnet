{#
 # Create entries for /etc/hosts from the current topology
 #}

vnet_populate_hosts() {
{% for n in nodes %}
    {% for i in n['topology:hasInterface'] %}
        {% set iface = elements[i[8:]] %}
        {% set addr = elements[iface['ip4:localIPAddress'][8:]] %}

        echo {{ addr['layer:label_ID'] }}
    {% endfor %}
{% endfor %}
}

vnet_populate_hosts >> /etc/hosts
