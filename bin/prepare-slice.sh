#!/bin/bash

if [[ "$1" == "" ]]; then
    SLICE="vnet"
else
    SLICE="$1"
fi

if [[ "$2" == "" ]]; then
    SITE="uva"
else
    SITE="$2"
fi
set -e

xo-rpc -u $SITE hosts $SLICE
xo-rpc -u $SITE ansible-inv $SLICE > inv
#ansible-playbook -i inv sc15/scripts/prepare.yml
