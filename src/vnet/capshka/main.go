package main

import (
	"github.com/elazarl/goproxy"
	"log"
	"net/http"
)

var capshka = `<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf8">
		<title>CAPSHKA</title>
		<style>body { font-family: sans-serif;
		margin: 2em auto;
		max-width: 40em;
	}
	.capshka { display: inline-block; border: 1px solid #000; background: #ddd; padding: 0.5em 1.3em; }</style>
	</head>
	<body>
		<h2>Security barrier</h2>
		<p>Before continuing, you must prove you are not a robot.
		<p><span class="capshka">S4RN3T</span>
		<p><input type="text"> <button>OK</button>
		<script>
		var btn = document.querySelector('button');
		var inp = document.querySelector('input');
		var c = document.querySelector('.capshka').textContent;
		btn.addEventListener('click', function() {
			if (inp.value == c) {
				inp.invalid = false;
				var exp = new Date();
				exp.setTime(exp.getTime() + 1000*60);
				document.cookie = 'security=1; expires=' + exp.toGMTString() + '; path=/';
				location.reload();
			} else {
				inp.invalid = false;
			}
		});
		</script>
	</body>
</html>
`

func main() {
	proxy := goproxy.NewProxyHttpServer()
	proxy.Verbose = true
	proxy.OnRequest().DoFunc(InjectCapshka)
	proxy.NonproxyHandler = http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if req.Header.Get("X-Capshka") != "" {
			http.Error(w, "Self-proxying", http.StatusForbidden)
			return
		}
		req.URL.Scheme = "http"
		req.URL.Host = req.Host
		proxy.ServeHTTP(w, req)
	})
	log.Fatal(http.ListenAndServe(":8999", proxy))
}

func InjectCapshka(r *http.Request, ctx *goproxy.ProxyCtx) (*http.Request, *http.Response) {
	// TODO: only on certain pages?
	secured := false
	for _, v := range r.Cookies() {
		if v.Name == "security" {
			secured = true
			break
		}
	}
	r.Header.Set("X-Capshka", "1")
	if !secured {
		return nil, goproxy.NewResponse(r, goproxy.ContentTypeHtml, http.StatusUnauthorized, capshka)
	}
	return r, nil
}
