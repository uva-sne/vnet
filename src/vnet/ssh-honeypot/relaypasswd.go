package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net"
	"net/http"
	"time"
)

type client struct {
	*http.Client

	friendly  bool
	addheader bool
}

func buildClient(ip string, friendly bool) *client {
	var tp = &http.Transport{
		DisableKeepAlives:   true,
		DisableCompression:  true,
		TLSHandshakeTimeout: 20 * time.Second,
		MaxIdleConnsPerHost: 1,
	}
	if ip != "" {
		addr := &net.TCPAddr{
			IP:   net.ParseIP(ip),
			Port: 0,
			Zone: "",
		}
		tp.Dial = (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
			LocalAddr: addr,
		}).Dial
	}
	return &client{
		Client: &http.Client{
			Timeout:   120 * time.Second,
			Transport: tp,
		},
		friendly: friendly,
	}
}

func buildIP(base string, part int) string {
	if base == "" {
		return ""
	}
	return fmt.Sprintf("%v.%v", base, part)
}

func initH(req *http.Request) {
	if req.Header == nil {
		req.Header = make(http.Header)
	}
}

// TODO: set as response to proxy failure
// TODO: time limit
func (cli *client) captcha(req *http.Request) {
	if cli.friendly {
		initH(req)
		req.Header.Set("Cookie", "captcha=1")
	}
}

// Send a request without a body
func (cli *client) request(server, page string) error {
	req, err := http.NewRequest("GET", server+page, nil)
	if err != nil {
		return err
	}
	cli.captcha(req)
	resp, err := cli.Client.Do(req)
	if err != nil {
		return err
	}
	return consume(resp)
}

// Send a request with a body
func (cli *client) form(server, page string, size int) error {
	fr := &FakeReader{0, size}
	req, err := http.NewRequest("POST", server+page, fr)
	if err != nil {
		return err
	}
	initH(req)
	req.Header.Set("Content-Type", "application/x-sale")
	cli.captcha(req)
	resp, err := cli.Client.Do(req)
	if err != nil {
		return err
	}
	return consume(resp)
}

// Read server response
func consume(resp *http.Response) error {
	defer resp.Body.Close()
	_, err := io.Copy(ioutil.Discard, resp.Body)
	return err
}

var servers = []string{"http://10.100.1.1:80/", "http://10.100.1.2:80/"}
var cli *client

func init() {
	// TODO: want a range
	ip := buildIP("10.100.4", 131)
	cli = buildClient(ip, false)
}

func tryPassword(pw string) {
	server := servers[rand.Int31n(int32(len(servers)))]
	cli.request(server, "?load=lo&pw="+pw)
}

var FakeBits [1024]byte

type FakeReader struct {
	pos, size int
}

func (fw *FakeReader) Read(p []byte) (n int, err error) {
	out := fw.size - fw.pos
	if out == 0 {
		return 0, io.EOF
	}
	max := cap(FakeBits)
	maxp := cap(p)
	if maxp > max {
		maxp = max
	}
	if out > maxp {
		out = maxp
	}
	copy(p, FakeBits[:out])
	fw.pos += out
	return out, nil
}

func chance(p float32) bool {
	return rand.Float32() <= p
}
