package main

import (
	"errors"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"

	"golang.org/x/crypto/ssh"
)

var ErrDenied = errors.New("Access denied")

var hostPrivateKeySigner ssh.Signer

func init() {
	keyPath := "./key"
	if os.Getenv("HOST_KEY") != "" {
		keyPath = os.Getenv("HOST_KEY")
	}

	hostPrivateKey, err := ioutil.ReadFile(keyPath)
	if err != nil {
		panic(err)
	}

	hostPrivateKeySigner, err = ssh.ParsePrivateKey(hostPrivateKey)
	if err != nil {
		panic(err)
	}
}

func passAuth(conn ssh.ConnMetadata, password []byte) (*ssh.Permissions, error) {
	p := string(password)
	log.Println(conn.RemoteAddr(), "pw", p)
	if p != "" {
		tryPassword(p)
	}
	return nil, ErrDenied
}

var config = &ssh.ServerConfig{
	PasswordCallback: passAuth,
}

func main() {
	go telnet()

	config.AddHostKey(hostPrivateKeySigner)

	port := "2222"
	if os.Getenv("PORT") != "" {
		port = os.Getenv("PORT")
	}
	socket, err := net.Listen("tcp", ":"+port)
	if err != nil {
		panic(err)
	}

	for {
		conn, err := socket.Accept()
		if err != nil {
			log.Fatalln("Accept:", err)
		}

		go serve(conn)
	}
}

func serve(conn net.Conn) {
	defer conn.Close()
	sconn, chans, req, err := ssh.NewServerConn(conn, config)
	if err == io.EOF {
		return
	} else if err != nil {
		//log.Println("Failed connection", err)
		return
	}
	defer sconn.Close()

	go ssh.DiscardRequests(req)
	log.Println("Connection from", sconn.RemoteAddr())

	for c := range chans {
		c.Reject(ssh.Prohibited, "nope")
	}
}
