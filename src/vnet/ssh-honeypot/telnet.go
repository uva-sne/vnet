package main

import (
	"bufio"
	"io"
	"log"
	"net"
	"time"
)

func telnet() {
	socket, err := net.Listen("tcp", ":23")
	if err != nil {
		log.Fatal(err)
	}

	for {
		conn, err := socket.Accept()
		if err != nil {
			log.Fatalln("Accept:", err)
		}

		go telnetClient(conn)
	}
}

func telnetClient(c net.Conn) {
	defer c.Close()
	if !send(c, "vnet trap terminal\nusername: ") {
		return
	}
	r := bufio.NewReader(c)
	if err := neg(c, r); err != nil {
		if err != io.EOF {
			log.Println(c.RemoteAddr(), "Negotiate error:", err)
		}
		return
	}
	u, err := line(r)
	if err != nil {
		log.Println(c.RemoteAddr(), "User error:", err)
		return
	}
	if !send(c, "\npassword: ") {
		return
	}
	p, err := line(r)
	if err != nil {
		log.Println(c.RemoteAddr(), "Password error:", err)
		return
	}
	send(c, "\nAccess denied.\n")
	log.Println(c.RemoteAddr(), "Telnet:", u, p)
	if p != "" {
		tryPassword(p)
	}
}

func send(c net.Conn, s string) bool {
	c.SetWriteDeadline(time.Now().Add(30 * time.Second))
	msg := []byte(s)
	n, err := c.Write(msg)
	return err == nil && n == len(msg)
}

const (
	WILL = 251
	WONT = 252
	DO   = 253
	DONT = 254
)

func neg(c net.Conn, r *bufio.Reader) error {
	var cap [3]byte
	c.Write([]byte{
		0xFF, WILL, 3,
	})
	for {
		pk, err := r.Peek(1)
		if err != nil {
			return err
		} else if pk[0] != 0xff {
			return nil
		}

		_, err = r.Read(cap[:])
		if err != nil {
			return err
		}

		inverse := 0
		if cap[1] == WILL {
			log.Println(c.RemoteAddr(), "WILL", cap[2])
			//inverse = WONT
		} else if cap[1] == DO {
			log.Println(c.RemoteAddr(), "DO", cap[2])
			//inverse = DONT
		} else {
			log.Println(c.RemoteAddr(), "?", cap[1], cap[2])
			continue
		}

		switch cap[2] {
		case 3: // No GO-AHEAD
		case 34: // Line mode
		case 5: // Status
		case 24, 31, 33, 35, 39:
			inverse = DONT

			// 5 35
		default:
		}
		if false && inverse != 0 {
			cap[1] = byte(inverse)
			n, err := c.Write(cap[:])
			if n != 3 || err != nil {
				return err
			}
		}
	}
}

func line(r *bufio.Reader) (string, error) {
	line, _, err := r.ReadLine()
	return string(line), err
}
